<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use PHPUnit\Framework\TestCase;

class CoordTest extends TestCase
{
    /**
     * Test Coordinate get neighbours.
     *
     * @dataProvider dataGetNeighbours
     */
    public function testGetNeighbours(Coord $subject, array $expected)
    {
        $neighbours = $subject->getNeighbours();
        $this->assertCount(4, $neighbours);
        $this->assertSame(
            $expected,
            array_map('strval', $neighbours)
        );
    }

    /**
     * Data provide for get neighbours test.
     *
     * @return array[]
     */
    public function dataGetNeighbours(): array
    {
        return [
            [
                Coord::fromString('10,10'),
                ['10,9', '11,10', '10,11', '9,10'],
            ],
            [
                Coord::fromString('0,0'),
                ['0,-1', '1,0', '0,1', '-1,0'],
            ],
            [
                Coord::fromString('-10,-10'),
                ['-10,-11', '-9,-10', '-10,-9', '-11,-10'],
            ],
        ];
    }

    /**
     * Test Coordinate get surrounding.
     *
     * @dataProvider dataGetSurrounding
     */
    public function testGetSurrounding(Coord $subject, array $expected)
    {
        $neighbours = $subject->getSurrounding();
        $this->assertCount(8, $neighbours);
        $this->assertSame(
            $expected,
            array_map('strval', $neighbours)
        );
    }

    /**
     * Data provide for get neighbours test.
     *
     * @return array[]
     */
    public function dataGetSurrounding(): array
    {
        return [
            [
                Coord::fromString('10,10'),
                ['10,9', '11,9', '11,10', '11,11', '10,11', '9,11', '9,10', '9,9'],
            ],
            [
                Coord::fromString('0,0'),
                ['0,-1', '1,-1', '1,0', '1,1', '0,1', '-1,1', '-1,0', '-1,-1'],
            ],
            [
                Coord::fromString('-10,-10'),
                ['-10,-11', '-9,-11', '-9,-10', '-9,-9', '-10,-9', '-11,-9', '-11,-10', '-11,-11'],
            ],
        ];
    }

    /**
     * Test Coordinate get line of sight.
     *
     * @dataProvider dataGetLineOfSight
     */
    public function testGetLineOfSight(Coord $subject, int $distance, array $expected)
    {
        $neighbours = $subject->getLineOfSight($distance);
        $this->assertSame(
            $expected,
            array_map('strval', $neighbours)
        );
    }

    /**
     * Data provide for get line of sight.
     *
     * @return array[]
     */
    public function dataGetLineOfSight(): array
    {
        return [
            [
                Coord::fromString('10,10'),
                1,
                ['10,9', '11,9', '11,10', '11,11', '10,11', '9,11', '9,10', '9,9'],
            ],
            [
                Coord::fromString('10,10'),
                2,
                [
                    // 1 Deep.
                    '10,9','11,9','11,10','11,11','10,11','9,11','9,10','9,9',
                    // 2 Deep
                    '10,8', '12,8', '12,10', '12,12', '10,12', '8,12', '8,10', '8,8',
                ],
            ],
            [
                Coord::fromString('10,10'),
                3,
                [
                    // 1 Deep.
                    '10,9','11,9','11,10','11,11','10,11','9,11','9,10','9,9',
                    // 2 Deep
                    '10,8', '12,8', '12,10', '12,12', '10,12', '8,12', '8,10', '8,8',
                    // 2 Deep
                    '10,7', '13,7', '13,10', '13,13', '10,13', '7,13', '7,10', '7,7',
                ],
            ],
            [
                Coord::fromString('0,0'),
                3,
                [
                    // 1 Deep.
                    '0,-1', '1,-1', '1,0', '1,1', '0,1', '-1,1', '-1,0', '-1,-1',
                    // 2 Deep
                    '0,-2', '2,-2', '2,0', '2,2', '0,2', '-2,2', '-2,0', '-2,-2',
                    // 2 Deep
                    '0,-3', '3,-3', '3,0', '3,3', '0,3', '-3,3', '-3,0', '-3,-3',
                ],
            ],
        ];
    }

    /**
     * Test Coordinate get first valid line of sight.
     *
     * @dataProvider dataGetFirstValidLineOfSight
     */
    public function testGetFirstValidLineOfSight(Coord $subject, int $maxDistance, array $valid, array $expected)
    {
        $neighbours = $subject->getFirstValidLineOfSight($valid, $maxDistance);
        $this->assertSame(
            $expected,
            array_map('strval', $neighbours)
        );
    }

    /**
     * Data provide for get first valid line of sight.
     *
     * @return array[]
     */
    public function dataGetFirstValidLineOfSight(): array
    {
        return [
            'All Neighbours Valid' => [
                Coord::fromString('10,10'),
                1,
                ['10,9' => 1, '11,10' => 1, '10,11' => 1, '9,10' => 1],
                ['10,9', '11,10', '10,11', '9,10'],
            ],
            'Single Neighbour Valid' => [
                Coord::fromString('10,10'),
                2,
                ['12,12' => 1],
                ['12,12'],
            ],
            'Single Neighbour Out Of Range' => [
                Coord::fromString('10,10'),
                1,
                ['12,12' => 1],
                [],
            ],
            'Only first on a vector' => [
                Coord::fromString('10,10'),
                5,
                [
                    // North
                    '10,9' => 1, '10,7' => 1,
                    // East
                    '11,10' => 1, '14,10' => 1,
                    // South
                    '10,11' => 1, '10,14' => 1,
                    // West
                    '9,10' => 1, '6,10' => 1,
                ],
                ['10,9', '11,10', '10,11', '9,10'],
            ],
            'Only first on a vector within range' => [
                Coord::fromString('10,10'),
                1,
                [
                    // North
                    '10,8' => 1, '10,7' => 1,
                    // East
                    '12,10' => 1, '14,10' => 1,
                    // South
                    '10,12' => 1, '10,14' => 1,
                    // West
                    '8,10' => 1, '6,10' => 1,
                ],
                [],
            ],
            'Only first on a vector within a wider range' => [
                Coord::fromString('10,10'),
                2,
                [
                    // North
                    '10,8' => 1, '10,7' => 1,
                    // East
                    '12,10' => 1, '14,10' => 1,
                    // South
                    '10,12' => 1, '10,14' => 1,
                    // West
                    '8,10' => 1, '6,10' => 1,
                ],
                ['10,8', '12,10', '10,12', '8,10'],
            ],
            'Only first on all vector' => [
                Coord::fromString('10,10'),
                5,
                [
                    // North
                    '10,9' => 1, '10,7' => 1,
                    // North East
                    '11,9' => 1, '13,9' => 1,
                    // East
                    '11,10' => 1, '14,10' => 1,
                    // South East
                    '11,11' => 1, '13,13' => 1,
                    // South
                    '10,11' => 1, '10,14' => 1,
                    // South West
                    '9,11' => 1, '7,13' => 1,
                    // West
                    '9,10' => 1, '6,10' => 1,
                    // North West
                    '9,9' => 1, '7,7' => 1,
                ],
                ['10,9', '11,9', '11,10', '11,11', '10,11', '9,11', '9,10', '9,9'],
            ],
        ];
    }
}
