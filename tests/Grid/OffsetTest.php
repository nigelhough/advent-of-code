<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use LogicException;
use PHPUnit\Framework\TestCase;

class OffsetTest extends TestCase
{
    /**
     * @dataProvider dataGetBearing
     */
    public function testGetBearing(Offset $subject, Bearing $expected)
    {
        $this->assertTrue($expected->isSame($subject->getBearing()));
    }

    public function dataGetBearing(): array
    {
        return [
            'NORTH'      => [new Offset(0, -1), Bearing::get(Bearing::NORTH)],
            'NORTH 10'      => [new Offset(0, -10), Bearing::get(Bearing::NORTH)],
            'EAST'       => [new Offset(1, 0), Bearing::get(Bearing::EAST)],
            'EAST 10'       => [new Offset(10, 0), Bearing::get(Bearing::EAST)],
            'SOUTH'      => [new Offset(0, 1), Bearing::get(Bearing::SOUTH)],
            'SOUTH 10'      => [new Offset(0, 10), Bearing::get(Bearing::SOUTH)],
            'WEST'       => [new Offset(-1, 0), Bearing::get(Bearing::WEST)],
            'WEST 10'       => [new Offset(-10, 0), Bearing::get(Bearing::WEST)],
            'NORTH_EAST' => [new Offset(1, -1), Bearing::get(Bearing::NORTH_EAST)],
            'NORTH_EAST 10' => [new Offset(10, -10), Bearing::get(Bearing::NORTH_EAST)],
            'SOUTH_EAST' => [new Offset(1, 1), Bearing::get(Bearing::SOUTH_EAST)],
            'SOUTH_EAST 10' => [new Offset(10, 10), Bearing::get(Bearing::SOUTH_EAST)],
            'SOUTH_WEST' => [new Offset(-1, 1), Bearing::get(Bearing::SOUTH_WEST)],
            'SOUTH_WEST 10' => [new Offset(-10, 10), Bearing::get(Bearing::SOUTH_WEST)],
            'NORTH_WEST' => [new Offset(-1, -1), Bearing::get(Bearing::NORTH_WEST)],
            'NORTH_WEST 10' => [new Offset(-10, -10), Bearing::get(Bearing::NORTH_WEST)],
        ];
    }

    /**
     * @dataProvider dataGetInvalidBearing
     */
    public function testGetInvalidBearing(Offset $offset)
    {
        $this->expectException(LogicException::class);
        $offset->getBearing();
    }

    public function dataGetInvalidBearing(): array
    {
        return [
            'Center' => [new Offset(0, 0)],
            'North North East' => [new Offset(1, -2)],
        ];
    }
}
