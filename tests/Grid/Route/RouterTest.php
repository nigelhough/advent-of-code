<?php

/**
 *
 */

namespace AdventOfCode\Grid\Route;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\CoordinateInterface;
use AdventOfCode\HexGrid;
use AdventOfCode\Cube;
use AdventOfCode\Grid\Path;
use AdventOfCode\Grid\Route;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    /**
     * Test Routing function.
     *
     * @param CoordinateInterface $source
     * @param CoordinateInterface $destination
     * @param Route $expected
     *
     * @dataProvider dataRoute
     */
    public function testRoute(CoordinateInterface $source, CoordinateInterface $destination, Route $expected)
    {
        $this->assertEquals($expected, Router::route($source, $destination));
    }

    /**
     * Route test data provider.
     *
     * @return array[]
     */
    public function dataRoute(): array
    {
        return [
            '2D Grid Same Source and Destination' => [
                new Coord(0, 0),
                new Coord(0, 0),
                new Route(new Coord(0, 0), new Path()),
            ],
            '3D Grid Same Source and Destination' => [
                new Cube\Coord(10, 10, 10),
                new Cube\Coord(10, 10, 10),
                new Route(new Cube\Coord(10, 10, 10), new Path()),
            ],
            'Hex Grid Same Source and Destination' => [
                new HexGrid\Coord(1, 2, 3),
                new HexGrid\Coord(1, 2, 3),
                new Route(new HexGrid\Coord(1, 2, 3), new Path()),
            ],
            '2D Grid Destination One Step East' => [
                new Coord(0, 0),
                new Coord(1, 0),
                new Route(new Coord(1, 0), new Path(new Coord(0, 0))),
            ],
            '2D Grid Destination One Step North East' => [
                new Coord(0, 0),
                new Coord(1, 1),
                new Route(new Coord(1, 1), new Path(new Coord(0, 0), new Coord(1, 0))),
            ],
            '2D Grid Destination Target South East' => [
                new Coord(-10, -10),
                new Coord(-15, -15),
                new Route(
                    new Coord(-15, -15),
                    new Path(
                        new Coord(-10, -10),
                        new Coord(-10, -11),
                        new Coord(-10, -12),
                        new Coord(-10, -13),
                        new Coord(-10, -14),
                        new Coord(-10, -15),
                        new Coord(-11, -15),
                        new Coord(-12, -15),
                        new Coord(-13, -15),
                        new Coord(-14, -15),
                    )
                ),
            ],
            'Hex Destination North 2 Steps' => [
                new HexGrid\Coord(0, 0, 0),
                new HexGrid\Coord(1, 1, -2),
                new Route(
                    new HexGrid\Coord(1, 1, -2),
                    new Path(
                        new HexGrid\Coord(0, 0, 0),
                        new HexGrid\Coord(1, 0, -1)
                    )
                ),
            ],
            'Hex Destination Diagonal South West' => [
                new HexGrid\Coord(0, 0, 0),
                new HexGrid\Coord(-3, 0, 3),
                new Route(
                    new HexGrid\Coord(-3, 0, 3),
                    new Path(
                        new HexGrid\Coord(0, 0, 0),
                        new HexGrid\Coord(-1, 0, 1),
                        new HexGrid\Coord(-2, 0, 2),
                    )
                ),
            ],
            'Hex Destination Along a Straight path East' => [
                new HexGrid\Coord(-3, 2, 1),
                new HexGrid\Coord(2, -3, 1),
                new Route(
                    new HexGrid\Coord(2, -3, 1),
                    new Path(
                        new HexGrid\Coord(-3, 2, 1),
                        new HexGrid\Coord(-2, 1, 1),
                        new HexGrid\Coord(-1, 0, 1),
                        new HexGrid\Coord(0, -1, 1),
                        new HexGrid\Coord(1, -2, 1),
                    )
                ),
            ],
        ];
    }
}
