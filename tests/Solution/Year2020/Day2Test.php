<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day2Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day2($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc',
            '2',
            '1',
        ];
    }
}
