<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day3Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day3($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#',
            '7',
            '336',
        ];
    }
}
