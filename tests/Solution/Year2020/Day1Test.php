<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day1Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day1($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '1721
979
366
299
675
1456',
            '514579',
            '241861950',
        ];
    }
}
