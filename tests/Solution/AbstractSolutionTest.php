<?php

declare(strict_types=1);

namespace AdventOfCode\Solution;

use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\Factory;
use AdventOfCode\Solution\Year2021\Day10;
use PHPUnit\Framework\TestCase;

abstract class AbstractSolutionTest extends TestCase
{
    /**
     * @dataProvider dataExecute
     */
    public function testExecute(string $input, string $answer1, string $answer2)
    {
        $this->setOutputCallback(function () {
        });
        $solution = $this->getSolution();
        $answer = $solution->execute($input);

        $this->assertSame($answer1, $answer->getPart1Answer());
        $this->assertSame($answer2, $answer->getPart2Answer());
    }

    protected function getSolution(): SolutionInterface
    {
        throw new \Exception("Please override this function");
    }

    protected function mockAnswerFactory(): Factory
    {
        // Mock the implementation of AnswerFactory so not dependent on its implementation.
        // The Abstract solution requires an AnswerFactory.
        // We don't require its usage, a solution could create an Answer object directly or use another method.
        $answerFactory = $this->createMock(Factory::class);
        $answerFactory->method('create')->willReturnCallback(function ($answer1, $answer2) {
            return new Answer((string)$answer1, (string)$answer2);
        });

        return $answerFactory;
    }
}
