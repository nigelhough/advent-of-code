<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day21Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day21($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '',
            '',
            ''
        ];
    }
}
