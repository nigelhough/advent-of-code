<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day12Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day12($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'start-A
start-b
A-c
A-b
b-d
A-end
b-end',
            '10',
            '36',
        ];

        yield [
            'dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc',
            '19',
            '103',
        ];

        yield [
            'fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW',
            '226',
            '3509',
        ];
    }
}
