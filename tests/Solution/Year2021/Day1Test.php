<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day1Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day1($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '199
200
208
210
200
207
240
269
260
263',
            '7',
            '5',
        ];
    }
}
