<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day16Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day16($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        // Part1 samples.
        yield ['D2FE28', '6', '2021'];
        yield ['38006F45291200', '9', '1'];
        yield ['EE00D40C823060', '14', '3'];
        yield ['8A004A801A8002F478', '16', '15'];
        yield ['620080001611562C8802118E34', '12', '46'];
        yield ['C0015000016115A2E0802F182340', '23', '46'];
        yield ['A0016C880162017C3686B18A3D4780', '31', '54'];

        // Part2 samples.
        yield ['C200B40A82', '14', '3'];
        yield ['04005AC33890', '8', '54'];
        yield ['CE00C43D881120', '11', '9'];
        yield ['D8005AC2A8F0', '13', '1'];
        yield ['F600BC2D8F', '19', '0'];
        yield ['9C005AC2F8F0', '16', '0'];
        yield ['9C0141080250320F1802104A08', '20', '1'];
    }
}
