<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day3Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day3($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010',
            '198',
            '230',
        ];
    }
}
