<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day15Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day15($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581',
            '40',
            '315',
        ];

        yield [
            '123
456
789',
            '20',
            '97',
        ];

        yield [
            '11111
91999
11999
19991
11111',
            '10',
            '213',
        ];

        yield [
            '11111
99991
11111
19999
11111',
            '16',
            '212',
        ];
    }
}
