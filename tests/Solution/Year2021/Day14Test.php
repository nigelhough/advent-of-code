<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day14Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day14($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C',
            '1588',
            '2188189693529',
        ];
    }
}
