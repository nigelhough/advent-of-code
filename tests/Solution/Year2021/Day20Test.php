<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day20Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day20($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '',
            '',
            ''
        ];
    }
}
