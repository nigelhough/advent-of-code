<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day2Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day2($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'forward 5
down 5
forward 8
up 3
down 8
forward 2',
            '150',
            '900',
        ];
    }
}
