<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day11Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day11($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526',
            '1656',
            '195',
        ];
    }
}
