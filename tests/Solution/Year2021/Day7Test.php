<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day7Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day7($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '16,1,2,0,4,2,7,1,2,14',
            '37',
            '168',
        ];
    }
}
