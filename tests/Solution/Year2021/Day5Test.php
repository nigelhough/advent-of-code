<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day5Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day5($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2',
            '5',
            '12',
        ];
    }
}
