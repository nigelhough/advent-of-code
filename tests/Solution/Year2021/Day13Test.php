<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day13Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day13($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5',
            '17',
            'EAHKRECP',
        ];
    }
}
