<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day6Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day6($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '3,4,3,1,2',
            '5934',
            '26984457539',
        ];
    }
}
