<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day9Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day9($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '2199943210
3987894921
9856789892
8767896789
9899965678',
            '15',
            '1134',
        ];
    }
}
