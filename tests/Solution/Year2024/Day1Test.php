<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2024;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day1Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day1($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '3   4
4   3
2   5
1   9
3   3
3   3',
            '11',
            '31'
        ];
    }
}
