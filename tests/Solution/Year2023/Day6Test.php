<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day6Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day6($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'Time:      7  15   30
Distance:  9  40  200',
            '288',
            '71503'
        ];
    }
}
