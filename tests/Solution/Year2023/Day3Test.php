<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day3Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day3($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..',
            '4361',
            '467835'
        ];
    }
}
