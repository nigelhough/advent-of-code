<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day1Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day1($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'two1nine
eightwothree3
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen',
            '242',
            '281'
        ];
        yield [
            '6nineight',
            '66',
            '68'
        ];
        yield [
            'nineight6',
            '66',
            '96'
        ];
    }
}
