<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day7Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day7($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483',
            '6440',
            '5905'
        ];
    }
}
