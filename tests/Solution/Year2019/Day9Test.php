<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day9Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day9($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '104,1125899906842624,99',
            '1125899906842624',
            '1125899906842624'
        ];
    }
}
