<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day8Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        define('BASE_DIR', sys_get_temp_dir());
        return new Day8($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '123456789012',
            '4',
            'AHFCB'
        ];
    }
}
