<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day4Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day4($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '134564-585159',
            '1929',
            '1306'
        ];
    }
}
