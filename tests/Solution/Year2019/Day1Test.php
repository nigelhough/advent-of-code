<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day1Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day1($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield ['14', '2', '2'];
        yield ['1969', '654', '966'];
        yield ['100756', '33583', '50346'];
    }
}
