<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day2Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day2($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'A Y
B X
C Z',
            '15',
            '12'
        ];
    }
}
