<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day9Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day9($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2',
            '13',
            '1'
        ];
        yield [
            'R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20',
            '88',
            '36'
        ];
    }
}
