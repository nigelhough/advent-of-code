<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day14Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day14($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '',
            '',
            ''
        ];
    }
}
