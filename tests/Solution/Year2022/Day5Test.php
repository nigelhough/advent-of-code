<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day5Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day5($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2',
            'CMZ',
            'MCD'
        ];
    }
}
