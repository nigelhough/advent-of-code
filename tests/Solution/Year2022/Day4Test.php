<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day4Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day4($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8',
            '2',
            '4'
        ];
    }
}
