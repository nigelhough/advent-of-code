<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day12Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day12($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            'Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi',
            '31',
            '29'
        ];
    }
}
