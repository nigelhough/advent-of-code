<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolutionTest;
use AdventOfCode\Solution\SolutionInterface;

class Day8Test extends AbstractSolutionTest
{
    protected function getSolution(): SolutionInterface
    {
        return new Day8($this->mockAnswerFactory());
    }

    public function dataExecute()
    {
        yield [
            '30373
25512
65332
33549
35390',
            '21',
            '8',
        ];
    }
}
