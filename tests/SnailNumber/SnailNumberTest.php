<?php

namespace AdventOfCode\SnailNumber;

use AdventOfCode\Input\Parser;
use PHPUnit\Framework\TestCase;

class SnailNumberTest extends TestCase
{
    /**
     * @dataProvider dataProcess
     */
    public function testProcess(string $list, string $expected)
    {
        $snailNumbers = array_map(fn($line) => new SnailNumber($line), Parser::iterateByLine($list));
        $snailNumber = SnailNumber::processList(...$snailNumbers);
        $this->assertSame($expected, (string)$snailNumber);
    }

    public function dataProcess()
    {
        yield [
            '[1,1]
[2,2]
[3,3]
[4,4]',
            '[[[[1,1],[2,2]],[3,3]],[4,4]]'
        ];

        yield [
            '[1,1]
[2,2]
[3,3]
[4,4]
[5,5]',
            '[[[[3,0],[5,3]],[4,4]],[5,5]]'
        ];

        yield [
            '[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]',
            '[[[[5,0],[7,4]],[5,5]],[6,6]]'
        ];

        yield [
            '[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]',
            '[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]'
        ];

        yield [
            '[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]',
            '[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]'
        ];

        yield [
            '[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
[[[[4,2],2],6],[8,7]]',
            '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]'
        ];

        yield [
            '[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]',
            '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]'
        ];
    }

    /**
     * @dataProvider dataReduce
     */
    public function testReduce(string $number, string $expected)
    {
        $snailNumber = new SnailNumber($number);

        $snailNumber->reduce();
        $this->assertSame($expected, (string)$snailNumber);
    }

    public function dataReduce()
    {
        yield ['[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]', '[[[[0,7],4],[[7,8],[6,0]]],[8,1]]'];
    }

    /**
     * @dataProvider dataExplode
     */
    public function testExplode(string $number, string $expected)
    {
        $snailNumber = new SnailNumber($number);

        $this->assertTrue($snailNumber->explode());
        $this->assertSame($expected, (string)$snailNumber);
    }

    public function dataExplode()
    {
        yield ['[[[[[9,8],1],2],3],4]', '[[[[0,9],2],3],4]'];

        yield ['[7,[6,[5,[4,[3,2]]]]]', '[7,[6,[5,[7,0]]]]'];

        yield ['[[6,[5,[4,[3,2]]]],1]', '[[6,[5,[7,0]]],3]'];

        yield ['[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]', '[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]'];

        yield ['[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]', '[[3,[2,[8,0]]],[9,[5,[7,0]]]]'];
    }

    /**
     * @dataProvider dataSplit
     */
    public function testSplit(string $number, string $expected)
    {
        $snailNumber = new SnailNumber($number);

        $this->assertTrue($snailNumber->split());
        $this->assertSame($expected, (string)$snailNumber);
    }

    public function dataSplit()
    {
        yield ['[10,0]', '[[5,5],0]'];

        yield ['[11,9]', '[[5,6],9]'];

        yield ['[12,11]', '[[6,6],11]'];
    }

    /**
     * @dataProvider dataAdd
     */
    public function testAdd(string $number, string $addition, string $expected)
    {
        $snailNumber = (new SnailNumber($number))->add(new SnailNumber($addition));
        $this->assertSame($expected, (string)$snailNumber);
    }

    public function dataAdd()
    {
        yield ['[1,2]', '[[3,4],5]', '[[1,2],[[3,4],5]]'];
        yield ['[[[[4,3],4],4],[7,[[8,4],9]]]', '[1,1]', '[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]'];
    }
}
