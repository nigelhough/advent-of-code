<?php

declare(strict_types=1);

namespace AdventOfCode\Device;

class CalibratorTest extends \PHPUnit\Framework\TestCase
{
    public function testCalibrate()
    {
        $this->assertEquals(-2, (new Calibrator())->calibrate(new FrequencyChanges('+1, +3, -6')));
    }
}
