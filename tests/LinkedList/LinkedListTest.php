<?php

declare(strict_types=1);

namespace AdventOfCode\LinkedList;

use PHPUnit\Framework\TestCase;

class LinkedListTest extends TestCase
{
    /**
     * @dataProvider dataReverseRange
     */
    public function testReverseRange(LinkedList $list, string $before, int $index, int $length, string $after)
    {
        $this->assertSame($before, (string) $list);

        $list->reverseRange($index, $length);

        $this->assertSame($after, (string) $list);
    }

    public function dataReverseRange(): array
    {
        return [
            'List of 1 Item'                    => [
                new LinkedList('A'),
                'A',
                1,
                1,
                'A',
            ],
            'List of 2 Item reverse range of 0' => [
                new LinkedList('A', 'B'),
                'AB',
                1,
                0,
                'AB',
            ],
            'List of 2 Item reverse range of 1' => [
                new LinkedList('A', 'B'),
                'AB',
                1,
                1,
                'AB',
            ],
            'List of 2 Items Reversed' => [
                new LinkedList('A', 'B'),
                'AB',
                1,
                2,
                'BA',
            ],
            'List of 3 Items Reverse 2' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                2,
                2,
                'ACB',
            ],
            'List of 3 Items Reverse 2 wrap around' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                3,
                2,
                'CBA',
            ],
            'List of 3 Items Reverse 2 at start' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                1,
                2,
                'BAC',
            ],
            'List of 7 Items Reverse first 3' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                1,
                3,
                'CBADEFG',
            ],
            'List of 7 Items Reverse 3 in middle' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                3,
                3,
                'ABEDCFG',
            ],
            'List of 7 Items Reverse 5 from end' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                5,
                5,
                'FECDBAG',
            ],
            'List of 7 Items Reverse 2 from end, first item remains' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                6,
                5,
                'AGFDECB',
            ],
            'Reverse All Items' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                1,
                7,
                'GFEDCBA',
            ],
            'Reverse All Items from a different index' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                3,
                7,
                'DCBAGFE',
            ],
            'Reverse All Items from a different index, first item remains' => [
                new LinkedList('A', 'B', 'C', 'D', 'E', 'F', 'G'),
                'ABCDEFG',
                5,
                7,
                'AGFEDCB',
            ],
            'List of 3 Items Reverse 2 wrap around, index wraps around' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                4,
                2,
                'BAC',
            ],
            'List of 3 Items Reverse 2 wrap around, index wraps around more' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                7,
                2,
                'BAC',
            ],
            'List of 3 Items Reverse 2 wrap around, index wraps around last element' => [
                new LinkedList('A', 'B', 'C'),
                'ABC',
                9,
                2,
                'CBA',
            ],
        ];
    }
}
