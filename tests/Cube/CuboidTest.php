<?php

declare(strict_types=1);

namespace AdventOfCode\Cube;

use PHPUnit\Framework\TestCase;

class CuboidTest extends TestCase
{
    public function testGetIntersection()
    {
        $subject = new Cuboid(new Coord(0, 0, 0), new Coord(5, 5, 5));

        $intersection = new Cuboid(new Coord(-2, -2, -2), new Coord(2, 2, 2));
        $this->assertSame('0,0,0,0x2,2,2,0', (string) $subject->getIntersection($intersection));

        $intersection = new Cuboid(new Coord(1, 1, 1), new Coord(7, 7, 7));
        $this->assertSame('1,1,1,0x5,5,5,0', (string) $subject->getIntersection($intersection));

        $intersection = new Cuboid(new Coord(-1, -1, -1), new Coord(7, 7, 7));
        $this->assertSame('0,0,0,0x5,5,5,0', (string) $subject->getIntersection($intersection));

        $intersection = new Cuboid(new Coord(-3, -3, -3), new Coord(-1, -1, -1));
        $this->assertSame(null, $subject->getIntersection($intersection));

        $subject = new Cuboid(new Coord(0, 0, 0), new Coord(3, 3, 3));
        $intersection = new Cuboid(new Coord(1, 1, 1), new Coord(2, 2, 2));
        $this->assertSame('1,1,1,0x2,2,2,0', (string) $subject->getIntersection($intersection));
    }

    public function testGetInverseIntersections()
    {
        $subject = new Cuboid(new Coord(0, 0, 0), new Coord(3, 3, 3));
        $intersection = new Cuboid(new Coord(1, 1, 1), new Coord(2, 2, 2));
        $this->assertSame(
            [
                '0,0,0,0x1,1,1,0',
                '0,0,1,0x1,1,2,0',
                '0,0,2,0x1,1,3,0',
                '0,1,0,0x1,2,1,0',
                '0,1,1,0x1,2,2,0',
                '0,1,2,0x1,2,3,0',
                '0,2,0,0x1,3,1,0',
                '0,2,1,0x1,3,2,0',
                '0,2,2,0x1,3,3,0',
                '1,0,0,0x2,1,1,0',
                '1,0,1,0x2,1,2,0',
                '1,0,2,0x2,1,3,0',
                '1,1,0,0x2,2,1,0',
                '1,1,2,0x2,2,3,0',
                '1,2,0,0x2,3,1,0',
                '1,2,1,0x2,3,2,0',
                '1,2,2,0x2,3,3,0',
                '2,0,0,0x3,1,1,0',
                '2,0,1,0x3,1,2,0',
                '2,0,2,0x3,1,3,0',
                '2,1,0,0x3,2,1,0',
                '2,1,1,0x3,2,2,0',
                '2,1,2,0x3,2,3,0',
                '2,2,0,0x3,3,1,0',
                '2,2,1,0x3,3,2,0',
                '2,2,2,0x3,3,3,0',
            ],
            array_map('strval', $subject->getInverseIntersections($intersection))
        );

        $subject = new Cuboid(new Coord(0, 0, 0), new Coord(1, 1, 2));
        $intersection = new Cuboid(new Coord(0, 0, 0), new Coord(1, 1, 1));
        $this->assertSame(
            ['0,0,1,0x1,1,2,0'],
            array_map('strval', $subject->getInverseIntersections($intersection))
        );

        $subject = new Cuboid(new Coord(0, 0, 0), new Coord(1, 2, 3));
        $intersection = new Cuboid(new Coord(0, 0, 1), new Coord(1, 1, 2));
        $this->assertSame(
            [
                '0,0,0,0x1,1,1,0',
                '0,0,2,0x1,1,3,0',
                '0,1,0,0x1,2,1,0',
                '0,1,1,0x1,2,2,0',
                '0,1,2,0x1,2,3,0',
            ],
            array_map('strval', $subject->getInverseIntersections($intersection))
        );
    }
}
