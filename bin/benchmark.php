<?php

declare(strict_types=1);

use SebastianBergmann\Timer\Timer;

$baseDir = dirname(__DIR__);
chdir($baseDir);
set_include_path($baseDir . PATH_SEPARATOR . get_include_path());
define('BASE_DIR', $baseDir);

require_once 'vendor/autoload.php';

ini_set('display_errors', '1');
//ini_set('error_reporting', '0');
ini_set('max_execution_time', '900');
ini_set('memory_limit', '4G');

// Build Dependency Injection Container.
$builder = new DI\ContainerBuilder();
$builder->addDefinitions($baseDir . '/config/definitions.php');
$container = $builder->build();

// Parse Input.
if ($argc !== 2) {
    echo "Invalid argument format. Must contain benchmark.php year\n";
}
$year = (int) $argv[1];
if (!($year >= 2015 && $year <= date('Y'))) {
    echo "Invalid Year! {$argv[1]}\n";
}

$overallTimeNanoSeconds = 0;
$timer = new Timer();
for ($day = 1; $day <= 25; $day++) {
    try {
        $className = "\AdventOfCode\Solution\Year{$year}\Day{$day}";
        if (!class_exists($className)) {
            continue;
        }
        $solution = new $className($container->get(\AdventOfCode\Answer\Factory::class));
        $inputRepository = $container->get(\AdventOfCode\Input\RepositoryInterface::class);
        $timer->start();
        ob_implicit_flush(false);
        ob_start();
        $answer = $solution->execute($inputRepository->get($day, $year));
        $time = $timer->stop();
    } catch (Throwable $e) {
        ob_end_clean();
        echo "{$year} " . str_pad((string) $day, 2, '0', STR_PAD_LEFT) . ": Exception! {$e->getMessage()}\n";
        continue;
    }
    ob_end_clean();

    echo "{$year} " . str_pad((string) $day, 2, '0', STR_PAD_LEFT) . ": " . $time->asString() . "\n";

    $overallTimeNanoSeconds += $time->asNanoseconds();
}

echo "\nOverall: " . (\SebastianBergmann\Timer\Duration::fromNanoseconds($overallTimeNanoSeconds))->asString() . "\n\n";

exit;

