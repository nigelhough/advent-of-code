<?php

declare(strict_types=1);

use AdventOfCode\Solution\EmptySolution;
use AdventOfCode\Solution\Legacy;
use SebastianBergmann\Timer\Timer;

$baseDir = dirname(__DIR__);
chdir($baseDir);
set_include_path($baseDir . PATH_SEPARATOR . get_include_path());
define('BASE_DIR', $baseDir);

require_once 'vendor/autoload.php';

ini_set('max_execution_time', '900');
ini_set('memory_limit', '4G');

// Build Dependency Injection Container.
$builder = new DI\ContainerBuilder();
$builder->addDefinitions($baseDir . '/config/definitions.php');
$container = $builder->build();

// Parse Input.
if ($argc !== 3) {
    echo "Invalid argument format. Must contain cli.php year day\n";
}
$year = (int)$argv[1];
if (!($year >= 2015 && $year <= date('Y'))) {
    echo "Invalid Year! {$argv[1]}\n";
}
$day = (int)$argv[2];
if (!($day >= 1 && $day <= 25)) {
    echo "Invalid Day! {$argv[2]}\n";
}

// Duplicate code to Route
// @todo remove duplicate

// Get Solution.
$file = BASE_DIR . "/public/{$year}/Day{$day}/index.php";
$className = "\AdventOfCode\Solution\Year{$year}\Day{$day}";
if (file_exists($file)) {
    $solution = new Legacy($file);
} elseif (class_exists($className)) {
    $solution = new $className($container->get(\AdventOfCode\Answer\Factory::class));
} else {
    $solution = new EmptySolution($year, $day);
}

// Execute Solution.
$inputRepository = $container->get(\AdventOfCode\Input\RepositoryInterface::class);
$timer = new Timer();
$timer->start();
$answer = $solution->execute($inputRepository->get($day, $year));
$time = $timer->stop();

// Submit Results.
$processorResults = new \AdventOfCode\Answer\Processor\ResultStack();
if ($answer !== null) {
    $processorResults = ($container->get(\AdventOfCode\Answer\ProcessorInterface::class))->process(
        $answer,
        $year,
        $day
    );
}

echo "\n" . $processorResults->getLatest() . "\n" . $time->asString() . "\n";
