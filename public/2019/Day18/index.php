<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

use AdventOfCode\Grid\Map;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Route;
use SebastianBergmann\Timer\Timer;

$mapData = require 'map.php';

//$mapData = "#########
//#b.A.@.a#
//#########";

//$mapData = "############
//#b.A.@.a..c#
//############";

//$mapData = "########################
//#f.D.E.e.C.b.A.@.a.B.c.#
//######################.#
//#d.....................#
//########################";
//
//$mapData = "########################
//#...............b.C.D.f#
//#.######################
//#.....@.a.B.c.d.A.e.F.g#
//########################";

//$mapData = "#################
//#i.G..c...e..F.p#
//########.########
//#j.A..b...f..C.o#
//########@########
//#k.E..a...g..B.n#
//#################";

//$mapData = "#################
//#i.G..c...e..H.p#
//########.########
//#j.A..b...f..D.o#
//########@########
//#k.E..a...g..B.n#
//########.########
//#l.F..d...h..C.m#
//#################";

//$mapData = "########################
//#@..............ac.GI.b#
//###d#e#f################
//###A#B#C################
//###g#h#i################
//########################";

$map = Map::createFromString($mapData);

echo $map->plot(true) . PHP_EOL;

$currentLocation = Coord::fromString(key($map->locate(function ($location) {
    return $location === '@';
})));
$keys = $map->locate(function ($location) {
    return ctype_alpha($location) && $location === strtolower($location);
});
$keyCoords = array_flip($keys);

// Explore Near by Locations.
$visibleLocations = $map->explore($currentLocation);
/** @var Route[] $visibleKeys */
$visibleKeys = array_filter($visibleLocations, function (Route $location) use ($map) {
    return $map->get($location->getTarget()) === strtolower($map->get($location->getTarget()));
});

$permutationFilters = [];
$initialKeys = [];
foreach ($visibleKeys as $visibleKey) {
    $permutationFilters[] = ($map->get($visibleKey->getTarget()) . "\w{" . count($visibleKeys) . "}");
    $initialKeys[] = $map->get($visibleKey->getTarget());
}

$noKeyRelationships = pow(count($keys), 2);
$noKeyPermutations = (int) gmp_fact(count($keys));
$filteredPermutations = $noKeyPermutations / (count($keys) / count($visibleKeys));

var_dump((int) $noKeyPermutations);
return;

echo "No of Keys : " . number_format(count($keys)) . "\n";
echo "No of Key Relationships : " . number_format($noKeyRelationships) . "\n";
echo "No of Key Permutations : " . number_format($noKeyPermutations) . "\n";
echo "Filtered Permutations : " . number_format($filteredPermutations) . "\n";

return;

$cachedRoutes = [];
$filename = md5($mapData) . ".routes.cache";
if (file_exists($filename)) {
    $cachedRoutes = unserialize(file_get_contents($filename));
}
//var_dump($keys);
//exit;

// Do I need this array if I have the Cached Routes, that is all I know
// @todo Replace uses of Relationships with Cache
$relationships = [];
foreach ($keys as $key) {
    if (isset($cachedRoutes["@>{$key}"])) {
        $route = $cachedRoutes["@>{$key}"];
    } else {
        $route = $map->findRoute($currentLocation, Coord::fromString($keyCoords[$key]));
        $cachedRoutes["@>{$key}"] = $route;
        file_put_contents($filename, serialize($cachedRoutes));
    }
    $relationships["@>{$key}"] = ["@", $key, $route];

    foreach ($keys as $pair) {
        if ($key === $pair) {
            continue;
        }
        if (in_array([$pair, $key], $relationships)) {
            continue;
        }

        if (isset($cachedRoutes["{$key}>{$pair}"])) {
            $route = $cachedRoutes["{$key}>{$pair}"];
        } else {
            $route = $map->findRoute(Coord::fromString($keyCoords[$key]), Coord::fromString($keyCoords[$pair]));
            $cachedRoutes["{$key}>{$pair}"] = $route;
            file_put_contents($filename, serialize($cachedRoutes));
        }
        $relationships["{$key}>{$pair}"] = [$key, $pair, $route];
    }
}

// Get Dependencies.
$dependencies = [];
foreach ($relationships as $relationship) {
    if ($relationship[0] !== "@") {
        continue;
    }
    /** @var Route $route */
    $route = $relationship[2];
    $target = $map->get($route->getTarget());
    $targetDependencies = [];
    foreach ($route->getPath()->follow() as $step) {
        $stepValue = $map->get($step);
        // Another Key
        if (ctype_alpha($stepValue) && $stepValue === strtolower($stepValue)) {
            $targetDependencies[] = $stepValue;
            continue;
        }
        // A Door
        if (ctype_alpha($stepValue) && $stepValue === strtoupper($stepValue)) {
            $targetDependencies[] = strtolower($stepValue);
        }
    }
    if (!empty($targetDependencies)) {
        $dependencies[$target] = $targetDependencies;
    }
}

//var_dump($dependencies);
//exit;

$i = 0;
ini_set('max_execution_time', '0');
$timer = new Timer();
$timer->start();
foreach (permuteKeys($keys, $initialKeys, $dependencies) as $permutation) {
//    echo "<p>" . implode($permutation) . "</p>";
    $i++;
}
$time = $timer->stop();

echo "No of Permutations : " . number_format($i) . PHP_EOL;
echo "Time to Generate Permutations : " . $time->asString() . PHP_EOL; //Timer::secondsToTimeString($time) . PHP_EOL;

return;

// Permute takes ages
//ini_set('max_execution_time', '900');
//$permutations = iterator_to_array(permute($keys));
//var_dump($permutations);
//exit;

$sequences = [];
$cachedSequences = [];
$filename = md5($mapData) . ".sequences.cache";
if (file_exists($filename)) {
    $cachedSequences = unserialize(file_get_contents($filename));
}
$i = 0;
foreach (permuteKeys($keys, $initialKeys, $dependencies) as $sequence) {
    if (isset($cachedSequences[implode($sequence)])) {
        $sequences[implode($sequence)] = $cachedSequences[implode($sequence)];
        continue;
    }
    $source = null;
    $totalSteps = 0;
    foreach ($sequence as $target) {
        // If first in sequence find steps from the start.
        if (is_null($source)) {
            $source = $target;

            if (empty($relationships["@>{$target}"])) {
                throw new \Exception("Unknown relationship @>{$target}");
            }
            $route = $relationships["@>{$target}"][2];
            // Is the Route Viable, do I check this here for evey permutation or afterwards in order of steps so can exit early

            $totalSteps += $route->count();
            continue;
        }

        if (empty($relationships["{$source}>{$target}"])) {
            throw new \Exception("Unknown relationship {$source}>{$target}");
        }

        /** @var Route $route */
        $route = $relationships["{$source}>{$target}"][2];
        // Is the Route Viable, do I check this here for evey permutation or afterwards in order of steps so can exit early

        $totalSteps += $route->count();

        $source = $target;
    }

    $sequences[implode($sequence)] = $totalSteps;
    $cachedSequences[implode($sequence)] = $totalSteps;
    file_put_contents($filename, serialize($cachedSequences));
    //echo "<p>{$i} :: "  . implode($sequence) . "</p>";
    $i++;
//    break;
}

//var_dump(count($sequences));
asort($sequences);
var_dump(count($sequences));
var_dump($sequences);
return;

foreach ($sequences as $sequence => $totalSteps) {
//    var_dump($sequence);
//    echo "<hr/>";
    $source = null;
    $heldKeys = [];
    $viable = true;
    foreach (str_split("@" . $sequence) as $target) {
        if (is_null($source)) {
            $source = $target;
            continue;
        }

//        var_dump($source);
//        var_dump($target);
        /** @var Route $route */
        $route = $relationships["{$source}>{$target}"][2];
        foreach ($route->getPath()->follow() as $step) {
            $stepValue = $map->get($step);
            if ($stepValue === ".") {
                continue;
            }
            if ($stepValue === $source || $stepValue === $target) {
                // If I am the start or end of the sequence its fine
                continue;
            }
            if ($stepValue === "@") {
                // The start is fine to walk through
                continue;
            }
            if ($stepValue === strtoupper($stepValue)) {
                // Is Door
                // @todo Create isDoor function

                // Key to door is held
                if (in_array(strtolower($stepValue), $heldKeys)) {
                    continue;
                }
            }
            if ($stepValue === strtolower($stepValue)) {
                // Is Key

                // If key is held we already have it.
                // If we don't hold it we should have gone there first.
                if (in_array($stepValue, $heldKeys)) {
                    continue;
                }
            }

            // If made it this far the step is blocked.
            //echo "Not Viable ({$stepValue}) {$source}>{$target}<br/>";
            $viable = false;
            break 2;
        }

//        echo "<hr/>";
        $heldKeys[] = $target;
        $source = $target;
    }

    if ($viable) {
        echo "Viable";
        break;
    }
//    break;
}

var_dump($sequence);
var_dump($totalSteps);

return;

/** @var Route[] $doors */
$doors = array_filter($locations, function (Route $location) use ($map) {
    return $map->get($location->getTarget()) === strtoupper($map->get($location->getTarget()));
});
/** @var Route[] $keys */
$keys = array_filter($locations, function (Route $location) use ($map) {
    return $map->get($location->getTarget()) === strtolower($map->get($location->getTarget()));
});

$map->clearCoordinate($currentLocation);
$heldKeys = [];

$totalSteps = 0;
while ($remainingKeys = $map->locate(function ($value) {
    return ctype_alpha($value) && $value === strtolower($value);
})) {
    // Explore Near by Locations.
    $locations = $map->explore($currentLocation);
    /** @var Route[] $doors */
    $doors = array_filter($locations, function (Route $location) use ($map) {
        return $map->get($location->getTarget()) === strtoupper($map->get($location->getTarget()));
    });
    /** @var Route[] $keys */
    $keys = array_filter($locations, function (Route $location) use ($map) {
        return $map->get($location->getTarget()) === strtolower($map->get($location->getTarget()));
    });

    if (!empty($keys)) {
        if (count($keys) > 1) {
            // Decision Maker
            echo "Pick Key";
            var_dump($keys);
            $key = array_pop($keys);
        } else {
            $key = array_pop($keys);
        }

        $heldKeys[] = $map->get($key->getTarget());

        $totalSteps += $key->count();
        $map->clearCoordinate($currentLocation);
        $currentLocation = $key->getTarget();
    } elseif (!empty($doors)) {
        /** @var Route $key */
        $door = null;
        foreach ($doors as $door) {
            // Unlock The Door
            if (in_array(strtolower($map->get($door->getTarget())), $heldKeys)) {
                $totalSteps += $door->count();
                $map->clearCoordinate($currentLocation);
                $currentLocation = $door->getTarget();
                break;
            }
        }
    }

    $map->mark($currentLocation, '@');
//    echo "<pre style='border:solid 1px red;'>" . $map->plot(true) . "</pre>";
    echo $map->plot(true);
}

echo "Total Steps: {$totalSteps}\n";
var_dump($heldKeys);

function permuteKeys(array $elements, array $initialKeys, array $dependencies): \Generator
{
    foreach ($elements as $element) {
        if (!in_array($element, $initialKeys)) {
            continue;
        }
        $elementsA = array_diff($elements, [$element]);

        foreach ($elementsA as $elementA) {
            $elementsB = array_diff($elementsA, [$elementA]);
            if (empty($elementsB)) {
                yield [$element, $elementA];
            }
            foreach ($dependencies[$elementA] ?? [] as $dependency) {
                if (in_array($dependency, $elementsB)) {
                    continue 2;
                }
            }

            foreach ($elementsB as $elementB) {
                $elementsC = array_diff($elementsB, [$elementB]);
                if (empty($elementsC)) {
                    yield [$element, $elementA, $elementB];
                }
                foreach ($dependencies[$elementB] ?? [] as $dependency) {
                    if (in_array($dependency, $elementsC)) {
                        continue 2;
                    }
                }

                foreach ($elementsC as $elementC) {
                    $elementsD = array_diff($elementsC, [$elementC]);
                    if (empty($elementsD)) {
                        yield [$element, $elementA, $elementB, $elementC];
                    }
                    foreach ($dependencies[$elementC] ?? [] as $dependency) {
                        if (in_array($dependency, $elementsD)) {
                            continue 2;
                        }
                    }

                    foreach ($elementsD as $elementD) {
                        $elementsE = array_diff($elementsD, [$elementD]);
                        if (empty($elementsE)) {
                            yield [$element, $elementA, $elementB, $elementC, $elementD];
                        }
                        foreach ($dependencies[$elementD] ?? [] as $dependency) {
                            if (in_array($dependency, $elementsE)) {
                                continue 2;
                            }
                        }

                        foreach ($elementsE as $elementE) {
                            $elementsF = array_diff($elementsE, [$elementE]);
                            if (empty($elementsF)) {
                                yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE];
                            }
                            foreach ($dependencies[$elementE] ?? [] as $dependency) {
                                if (in_array($dependency, $elementsF)) {
                                    continue 2;
                                }
                            }

                            foreach ($elementsF as $elementF) {
                                $elementsG = array_diff($elementsF, [$elementF]);
                                if (empty($elementsG)) {
                                    yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF];
                                }
                                foreach ($dependencies[$elementF] ?? [] as $dependency) {
                                    if (in_array($dependency, $elementsG)) {
                                        continue 2;
                                    }
                                }

                                foreach ($elementsG as $elementG) {
                                    $elementsH = array_diff($elementsG, [$elementG]);
                                    if (empty($elementsH)) {
                                        yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG];
                                    }
                                    foreach ($dependencies[$elementG] ?? [] as $dependency) {
                                        if (in_array($dependency, $elementsH)) {
                                            continue 2;
                                        }
                                    }

                                    foreach ($elementsH as $elementH) {
                                        $elementsI = array_diff($elementsH, [$elementH]);
                                        if (empty($elementsI)) {
                                            yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH];
                                        }
                                        foreach ($dependencies[$elementH] ?? [] as $dependency) {
                                            if (in_array($dependency, $elementsI)) {
                                                continue 2;
                                            }
                                        }

                                        foreach ($elementsI as $elementI) {
                                            $elementsJ = array_diff($elementsI, [$elementI]);
                                            if (empty($elementsJ)) {
                                                yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI];
                                            }
                                            foreach ($dependencies[$elementI] ?? [] as $dependency) {
                                                if (in_array($dependency, $elementsJ)) {
                                                    continue 2;
                                                }
                                            }

                                            foreach ($elementsJ as $elementJ) {
                                                $elementsK = array_diff($elementsJ, [$elementJ]);
                                                if (empty($elementsK)) {
                                                    yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH];
                                                }
                                                foreach ($dependencies[$elementJ] ?? [] as $dependency) {
                                                    if (in_array($dependency, $elementsK)) {
                                                        continue 2;
                                                    }
                                                }

                                                foreach ($elementsK as $elementK) {
                                                    $elementsL = array_diff($elementsK, [$elementK]);
                                                    if (empty($elementsL)) {
                                                        yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ];
                                                    }
                                                    foreach ($dependencies[$elementK] ?? [] as $dependency) {
                                                        if (in_array($dependency, $elementsL)) {
                                                            continue 2;
                                                        }
                                                    }

                                                    foreach ($elementsL as $elementL) {
                                                        $elementsM = array_diff($elementsL, [$elementL]);
                                                        if (empty($elementsM)) {
                                                            yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ, $elementK];
                                                        }
                                                        foreach ($dependencies[$elementL] ?? [] as $dependency) {
                                                            if (in_array($dependency, $elementsM)) {
                                                                continue 2;
                                                            }
                                                        }


                                                        foreach ($elementsM as $elementM) {
                                                            $elementsN = array_diff($elementsM, [$elementM]);
                                                            if (empty($elementsN)) {
                                                                yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ, $elementK, $elementL];
                                                            }
                                                            foreach ($dependencies[$elementM] ?? [] as $dependency) {
                                                                if (in_array($dependency, $elementsN)) {
                                                                    continue 2;
                                                                }
                                                            }

                                                            foreach ($elementsN as $elementN) {
                                                                $elementsO = array_diff($elementsN, [$elementN]);
                                                                if (empty($elementsO)) {
                                                                    yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ, $elementK, $elementL, $elementM];
                                                                }
                                                                foreach ($dependencies[$elementN] ?? [] as $dependency) {
                                                                    if (in_array($dependency, $elementsO)) {
                                                                        continue 2;
                                                                    }
                                                                }

                                                                foreach ($elementsO as $elementO) {
                                                                    $elementsP = array_diff($elementsO, [$elementO]);
                                                                    if (empty($elementsP)) {
                                                                        yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ, $elementK, $elementL, $elementM, $elementN];
                                                                    }
                                                                    foreach ($dependencies[$elementO] ?? [] as $dependency) {
                                                                        if (in_array($dependency, $elementsP)) {
                                                                            continue 2;
                                                                        }
                                                                    }

                                                                    foreach ($elementsP as $elementP) {
                                                                        $elementsQ = array_diff($elementsP, [$elementP]);
                                                                        if (empty($elementsQ)) {
                                                                            yield [$element, $elementA, $elementB, $elementC, $elementD, $elementE, $elementF, $elementG, $elementH, $elementI, $elementH, $elementJ, $elementK, $elementL, $elementM, $elementN, $elementO];
                                                                        }
                                                                        foreach ($dependencies[$elementP] ?? [] as $dependency) {
                                                                            if (in_array($dependency, $elementsQ)) {
                                                                                continue 2;
                                                                            }
                                                                        }
                                                                    }
                                                                    //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}{$elementJ}{$elementK}{$elementL}{$elementM}{$elementO}" . PHP_EOL;
                                                                }
                                                                //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}{$elementJ}{$elementK}{$elementL}{$elementM}" . PHP_EOL;
                                                            }
                                                            //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}{$elementJ}{$elementK}{$elementL}" . PHP_EOL;
                                                        }
                                                        //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}{$elementJ}{$elementK}" . PHP_EOL;
                                                    }
                                                    //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}{$elementJ}" . PHP_EOL;
                                                }
                                                //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}{$elementI}" . PHP_EOL;
                                            }
                                            //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}{$elementH}" . PHP_EOL;
                                        }
                                        //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}{$elementI}" . PHP_EOL;
                                    }
                                    //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}{$elementH}" . PHP_EOL;
                                }
                                //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}{$elementG}" . PHP_EOL;
                            }
                            //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}{$elementF}" . PHP_EOL;
                        }
                        //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}{$elementE}" . PHP_EOL;
                    }
                    //echo "{$element}{$elementA}{$elementB}{$elementC}{$elementD}" . PHP_EOL;
                }
                //echo "{$element}{$elementA}{$elementB}{$elementC}" . PHP_EOL;
            }
            //echo "{$element}{$elementA}{$elementB}" . PHP_EOL;
        }
        //echo "{$element}{$elementA}" . PHP_EOL;
    }
    //echo "{$element}" . PHP_EOL;
}

function permute(array $elements): \Generator
{
    $elements = array_values($elements);
    if (count($elements) <= 1) {
        yield $elements;
    } else {
        foreach (permute(array_slice($elements, 1)) as $permutation) {
            foreach (range(0, count($elements) - 1) as $i) {
                yield array_merge(
                    array_slice($permutation, 0, $i),
                    [$elements[0]],
                    array_slice($permutation, $i)
                );
            }
        }
    }
}
