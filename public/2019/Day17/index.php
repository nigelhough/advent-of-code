<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

use AdventOfCode\IntCode;
use AdventOfCode\Grid\Coord;

$initialMemory = require 'memory.php';
$inputBuffer = new IntCode\InputBuffer();
$outputBuffer = new IntCode\OutputBuffer();
$computer = new IntCode\Computer($inputBuffer, $outputBuffer);
//$generator = $computer->stepFeature($initialMemory);

$computer->compute($initialMemory);

//var_dump($outputBuffer->read());
$x = 0;
$y = 0;
$map = [];

foreach ($outputBuffer->read() as $code) {

    echo chr($code);

    if ($code == 10) {
        $x = 0;
        $y++;

        continue;
    }

    $coord = new Coord($x, $y);
    $map[(string) $coord] = $code;

    $x++;
}

/** @var Coord[] $intersections */
$intersections = [];

foreach ($map as $stringCoord => $code) {
    $coord = Coord::fromString($stringCoord);
    if ($map[(string) $coord] !== 35) {
        continue;
    }

    foreach ($coord->getNeighbours() as $neighbour) {
        if (empty($map[(string) $neighbour]) || $map[(string) $neighbour] !== 35) {
            continue 2;
        }
    }
    $intersections[(string) $coord] = $coord;
}

var_dump($intersections);
$x = 0;
$y = 0;
foreach ($outputBuffer->read() as $code) {

    if (!empty($intersections["{$x},{$y}"])) {
        echo "0";
    } else {
        echo chr($code);
    }

    if ($code == 10) {
        $x = 0;
        $y++;
        continue;
    }

    $coord = new Coord($x, $y);
    $map[(string) $coord] = $code;

    $x++;
}

$alignment = 0;
foreach ($intersections as $intersection) {
    $alignment += ($intersection->getXPosition() * $intersection->getYPosition());
}

// 1961 High
var_dump($alignment);
