<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

$signal = str_split(require __DIR__ . '/signal.php');

// Test Inputs
$signal = str_split("12345678");
//$signal = str_split("80871224585914546619083218645595");
//$signal = str_split("19617804207202209144916044189917");
//$signal = str_split("69317163492948606335995924319873");

echo "Times out!";
return;

// Cheat to get the container in this older procedural code (so I can get the cache directory)
$baseDir = dirname(__DIR__, 3);
$builder = new DI\ContainerBuilder();
$builder->addDefinitions($baseDir . '/config/definitions.php');
$container = $builder->build();

$patternGenerator = new PatternGenerator([0, 1, 0, -1]);
$cache = new Cache($container->get('application.cache.directory') . '/Cache.json');

$reportPhases = [100, 101];
$cache = new NoCache();
$reportChars = 8;

$cachedOutput = $cache->read();

if (isset($cachedOutput[100])) {
    $output = $cachedOutput[100];
    echo "After Phase 100 (cached): " . substr(implode($output), 0, $reportChars) . "\n";
}

$phaseStart = 1;
if (!empty($cachedOutput)) {
    $cachedOutputKeys = array_keys($cachedOutput);
    $phaseStart = (max($cachedOutputKeys) + 1);
}

ini_set('max_execution_time', '900');
//echo "<pre>";
for ($phase = $phaseStart; $phase < 10; $phase++) {
    echo "<p>Phase {$phase}</p>";

    $output = [];
    $set1 = [];

    // Set 1
    for ($outputElement = 1; $outputElement <= count($signal); $outputElement++) {
        $pattern = $patternGenerator->generate($outputElement, count($signal));

        $total = 0;
        foreach ($signal as $i => $number) {
            $total += $number * $pattern[$i];
        }
        $outputValue = substr((string) $total, -1);
        $output[] = $outputValue;
        $set1[] = $outputValue;
    }
    echo "<p>" . implode($set1) . "</p>";
    $set2= [];

//    var_dump(implode(array_slice($set1, 0,  count($signal)/2)));
//    var_dump(implode(array_slice($set1, count($signal)/2)));

//    echo('<p>02741365962795660749016751878030</p>');
    //$signal = array_slice($signal, 0,  count($signal)/2);
    //$signal = array_slice($signal, 0,  count($signal)/2);

    // Set 2
    for ($outputElement = 1; $outputElement <= (count($signal)/2); $outputElement++) {
        $pattern = $patternGenerator->generate($outputElement, count($signal), false);

        $total = 0;
        foreach ($signal as $i => $number) {
            $total += $number * $pattern[$i];
        }
        $outputValue = substr((string) $total, -1);
        $output[] = $outputValue;
        $set2[] = $outputValue;
    }
//    echo "<p>" . implode($set2) . "</p>";
//    echo "<p>" . implode($set2) . implode(array_slice($set1, (count($signal)/2) + 1)) . "0</p>";
    echo "<p>" .  implode($set1) . implode($set2) . implode(array_slice($set1, (count($signal)/2) + 1)). "0</p>";
    $signal = str_split(implode($set1) . implode($set2) . implode(array_slice($set1, (count($signal)/2) + 1)). "0");

    //echo "<p>" .  implode($set1) . implode($set2). "</p>";
    //$signal = $output;

    //echo "\nAfter Phase {$phase}: ".implode($output) . "\n\n";
    if (in_array($phase, $reportPhases)) {
        echo "After Phase {$phase}: " . substr(implode($output), 0, $reportChars) . "\n";
    }
    $cache->save($phase, $signal);
}
//echo "</pre>";

//var_dump($patternGenerator->generate(1, count($signal)));
//var_dump($patternGenerator->generate(2, count($signal)));

class PatternGenerator
{
    private $basePattern;

    private $patterns = [];

    public function __construct(array $basePattern)
    {
        $this->basePattern = $basePattern;
    }

    public function generate($outputElement, $length, $skipFirst = true)
    {
        if (isset($this->patterns["{$outputElement},{$length},{$skipFirst}"])) {
            return $this->patterns["{$outputElement},{$length},{$skipFirst}"];
        }
        $pattern = "";
        foreach ($this->basePattern as $number) {
            $pattern .= str_repeat((string) $number . ",", $outputElement);
        }

        $pattern .= trim(str_repeat($pattern, $length));
        $pattern = explode(",", $pattern);
        $pattern = array_slice($pattern, ($skipFirst ? 1 : 0), $length);

        $this->patterns["{$outputElement},{$length},{$skipFirst}"] = $pattern;
        return $pattern;
    }
}

class Cache
{
    private $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function read()
    {
        if (!file_exists($this->filename)) {
            return [];
        }
        return json_decode(file_get_contents($this->filename), true);
    }

    public function save(int $phase, array $output)
    {
        $current = $this->read();
        $current[$phase] = $output;
        file_put_contents($this->filename, json_encode($current));
    }
}

class NoCache
{
    public function read()
    {
        return [];
    }

    public function save(int $phase, array $output)
    {
    }
}
