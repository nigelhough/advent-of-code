<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

use AdventOfCode\IntCode;
use AdventOfCode\Grid\Coord;

const MOVE_NORTH = 1;
const MOVE_SOUTH = 2;
const MOVE_WEST = 3;
const MOVE_EAST = 4;

const RESPONSE_BLOCKED = 0;
const RESPONSE_MOVED = 1;
const RESPONSE_FOUND = 2;

echo "Times out!";
return;

$initialMemory = require __DIR__ . '/memory.php';
$inputBuffer = new IntCode\InputBuffer();
$outputBuffer = new IntCode\OutputBuffer();
$computer = new IntCode\Computer($inputBuffer, $outputBuffer);
$generator = $computer->stepFeature($initialMemory);

$map = [];
$location = new Coord(0, 0);

$map[(string) $location] = 'S';

$unknownLocations = [
    [new Coord(0, 1), []], // North of start
    [new Coord(1, 0), []], // East of start
    [new Coord(0, -1), []], // South of start
    [new Coord(-1, 0), []], // West of start
];

$pathToStart = [];
$limit = 0;
// Iterate through all unknown location.
ini_set('max_execution_time', '900');
while (!empty($unknownLocations) && $limit < 5000) {

    /** @var Coord $unknownLocation */
    $unknownLocation = array_shift($unknownLocations);

    if (array_reverse($pathToStart) == $unknownLocation[1]) {
        // Skip the path backtrace as we are already where we need to be to see the unknown location.
    } else {
        // Can this still be improved with partial path matches
//        if ($limit > 300) {
//            var_dump(array_reverse($pathToStart));
//            var_dump($unknownLocation[1]);
//            exit;
//        }
        // @todo Find how PathToStart and PathToUnknown Intersect. Can it skip a lot of steps.

        // Navigate back to start.
        while (!empty($pathToStart)) {
            $direction = array_pop($pathToStart);
            $inputBuffer->set($direction);
            $response = getNextOutput($generator, $outputBuffer);
            if ($response !== RESPONSE_MOVED) {
                throw new \Exception('Unable to Backtrack');
            }
            $location = getCoords($location, $direction);
        }

        if (!$location->isSame(new Coord(0, 0))) {
            var_dump($limit);
            var_dump($location);
            throw new \Exception('Should be at the start');
        }

        // Navigate to where unknown location was discovered.
        $pathToUnknownLocation = $unknownLocation[1];
        while (!empty($pathToUnknownLocation)) {
            $direction = getInvertedDirection(array_pop($pathToUnknownLocation));
            $inputBuffer->set($direction);
            $response = getNextOutput($generator, $outputBuffer);
            if ($response !== RESPONSE_MOVED) {
                throw new \Exception('Unable to Backtrack');
            }
            $location = getCoords($location, $direction);
            $pathToStart[] = getInvertedDirection($direction);
        }
    }

    // Set direction to get to unknown location.
    $direction = getDesiredDirection($unknownLocation[0], $location);
    $inputBuffer->set($direction);

    $response = getNextOutput($generator, $outputBuffer);

    if ($response === RESPONSE_BLOCKED) {
        $target = getCoords($location, $direction);
        $map[(string) $target] = '#';
    } elseif ($response === RESPONSE_MOVED) {
        $location = getCoords($location, $direction);
        $pathToStart[] = getInvertedDirection($direction);
        $map[(string) $location] = '.';
        chartUnknownLocations($location, $direction, $pathToStart, $map, $unknownLocations);
    } elseif ($response === RESPONSE_FOUND) {
        $target = getCoords($location, $direction);
        $map[(string) $target] = '0';
//        break;
    }

    //var_dump("{$location->getXPosition()}, {$location->getYPosition()}. ({$response})");
//    var_dump($map);
//    var_dump($unknownLocations);
    $generator->next();
    $limit++;

    if ($limit % 400 == 0) {
        echo "<p>$limit</p>";
        renderMap($map);
//        exit;
    }
}

//var_dump($map);
renderMap($map);

function renderMap($map)
{
    $height = 35;
    $width = 45;

    $mapCopy = $map;
    $mapCopy[(string) new Coord(-$width, $height)] = 'A';
    $mapCopy[(string) new Coord($width, $height)] = 'B';
    $mapCopy[(string) new Coord(-$width, -$height)] = 'C';
    $mapCopy[(string) new Coord($width, -$height)] = 'D';

    echo "<pre style='border:solid 1px black;'>";
    drawMap($mapCopy, $height, $width);
    echo "</pre>";
}

function drawMap(array $map, int $height = 10, int $width = 10)
{

        for ($h = $height; $h > (0 - $height); $h--) {
            for ($w = 0 - $width; $w <= $width; $w++) {
            if (isset($map["$w,$h"])) {
                echo $map["$w,$h"];
            } else {
                echo "*";
            }
            //var_dump("$w,$h");
        }
        echo PHP_EOL;
    }
}

function getCoords(Coord $coord, int $direction): Coord
{
    return match($direction) {
        MOVE_NORTH => new Coord($coord->getXPosition(), $coord->getYPosition() + 1),
        MOVE_SOUTH => new Coord($coord->getXPosition(), $coord->getYPosition() - 1),
        MOVE_EAST => new Coord($coord->getXPosition() + 1, $coord->getYPosition()),
        MOVE_WEST => new Coord($coord->getXPosition() - 1, $coord->getYPosition()),
    };
}

function getNextOutput(\Generator $generator, IntCode\OutputInterface $outputBuffer)
{
    while (empty($outputBuffer->read())) {
        $generator->next();
    }
    $response = $outputBuffer->read()[0];
    $outputBuffer->clear();

    return $response;
}

function getDesiredDirection(Coord $unknownLocation, Coord $currentLocation): int
{
    $currentNorth = getCoords($currentLocation, MOVE_NORTH);

    if ($currentNorth->isSame($unknownLocation)) {
        return MOVE_NORTH;
    }

    $currentSouth = getCoords($currentLocation, MOVE_SOUTH);
    if ($currentSouth->isSame($unknownLocation)) {
        return MOVE_SOUTH;
    }

    $currentEast = getCoords($currentLocation, MOVE_EAST);
    if ($currentEast->isSame($unknownLocation)) {
        return MOVE_EAST;
    }

    $currentWest = getCoords($currentLocation, MOVE_WEST);
    if ($currentWest->isSame($unknownLocation)) {
        return MOVE_WEST;
    }

    var_dump($currentLocation);
    var_dump($unknownLocation);
    throw new \Exception('Not Next to Unknown Location');
}

function getInvertedDirection(int $direction)
{
    return match ($direction) {
        MOVE_NORTH => MOVE_SOUTH,
        MOVE_SOUTH => MOVE_NORTH,
        MOVE_EAST => MOVE_WEST,
        MOVE_WEST => MOVE_EAST,
    };
}

function chartUnknownLocations(Coord $location, int $direction, array $path, array $map, array &$unknownLocations)
{
    $possibleLocations = [];
    // Add the directions visible from current, excluding where we just cam from.
    if (getInvertedDirection($direction) != MOVE_NORTH) {
        $possibleLocations[] = getCoords($location, MOVE_NORTH);
    }
    if (getInvertedDirection($direction) != MOVE_SOUTH) {
        $possibleLocations[] = getCoords($location, MOVE_SOUTH);
    }
    if (getInvertedDirection($direction) != MOVE_EAST) {
        $possibleLocations[] = getCoords($location, MOVE_EAST);
    }
    if (getInvertedDirection($direction) != MOVE_WEST) {
        $possibleLocations[] = getCoords($location, MOVE_WEST);
    }


    foreach ($possibleLocations as $possibleLocation) {
        if (isset($map[(string) $possibleLocation])) {
            // Don't add already charted location.
            continue;
        }
        foreach ($unknownLocations as $unknownLocation) {
            if ($unknownLocation[0]->isSame($possibleLocation)) {
                // Don't add a location already in unknown list.
                continue 2;
            }
        }

        // Add unknown location with path to get to this point from the start (the reverse of the path to the start)
        $unknownLocations[] = [$possibleLocation, array_reverse($path)];
    }
}
