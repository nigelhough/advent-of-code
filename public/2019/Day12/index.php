<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

use AdventOfCode\IntCode;

// Puzzle
$input = "<x=1, y=4, z=4>
<x=-4, y=-1, z=19>
<x=-15, y=-14, z=12>
<x=-17, y=1, z=10>";

$moons = [
    new Moon(1, 4, 4),
    new Moon(-4, -1, 19),
    new Moon(-15, -14, 12),
    new Moon(-17, 1, 10),
];

// Example
$input = "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>";
/** @var Moon[] $moons */
$moons = [
    new Moon(-1, 0, 2),
    new Moon(2, -10, -7),
    new Moon(4, -8, 8),
    new Moon(3, 5, -1),
];

//// Example 2
//$input = "<x=-8, y=-10, z=0>
//<x=5, y=5, z=10>
//<x=2, y=-7, z=3>
//<x=9, y=-8, z=-3>";
//
//$moons = [
//    new Moon(-8, -10, 0),
//    new Moon(5, 5, 10),
//    new Moon(2, -7, 3),
//    new Moon(9, -8, -3),
//];

$states = [];
$state = "";
foreach ($moons as $subject) {
    $state .= $subject->getState() . "#";
}
$states[$state] = 0;

ini_set('max_execution_time', '900');
for ($steps = 1; $steps <= PHP_INT_MAX; $steps++) {
    foreach ($moons as $subject) {
        foreach ($moons as $target) {
            if ($subject->isSame($target)) {
                continue;
            }

            $subject->updateVelocity($target);
        }
    }
    foreach ($moons as $subject) {
        $subject->applyVelocities();
    }

    $state = "";
    foreach ($moons as $subject) {
        $state .= $subject->getState() . "#";
    }

    if (isset($states[$state])) {
        var_dump($state);
        var_dump($steps);
        break;
    }

    $states[$state] = $steps;
//    echo "Steps {$steps}";
//    var_dump($moons);
}

$total = 0;
foreach ($moons as $subject) {
    $total += $subject->calculateEnergy();
}
echo "Total {$total}";

class Moon
{
    private $identifier;

    private $xPosition;
    private $yPosition;
    private $zPosition;

    private $xVelocity = 0;
    private $yVelocity = 0;
    private $zVelocity = 0;

    public function __construct(int $x, int $y, int $z)
    {
        $this->identifier = md5((string) rand());
        $this->xPosition = $x;
        $this->yPosition = $y;
        $this->zPosition = $z;
    }

    public function applyVelocities()
    {
        $this->xPosition += $this->xVelocity;
        $this->yPosition += $this->yVelocity;
        $this->zPosition += $this->zVelocity;
    }

    public function updateVelocity(self $moon)
    {
        if ($moon->xPosition > $this->xPosition) {
            $this->xVelocity++;
        } elseif ($moon->xPosition < $this->xPosition) {
            $this->xVelocity--;
        }
        if ($moon->yPosition > $this->yPosition) {
            $this->yVelocity++;
        } elseif ($moon->yPosition < $this->yPosition) {
            $this->yVelocity--;
        }
        if ($moon->zPosition > $this->zPosition) {
            $this->zVelocity++;
        } elseif ($moon->zPosition < $this->zPosition) {
            $this->zVelocity--;
        }
    }

    public function calculateEnergy(): int
    {
        return
            (abs($this->xPosition) + abs($this->yPosition) + abs($this->zPosition)) *
            (abs($this->xVelocity) + abs($this->yVelocity) + abs($this->zVelocity));
    }


    public function isSame(self $moon)
    {
        return $this->identifier === $moon->identifier;
    }

    public function getState(): string
    {
        return "{$this->xPosition}:{$this->xVelocity}:{$this->yPosition}:{$this->yVelocity}:{$this->zPosition}:{$this->zVelocity}";
    }
}
