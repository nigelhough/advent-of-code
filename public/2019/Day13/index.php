<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

use AdventOfCode\Game;
use AdventOfCode\IntCode;

$initialMemory = require __DIR__ . '/memory.php';
$inputBuffer = new IntCode\InputBuffer();
$outputBuffer = new IntCode\OutputBuffer();
$computer = new IntCode\Computer($inputBuffer, $outputBuffer);
$computer->compute($initialMemory);

$instructionSorter = new Game\InstructionSorter(new Game\TileFactory(), new Game\ScoreFactory());
$tiles = $instructionSorter->getTiles($outputBuffer->read());
$gameTileCount = count($tiles);

$blockTiles = array_filter($tiles, function ($tile) {
    return $tile instanceOf Game\Tile\Block;
});

if (php_sapi_name() != "cli") {
    echo "<p>Block Tiles: " . count($blockTiles) . "</p>";
    echo "<p>Tiles: " . $gameTileCount . "</p>";
    echo "<hr/>";
}

// Part 2
$outputBuffer->clear();
$memory = $initialMemory;
$memory[0] = 2;

$generator = $computer->stepFeature($memory);

$instructionSorter = new Game\InstructionSorter(new Game\TileFactory(), new Game\ScoreFactory());
$initialTileLocations = [];
$lastOutputLength = 0;
while ($generator->valid()) {
    $output = $outputBuffer->read();
    $isDivisibleeByThree = ((count($output) % 3) === 0);
    if (empty($output) || !$isDivisibleeByThree) {
        @$generator->next();
        continue;
    }

    if (count($output) == $lastOutputLength) {
        @$generator->next();
        continue;
    }
    $lastOutputLength = count($output);

    $initialTileLocations = $instructionSorter->getTiles($outputBuffer->read());
    if (count($initialTileLocations) >= $gameTileCount) {
        break;
    }

    @$generator->next();
}
$board = new Game\Board($initialTileLocations);
if (php_sapi_name() == "cli") {
    $board->render();
}
$outputBuffer->clear();

$inputBuffer->set(1);

const UP = 1;
const DOWN = 0;

const RIGHT = 1;
const LEFT = 0;

$yDirection = DOWN;
$xDirection = RIGHT;
$lastOutputLength = 0;
/** @var Game\Tile\Ball $lastPosition */
$lastPosition = null;
$moveTiles = [];
$scoreTiles = [];
$command = 0;
$score = 0;
while ($generator->valid()) {
    while ($generator->valid()) {
        $output = $outputBuffer->read();
        $isDivisibleeByThree = ((count($output) % 3) === 0);
        if (empty($output) || !$isDivisibleeByThree) {
            @$generator->next();
            continue;
        }

        if (count($output) == $lastOutputLength) {
            @$generator->next();
            continue;
        }
        $lastOutputLength = count($output);

        $moveTiles = $instructionSorter->getTiles($outputBuffer->read());
        if (!empty($moveTiles) && $moveTiles[count($moveTiles) - 1]->getId() === Game\Tile\Ball::TILE_ID) {
            break;
        }
        $scoreTiles = $instructionSorter->getScores($outputBuffer->read());

        @$generator->next();
    }

    if (!empty($scoreTiles)) {
        /** @var Game\Score $scoreTile */
        $scoreTile = @array_pop($scoreTiles);
        $score = $scoreTile->getScore();
    }

    /** @var Game\Tile\Paddle $paddlePosition */
    $paddlePositions = (array_filter($moveTiles, function ($tile) {
        return $tile instanceOf Game\Tile\Paddle;
    }));
    if (!empty($paddlePositions)) {
        $paddlePosition = array_pop($paddlePositions);
    }
    $ballPositions = array_filter($moveTiles, function ($tile) {
        return $tile instanceOf Game\Tile\Ball;
    });
    if (empty($ballPositions)) {
        break;
    }
    /** @var Game\Tile\Ball $ballPosition */
    $ballPosition = array_pop($ballPositions);

    $distanceToBottom = (22 - $ballPosition->getYPosition());

    // Work out ball Y Direction
    if ($lastPosition !== null && $lastPosition->getYPosition() < $ballPosition->getYPosition()) {
        $yDirection = DOWN;
    } elseif ($lastPosition !== null && $lastPosition->getYPosition() > $ballPosition->getYPosition()) {
        $yDirection = UP;
    }
    // Work out ball X Direction
    if ($lastPosition !== null && $lastPosition->getXPosition() < $ballPosition->getXPosition()) {
        $xDirection = RIGHT;
    } elseif ($lastPosition !== null && $lastPosition->getXPosition() > $ballPosition->getXPosition()) {
        $xDirection = LEFT;
    }

    if ($xDirection === RIGHT) {
        $targetX = $ballPosition->getXPosition() + $distanceToBottom; // Dependent on direction of travel
    } else {
        $targetX = $ballPosition->getXPosition() - $distanceToBottom; // Dependent on direction of travel
    }

    $isHit = ($paddlePosition->getYPosition() === ($ballPosition->getYPosition() + 1))
        && ($paddlePosition->getXPosition() === $targetX);

    if ($isHit) {
        $command = 0;
        // Fiddled the start, @todo Work out what to do, perhaps invert
        if ($paddlePosition->getXPosition() === 26 || $paddlePosition->getXPosition() === 34) {
            $command = 1;
        } else {
            $command = -1;
        }
    } else {
        if ($paddlePosition->getXPosition() > $targetX) {
            $command = -1;
        } elseif ($paddlePosition->getXPosition() < $targetX) {
            $command = 1;
        } else {
            $command = 0;
        }
    }
    $inputBuffer->set($command);

    $board->updatePositions($moveTiles);
    if (php_sapi_name() == "cli") {
        $board->render();

        echo 'Ball: ' . $ballPosition->getYPosition() . ", " . $ballPosition->getYPosition() . PHP_EOL;
        if ($lastPosition !== null) {
            echo 'Last: ' . $lastPosition->getYPosition() . ", " . $lastPosition->getYPosition() . PHP_EOL;
        }
        echo 'Paddle: ' . $paddlePosition->getXPosition() . ", " . $paddlePosition->getYPosition() . PHP_EOL;
        echo "Y Direction {$yDirection}: " . ($yDirection === UP ? 'Up' : 'Down') . PHP_EOL;
        echo "X Direction {$xDirection}: " . ($xDirection === RIGHT ? 'Right' : 'Left') . PHP_EOL;
        echo "Target {$targetX}" . PHP_EOL;
        echo "Command {$command}" . PHP_EOL;
        echo $isHit ? "Hit!" : "" . PHP_EOL;
        echo "Score {$score}" . PHP_EOL;
    }
    $outputBuffer->clear();

    $lastPosition = $ballPosition;
}

echo "Fin!" . PHP_EOL;
echo "Score {$score}" . PHP_EOL;

