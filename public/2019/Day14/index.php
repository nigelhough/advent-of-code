<?php
declare(strict_types = 1);

set_include_path(realpath(__DIR__ . '/../../..') . PATH_SEPARATOR . get_include_path());
require_once 'vendor/autoload.php';

$reactionFormulas = require __DIR__ . '/reactions.php';
//$reactionFormulas = "10 ORE => 10 A
//1 ORE => 1 B
//7 A, 1 B => 1 C
//7 A, 1 C => 1 D
//7 A, 1 D => 1 E
//7 A, 1 E => 1 FUEL";
//
//$reactionFormulas = "9 ORE => 2 A
//8 ORE => 3 B
//7 ORE => 5 C
//3 A, 4 B => 1 AB
//5 B, 7 C => 1 BC
//4 C, 1 A => 1 CA
//2 AB, 3 BC, 4 CA => 1 FUEL";
//
//$reactionFormulas = "157 ORE => 5 NZVS
//165 ORE => 6 DCFZ
//44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
//12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
//179 ORE => 7 PSHF
//177 ORE => 5 HKGWZ
//7 DCFZ, 7 PSHF => 2 XJWVT
//165 ORE => 2 GPVTF
//3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
//
//$reactionFormulas = "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
//17 NVRVD, 3 JNWZP => 8 VPVL
//53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
//22 VJHF, 37 MNCFX => 5 FWMGM
//139 ORE => 4 NVRVD
//144 ORE => 7 JNWZP
//5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
//5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
//145 ORE => 6 MNCFX
//1 NVRVD => 8 CXFTF
//1 VJHF, 6 MNCFX => 4 RFSQX
//176 ORE => 6 VJHF";
//
//$reactionFormulas = "171 ORE => 8 CNZTR
//7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
//114 ORE => 4 BHXH
//14 VRPVC => 6 BMBT
//6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
//6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
//15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
//13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
//5 BMBT => 4 WPTQ
//189 ORE => 9 KTJDG
//1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
//12 VRPVC, 27 CNZTR => 2 XDBXC
//15 KTJDG, 12 BHXH => 5 XCVML
//3 BHXH, 2 VRPVC => 7 MZWV
//121 ORE => 7 VRPVC
//7 XCVML => 6 RJRHP
//5 BHXH, 4 VRPVC => 5 LTCX";

$reactions = [];
foreach (explode("\n", $reactionFormulas) as $formula) {
    $reactions[] = new Reaction($formula);
}
$reactor = new Reactor(new ReactionCollection($reactions));

global $requiredOre;
$reactor->forge('FUEL');
var_dump($requiredOre);

$trillion = 1000000000000;

//$below = 82892712;
//$above = 82896789;
//$below = 1;
//$above = $trillion;
$below = ($trillion / $requiredOre) / 2;
$above = ($trillion / $requiredOre) * 2;
$middle = (int) (floor($below + (($above - $below) / 2)));

// Bisect until you find the fuel that can be built by 1 trillon ore.
$found = false;
while ($found === false) {
    $reactor->purge();
    $reactor->forge('FUEL', $middle);
    $beforeRequiredOre = $requiredOre;

    $reactor->purge();
    $reactor->forge('FUEL', $middle + 1);
    $afterRequiredOre = $requiredOre;

    if ($beforeRequiredOre < $trillion && $afterRequiredOre > $trillion) {
        $found = true;
        break;
    }

    if ($afterRequiredOre < $trillion) {
        $below = $middle;
    }

    if ($beforeRequiredOre > $trillion) {
        $above = $middle;
    }
    $middle = (int) (floor($below + (($above - $below) / 2)));

    if($below === $above) {
        throw new \Exception('Bisect Error');
    }
}

var_dump($middle);

class Reactor
{
    private $reactions = [];

    private $store = [];

    public function __construct(ReactionCollection $reactions)
    {
        $this->reactions = $reactions;
    }

    public function forge(string $targetMineral, int $targetQuantity = 1)
    {
        $reaction = $this->reactions->findRequiredReaction($targetMineral);
        $this->react($reaction, $targetQuantity);
    }

    public function react(Reaction $reaction, int $targetQuantity)
    {
        $output = $reaction->getOutput();
        if (!empty($this->store[$output->getMineral()])) {
            // Target quantity material is filed from storage.
            if ($this->store[$output->getMineral()] >= $targetQuantity) {
                $this->store[$output->getMineral()] = $this->store[$output->getMineral()] - $targetQuantity;
                return;
            }

            // Store has some of the material, reduce target quantity.
            if ($this->store[$output->getMineral()] < $targetQuantity) {
                $targetQuantity = $targetQuantity - $this->store[$output->getMineral()];
                $this->store[$output->getMineral()] = 0;
            }
        }
        $noReactions = ceil($targetQuantity / $output->getQuantity());
        $leftovers = ($output->getQuantity() * $noReactions) - $targetQuantity;

        // Store any leftover material
        $this->store[$output->getMineral()] = empty($this->store[$output->getMineral()])
            ? $leftovers
            : $this->store[$output->getMineral()] + $leftovers;

        foreach ($reaction->getInputs() as $reactionInput) {
            $requiredQuantity = (int) ($reactionInput->getQuantity() * $noReactions);
            $requiredMineral = $reactionInput->getMineral();

            if ($requiredMineral !== 'ORE') {
                $this->forge($requiredMineral, $requiredQuantity);
            } else {
                global $requiredOre;
                $requiredOre += $requiredQuantity;
//                var_dump($requiredQuantity);
//                var_dump($requiredMineral);
            }
        }
    }

    public function purge()
    {
        $this->store = [];
        global $requiredOre;
        $requiredOre = 0;
    }
}

class ReactionCollection
{
    /** @var Reaction[] */
    private $reactions;

    public function __construct(array $reactions)
    {
        $this->reactions = $reactions;
    }

    public function findRequiredReaction(string $targetMineral): Reaction
    {
        foreach ($this->reactions as $reaction) {
            if ($reaction->getOutput()->getMineral() === $targetMineral) {
                return $reaction;
            }
        }
        throw new \Exception("Unachiavble Mineral {$targetMineral}");
    }
}

class Reaction
{
    /** @var array MineralQuantity[] */
    private $inputs = [];

    /** @var MineralQuantity */
    private $output;

    public function __construct(string $formula)
    {
        list($inputs, $output) = explode("=>", $formula);
        foreach (explode(",", $inputs) as $input) {
            $this->inputs[] = new MineralQuantity($input);
        }
        $this->output = new MineralQuantity($output);
    }

    /**
     * @return MineralQuantity[]
     */
    public function getInputs(): array
    {
        return $this->inputs;
    }

    /**
     * @return MineralQuantity
     */
    public function getOutput(): MineralQuantity
    {
        return $this->output;
    }
}

class MineralQuantity
{
    private $quantity;

    private $mineral;

    public function __construct(string $mineralQuantity)
    {
        list($quantity, $mineral) = explode(" ", trim($mineralQuantity));
        $this->quantity = (int) trim($quantity);
        $this->mineral = trim($mineral);
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getMineral(): string
    {
        return $this->mineral;
    }

}
