<?php
declare(strict_types = 1);

$baseDir = dirname(__DIR__);
chdir($baseDir);
set_include_path($baseDir . PATH_SEPARATOR . get_include_path());
define('BASE_DIR', $baseDir);

require_once 'vendor/autoload.php';

// Build Dependency Injection Container.
$builder = new DI\ContainerBuilder();
$builder->addDefinitions($baseDir . '/config/definitions.php');
$container = $builder->build();

// Build App.
$app = DI\Bridge\Slim\Bridge::create($container);

// Whoops error page.
$app->add(
    new Zeuxisoo\Whoops\Slim\WhoopsMiddleware(
        [
            'enable' => true,
            'editor' => 'phpstorm',
            'title'  => 'Error',
        ]
    )
);

// Add App Routes.
require_once $baseDir . '/config/routes.php';

// Run App!
$app->run();
