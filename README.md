# Advent Of Code - Nigel Hough

This is Nigel Hough's repository for Advent Of code 2018, 2019, 2020 and 2021.

This project has evolved over the years from just a collection of procedural files to a structure and helpers to 
automatically pull data from AdventOfCode.

The project and solutions are all in PHP!

Don't judge me by the code here, it's just for fun 😊.

## SetUp

The project contains my solutions but the structure and helpers can be used by anyone.

### Docker

The project is designed to run in Docker and can be spun up with Docker Compose.
To start containers detached run this.
```
docker-compose up -d
```

This starts an NGINX web server and two PHP containers, one for hosting the project and one for building the project 
(with extra bits like Composer).
Once up and running the webserver will serve on port 80. `http://localhost` or if you add a host record 
`http://dev.adventofcode.co.uk`.

The project will automatically pull AdventOfCode Input and Leaderboards, to do this you need to set your session and
user id as environment variables. Do this by creating a `.env` file copied from `.env.dist`.

#### Published Containers

The build image of this project is pushed to a private image registry and used in the BitBucket Pipelines build.
Publishing and Pulling the images requires authentication with the registry using `docker login registry.digitalocean.com`
To publish the build image.
```
docker-compose build php-build
docker tag advent-of-code_php-build registry.digitalocean.com/nigelhough/advent-of-code_php-build
docker push registry.digitalocean.com/nigelhough/advent-of-code_php-build
```

## Commands
There is a dedicated container for building the project `adventofcode-php-build`. You can execute commands in this
container for building the project.

All below commands use `docker exec` assumes you have a running build container with `docker-compose up -d` or `docker-compose up -d adventofcode-php-build`.
They can all be run in a clean container by changing the start to `docker-compose run --rm php-build`
```
docker-compose exec php-build composer update
docker-compose exec php-build composer install
```

There is a container that hosts the project `adventofcode-php`.
You can execute commands in this container, there is a CLI interface you can use for long-running solutions.
```
docker-compose exec php php bin/cli.php 2021 1
```

### Execute Unit Tests
Execute unit tests.
```
docker-compose exec php-build composer test
```

### Execute Static Code Analysis
Execute static code analysis.
```
docker-compose exec php-build composer analyse
```

### Execute Checks
Execute a number of code checks
```
docker-compose exec php-build composer check
```
#### Out of Date Dependencies
Check dependencies are as up-to-date as possible.
* All direct Major version up-to-date
* All minor versions up-to-date

## Evolution

This started as a collection of procedural files see these under `public/20*`.

Since then, it was updated to host a simple Slim App. See `public/index.php`
The app configuration is in `config/definitions.php` and `config/routes.php`

To work with the older procedural files and to make the new solutions easier the app just shows the output of the 
solutions as text as if run on the CLI. This means they can freely echo or dump data.

The App will search for a solution under the namespace `AdventOfCode\Solution` and if nothing is there it will fall back
to the `public/20^` files.

If no solution exists when you view the page in the UI it will offer to generate a solution under `AdventOfCode\Solution`.

Solutions that match the interface `\AdventOfCode\Solution\SolutionInterface` will have a method
`public function execute(string $input): ?ResultInterface;`.
This method will be called with the input automatically injected fetched from the AdventOfCode website. These inputs will be cached so
that it limits the number of requests made to the website. All of this makes it quicker when you are coding against the
clock.

To see how the input is fetched from `adventofcode.com` you can see `\AdventOfCode\Solution`.

## Output

The Homepage of the App.

![The Home Page](home.png)


A Solution page

![A Solution Page](solution.png)
