<?php

use AdventOfCode\Answer;
use AdventOfCode\Input;
use AdventOfCode\LeaderBoard;

return [
    'application.baseUrl' => getenv('BASE_URL') ?: 'http://localhost',
    'application.cache.directory' => getenv('CACHE_DIR') ?: __DIR__ . '/../cache',
    'adventofcode.session_cookie' => getenv('SESSION_COOKIE') ?: '',
    'adventofcode.user_id' => (int)getenv('USER_ID') ?: 0,
    'adventofcode.leaderboardId' => (int)getenv('LEADERBOARD_ID') ?: 0,

    // Controllers.
    \AdventOfCode\IndexController::class => DI\autowire(\AdventOfCode\IndexController::class)
        ->constructorParameter('leaderboardId', DI\get('adventofcode.leaderboardId')),
    \AdventOfCode\LeagueController::class => DI\autowire(\AdventOfCode\LeagueController::class)
        ->constructorParameter('leaderboardId', DI\get('adventofcode.leaderboardId')),
    \AdventOfCode\SolutionController::class => DI\autowire(\AdventOfCode\SolutionController::class)
        ->constructorParameter('leaderboardId', DI\get('adventofcode.leaderboardId')),

    // Input.
    Input\RepositoryInterface::class => DI\autowire(Input\Cache::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('fallback', DI\get(Input\Remote::class)),

    // Leaderboard.
    LeaderBoard\RepositoryInterface::class => DI\autowire(LeaderBoard\Cache::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('fallback', DI\get(LeaderBoard\Remote::class)),
    LeaderBoard\Individual\IndividualLeaderboardRepository::class => DI\autowire(
        LeaderBoard\Individual\IndividualLeaderboardRepository::class
    )
        ->constructorParameter('repository', DI\get(LeaderBoard\RepositoryInterface::class))
        ->constructorParameter('userId', DI\get('adventofcode.user_id')),

    // Results.
    Answer\ProcessorInterface::class => DI\autowire(Answer\Processor\NullAware::class)
        ->constructorParameter('processor', DI\get(Answer\Processor\SuperGlobalAware::class)),
    Answer\Processor\SuperGlobalAware::class => DI\autowire(Answer\Processor\SuperGlobalAware::class)
        ->constructorParameter('processor', DI\get(Answer\Processor\LeaderboardAware::class)),
    Answer\Processor\LeaderboardAware::class => DI\autowire(Answer\Processor\LeaderboardAware::class)
        ->constructorParameter('leaderboardId', DI\get('adventofcode.leaderboardId'))
        ->constructorParameter('processor', DI\get(Answer\Processor\HintAware::class)),
    Answer\Processor\HintAware::class => DI\autowire(Answer\Processor\HintAware::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('processor', DI\get(Answer\Processor\Cooldown::class)),
    Answer\Processor\Cooldown::class => DI\autowire(Answer\Processor\Cooldown::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('processor', DI\get(Answer\Processor\Cache::class)),
    Answer\Processor\Cache::class => DI\autowire(Answer\Processor\Cache::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('processor', DI\get(Answer\Processor\Remote::class)),
    Answer\Processor\Remote::class => DI\autowire(Answer\Processor\Remote::class),

    // Client
    \AdventOfCode\Client\ClientInterface::class => DI\autowire(\AdventOfCode\Client\RateLimited::class)
        ->constructorParameter('cacheDir', DI\get('application.cache.directory'))
        ->constructorParameter('fallback', DI\get(\AdventOfCode\Client\Guzzle::class)),
    \AdventOfCode\Client\Guzzle::class => DI\autowire(\AdventOfCode\Client\Guzzle::class)
        ->constructorParameter('sessionCookie', DI\get('adventofcode.session_cookie')),

    // Templating System.
    Twig\Environment::class => DI\factory(
        function () {
            $twig = new Twig\Environment(
                new Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/templates'),
                [
                    'debug' => true,
                    'strict_variables' => true,
                    'cache' => (getenv('CACHE_DIR') ?: __DIR__ . '/../cache') . '/twig',
                ]
            );
            $twig->addExtension(new Twig\Extension\DebugExtension());
            return $twig;
        }
    ),
];
