<?php

use Slim\App;

/**
 * @var $app App The Application
 */

use AdventOfCode\IndexController;
use AdventOfCode\Input;
use AdventOfCode\LeagueController;
use AdventOfCode\SolutionController;
use Psr\Http\Message\ResponseInterface;

// List.
$app->get('/', IndexController::class . ':index');

// League
$app->get('/league', LeagueController::class . ':league');

// Solution Routes.
$app->get('/{year}/{day}', SolutionController::class . ':solution')->add(Input\Middleware::class);
$app->get('/generate/{year}/{day}', SolutionController::class . ':generate');
