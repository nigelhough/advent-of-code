<?php

declare(strict_types=1);

namespace AdventOfCode;

use DateTime;
use DateTimeZone;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class IndexController
{
    public function __construct(private int $leaderboardId)
    {
    }

    /**
     * Index Controller
     */
    public function index(
        ResponseInterface $response,
        Environment $template,
        LeaderBoard\Individual\IndividualLeaderboardRepository $userLeague
    ): ResponseInterface {
        // Advent of Code Always Starts midnight in EST.
        $now = new DateTime('now', new DateTimeZone('America/New_York'));
        $response->getBody()->write(
            $template->render(
                'index.html',
                [
                    'years' => range(2015, 2024),
                    'currentYear' => (int)$now->format('Y'),
                    'currentDay' => $now->format('m') === '12' ? (int)$now->format('d') : 0,
                    'userLeague' => $userLeague,
                    'leaderboardId' => $this->leaderboardId,
                ]
            )
        );
        return $response;
    }
}
