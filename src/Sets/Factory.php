<?php

declare(strict_types=1);

namespace AdventOfCode\Sets;

use Generator;

/**
 * A factory for creating sets.
 */
class Factory
{
    /**
     * Iterate the power set of a set.
     *
     * @param array $set A set to calculate powerSet.
     *
     * @return Generator
     */
    public static function powerSet(array $set): Generator
    {
        $results = [[]];
        foreach ($set as $element) {
            foreach ($results as $combination) {
                $results[] = array_merge([$element], $combination);
                yield array_merge([$element], $combination);
            }
        }
    }
}
