<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

class SequenceGenerator
{
    public function generate($min = 0, $max = 4)
    {
        $sequences = [];
        for ($a = $min; $a <= $max; $a++) {
            for ($b = $min; $b <= $max; $b++) {
                for ($c = $min; $c <= $max; $c++) {
                    for ($d = $min; $d <= $max; $d++) {
                        for ($e = $min; $e <= $max; $e++) {
                            $combination = [$a, $b, $c, $d, $e];
                            $uniques = array_unique($combination);

                            if (count($uniques) == 5) {
                                $sequences[] = [$a, $b, $c, $d, $e];
                            }
                        }
                    }
                }
            }
        }

        return $sequences;
    }
}
