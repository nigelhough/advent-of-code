<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

interface OutputInterface
{
    public function put(int $value);

    public function clear();

    public function read(): array;
}
