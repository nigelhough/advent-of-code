<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

use AdventOfCode\IntCode\Feature\AbstractFeature;
use Exception;
use Generator;

/**
 * IntCode Calculator
 */
class Computer
{
    public const LOCATE_LIMIT = 99;

    public const FEATURE = [
        Feature\Addition::OPCODE       => Feature\Addition::class,
        Feature\Multiplication::OPCODE => Feature\Multiplication::class,
        Feature\Input::OPCODE          => Feature\Input::class,
        Feature\Output::OPCODE         => Feature\Output::class,
        Feature\JumpIfTrue::OPCODE     => Feature\JumpIfTrue::class,
        Feature\JumpIfFalse::OPCODE    => Feature\JumpIfFalse::class,
        Feature\LessThan::OPCODE       => Feature\LessThan::class,
        Feature\Equals::OPCODE         => Feature\Equals::class,
        Feature\RelativeBase::OPCODE   => Feature\RelativeBase::class,
    ];

    private $inputBuffer;

    private $outputBuffer;

    public function __construct(InputInterface $inputBuffer, OutputInterface $outputBuffer)
    {
        $this->inputBuffer = $inputBuffer;
        $this->outputBuffer = $outputBuffer;
    }

    public function locate(array $initialMemory, int $target)
    {
        foreach ($this->iterateNounsAndVerbs() as $nounVerb) {
            $noun = $nounVerb[0];
            $verb = $nounVerb[1];

            $code = $this->compute($initialMemory, $noun, $verb);
            if ($code == $target) {
                return ($noun * 100) + $verb;
            }
        }

        throw new Exception("Unable to Locate {$target}");
    }

    private function iterateNounsAndVerbs()
    {
        $noun = $verb = 0;
        yield [$noun, $verb];

        while (true) {
            $noun++;
            if ($noun >= self::LOCATE_LIMIT) {
                $noun = 0;
                $verb++;
                if ($verb > self::LOCATE_LIMIT) {
                    throw new Exception("No more Nouns and Verbs to Yield");
                }
            }

            yield [$noun, $verb];
        }
    }

    public function compute(array $initialMemory, int $noun = null, int $verb = null): int
    {
        $memory = $this->executeFeatures($initialMemory, $noun, $verb);
//        var_dump($memory);
        return $memory[0];
    }

    private function executeFeatures(array $memory, int $noun = null, int $verb = null): array
    {
        $instructionPointer = 0;
        $nounAddress = 1;
        $verbAddress = 2;
        if (!empty($noun)) {
            $memory[$nounAddress] = $noun;
        }
        if (!empty($verb)) {
            $memory[$verbAddress] = $verb;
        }

        while (isset($memory[$instructionPointer])) {
            $initialParam = $memory[$instructionPointer];
            $opCode = (int) substr((string) $initialParam, -2);
            $parameterModes = substr((string) $initialParam, 0, strlen((string) $initialParam) - 2);
            $parameterModes = $parameterModes ? str_split($parameterModes) : [];
            $parameterModes = array_map('intval', $parameterModes);
            $parameterModes = array_reverse($parameterModes);
//            var_dump($opCode);
//            var_dump($parameterModes);

            // Special OpCode end the program.
            if ($opCode == 99) {
                $instructionPointer++;
                break;
            }

            if (!isset(self::FEATURE[$opCode])) {
                throw new Exception("Unknown OpCode: {$opCode}");
            }
            $class = self::FEATURE[$opCode];
            /** @var AbstractFeature $feature */
            $feature = new $class($this->inputBuffer, $this->outputBuffer);

            $instruction = $feature->getInstruction($opCode, $memory, $instructionPointer);

            $instructionPointer = $feature->act($instruction, $parameterModes, $instructionPointer, $memory);
        }

        return $memory;
    }

    public function stepFeature(array $memory, int $noun = null, int $verb = null): Generator
    {
        $instructionPointer = 0;
        $nounAddress = 1;
        $verbAddress = 2;
        if (!empty($noun)) {
            $memory[$nounAddress] = $noun;
        }
        if (!empty($verb)) {
            $memory[$verbAddress] = $verb;
        }
        //yield;

        while (isset($memory[$instructionPointer])) {
            $initialParam = $memory[$instructionPointer];
            $opCode = (int) substr((string) $initialParam, -2);
            $parameterModes = substr((string) $initialParam, 0, strlen((string) $initialParam) - 2);
            $parameterModes = $parameterModes ? str_split($parameterModes) : [];
            $parameterModes = array_map('intval', $parameterModes);
            $parameterModes = array_reverse($parameterModes);

            // Special OpCode end the program.
            if ($opCode == 99) {
                $instructionPointer++;
                break;
            }

            if (!isset(self::FEATURE[$opCode])) {
                throw new Exception("Unknown OpCode: {$opCode}");
            }
            $class = self::FEATURE[$opCode];
            /** @var AbstractFeature $feature */
            $feature = new $class($this->inputBuffer, $this->outputBuffer);

            $instruction = $feature->getInstruction($opCode, $memory, $instructionPointer);

            $instructionPointer = $feature->act($instruction, $parameterModes, $instructionPointer, $memory);

            yield;
        }
    }
}
