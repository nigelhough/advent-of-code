<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class RelativeBase extends AbstractFeature
{
    public const OPCODE = 9;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
        global $relativeBase;
//        echo "<p>Relative Base</p>";
//        var_dump($relativeBase);
        $increment = $this->getParameter($instruction[1], $parameterModes[0] ?? 0, $memory);
        $relativeBase += $increment;

//        var_dump($instruction);
//        echo "<br/>";
//        var_dump($parameterModes[0] ?? 0);
//        echo "<br/>";
//        var_dump($increment);
//        echo "<br/>";
//        var_dump($relativeBase);
//        echo "<hr/>";

        return $instructionPointer + $this->getSize();
    }

    public function getSize(): int
    {
        return 2;
    }
}
