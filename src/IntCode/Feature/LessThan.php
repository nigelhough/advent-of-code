<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class LessThan extends AbstractFeature
{
    public const OPCODE = 7;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
//        if (isset($parameterModes[2])) {
//            throw new \Exception('Unexpected Parameter Mode');
//        }
        $subject1 = $this->getParameter($instruction[1], $parameterModes[0] ?? 0, $memory);
        $subject2 = $this->getParameter($instruction[2], $parameterModes[1] ?? 0, $memory);
//        $target = $instruction[3];
//        var_dump($instruction);
//        echo "<br/>";
//        var_dump($parameterModes);
//        echo "<br/>";
//        var_dump($memory[$target]);
//        echo "<br/>";
//        var_dump($target);
        $target = $this->getParameterTarget($instruction[3], $parameterModes[2] ?? 0, $memory); //$instruction[3];
//var_dump($target);
//var_dump($parameterModes);
//exit;
        $this->setMemory($target, intval($subject1 < $subject2), $memory);
        return $instructionPointer + $this->getSize();
    }

    public function getSize(): int
    {
        return 4;
    }
}
