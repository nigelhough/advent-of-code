<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class Addition extends AbstractFeature
{
    public const OPCODE = 1;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
//        var_dump($instruction);
//        var_dump($parameterModes);
//        echo "<hr/>";
//        if (isset($parameterModes[2])) {
//            throw new \Exception('Unexpected Parameter Mode');
//        }
        $target = $this->getParameterTarget($instruction[3], $parameterModes[2] ?? 0, $memory); //$instruction[3];
        $subject1 = $this->getParameter($instruction[1], $parameterModes[0] ?? 0, $memory);
        $subject2 = $this->getParameter($instruction[2], $parameterModes[1] ?? 0, $memory);

        $this->setMemory($target, ($subject1 + $subject2), $memory);
        return $instructionPointer + $this->getSize();
    }

    public function getSize(): int
    {
        return 4;
    }
}
