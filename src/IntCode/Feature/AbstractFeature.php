<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

use AdventOfCode\IntCode\FeatureInterface;
use AdventOfCode\IntCode\InputInterface;
use AdventOfCode\IntCode\OutputInterface;
use Exception;
use LogicException;

abstract class AbstractFeature implements FeatureInterface
{
    protected $inputBuffer;

    protected $outputBuffer;

    public function __construct(InputInterface $inputBuffer, OutputInterface $outputBuffer)
    {
        $this->inputBuffer = $inputBuffer;
        $this->outputBuffer = $outputBuffer;
    }

    public function getInstruction(int $opCode, array $memory, int $instructionPointer): array
    {
        if ($opCode !== $this->getOpCode()) {
            throw new LogicException('Can not get Instruction for another opCode');
        }

        $instruction = [];
        for ($i = 0; $i < $this->getSize(); $i++) {
            $instruction[] = $memory[$instructionPointer + $i];
        }

        return $instruction;
    }

    protected function getParameter(int $parameterValue, int $parameterMode, array $memory)
    {
        if ($parameterMode === 1) {
            // Immediate Mode
            return $parameterValue;
        } elseif ($parameterMode === 0) {
            // Position Mode
            if ($parameterValue < 0) {
                throw new Exception("Reading Negative Memory {$parameterValue}");
            }
            return $memory[$this->getParameterTarget($parameterValue, $parameterMode, $memory)];
        } elseif ($parameterMode === 2) {
            // Relative Mode
            global $relativeBase;
            if ($relativeBase + $parameterValue < 0) {
                throw new Exception("Reading Negative Memory {$relativeBase} + {$parameterValue}");
            }
            return $memory[$this->getParameterTarget($parameterValue, $parameterMode, $memory)];
        }

        throw new Exception("Unknown Parameter Mode {$parameterMode}");
    }

    protected function getParameterTarget(int $parameterValue, int $parameterMode, array $memory)
    {
        if ($parameterMode === 1) {
            // Immediate Mode
            return $parameterValue;
        } elseif ($parameterMode === 0) {
            // Position Mode
            if ($parameterValue < 0) {
                throw new Exception("Reading Negative Memory {$parameterValue}");
            }
            return $parameterValue;
        } elseif ($parameterMode === 2) {
            // Relative Mode
            global $relativeBase;
            if ($relativeBase + $parameterValue < 0) {
                throw new Exception("Reading Negative Memory {$relativeBase} + {$parameterValue}");
            }
            return $relativeBase + $parameterValue;
        }

        throw new Exception("Unknown Parameter Mode {$parameterMode}");
    }

    protected function setMemory(int $key, int $value, array &$memory)
    {
        if ($key < 0) {
            throw new Exception("Setting negative memory {$key}");
        }
        $memory[$key] = $value;
    }
}
