<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class JumpIfFalse extends AbstractFeature
{
    public const OPCODE = 6;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
        $value = $this->getParameter($instruction[1], $parameterModes[0] ?? 0, $memory);
        $target = $this->getParameter($instruction[2], $parameterModes[1] ?? 0, $memory);

        if ($value === 0) {
            return $target;
        }

        return $instructionPointer + $this->getSize();
    }

    public function getSize(): int
    {
        return 3;
    }
}
