<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class Input extends AbstractFeature
{
    public const OPCODE = 3;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
        if (isset($parameterModes[0]) && $parameterModes[0] == 1) {
            // Immediate Mode
            exit('in 1');
        } elseif (isset($parameterModes[0]) && $parameterModes[0] == 2) {
            // Relative Mode
            global $relativeBase;
//            var_dump($instruction);
//            echo "<br/>";
//            var_dump($instruction[1]);
//            echo "<br/>";
//            var_dump($relativeBase);
//            echo "<br/>";
//            var_dump($relativeBase + $instruction[1]);
//            echo "<br/>";
//            var_dump($memory[$relativeBase + $instruction[1]]);
//            echo "<br/><br/>";
            ////            var_dump($memory);
            ////            echo "<hr/><hr/>";
//            var_dump($this->inputBuffer->get());

//            var_dump($instruction[1] + $relativeBase);
//            exit('in 2');

            $this->setMemory($instruction[1] + $relativeBase, $this->inputBuffer->get(), $memory);
            return $instructionPointer + $this->getSize();
        } else {
            $this->setMemory($instruction[1], $this->inputBuffer->get(), $memory);
            return $instructionPointer + $this->getSize();
        }
    }

    public function getSize(): int
    {
        return 2;
    }
}
