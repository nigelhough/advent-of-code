<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode\Feature;

class Output extends AbstractFeature
{
    public const OPCODE = 4;

    public function getOpCode(): int
    {
        return self::OPCODE;
    }

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int
    {
        if (isset($parameterModes[0]) && $parameterModes[0] == 1) {
//            var_dump($instruction);
//            exit('out 1');
            // Immediate Mode
            $this->outputBuffer->put($instruction[1]);
            return $instructionPointer + $this->getSize();
        } elseif (isset($parameterModes[0]) && $parameterModes[0] == 2) {
//            exit('out 2');
            // Relative Mode
            global $relativeBase;

//            $this->setMemory($memory[$relativeBase + $instruction[1]], $this->inputBuffer->get(), $memory);
//            return $instructionPointer + $this->getSize();
            $this->outputBuffer->put($memory[(int) $relativeBase + $instruction[1]]);
            return $instructionPointer + $this->getSize();
        } else {
//            exit('out 0');
            $this->outputBuffer->put($memory[$instruction[1]]);
            return $instructionPointer + $this->getSize();
        }

//        $this->outputBuffer->put($memory[$instruction[1]]);
//        return $instructionPointer + $this->getSize();
//
//        $output = $this->getParameter($instruction[1], $parameterModes[0] ?? 0, $memory);
//        $this->outputBuffer->put($output);
//
////        var_dump($instruction);
////        var_dump($parameterModes);
////        var_dump($output);
////        echo "<hr/>";
//        return $instructionPointer + $this->getSize();
    }

    public function getSize(): int
    {
        return 2;
    }
}
