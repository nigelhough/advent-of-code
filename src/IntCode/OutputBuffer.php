<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

class OutputBuffer implements OutputInterface
{
    private $buffer = [];

    public function put(int $value)
    {
        $this->buffer[] = $value;
    }

    public function clear()
    {
        $this->buffer = [];
    }

    public function read(): array
    {
        return $this->buffer;
    }
}
