<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

class InputBuffer implements InputInterface
{
    private $input;

    public function __construct(int $input = null)
    {
        $this->input = $input;
    }

    public function set(int $input)
    {
        $this->input = $input;
    }

    public function get()
    {
        return $this->input;
    }
}
