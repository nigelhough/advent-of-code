<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

use Exception;

class InputOutputBuffer implements InputInterface, OutputInterface
{
    private $input = [];

    private $buffer = [];

    public function __construct(int $input = null)
    {
        if (!empty($input)) {
            $this->input[] = $input;
        }
    }

    public function set(int $input)
    {
        $this->input[] = $input;
    }

    public function get()
    {
        if (!empty($this->input)) {
            return array_shift($this->input);
        }

        throw new Exception('Unexpected No Input to Get');
    }

    public function put(int $value)
    {
        $this->buffer[] = $value;
    }

    public function clear()
    {
        $this->input = [];
        $this->buffer = [];
    }

    public function read(): array
    {
        if (!empty($this->buffer)) {
            return [array_shift($this->buffer)];
        } else {
            return [];
        }
    }
}
