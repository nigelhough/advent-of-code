<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

interface InputInterface
{
    public function set(int $input);

    public function get();
}
