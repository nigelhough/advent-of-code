<?php

declare(strict_types=1);

namespace AdventOfCode\IntCode;

interface FeatureInterface
{
    public function getOpCode(): int;

    public function getSize(): int;

    public function act(array $instruction, array $parameterModes, int $instructionPointer, array &$memory): int;
}
