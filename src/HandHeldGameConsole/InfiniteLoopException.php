<?php

declare(strict_types=1);

namespace AdventOfCode\HandHeldGameConsole;

use Exception;
use Throwable;

class InfiniteLoopException extends Exception
{
    private int $accumulator;

    public function __construct($accumulator, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->accumulator = $accumulator;
        parent::__construct($message, $code, $previous);
    }

    public function getAccumulator(): int
    {
        return $this->accumulator;
    }
}
