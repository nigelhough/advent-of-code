<?php

declare(strict_types=1);

namespace AdventOfCode\HandHeldGameConsole;

class GameConsole
{
    public function boot(array $bootCode): int
    {
        $accumulator = 0;
        $pointer = 0;
        $executedCommands = [];
        while (empty($executedCommands[$pointer])) {
            if (!isset($bootCode[$pointer])) {
                return $accumulator;
            }
            $command = $bootCode[$pointer];
            $executedCommands[$pointer] = true;
            /** $argument int */
            [$instruction, $argument] = $command;

            if ($instruction === 'nop') {
                $pointer++;
            } elseif ($instruction === 'acc') {
                $accumulator += $argument;
                $pointer++;
            } elseif ($instruction === 'jmp') {
                $pointer += $argument;
            }
        }

        throw new InfiniteLoopException($accumulator);
    }
}
