<?php

declare(strict_types=1);

namespace AdventOfCode\LinkedList;

use Countable;
use JsonSerializable;
use LogicException;
use Stringable;

class LinkedList implements Stringable, Countable, JsonSerializable
{
    /** @var array List items. */
    private array $items;

    /** @var mixed The current first item in the list */
    private $firstItem;

    /**
     * Create a linked list.
     *
     * @param mixed ...$items List items.
     */
    public function __construct(...$items)
    {
        $previous = null;
        foreach ($items as $item) {
            if (!is_null($previous)) {
                // Add previous item lined to next.
                $this->items[$previous] = $item;
            } else {
                // Must be the first item.
                $this->firstItem = $item;
            }
            $previous = $item;
        }
        $this->items[$previous] = $this->firstItem;
    }

    public function __toString(): string
    {
        return $this->printDelimited('');
    }

    private function ordered()
    {
        $orderedItems = [];
        $item = $this->firstItem;
        $orderedItems[] = $item;
        while ($this->items[$item] !== $this->firstItem) {
            $item = $this->items[$item];
            $orderedItems[] = $item;
        }

        return $orderedItems;
    }

    public function printDelimited($delimiter = ','): string
    {
        return implode($delimiter, $this->ordered());
    }

    public function jsonSerialize(): array
    {
        return array_values($this->ordered());
    }

    /**
     * Get a dump of the items in the list.
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getItemAfter($precedingItem)
    {
        return $this->items[$precedingItem];
    }

    public function reverseRange(int $index, int $length): void
    {
        if ($length > count($this->items)) {
            throw new LogicException('Can\'t reverse a length longer than the list.');
        }
        if ($length < 2) {
            // No need to reverse 0.
            // 1 reverses to itself.
            return;
        }
        if (count($this->items) === 2) {
            // If we only have 2 items the links stay the same but we change the first item.
            $this->firstItem = $this->items[$this->firstItem];
            return;
        }
        $range = $this->getRange($index, $length);
        if (count($this->items) === $length) {
            // If you are reversing all items, just flip the array.
            $this->items = array_flip($this->items);

            foreach ($range as $index => $item) {
                if ($item === $this->firstItem) {
                    $rangeOpposite = (count($range) - $index) - 1;
                    $this->firstItem = $range[$rangeOpposite];
                    break;
                }
            }
            return;
        }

        $swaps = [];
        $newFirst = null;

        // Point the first item in the range to where the last item pointed.
        $swaps[$range[array_key_first($range)]] = $this->items[$range[array_key_last($range)]];
        if ($range[array_key_first($range)] === $this->firstItem) {
            // If first in range is the first item. Last in range becomes first item.
            $newFirst = $range[array_key_last($range)];
        }
        // Point item before first item to last item.
        $swaps[$this->findPreceding($range[array_key_first($range)])] = $range[array_key_last($range)];

        // Repoint all other items to the preceding item.
        foreach ($range as $index => $item) {
            if (!isset($range[$index - 1])) {
                continue;
            }
            $swapIndex = $index - 1;
            $swaps[$item] = $range[$swapIndex];
            if ($item === $this->firstItem) {
                $rangeOpposite = (count($range) - $index) - 1;
                $newFirst = $range[$rangeOpposite];
            }
        }

        // Perform swaps.
        foreach ($swaps as $item => $next) {
            $this->items[$item] = $next;
        }
        $this->firstItem = $newFirst ?? $this->firstItem;
    }

    public function getRange(int $index, int $length): array
    {
        $items = [];
        $item = $this->getItem($index);
        $items[] = $item;
        for ($i = 1; $i < $length; $i++) {
            $item = $this->items[$item];
            $items[] = $item;
        }
        return $items;
    }

    /**
     * @param int $index
     *
     * @return mixed
     */
    public function getItem(int $index)
    {
        $item = $this->firstItem;
        for ($i = 1; $i < $index; $i++) {
            $item = $this->items[$item];
        }
        return $item;
    }

    /**
     * Find the preceding item in the list to a target item.
     *
     * @return mixed
     */
    public function findPreceding($targetItem)
    {
        $item = $this->firstItem;
        while ($this->items[$item] !== $targetItem) {
            $item = $this->items[$item];
        }
        return $item;
    }

    public function setFirstItem($item): void
    {
        //Check Item exists.
        $this->firstItem = $item;
    }

    public function insertAfterIndex(int $index, $newItem): void
    {
        $index = $index % count($this->items);

        $item = $this->firstItem;
        for ($i = 0; $i < $index; $i++) {
            $item = $this->items[$item];
            //var_dump($item);
        }
        $this->insertAfterItem($item, $newItem);
    }

    public function insertAfterItem($item, $newItem)
    {
        //var_dump($this->items);
        $currentTarget = $this->items[$item];
        $this->items[$item] = $newItem;
        $this->items[$newItem] = $currentTarget;
        //var_dump($this->items);
    }

    /**
     * Spin the list so X items at the end are moved to the start.
     *
     * @param int $noItems Number of items
     */
    public function spin(int $noItems): void
    {
        $newLastIndex = ($this->count() - $noItems);
        $newLastItem = $this->getItem($newLastIndex);

        // Set new first item.
        $this->firstItem = $this->items[$newLastItem];
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function swapPositions(int $sourcePosition, int $targetPosition): void
    {
        if (
            $sourcePosition > $this->count() ||
            $targetPosition > $this->count() ||
            $sourcePosition <= 0 ||
            $targetPosition <= 0
        ) {
            throw new LogicException("Can\'t swap positions out of range. {$sourcePosition}, {$targetPosition}");
        }

        // Find items at indexes.
        $sourceItem = $this->getItem($sourcePosition);
        $targetItem = $this->getItem($targetPosition);

        // Swap Items
        $this->swapItems($sourceItem, $targetItem);
    }

    public function swapItems(string $sourceItem, string $targetItem): void
    {
        // Find item parents.
        $sourceParent = $this->findPreceding($sourceItem);
        $sourceChild = $this->items[$sourceItem];

        $targetParent = $this->findPreceding($targetItem);
        $targetChild = $this->items[$targetItem];

//        echo "\n";
//        var_dump($sourceParent);
//        var_dump($targetItem);
//        echo "\n";
//        var_dump($sourceItem);
//        var_dump($targetChild);
//        echo "\n";
//        var_dump($targetParent);
//        var_dump($sourceItem);
//        echo "\n";
//        var_dump($targetItem);
//        var_dump($sourceChild);
//        echo "\n";

        // Swap Items
        if ($sourceParent !== $targetItem) {
            $this->items[$sourceParent] = $targetItem;
            $this->items[$sourceItem] = $targetChild;
        } else {
            $this->items[$sourceItem] = $targetItem;
        }

        if ($targetParent !== $sourceItem) {
            $this->items[$targetParent] = $sourceItem;
            $this->items[$targetItem] = $sourceChild;
        } else {
            $this->items[$targetItem] = $sourceItem;
        }

        if ($this->firstItem === $sourceItem) {
            $this->firstItem = $targetItem;
        } elseif ($this->firstItem === $targetItem) {
            $this->firstItem = $sourceItem;
        }
//        var_dump($this->items);
//        var_dump($this->firstItem);
    }
}
