<?php

declare(strict_types=1);

namespace AdventOfCode\Input;

use DateTime;
use DateTimeZone;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Input Middleware.
 */
class Middleware
{
    public function __construct(private RepositoryInterface $inputRepository)
    {
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {
        $day = (int) $request->getAttribute('__route__')->getArgument('day');
        $year = (int) $request->getAttribute('__route__')->getArgument('year');

        // Check if it's time.
        // This could be it's own middleware.
        $now = new DateTime('now', new DateTimeZone('America/New_York'));
        $challengeDay = (new DateTime('now', new DateTimeZone('America/New_York')))->setDate($year, 12, $day)->setTime(
            0,
            0
        );
        if ($now < $challengeDay) {
            throw new Exception('Too Early!!!');
        }

        return $handler->handle($request->withAttribute('input', $this->inputRepository->get($day, $year)));
    }
}
