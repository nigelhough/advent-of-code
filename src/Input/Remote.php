<?php

declare(strict_types=1);

namespace AdventOfCode\Input;

use AdventOfCode\Client\ClientInterface;

/**
 * Fetch an input from a remote source.
 */
class Remote implements RepositoryInterface
{
    public function __construct(private ClientInterface $client)
    {
    }

    public function get(int $day, int $year): string
    {
        return rtrim(
            (string) $this->client->get("https://adventofcode.com/{$year}/day/{$day}/input")->getBody(),
            "\n"
        );
    }
}
