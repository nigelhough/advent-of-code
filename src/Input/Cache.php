<?php

declare(strict_types=1);

namespace AdventOfCode\Input;

/**
 * Fetch an input from a cache.
 */
class Cache implements RepositoryInterface
{
    public function __construct(private string $cacheDir, private RepositoryInterface $fallback)
    {
    }

    public function get(int $day, int $year): string
    {
        $cacheDir = "{$this->cacheDir}/$year";
        $cacheFile = "{$cacheDir}/$day";
        if (file_exists($cacheFile)) {
            return file_get_contents($cacheFile);
        }
        $input = $this->fallback->get($day, $year);

        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }
        if (!file_exists($cacheFile)) {
            touch($cacheFile);
        }
        file_put_contents($cacheFile, $input);

        return $input;
    }
}
