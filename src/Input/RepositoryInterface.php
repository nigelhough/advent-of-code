<?php

namespace AdventOfCode\Input;

/**
 * Describes an Input Repository.
 */
interface RepositoryInterface
{
    /**
     * Get the input for a day.
     *
     * @param int $day
     * @param int $year
     *
     * @return string
     */
    public function get(int $day, int $year): string;
}
