<?php

declare(strict_types=1);

namespace AdventOfCode\Input;

/**
 * A function library for parsing inputs.
 */
class Parser
{
    /**
     * Iterate an input by line.
     *
     * @param string $input
     *
     * @return string[]
     */
    public static function iterateByLine(string $input, bool $trim = true): array
    {
        return $trim ? explode("\n", trim($input)) : explode("\n", $input);
    }

    /**
     * Iterate an input of numbers by line.
     *
     * @param string $input
     *
     * @return int[]
     */
    public static function iterateNumbersByLine(string $input, bool $trim = true): array
    {
        return array_map('intval', $trim ? explode("\n", trim($input)) : explode("\n", $input));
    }

    public static function iterateNumbersByTab(string $input, bool $trim = true): array
    {
        return array_map('intval', $trim ? explode("\t", trim($input)) : explode("\t", $input));
    }

    /**
     * Iterate an input by group.
     * Groups of input are separated by blank lines.
     *
     * @param string $input
     *
     * @return string[]
     */
    public static function iterateByGroup(string $input): array
    {
        return explode("\n\n", trim($input));
    }
}
