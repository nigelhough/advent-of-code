<?php

declare(strict_types=1);

namespace AdventOfCode\FuelCounterUpper;

class Counter
{
    private $fuelCalculator;

    private $recursiveFuelCalculator;

    public function __construct()
    {
        $this->fuelCalculator = new FuelCalculator();
        $this->recursiveFuelCalculator = new RecursiveFuelCalculator($this->fuelCalculator);
    }

    public function count(array $moduleMassCollection): int
    {
        $totalFuel = 0;
        foreach ($moduleMassCollection as $mass) {
            $totalFuel += $this->fuelCalculator->calculate($mass);
        }

        return (int) $totalFuel;
    }

    public function recursiveCount(array $moduleMassCollection): int
    {
        $totalFuel = 0;
        foreach ($moduleMassCollection as $mass) {
            $totalFuel += $this->recursiveFuelCalculator->calculate($mass);
        }

        return (int) $totalFuel;
    }
}
