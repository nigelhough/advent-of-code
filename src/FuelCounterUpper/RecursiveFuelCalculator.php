<?php

declare(strict_types=1);

namespace AdventOfCode\FuelCounterUpper;

class RecursiveFuelCalculator
{
    private $fuelCalculator;

    public function __construct(FuelCalculator $fuelCalculator)
    {
        $this->fuelCalculator = $fuelCalculator;
    }

    public function calculate(int $mass): int
    {
        $totalMass = 0;
        while ($mass = $this->fuelCalculator->calculate($mass)) {
            if ($mass <= 0) {
                break;
            }
            $totalMass += $mass;
        }
        return (int) $totalMass;
    }
}
