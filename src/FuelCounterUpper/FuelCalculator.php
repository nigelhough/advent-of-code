<?php

declare(strict_types=1);

namespace AdventOfCode\FuelCounterUpper;

class FuelCalculator
{
    public const DIVIDER = 3;

    public const SUBTRACTOR = 2;

    public function calculate(int $mass): int
    {
        return (int) floor($mass / self::DIVIDER) - self::SUBTRACTOR;
    }
}
