<?php

declare(strict_types=1);

namespace AdventOfCode\MonkeyInTheMiddle;

class Monkey
{
    public readonly self $trueMonkey;
    public readonly self $falseMonkey;

    public function __construct(
        public readonly int $id,
        public readonly string $operation,
        public readonly int $test
    ) {
    }

    public function operate(int $worryLevel)
    {
        $operator = substr($this->operation, 0, 1);
        if (substr($this->operation, 1) === 'old') {
            $value = $worryLevel;
        } else {
            $value = substr($this->operation, 1);
        }

        if ($operator === "*") {
            return ($worryLevel * $value);
        }
        if ($operator === "+") {
            return (int) ($worryLevel + $value);
        }
        throw new \Exception();
    }

    public function test(int $worryLevel): Monkey
    {
        if ($worryLevel % $this->test == 0) {
            return $this->trueMonkey;
        }
        return $this->falseMonkey;
    }

    public function setTrueMonkey(Monkey $trueMonkey): void
    {
        $this->trueMonkey = $trueMonkey;
    }

    public function setFalseMonkey(Monkey $falseMonkey): void
    {
        $this->falseMonkey = $falseMonkey;
    }
}
