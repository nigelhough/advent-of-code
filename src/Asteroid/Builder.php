<?php

declare(strict_types=1);

namespace AdventOfCode\Asteroid;

class Builder
{
    public const ASTEROID_MARKER = '#';

    /**
     * @param string $asteroidString
     *
     * @return Point[]
     */
    public static function build(string $asteroidString): array
    {
        $asteroids = [];
        $lines = explode("\n", $asteroidString);
        foreach ($lines as $y => $line) {
            $chars = str_split($line);
            foreach ($chars as $x => $char) {
                if ($char === self::ASTEROID_MARKER) {
                    $asteroids[] = new Point($x, $y);
                }
            }
        }

        return $asteroids;
    }
}
