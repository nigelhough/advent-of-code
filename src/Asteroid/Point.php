<?php

declare(strict_types=1);

namespace AdventOfCode\Asteroid;

class Point
{
    private $xCoordinate;

    private $yCoordinate;

    public function __construct(int $xCoordinate, int $yCoordinate)
    {
        $this->xCoordinate = $xCoordinate;
        $this->yCoordinate = $yCoordinate;
    }

    public function getVectorsFromPoint(array $asteroids, array &$vectors): array
    {
        $keys = [];
        foreach ($asteroids as $asteroid) {
            // Don't try to get an angle to yourself.
            if ($this->isSame($asteroid)) {
                continue;
            }

            // Get the angle, adjust a bit to get 360 from North of the coordinate.
            $angle = (rad2deg(
                atan2($asteroid->getY() - $this->yCoordinate, $asteroid->getX() - $this->xCoordinate)
            ) + 90);
            if ($angle < 0) {
                $angle += 360;
            }

            $vectors[(string) $angle][] = $asteroid;
            $keys[] = $angle;
        }

        // Sort Keys to be unique and ordered.
        $keys = array_unique($keys);
        sort($keys);
        return $keys;
    }

    public function isSame(self $point): bool
    {
        return ($this->xCoordinate === $point->xCoordinate) && ($this->yCoordinate === $point->yCoordinate);
    }

    public function isPointBetweenPoints(Point $source, Point $target): bool
    {
        $dxc = $this->xCoordinate - $source->getX();
        $dyc = $this->yCoordinate - $source->getY();

        $dxl = $target->getX() - $source->getX();
        $dyl = $target->getY() - $source->getY();

        $cross = $dxc * $dyl - $dyc * $dxl;

        if ($cross != 0) {
            return false;
        }

        if (abs($dxl) >= abs($dyl)) {
            return $dxl > 0
                ?
                $source->getX() <= $this->xCoordinate && $this->xCoordinate <= $target->getX()
                :
                $target->getX() <= $this->xCoordinate && $this->xCoordinate <= $source->getX();
        } else {
            return $dyl > 0
                ?
                $source->getY() <= $this->yCoordinate && $this->yCoordinate <= $target->getY()
                :
                $target->getY() <= $this->yCoordinate && $this->yCoordinate <= $source->getY();
        }
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->xCoordinate;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->yCoordinate;
    }

    public function distance(self $point): float
    {
        $x = (pow($point->xCoordinate, 2) - pow($this->xCoordinate, 2));
        $y = (pow($point->yCoordinate, 2) - pow($this->yCoordinate, 2));

        return abs(sqrt($x + $y));
    }

    public function __toString()
    {
        return $this->xCoordinate . "," . $this->yCoordinate;
    }
}
