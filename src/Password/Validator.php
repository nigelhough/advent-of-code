<?php

declare(strict_types=1);

namespace AdventOfCode\Password;

class Validator
{
    public const PREVENT_ALL = 3;

    public const PREVENT_DECREASING = 1;

    public const PREVENT_STREAK = 2;

    private $rules;

    public function __construct(int $rules = self::PREVENT_ALL)
    {
        $this->rules = $rules;
    }

    public function isValid(int $number): bool
    {
        $previousCharacter = null;
        $potentialDouble = false;
        $hasDouble = false;
        $streak = 0;
        foreach (str_split((string) $number) as $character) {
            $character = (int) $character;
            if (!is_null($previousCharacter) && $character < $previousCharacter) {
                // Decreasing
                if ($this->rules & self::PREVENT_DECREASING) {
                    return false;
                }
            }
            if ($previousCharacter === $character) {
                if ($potentialDouble) {
                    $streak++;
                }
                if ($this->rules & self::PREVENT_STREAK) {
                    $potentialDouble = true;
                } else {
                    $hasDouble = true;
                }
            } elseif ($potentialDouble) {
                if ($streak == 0) {
                    $hasDouble = true;
                }
                if ($this->rules & self::PREVENT_STREAK) {
                    $potentialDouble = false;
                }
                $streak = 0;
            }
            $previousCharacter = $character;
        }

        if ($potentialDouble && $streak == 0) {
            $hasDouble = true;
        }

        return $hasDouble;
    }
}
