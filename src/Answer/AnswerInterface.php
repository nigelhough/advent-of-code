<?php

declare(strict_types=1);

namespace AdventOfCode\Answer;

interface AnswerInterface
{
    public function getPart1Answer(): ?string;

    public function getPart2Answer(): ?string;
}
