<?php

declare(strict_types=1);

namespace AdventOfCode\Answer;

/**
 * An Answer Factory.
 */
class Factory
{
    /**
     * Create an Answer from mixed input.
     * Designed to convert answers from any format to a string that is accepted by AoC website.
     *
     * @param ?mixed $answer1 An answer for part1.
     * @param ?mixed $answer2 An answer for part2.
     * @param bool $isDebug Some Solutions use a debug to test sample inputs. We never want to answer with those inputs.
     *
     * @return AnswerInterface
     */
    public function create(mixed $answer1, mixed $answer2, bool $isDebug = false): AnswerInterface
    {
        if ($isDebug) {
            return new Answer(null, null);
        }
        return new Answer(
            $this->parseAnswer($answer1),
            $this->parseAnswer($answer2),
        );
    }

    /**
     * Parse an answer from any format to a string.
     *
     * @param ?mixed $answer An answer.
     *
     * @return string
     */
    private function parseAnswer(mixed $answer): ?string
    {
        if ($answer === null) {
            return null;
        }
        if (is_array($answer)) {
            return implode(",", $answer);
        }
        $strAnswer = trim((string) $answer);
        if ($strAnswer === '' || $strAnswer === '0') {
            return null;
        }
        return $strAnswer;
    }
}
