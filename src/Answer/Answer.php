<?php

declare(strict_types=1);

namespace AdventOfCode\Answer;

class Answer implements AnswerInterface
{
    public function __construct(public readonly ?string $part1Answer, public readonly ?string $part2Answer)
    {
    }

    public function getPart1Answer(): ?string
    {
        return $this->part1Answer;
    }

    public function getPart2Answer(): ?string
    {
        return $this->part2Answer;
    }
}
