<?php

declare(strict_types=1);

namespace AdventOfCode\Answer\Processor;

/**
 * A stack of Processor Results.
 */
class ResultStack implements ResultStackInterface, \Countable
{
    /** @var string[] A stack of responses. */
    private array $stack = [];

    public function __construct(?string $result = null)
    {
        if ($result !== null) {
            $this->append($result);
        }
    }

    public function append(string $result)
    {
        $this->stack[] = $result;
    }

    public function getLatest(): string
    {
        if (count($this->stack) === 0) {
            return '';
        }
        return $this->stack[array_key_last($this->stack)];
    }

    public function count(): int
    {
        return count($this->stack);
    }
}
