<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\ProcessorInterface;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Client\ClientInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SessionCookieJar;

/**
 * A remote processor. Interacts with the remote AdventOfCode website.
 */
class Remote implements ProcessorInterface
{
    public function __construct(private ClientInterface $client)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStack
    {
        // Nothing to do.
        if ($answer->getPart1Answer() === null) {
            return new ResultStack();
        }

        // We have part2. We can submit that.
        if ($answer->getPart2Answer() !== null) {
            // We know this is all we will be submitting
            return new ResultStack($this->parseResponse($this->submitPart($year, $day, 2, $answer->getPart2Answer())));
        }

        // We can submit part2.
        return new ResultStack($this->parseResponse($this->submitPart($year, $day, 1, $answer->getPart1Answer())));
    }

    private function parseResponse(string $response): string
    {
        if (str_contains($response, "<article>") && str_contains($response, "</article>")) {
            $start = strpos($response, "<article>");
            $end = strpos($response, "</article>", $start);
            return strip_tags(substr($response, $start, ($end - $start)));
        }
        if (str_contains($response, "<main>") && str_contains($response, "</main>")) {
            $start = strpos($response, "<main>");
            $end = strpos($response, "</main>", $start);
            return strip_tags(substr($response, $start, ($end - $start)));
        }

        return $response;
    }

    private function submitPart(int $year, int $day, int $part, string $answer): string
    {
        return $this->client->post(
            "https://adventofcode.com/{$year}/day/{$day}/answer",
            [
                'level'  => $part,
                'answer' => $answer,
            ],
        )->getBody();
    }
}
