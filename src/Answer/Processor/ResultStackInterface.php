<?php

declare(strict_types=1);

namespace AdventOfCode\Answer\Processor;

/**
 * A stack of responses from processing a solution answer.
 * @todo Consider some sort of result code, enum etc instead of string responses.
 */
interface ResultStackInterface
{
    public function append(string $result);

    public function getLatest(): string;
}
