<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\ProcessorInterface;
use AdventOfCode\Answer\AnswerInterface;

/**
 * A cache processor.
 * It will not process the same answer more than once.
 */
class Cache implements ProcessorInterface
{
    public function __construct(private string $cacheDir, private ProcessorInterface $processor)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStackInterface
    {
        // Nothing to do.
        if ($answer->getPart1Answer() === null) {
            return new ResultStack();
        }

        $cacheDir = "{$this->cacheDir}/$year/results/$day";

        // We have part2 answer. We can submit that.
        if ($answer->getPart2Answer() !== null) {
            /**
             * @todo create a Cache domain and a FileCache to remove duplication
             * See Also
             * \AdventOfCode\Input\Cache
             * \AdventOfCode\LeaderBoard\RateLimited
             * \AdventOfCode\LeaderBoard\Cache
             * \AdventOfCode\Answer\Processor\Cache
             * \AdventOfCode\Answer\Processor\HintAware
             */
            $cacheDir .= "/part2";
            $cacheFile = "{$cacheDir}/" . md5($answer->getPart2Answer());
            if (file_exists($cacheFile)) {
                // Nothing to do this answer has already been submitted.
                return new ResultStack(file_get_contents($cacheFile));
            }
            $response = $this->processor->process($answer, $year, $day);
            // Don't Cache if the response was cooldown timeout. We want to resubmit.
            if (str_contains($response->getLatest(), 'You gave an answer too recently;')) {
                return $response;
            }
            // Don't cache the cool-down part of the response, but return it, this will only be in effect once.
            $cachedResponse = str_replace('before trying again', '', $response->getLatest());

            // Store submitted answer in file cache.
            if (!is_dir($cacheDir)) {
                mkdir($cacheDir, 0777, true);
            }
            touch($cacheFile);
            file_put_contents($cacheFile, $cachedResponse);

            // We know this is all we will be submitting
            return $response;
        }

        $cacheDir .= "/part1";
        $cacheFile = "{$cacheDir}/" . md5($answer->getPart1Answer());
        if (file_exists($cacheFile)) {
            // Nothing to do this answer has already been submitted.
            return new ResultStack(file_get_contents($cacheFile));
        }
        $response = $this->processor->process($answer, $year, $day);
        // Don't Cache if the response was cool down timeout. We want to resubmit.
        if (str_contains($response->getLatest(), 'You gave an answer too recently;')) {
            return $response;
        }
        // Don't cache the cool-down part of the response, but return it, this will only be in effect once.
        $cachedResponse = str_replace('before trying again', '', $response->getLatest());

        // Store submitted answer in file cache.
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }
        touch($cacheFile);
        file_put_contents($cacheFile, $cachedResponse);

        return $response;
    }
}
