<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\ProcessorInterface;
use AdventOfCode\Answer\AnswerInterface;

/**
 * A null aware processor that will stop processing an answer with no parts completed.
 */
class NullAware implements ProcessorInterface
{
    public function __construct(private ProcessorInterface $processor)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStackInterface
    {
        // If part1 is null it doesn't matter what part2 is. You must do them in order.
        if ($answer->getPart1Answer() === null || $answer->getPart1Answer() === '') {
            return new ResultStack();
        }

        return $this->processor->process($answer, $year, $day);
    }
}
