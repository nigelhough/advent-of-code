<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Answer\ProcessorInterface;
use DateInterval;
use DateTimeImmutable;
use Exception;

/**
 * A cool-down processor.
 * It will read cool-down responses and not allow resubmission until the time has elapsed.
 */
class Cooldown implements ProcessorInterface
{
    public function __construct(private string $cacheDir, private ProcessorInterface $processor)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStackInterface
    {
        // Nothing to do.
        if ($answer->getPart1Answer() === null) {
            return new ResultStack();
        }

        $cacheDir = "{$this->cacheDir}/$year/results/$day";
        $cacheFile = "{$cacheDir}/cooldown";

        // Check for a cached cool-down.
        if (file_exists($cacheFile)) {
            $nextValidTime = new DateTimeImmutable(file_get_contents($cacheFile));
            $now = new DateTimeImmutable();
            if ($nextValidTime > $now) {
                $diff = $nextValidTime->diff($now);
                $numSeconds = $diff->s + ($diff->i * 60);
                return new ResultStack(
                    "You gave an answer too recently; " .
                    "You are in a cool-down and can't resubmit for {$numSeconds} seconds."
                );
            }
            // Cool-down no longer valid.
            unlink($cacheFile);
        }

        // No cached timeout, so continue to process the answer.
        // The cool-down response is irrespective of part.
        $response = $this->processor->process($answer, $year, $day);

        // If the response is a cool-down then cache the next valid time.
        if (str_contains($response->getLatest(), 'You gave an answer too recently;')) {
            if (!is_dir($cacheDir)) {
                mkdir($cacheDir, 0600, true);
            }
            touch($cacheFile);
            $nextValidTime = $this->parseCooldown($response->getLatest());
            file_put_contents($cacheFile, $nextValidTime->format('c'));
        }
        // If the response is incorrect but asks to wait before trying again.
        if (str_contains($response->getLatest(), 'before trying again')) {
            if (!is_dir($cacheDir)) {
                mkdir($cacheDir, 0600, true);
            }
            touch($cacheFile);
            $nextValidTime = $this->parseIncorrectResponse($response->getLatest());
            file_put_contents($cacheFile, $nextValidTime->format('c'));
        }

        return $response;
    }

    /**
     * Parse the cool-down response message returning the next valid submission time.
     * Assume the message was returned now and interval calculated accordingly.
     *
     * @param string $cooldownMessage Response message containing cool-down details.
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    private function parseCooldown(string $cooldownMessage): DateTimeImmutable
    {
        $start = strpos($cooldownMessage, 'You have') + strlen('You have');
        $end = strpos($cooldownMessage, 'left to wait');
        $cooldown = trim(substr($cooldownMessage, $start, $end - $start));
        $cooldown = str_replace(' ', '', $cooldown);

        $interval = new DateInterval('PT' . str_replace(' ', '', strtoupper($cooldown)));
        $now = new DateTimeImmutable();
        return $now->add($interval);
    }

    /**
     * Parse a response message that has a Please wait before trying again message.
     * Assume the message was returned now and interval calculated accordingly.
     *
     * @param string $response Response message containing cool-down details.
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    private function parseIncorrectResponse(string $response): DateTimeImmutable
    {
        $start = strpos($response, 'lease wait') + strlen('lease wait');
        $end = strpos($response, 'before trying again');
        $cooldown = trim(substr($response, $start, $end - $start));
        $cooldown = str_replace(' ', '', $cooldown);
        $cooldown = str_replace(['minutes', 'minute', 'seconds', 'second'], ['M', 'M', 'S', 'S'], $cooldown);
        $cooldown = str_replace(['one', 'two', 'three'], ['1', '2', '3'], $cooldown);

        $interval = new DateInterval('PT' . strtoupper($cooldown));
        $now = new DateTimeImmutable();
        return $now->add($interval);
    }
}
