<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\ProcessorInterface;
use AdventOfCode\Answer\AnswerInterface;

/**
 * A super global aware processor that will stop processing answers where input is overridden by a super global.
 * So super globals are bad practice. GET and POST parameters should be read from a request object.
 * A sample input should be flagged and ingested another way, perhaps an additional method on the controller?
 * We are where we are, many Solutions have a super global $GET['debug'],$GET['sample'] that overrides the input.
 * In these cases we never want the answers to be processed, as they aren't the real answers.
 * This is nothing more than a safety mechanism.
 * If you don't like it and don't use super globals then don't include this in the DI stack of ProcessorInterface.
 */
class SuperGlobalAware implements ProcessorInterface
{
    public function __construct(private ProcessorInterface $processor)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStackInterface
    {
        // If the input is overridden by a super global these aren't the real answers.
        if (isset($_GET['debug']) || isset($_GET['debug2']) || isset($_GET['sample'])) {
            return new ResultStack('These are not the input data your looking for');
        }

        return $this->processor->process($answer, $year, $day);
    }
}
