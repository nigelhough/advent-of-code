<?php

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\LeaderBoard\Individual\IndividualLeaderboardRepository;
use AdventOfCode\Answer\ProcessorInterface;
use AdventOfCode\Answer\AnswerInterface;

/**
 * A leaderboard aware processor.
 * It won't process answer if the leaderboard already has a result.
 * Assumed knowledge that part1 and part2 are submitted in order and can't be submitted together.
 */
class LeaderboardAware implements ProcessorInterface
{
    public function __construct(
        private IndividualLeaderboardRepository $userLeaderboard,
        private ProcessorInterface $processor,
        private int $leaderboardId
    ) {
    }

    public function process(AnswerInterface $answer, int $year, int $day): ResultStackInterface
    {
        // If there isn't a part1 answer there is nothing to do.
        if ($answer->getPart1Answer() === null) {
            return new ResultStack();
        }

        $individualLeaderboard = $this->userLeaderboard->get($year, $this->leaderboardId);
        if (!$individualLeaderboard->hasCompletedPart1($day)) {
            if ($answer->getPart2Answer() !== null) {
                return new ResultStack("You can't have answer for part2, part1 has not been completed.");
            }
            // Submit Part1.
            $response = $this->processor->process($answer, $year, $day);
            if (str_contains($response->getLatest(), 'That\'s the right answer!')) {
                // Invalidate Leaderboard Cache.
                $this->userLeaderboard->get($year, $this->leaderboardId, true);
            }
            // We can stop here because we know that if part one isn't submitted we can't have a answer for part2.
            return $response;
        }

        // If there isn't a part2 answer there is nothing further to do.
        if ($answer->getPart2Answer() === null) {
            return new ResultStack("You have completed part 1!");
        }
        if ($individualLeaderboard->hasCompletedPart2($day)) {
            return new ResultStack("You have completed both parts!");
        }
        // Submit Part2.
        $response = $this->processor->process($answer, $year, $day);
        if (str_contains($response->getLatest(), 'That\'s the right answer!')) {
            // Invalidate Leaderboard Cache.
            $this->userLeaderboard->get($year, $this->leaderboardId, true);
        }
        return $response;
    }
}
