<?php

declare(strict_types=1);

namespace AdventOfCode\Answer\Processor;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Answer\Processor;
use AdventOfCode\Answer\ProcessorInterface;

/**
 * A hint aware processor.
 * A hint aware processor.
 * It will read hints from responses and not allow resubmission if answer doesn't adhere to hint.
 */
class HintAware implements ProcessorInterface
{
    public function __construct(private string $cacheDir, private ProcessorInterface $processor)
    {
    }

    public function process(AnswerInterface $answer, int $year, int $day): Processor\ResultStackInterface
    {
        // Nothing to do.
        if ($answer->getPart1Answer() === null) {
            return new ResultStack();
        }

        $cacheDir = "{$this->cacheDir}/$year/results/$day";
        $cacheFile = "{$cacheDir}/hints";

        $hints = [];
        if (file_exists($cacheFile)) {
            $hints = require $cacheFile;
        }

        // Check Part.
        $part = $answer->getPart2Answer() ? 'part2' : 'part1';
        $answerValue = $answer->getPart2Answer() ?? $answer->getPart1Answer();

        // Check against Hints. Don't submit an answer if it breaks a previously obtained hint.
        $brokenHints = '';
        foreach (($hints[$part] ?? []) as $hintType => $hint) {
            if ($hintType == 'greaterThan' && $answerValue < max($hint)) {
                $brokenHints = "Too low! correct answer is greater than " . number_format((float) max($hint));
            }
            if ($hintType == 'lessThan' && $answerValue > min($hint)) {
                $brokenHints = "Too high! correct answer is less than " . number_format((float) min($hint));
            }
        }

        if ($brokenHints != '') {
            return new ResultStack("You are not listening to your hints;\n" . $brokenHints);
        }

        // Pass on to process.
        $response = $this->processor->process($answer, $year, $day);

        // Save any hints.
        if (str_contains($response->getLatest(), 'your answer is too ')) {
            $hintKey = str_contains($response->getLatest(), 'your answer is too low') ? 'greaterThan' : 'lessThan';
            if (!isset($hints[$part][$hintKey])) {
                $hints[$part][$hintKey] = [];
            }
            if (!in_array($answerValue, $hints[$part][$hintKey])) {
                $hints[$part][$hintKey][] = $answerValue;
            }

            // Save Hints.
            if (!is_dir($cacheDir)) {
                mkdir($cacheDir, 0600, true);
            }
            if (!file_exists($cacheFile)) {
                touch($cacheFile);
            }

            file_put_contents(
                $cacheFile,
                "<?php\nreturn " . var_export($hints, true) . ";\n"
            );
        }

        return $response;
    }
}
