<?php

namespace AdventOfCode\Answer;

use AdventOfCode\Answer\Processor;

/**
 * Describes a Processor.
 *
 * @todo Hint Aware Processor.
 */
interface ProcessorInterface
{
    public function process(AnswerInterface $answer, int $year, int $day): Processor\ResultStackInterface;
}
