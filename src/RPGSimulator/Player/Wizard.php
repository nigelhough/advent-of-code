<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\Player;

use AdventOfCode\RPGSimulator\Action\Spell;
use AdventOfCode\RPGSimulator\ActionInterface;
use AdventOfCode\RPGSimulator\Effect\Magical;
use AdventOfCode\RPGSimulator\EffectInterface;
use AdventOfCode\RPGSimulator\PlayerInterface;
use LogicException;

/**
 * AN RPG Simulator Wizard.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
class Wizard implements PlayerInterface
{
    private $actions = [];

    public function __construct(private Player $player, private int $mana)
    {
        $this->actions = [
            new Spell('Magic Missile', 53, 4),
            new Spell('Drain', 73, 2, hitPoints: 2),
            new Spell('Shield', 113, effect: new Magical('Shield', 6, armourIncrease: 7)),
            new Spell('Poison', 173, effect: new Magical('Poison', 6, damageDealt: 3)),
            new Spell('Recharge', 229, effect: new Magical('Recharge', 5, manaIncrease: 101)),
        ];
    }

    public function applyEffect(Magical $effect): self
    {
        $player = clone $this->player;
        $player->setArmour($player->getArmour() + $effect->getArmourIncrease());

        return new self(
            $player,
            $this->mana + $effect->getManaIncrease()
        );
    }

    public function takeEffect(EffectInterface $effect)
    {
        $this->player->takeEffect($effect);
    }

    public function takeAction(Spell $spell): self
    {
        return new self(
            $this->player->takeAction($spell),
            $this->mana - $spell->getManaCost()
        );
    }

    public function applyAction(ActionInterface $action): self
    {
        return new self(
            $this->player->applyAction($action),
            $this->mana
        );
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->player->getDamage();
    }

    /**
     * @return int
     */
    public function getArmour(): int
    {
        return $this->player->getArmour();
    }

    /**
     * @param int $armour
     */
    public function setArmour(int $armour): void
    {
        $this->player->setArmour($armour);
    }

    /**
     * @return int
     */
    public function getHitPoints(): int
    {
        return $this->player->getHitPoints();
    }

    /**
     * @param int $hitPoints
     */
    public function setHitPoints(int $hitPoints): void
    {
        $this->player->setHitPoints($hitPoints);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->player->getName();
    }

    /**
     * @return int
     */
    public function getMana(): int
    {
        return $this->mana;
    }

    /**
     * @return Spell[]
     */
    public function getAvailableActions(): array
    {
        return array_filter(
            $this->actions,
            function (Spell $action) {
                return $action->getManaCost() <= $this->mana;
            }
        );
    }

    public function getTurnsForVictory(PlayerInterface $opponent): int
    {
        throw new LogicException('Wizards can\'t perform a standard Battle');
    }

    public function __toString(): string
    {
        return "{$this->player->getName()}: " .
            "Health:{$this->player->getHitPoints()}, " .
            "Damage:{$this->player->getDamage()}, " .
            "Armor:{$this->player->getArmour()}, " .
            "Mana:{$this->mana}";
    }
}
