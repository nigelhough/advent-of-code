<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\Player;

use AdventOfCode\RPGSimulator\Action\Normal;
use AdventOfCode\RPGSimulator\ActionInterface;
use AdventOfCode\RPGSimulator\EffectInterface;
use AdventOfCode\RPGSimulator\PlayerInterface;

/**
 * AN RPG Simulator Player.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
class Player implements PlayerInterface
{
    private array $actions = [];

    public function __construct(private string $name, private int $hitPoints, private int $damage, private int $armour)
    {
        $this->actions = [
            new Normal('Attacks', $this->getDamage(), 0, 0),
        ];
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->damage;
    }

    /**
     * Construct a player from a string.
     *
     * @param string $player
     */
    public static function fromString(string $name, string $player): self
    {
        $hitPoints = 100;
        $damage = 0;
        $armour = 0;
        foreach (explode("\n", $player) as $playerInfo) {
            [$attribute, $value] = explode(':', $playerInfo);
            if ($attribute === 'Hit Points') {
                $hitPoints = (int) $value;
            } elseif ($attribute === 'Damage') {
                $damage = (int) $value;
            } elseif ($attribute === 'Armor') {
                $armour = (int) $value;
            }
        }
        return new self($name, $hitPoints, $damage, $armour);
    }

    public function takeAction(ActionInterface $action): self
    {
        return new self(
            $this->name,
            $this->hitPoints + $action->getHitPoints(),
            $this->damage,
            $this->armour
        );
    }

    public function applyAction(ActionInterface $action): self
    {
        $hitPoints = get_class($action) === Normal::class
            ? max(1, $this->hitPoints - ($action->getDamage() - $this->armour))
            : $this->hitPoints - $action->getDamage();

        return new self(
            $this->name,
            $hitPoints,
            $this->damage,
            $this->armour
        );
    }

    public function takeEffect(EffectInterface $effect)
    {
        return new self(
            $this->name,
            $this->hitPoints - $effect->getDamageDealt(),
            $this->damage,
            $this->armour
        );
    }

    public function getTurnsForVictory(PlayerInterface $opponent): int
    {
        $damageDealt1 = max(1, $this->getDamage() - $opponent->getArmour());
        return (int) ceil($opponent->getHitPoints() / $damageDealt1);
    }

    /**
     * @return int
     */
    public function getArmour(): int
    {
        return $this->armour;
    }

    /**
     * @return int
     */
    public function getHitPoints(): int
    {
        return $this->hitPoints;
    }

    /**
     * @param int $armour
     */
    public function setArmour(int $armour): void
    {
        $this->armour = $armour;
    }

    /**
     * @param int $hitPoints
     */
    public function setHitPoints(int $hitPoints): void
    {
        $this->hitPoints = $hitPoints;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getAvailableActions(): array
    {
        return $this->actions;
    }

    public function __toString(): string
    {
        return "{$this->name}: Health:{$this->hitPoints}, Damage:{$this->damage}, Armor:{$this->armour}";
    }
}
