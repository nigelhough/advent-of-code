<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\Action;

use AdventOfCode\RPGSimulator\ActionInterface;
use AdventOfCode\RPGSimulator\EffectInterface;

/**
 * A Normal Action.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
class Normal implements ActionInterface
{
    /**
     * Action constructor.
     */
    public function __construct(private string $name, private int $damage, private int $armour, private int $hitPoints)
    {
    }

    public function getDamage(): int
    {
        return $this->damage;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getHitPoints(): int
    {
        return $this->hitPoints;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEffect(): ?EffectInterface
    {
        return null;
    }

    public function __toString(): string
    {
        return "{$this->name}: Damage:{$this->damage}, Heal:{$this->hitPoints}";
    }
}
