<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\Action;

use AdventOfCode\RPGSimulator\ActionInterface;
use AdventOfCode\RPGSimulator\EffectInterface;

/**
 * A Normal Action.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
class Spell implements ActionInterface
{
    /**
     * Action constructor.
     */
    public function __construct(
        private string $name,
        private int $manaCost,
        private int $damage = 0,
        private int $armour = 0,
        private int $hitPoints = 0,
        private ?EffectInterface $effect = null
    ) {
    }

    /**
     * @return int
     */
    public function getManaCost(): int
    {
        return $this->manaCost;
    }

    public function getDamage(): int
    {
        return $this->damage;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getHitPoints(): int
    {
        return $this->hitPoints;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEffect(): ?EffectInterface
    {
        return $this->effect;
    }

    public function __toString(): string
    {
        return "{$this->name}: Mana Cost:{$this->manaCost}, Damage:{$this->damage}, Heal:{$this->hitPoints}";
    }
}
