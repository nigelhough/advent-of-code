<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator;

/**
 * Describes an RPG Simulator Player.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
interface PlayerInterface
{
    /**
     * @return int
     */
    public function getDamage(): int;

    /**
     * @return int
     */
    public function getArmour(): int;

    /**
     * @return int
     */
    public function getHitPoints(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return ActionInterface[]
     */
    public function getAvailableActions(): array;

    /**
     * @param PlayerInterface $opponent
     *
     * @return int
     */
    public function getTurnsForVictory(self $opponent): int;
}
