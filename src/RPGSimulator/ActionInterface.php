<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator;

use Stringable;

/**
 * Describes an RPG Simulator Player.
 *
 * @package AdventOfCode\RPGSimulator
 * @private
 */
interface ActionInterface extends Stringable
{
    /**
     * @return int
     */
    public function getDamage(): int;

    /**
     * @return int
     */
    public function getArmour(): int;

    /**
     * @return int
     */
    public function getHitPoints(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return ?EffectInterface
     */
    public function getEffect(): ?EffectInterface;
}
