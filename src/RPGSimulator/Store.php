<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator;

use Generator;

class Store
{
    public function __construct(private array $goods)
    {
    }

    public function iterateGoodsCombinations(): Generator
    {
        foreach ($this->goods['Weapons'] as $weapon) {
            [$wName, $wCost, $wDamage, $wArmor] = $weapon;
            foreach ($this->goods['Armor'] as $armor) {
                [$aName, $aCost, $aDamage, $aArmor] = $armor;
                foreach ($this->goods['Rings'] as $left) {
                    [$lName, $lCost, $lDamage, $lArmor] = $left;
                    foreach ($this->goods['Rings'] as $right) {
                        [$rName, $rCost, $rDamage, $rArmor] = $right;
                        if ($lName === $rName) {
                            continue;
                        }
                        //echo "{$wName} {$aName} {$lName} {$rName} ".($wCost + $aCost + $lCost + $rCost)."\n";
                        yield [
                            $wCost + $aCost + $lCost + $rCost,
                            $wDamage + $aDamage + $lDamage + $rDamage,
                            $wArmor + $aArmor + $lArmor + $rArmor,
                        ];
                    }
                }
            }
        }
    }
}
