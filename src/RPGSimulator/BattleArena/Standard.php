<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\BattleArena;

use AdventOfCode\RPGSimulator\BattleArenaInterface;
use AdventOfCode\RPGSimulator\PlayerInterface;

class Standard implements BattleArenaInterface
{
    public function battle(PlayerInterface $player1, PlayerInterface $player2): PlayerInterface
    {
        return $player1->getTurnsForVictory($player2) <= $player2->getTurnsForVictory($player1) ? $player1 : $player2;
    }
}
