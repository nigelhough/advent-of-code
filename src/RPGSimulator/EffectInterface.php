<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator;

use Stringable;

/**
 * Describes an Effect.
 *
 * @package AdventOfCode\RPGSimulator
 * @public
 */
interface EffectInterface extends Stringable
{
    public function getTurnDuration(): int;

    public function getDamageDealt(): int;

    public function getArmourIncrease(): int;
}
