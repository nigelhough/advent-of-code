<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator;

/**
 * Describes a Battle Arena.
 *
 * @package AdventOfCode\RPGSimulator
 * @public
 */
interface BattleArenaInterface
{
    public function battle(PlayerInterface $player1, PlayerInterface $player2): PlayerInterface;
}
