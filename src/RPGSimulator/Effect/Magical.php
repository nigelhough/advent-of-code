<?php

declare(strict_types=1);

namespace AdventOfCode\RPGSimulator\Effect;

use AdventOfCode\RPGSimulator\EffectInterface;

/**
 * A Magical Effect.
 *
 * @package AdventOfCode\RPGSimulator\Effect
 * @private
 */
class Magical implements EffectInterface
{
    /**
     * Magical constructor.
     */
    public function __construct(
        private string $name,
        private int $turnDuration,
        private int $damageDealt = 0,
        private int $armourIncrease = 0,
        private int $manaIncrease = 0
    ) {
    }

    /**
     * @return int
     */
    public function getDamageDealt(): int
    {
        return $this->damageDealt;
    }

    /**
     * @return int
     */
    public function getArmourIncrease(): int
    {
        return $this->armourIncrease;
    }

    /**
     * @return int
     */
    public function getManaIncrease(): int
    {
        return $this->manaIncrease;
    }

    public function getTurnDuration(): int
    {
        return $this->turnDuration;
    }

    public function __toString(): string
    {
        return "{$this->name} - " .
            "Duration:{$this->turnDuration}, " .
            "Damage:{$this->damageDealt}, " .
            "ArmourBoost:{$this->armourIncrease}, " .
            "ManaIncrease:{$this->manaIncrease}";
    }
}
