<?php

declare(strict_types=1);

namespace AdventOfCode\Device;

class Calibrator
{
    public const BASE = 0;

    public function calibrate(FrequencyChanges $frequencyChanges): int
    {
        $resultingFrequency = self::BASE;

        /** @var FrequencyChange $frequencyChange */
        foreach ($frequencyChanges as $frequencyChange) {
            $resultingFrequency += $frequencyChange->getChangeOffset();
        }

        return $resultingFrequency;
    }

    public function duplicate(FrequencyChanges $frequencyChanges): int
    {
        $resultingFrequency = self::BASE;
        $discoveredFrequencies = [];


        while (true) {
            /** @var FrequencyChange $frequencyChange */
            foreach ($frequencyChanges as $frequencyChange) {
                $resultingFrequency += $frequencyChange->getChangeOffset();
                if (in_array($resultingFrequency, $discoveredFrequencies)) {
                    return $resultingFrequency;
                }
                $discoveredFrequencies[] = $resultingFrequency;
            }
        }
    }
}
