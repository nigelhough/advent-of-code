<?php

declare(strict_types=1);

namespace AdventOfCode\Device;

class DataStream
{
    private const PACKET_MARKER_LENGTH = 4;

    private const MESSAGE_MARKER_LENGTH = 14;

    public readonly int $packetMarkerEnd;

    public readonly int $messageMarkerEnd;

    public function __construct(public readonly string $dataStream)
    {
        for ($i = 0; $i < strlen($dataStream); $i++) {
            $packetMarker = str_split(substr($dataStream, $i, self::PACKET_MARKER_LENGTH));
            if (!isset($this->packetMarkerEnd) && count(array_unique($packetMarker)) == self::PACKET_MARKER_LENGTH) {
                $this->packetMarkerEnd = $i + self::PACKET_MARKER_LENGTH;
            }

            $messageMarker = str_split(substr($dataStream, $i, self::MESSAGE_MARKER_LENGTH));
            if (count(array_unique($messageMarker)) == self::MESSAGE_MARKER_LENGTH) {
                $this->messageMarkerEnd = $i + self::MESSAGE_MARKER_LENGTH;
                break;
            }
        }
    }
}
