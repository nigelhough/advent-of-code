<?php

declare(strict_types=1);

namespace AdventOfCode\Device;

use ArrayIterator;

class FrequencyChanges extends ArrayIterator
{
    public function __construct(string $sequence)
    {
        parent::__construct(
            array_map(function ($changeOffset) {
                return new FrequencyChange($changeOffset);
            }, explode("\n", $sequence))
        );
    }
}
