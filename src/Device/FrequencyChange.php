<?php

declare(strict_types=1);

namespace AdventOfCode\Device;

/**
 * A frequency change has a frequency offset as a signed Integer
 */
class FrequencyChange
{
    /** @var int Frequency change offset as a signed Integer */
    private $offset;

    /**
     * Construct FrequencyChange.
     *
     * @param string $change Offset as a string message from device.
     */
    public function __construct(string $change)
    {
        $this->offset = (int) trim($change);
    }

    /**
     * Frequency change offset as a signed Integer
     *
     * @return int
     */
    public function getChangeOffset(): int
    {
        return $this->offset;
    }
}
