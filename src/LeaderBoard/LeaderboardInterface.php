<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard;

interface LeaderboardInterface
{
    /**
     * Get the raw array of leader board data.
     *
     * @return array
     */
    public function getRaw(): array;

    public function getYear(): int;

    public function getOwner(): int;

    public function getMembers(bool $filtered = true): array;

    public function getMember(int $userId): Individual\LeaderboardInterface;
}
