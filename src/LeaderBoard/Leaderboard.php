<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard;

use Exception;

class Leaderboard implements LeaderboardInterface
{
    public function __construct(private array $raw)
    {
    }

    public function getOwner(): int
    {
        return (int) $this->getRaw()['owner_id'];
    }

    /**
     * @inheritDoc
     */
    public function getRaw(): array
    {
        return $this->raw;
    }

    public function getMembers(bool $filtered = true): array
    {
        $members = [];
        foreach ($this->getRaw()['members'] as $memberDetails) {
            $member = new Individual\IndividualLeaderboard($this->getYear(), $memberDetails);
            if ($member->getParticipatedDays() === 0) {
                continue;
            }
            $members[] = $member;
        }
        return $members;
    }

    public function getYear(): int
    {
        return (int) $this->getRaw()['event'];
    }

    public function getMember(int $userId): Individual\LeaderboardInterface
    {
        if (!array_key_exists($userId, $this->getRaw()['members'])) {
            throw new Exception('Can\'t get individual leaderboard for invalid user');
        }
        return new Individual\IndividualLeaderboard($this->getYear(), $this->getRaw()['members'][$userId]);
    }
}
