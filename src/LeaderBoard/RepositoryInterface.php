<?php

namespace AdventOfCode\LeaderBoard;

/**
 * Describes a Leaderboard Repository.
 */
interface RepositoryInterface
{
    /**
     * Get the leaderboard object.
     *
     * @param int  $year        The year to get the leaderboard for.
     * @param int  $leaderboard The private leaderboard to get. Defaults to WFS
     * @param bool $ignoreCache Ignore Leaderboard Cache, use this sparingly to not overload their systems.
     *
     * @return LeaderboardInterface
     */
    public function get(int $year, int $leaderboard, bool $ignoreCache = false): LeaderboardInterface;
}
