<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard;

use AdventOfCode\Client\ClientInterface;

/**
 * Fetch a leaderboard from a remote source.
 */
class Remote implements RepositoryInterface
{
    public function __construct(private ClientInterface $client)
    {
    }

    public function get(int $year, int $leaderboard, bool $ignoreCache = false): LeaderboardInterface
    {
        $response = $this->client->get("https://adventofcode.com/{$year}/leaderboard/private/view/{$leaderboard}.json");
        $leaderboardBody = json_decode($response->getBody()->getContents(), true);
        if (empty($leaderboardBody)) {
            return new Leaderboard([]);
        }
        return new Leaderboard($leaderboardBody);
    }
}
