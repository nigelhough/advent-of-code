<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard\Individual;

interface LeaderboardInterface
{
    public function getRaw(): array;

    public function getYear(): int;

    public function getUserId(): int;

    public function getName(): string;

    public function getStarCount(): int;

    public function hasCompletedPart1(int $day): bool;

    public function hasCompletedPart2(int $day): bool;

    public function getLocalScore(): int;

    public function getParticipatedDays(): int;

    public function getAcquiredStars(int $day): int;
}
