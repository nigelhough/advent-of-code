<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard\Individual;

/**
 * Fetches individuals leaderboards.
 */
class IndividualLeaderboardRepository implements RepositoryInterface
{
    public function __construct(private \AdventOfCode\LeaderBoard\RepositoryInterface $repository, private int $userId)
    {
    }

    public function get(int $year, int $leaderboard, bool $ignoreCache = false): LeaderboardInterface
    {
        return new IndividualLeaderboard(
            $year,
            $this->repository->get($year, $leaderboard, $ignoreCache)->getMember($this->userId)->getRaw(),
        );
    }
}
