<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard\Individual;

class IndividualLeaderboard implements LeaderboardInterface
{
    public function __construct(private int $year, private array $raw)
    {
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getUserId(): int
    {
        return (int) $this->getRaw()['id'];
    }

    public function getRaw(): array
    {
        return $this->raw;
    }

    public function getName(): string
    {
        return $this->getRaw()['name'];
    }

    public function getStarCount(): int
    {
        return (int) $this->getRaw()['stars'];
    }

    public function hasCompletedPart1(int $day): bool
    {
        if (!array_key_exists($day, $this->getRaw()['completion_day_level'])) {
            return false;
        }
        return array_key_exists(1, $this->getRaw()['completion_day_level'][$day]);
    }

    public function hasCompletedPart2(int $day): bool
    {
        if (!array_key_exists($day, $this->getRaw()['completion_day_level'])) {
            return false;
        }
        return array_key_exists(2, $this->getRaw()['completion_day_level'][$day]);
    }

    public function getLocalScore(): int
    {
        return (int) $this->getRaw()['local_score'];
    }

    public function getParticipatedDays(): int
    {
        return count($this->getRaw()['completion_day_level']);
    }

    public function getAcquiredStars(int $day): int
    {
        if (!array_key_exists($day, $this->getRaw()['completion_day_level'])) {
            return 0;
        }
        return count($this->getRaw()['completion_day_level'][$day]);
    }
}
