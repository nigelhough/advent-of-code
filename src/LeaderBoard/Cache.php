<?php

declare(strict_types=1);

namespace AdventOfCode\LeaderBoard;

use DateTime;

/**
 * Fetch an input from a cache.
 */
class Cache implements RepositoryInterface
{
    private const THRESHOLD_SECONDS = 15 * 60;

    public function __construct(private string $cacheDir, private RepositoryInterface $fallback)
    {
    }

    public function get(int $year, int $leaderboard, bool $ignoreCache = false): LeaderboardInterface
    {
        $cacheFile = "{$this->cacheDir}/{$year}/leaderboard.{$leaderboard}";
        $cacheExists = file_exists($cacheFile);
        $cacheExpired = $cacheExists && time() > $this->getCacheExpiry(filemtime($cacheFile), $year);

        if (!$ignoreCache && file_exists($cacheFile) && !$cacheExpired) {
            // Cached leaderboard.
            return new Leaderboard(json_decode(file_get_contents($cacheFile), true));
        }

        $leaderboard = $this->fallback->get($year, $leaderboard)->getRaw();

        touch($cacheFile);
        file_put_contents($cacheFile, json_encode($leaderboard));
        return new Leaderboard($leaderboard);
    }

    /**
     * Get the time a cache expires based on the age of the cache and the year of the leaderboard.
     * The current year is fetched more commonly, a factor is applied to older years.
     * Consider submitting a valid answer will force a fetch of the leaderboard for star status
     *
     * @param int $time The time the cache was created
     * @param int $year The year the leaderboard is being fetched for
     *
     * @return int
     */
    private function getCacheExpiry(int $time, int $year): int
    {
        $factor = (new DateTime())->format('Y') == $year ? 1 : 12;
        return (int) ($time + (self::THRESHOLD_SECONDS * $factor));
    }
}
