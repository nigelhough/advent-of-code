<?php

declare(strict_types=1);

namespace AdventOfCode\HexGrid;

use AdventOfCode\Grid\CoordinateInterface;

class Coord implements CoordinateInterface
{
    public function __construct(private int $xPosition, private int $yPosition, private int $zPosition)
    {
    }

    public static function fromString(string $coord): self
    {
        [$x, $y, $z] = explode(',', $coord);
        return new self((int) $x, (int) $y, (int) $z);
    }

    public function __toString()
    {
        return "{$this->xPosition},{$this->yPosition},{$this->zPosition}";
    }

    public function moveAlongBearing(Bearing $bearing, int $distance = 1): self
    {
        return new self(
            $this->xPosition + ($bearing->getXMod() * $distance),
            $this->yPosition + ($bearing->getYMod() * $distance),
            $this->zPosition + ($bearing->getZMod() * $distance),
        );
    }

    public function getCost(): int
    {
        return 1;
    }

    public function getNeighbours(): array
    {
        return $this->getSurrounding();
    }

    /**
     * @return self[]
     */
    public function getSurrounding(): array
    {
        $coordinates = [];
        foreach (Bearing::neighbours() as $bearing) {
            [$xMod, $yMod, $zMod] = $bearing->getModifiers();
            $coordinates[] = new self(
                $this->getXPosition() + $xMod,
                $this->getYPosition() + $yMod,
                $this->getZPosition() + $zMod
            );
        }
        return $coordinates;
    }

    /**
     * @return int
     */
    public function getXPosition(): int
    {
        return $this->xPosition;
    }

    /**
     * @return int
     */
    public function getYPosition(): int
    {
        return $this->yPosition;
    }

    /**
     * @return int
     */
    public function getZPosition(): int
    {
        return $this->zPosition;
    }

    /**
     * @param self $subject
     *
     * @return bool
     */
    public function isSame($subject)
    {
        return ($this->xPosition === $subject->getXPosition())
            && ($this->yPosition === $subject->getYPosition())
            && ($this->zPosition === $subject->getZPosition());
    }

    /**
     * @param self $target
     *
     * @return float
     */
    public function distance($target): float
    {
        return pow(
            (($target->getXPosition() - $this->getXPosition()) ** 2) +
            (($target->getYPosition() - $this->getYPosition()) ** 2) +
            (($target->getZPosition() - $this->getZPosition()) ** 2),
            0.5
        );
    }
}
