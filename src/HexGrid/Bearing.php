<?php

declare(strict_types=1);

namespace AdventOfCode\HexGrid;

class Bearing
{
    public const NORTH_WEST = 'NW';

    public const NORTH_EAST = 'NE';

    public const EAST = 'E';

    public const SOUTH_EAST = 'SE';

    public const SOUTH_WEST = 'SW';

    public const WEST = 'W';

    public function __construct(private int $xMod, private int $yMod, private int $zMod)
    {
    }

    /**
     * Rotate a Hex Coordinate from a systems with 1 top and bottom to 1 right 1 left.
     *
     * @return string
     */
    public static function rotateHexGrid(string $bearingIdentifier): string
    {
        return match (strtoupper($bearingIdentifier)) {
            'N' => self::EAST,
            'NE' => self::SOUTH_EAST,
            'SE' => self::SOUTH_WEST,
            'S' => self::WEST,
            'SW' => self::NORTH_WEST,
            'NW' => self::NORTH_EAST,
        };
    }

    public static function get(string $bearingIdentifier): self
    {
        return match (strtoupper($bearingIdentifier)) {
            self::NORTH_EAST => new self(1, 0, -1),
            self::EAST => new self(1, -1, 0),
            self::SOUTH_EAST => new self(0, -1, 1),
            self::SOUTH_WEST => new self(-1, 0, 1),
            self::WEST => new self(-1, 1, 0),
            self::NORTH_WEST => new self(0, 1, -1),
        };
    }

    /**
     * @return self[]
     */
    public static function neighbours(): array
    {
        return [
            self::NORTH_EAST => new self(1, 0, -1),
            self::EAST       => new self(1, -1, 0),
            self::SOUTH_EAST => new self(0, -1, 1),
            self::SOUTH_WEST => new self(-1, 0, 1),
            self::WEST       => new self(-1, 1, 0),
            self::NORTH_WEST => new self(0, 1, -1),
        ];
    }

    public function getModifiers(): array
    {
        return [$this->xMod, $this->yMod, $this->zMod];
    }

    /**
     * @return int
     */
    public function getXMod(): int
    {
        return $this->xMod;
    }

    /**
     * @return int
     */
    public function getYMod(): int
    {
        return $this->yMod;
    }

    /**
     * @return int
     */
    public function getZMod(): int
    {
        return $this->zMod;
    }

    public function isSame(self $subject)
    {
        return ($this->xMod === $subject->xMod) && ($this->yMod === $subject->yMod) && ($this->zMod === $subject->zMod);
    }
}
