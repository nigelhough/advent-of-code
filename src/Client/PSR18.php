<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Exception;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * A PSR18 compatible AOC Client.
 */
class PSR18 implements ClientInterface
{
    public function __construct(
        private \Psr\Http\Client\ClientInterface $client,
        private RequestFactoryInterface $requestFactory,
        private string $sessionCookie
    ) {
    }

    /**
     * @todo Implement
     */
    public function get(string $endpoint): ResponseInterface
    {
        throw new Exception('Not Implemented');
        $request = $this->requestFactory->createRequest('get', $endpoint);
        $request->withHeader('cookie', $this->sessionCookie);
        $request->with('cookie', $this->sessionCookie);
        $response = $this->client->sendRequest($request);
        return $response;
    }

    /**
     * @todo Implement
     */
    public function post(string $endpoint, array $form): ResponseInterface
    {
        throw new Exception('Not Implemented');
        $request = $this->requestFactory->createRequest('get', $endpoint);
        $request->withHeader('cookie', $this->sessionCookie);
        //$request->withBody($form);
        $response = $this->client->sendRequest($request);
        return $response;
    }
}
