<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Exception;

/**
 * An exception thrown when an AdventOfCode client encounters an error fetching a response.
 */
class ErrorFetchingResponse extends Exception implements ExceptionInterface
{
}
