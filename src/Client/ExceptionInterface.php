<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Describes an AdventOfCode Client Exception.
 */
interface ExceptionInterface extends ClientExceptionInterface
{
}
