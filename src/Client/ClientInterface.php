<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Psr\Http\Message\ResponseInterface;

/**
 * Describes an AOC Client.
 */
interface ClientInterface
{
    public function get(string $endpoint): ResponseInterface;

    public function post(string $endpoint, array $form): ResponseInterface;
}
