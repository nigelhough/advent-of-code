<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Exception;

/**
 * An exception thrown when an AdventOfCode client encounters an unknown response.
 */
class UnknownResponseException extends Exception implements ExceptionInterface
{
}
