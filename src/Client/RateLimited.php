<?php

namespace AdventOfCode\Client;

use DateTimeImmutable;
use Psr\Http\Message\ResponseInterface;

/**
 * A rate limited client that will limit the amount of requests made to the AOC website.
 */
class RateLimited implements ClientInterface
{
    private const HOURLY_LIMIT = 20;

    public function __construct(private string $cacheDir, private ClientInterface $fallback)
    {
    }

    /**
     * Check if this request will exceed the limit.
     *
     * @param string $endpoint the endpoint being requested
     * @return void
     * @throws RateLimitExceeded
     */
    private function checkLimit(string $endpoint): void
    {
        $cacheFile = "{$this->cacheDir}/ratelimit";
        $rateLimit = [];
        if (file_exists($cacheFile)) {
            $rateLimit = require $cacheFile;
        }
        $now = new DateTimeImmutable();
        $rateLimit[] = ['endpoint' => $endpoint, 'ts' => $now->getTimestamp()];

        // Filter to the last Hour.
        $oneHourAgo = $now->getTimestamp() - (60 * 60);
        $rateLimit = array_filter($rateLimit, fn($request) => (int)$request['ts'] > $oneHourAgo);

        // Save new rate limit list;
        file_put_contents(
            $cacheFile,
            "<?php\nreturn " . var_export($rateLimit, true) . ";\n"
        );
        // Check Rate Limit
        if (count($rateLimit) > self::HOURLY_LIMIT) {
            throw new RateLimitExceeded('Rate Limited Exceeded');
        }
    }

    public function get(string $endpoint): ResponseInterface
    {
        $this->checkLimit($endpoint);
        return $this->fallback->get($endpoint);
    }

    public function post(string $endpoint, array $form): ResponseInterface
    {
        $this->checkLimit($endpoint);
        return $this->fallback->post($endpoint, $form);
    }
}
