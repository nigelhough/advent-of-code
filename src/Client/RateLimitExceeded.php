<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Exception;

/**
 * An exception thrown when a client rate limit is exceeded.
 */
class RateLimitExceeded extends Exception implements ExceptionInterface
{
}
