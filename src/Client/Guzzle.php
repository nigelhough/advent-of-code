<?php

declare(strict_types=1);

namespace AdventOfCode\Client;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SessionCookieJar;
use Psr\Http\Message\ResponseInterface;

/**
 * An AOC Client that uses Guzzle for requests.
 */
class Guzzle implements ClientInterface
{
    public function __construct(private Client $client, private string $sessionCookie)
    {
    }

    public function get(string $endpoint): ResponseInterface
    {
        return $this->request($endpoint, 'GET');
    }

    private function request(string $endpoint, string $method, ?array $form = null): ResponseInterface
    {
        try {
            $options = [
                'cookies' => SessionCookieJar::fromArray(
                    ['session' => $this->sessionCookie,],
                    '.adventofcode.com'
                ),
            ];
            if ($form !== null) {
                $options['form_params'] = $form;
            }
            $response = $this->client->request($method, $endpoint, $options);
            if ($response->getStatusCode() !== 200) {
                throw new UnknownResponseException('Invalid Submission Request');
            }
            // @todo handle expired sessions
            // If the response is not JSON it might be that the session is expired and its rendered HTML
            return $response;
        } catch (Exception $e) {
            throw new ErrorFetchingResponse('Unknown Error Submitting Answer', previous: $e);
        }
    }

    public function post(string $endpoint, array $form): ResponseInterface
    {
        return $this->request($endpoint, 'POST', $form);
    }
}
