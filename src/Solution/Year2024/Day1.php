<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2024;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day1 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '3   4
4   3
2   5
1   9
3   3
3   3';
        }
        $input = str_replace( '   ', ' ', $input);

        $left = $right = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$l, $r] = explode(' ', $line);
            $left[] = $l;
            $right[] = $r;
        }
        sort($left);
        sort($right);
        $answer1 = 0;
        for($i=0; $i < count($left); $i++) {
            $answer1 += abs(($left[$i]) - ($right   [$i]));
        }

        $counts = array_count_values($right);
        foreach($left as $l) {
            $answer2 += $l * ($counts[$l] ?? 0);
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
