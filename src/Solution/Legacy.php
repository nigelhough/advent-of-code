<?php

declare(strict_types=1);

namespace AdventOfCode\Solution;

use AdventOfCode\Answer\AnswerInterface;

class Legacy implements SolutionInterface
{
    public function __construct(private string $legacyFile)
    {
    }

    public function execute(string $input): ?AnswerInterface
    {
        require $this->legacyFile;

        return null;
    }
}
