<?php

declare(strict_types=1);

namespace AdventOfCode\Solution;

use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\AnswerInterface;

class EmptySolution extends AbstractSolution
{
    public function __construct(private int $year, private int $day)
    {
    }

    public function execute(string $input): ?AnswerInterface
    {
        echo "I am a new Problem without a Solution\n
Input:\n
$input";

        return new Answer(null, null);
    }
}
