<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;

        // Part 1
        foreach (Parser::iterateByLine($input) as $line) {
            $answer1 += $this->getFirstLastDigit($line);
        }

        // Part 2
        $input = str_replace(
            ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'],
            ['o1e', 't2o', 't3e', 'f4r', 'f5e', 's6x', 's7n', 'e8t', '9'],
            $input
        );
        foreach (Parser::iterateByLine($input) as $line) {
            $answer2 += $this->getFirstLastDigit($line);
        }

        echo "Part 1: {$answer1}\nPart 2: {$answer2}";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function getFirstLastDigit(string $text): int
    {
        $characters = array_filter(str_split($text), 'is_numeric');
        return (int)($characters[array_key_first($characters)] . $characters[array_key_last($characters)]);
    }
}
