<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day7 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $hands = [];
        $jokerHands = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$hand, $bid] = explode(' ', $line);
            $hands[] = new Hand($hand, (int)$bid);
            $jokerHands[] = new HandWithJokers($hand, (int)$bid);
        }

        $answer1 = 0;
        usort($hands, [Hand::class, 'sort']);
        foreach ($hands as $i => $hand) {
            $answer1 += ($i + 1) * $hand->bid;
        }

        $answer2 = 0;
        usort($jokerHands, [Hand::class, 'sort']);
        foreach ($jokerHands as $i => $hand) {
            $answer2 += ($i + 1) * $hand->bid;
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}

class Hand
{
    public $handType = '';

    public function __construct(public string $hand, public int $bid)
    {
    }

    public function getHandType(): int
    {
        $cards = (str_split($this->hand));
        $uniqueCards = array_count_values($cards);
        if (count($uniqueCards) === 1) {
            // Five of a Kind
            $this->handType = 'Five of a kind';
            return 7;
        }
        if (count($uniqueCards) === 2) {
            if (in_array(4, $uniqueCards)) {
                // Four of a Kind
                $this->handType = 'Four of a kind';
                return 6;
            }
            // Full House
            $this->handType = 'Full House';
            return 5;
        }
        if (count($uniqueCards) === 3) {
            if (in_array(3, $uniqueCards)) {
                // Three of a Kind
                $this->handType = 'Three of a kind';
                return 4;
            }

            // Two Pair
            $this->handType = 'Two Pair';
            return 3;
        }
        if (count($uniqueCards) === 4) {
            // Pair
            $this->handType = 'Pair';
            return 2;
        }

        $this->handType = 'High';
        return 1;
    }

    public function getCardValues(): array
    {
        return array_map(fn(string $card) => $this->getCardValue($card), str_split($this->hand));
    }

    public function getCardValue(string $card): int
    {
        if (is_numeric($card)) {
            return (int)$card;
        }
        if ($card === 'T') {
            return 10;
        }
        if ($card === 'J') {
            return 11;
        }
        if ($card === 'Q') {
            return 12;
        }
        if ($card === 'K') {
            return 13;
        }
        if ($card === 'A') {
            return 14;
        }
        throw new \Exception('Bad Card');
    }

    public static function sort(self $a, self $b)
    {
        $at = $a->getHandType();
        $bt = $b->getHandType();

        if ($at != $bt) {
            return $at <=> $bt;
        }

        $av = $a->getCardValues();
        $bv = $b->getCardValues();
        for ($i = 0; $i < count($av); $i++) {
            if ($av[$i] === $bv[$i]) {
                continue;
            }
            return $av[$i] <=> $bv[$i];
        }
        throw new Exception('Bad Sort');
    }
}

class HandWithJokers extends Hand
{
    public function getHandType(): int
    {
        $cards = (str_split($this->hand));
        $uniqueCards = array_count_values($cards);

        $jokers = $uniqueCards['J'] ?? 0;
        if ($jokers === 0) {
            return parent::getHandType();
        }

        if (count($uniqueCards) === 1) {
            // Five of a Kind
            $this->handType = 'Five of a kind';
            return 7;
        }
        if (count($uniqueCards) === 2) {
            // 1, 2 or 4 Jokers. 5 of a kind
            $this->handType = 'Five of a kind (J)';
            return 7;
        }
        if (count($uniqueCards) === 3) {
            if ($jokers === 3) {
                // 4 Jokers plus something else.
                $this->handType = 'Four of a kind (J)';
                return 6;
            } elseif ($jokers === 2) {
                // 2 Jokers, 2 something else, 1 something else
                $this->handType = 'Four of a kind (J)';
                return 6;
            }
            // 1 joker
            if (in_array(3, $uniqueCards)) {
                // 1 Joker and 3 of a kind
                $this->handType = 'Four of a kind (J)';
                return 6;
            }

            // 1 joker, 2 pairs
            $this->handType = 'Full House (J)';
            return 5;
        }
        if (count($uniqueCards) === 4) {
            if ($jokers === 2) {
                // 2 jokers and anything else
                $this->handType = 'Three of a kind (J)';
                return 4;
            }
            // 1 jokers and a par
            $this->handType = 'Three of a kind (J)';
            return 4;
        }

        $this->handType = 'Pair';
        return 2;
    }

    public function getCardValue(string $card): int
    {
        if ($card === 'J') {
            return 0;
        }
        return parent::getCardValue($card);
    }
}