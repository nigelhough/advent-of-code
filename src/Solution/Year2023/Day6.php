<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day6 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        // Format Input.
        [$time, $distance] = explode(PHP_EOL, $input);
        $time = array_values(array_map('intval', array_filter(explode(' ', $time), 'is_numeric')));
        $distance = array_values(array_map('intval', array_filter(explode(' ', $distance), 'is_numeric')));

        /** @var Race[] $races */
        $races = [];
        for ($race = 0; $race < count($time); $race++) {
            $races[] = new Race($time[$race], $distance[$race]);
        }

        $answer1 = 1;
        foreach ($races as $race) {
            $answer1 *= $race->winningPermitations();
        }

        $answer2 = (new Race((int) implode('', $time), (int) implode('', $distance)))->winningPermitations();

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}

class Race
{
    public function __construct(public readonly int $time, public readonly int $record)
    {
    }

    public function winningPermitations()
    {
        // An Inclusive range so is the diff plus 1.
        return ($this->findWinnerWithLongestHold()-$this->findWinnerWithShortestHold())+1;
    }

    private function isRecord(int $holdTime): bool
    {
        return $this->getDistance($holdTime) > $this->record;
    }

    private function getDistance(int $holdTime): int
    {
        return ($this->time - $holdTime) * $holdTime;
    }

    /**
     * This can be a binary search.
     */
    private function findWinnerWithShortestHold()
    {
        for ($holdTime = 1; $holdTime < $this->time; $holdTime++) {
            if ($this->isRecord($holdTime)) {
                return $holdTime;
            }
        }
    }

    /**
     * This can be a binary search.
     */
    private function findWinnerWithLongestHold()
    {
        for ($holdTime = ($this->time-1); $holdTime > 0; $holdTime--) {
            if ($this->isRecord($holdTime)) {
                return $holdTime;
            }
        }
    }
}
