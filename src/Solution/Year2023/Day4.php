<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day4 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        // Format Input.
        $input = str_replace([': '], ':', $input);
        $input = str_replace([' | '], '|', $input);
        $input = str_replace(['', 'Card '], '', $input);
        $input = str_replace(['  '], ' 0', $input);
        $input = str_replace([': '], ':0', $input);
        $input = str_replace(['| '], '|0', $input);

        $answer1 = 0;
        $copiesOfCards = [];
        foreach (Parser::iterateByLine($input) as $card) {
            [$cardId, $cardDetails] = explode(':', $card);
            $cardId = (int) $cardId;
            [$winningNumbers, $have] = explode('|', $cardDetails);
            $winningNumbers = array_map('intval', explode(' ', $winningNumbers));
            $have = array_map('intval', explode(' ', $have));
            $matches = array_intersect($have, $winningNumbers);
            // This bit is overly complicated and didn't really need binary.
            $answer1 += (bindec(str_pad(count($matches) ? '1' : '0', count($matches), '0')));

            // Include Original Copy of card.
            $copiesOfCards[$cardId] = empty($copiesOfCards[$cardId]) ? 1 : $copiesOfCards[$cardId]+1;

            // For the number of matches found we will increase the copies of the next X Cards.
            for($i=1; $i <= count($matches); $i++) {
                // For each copy of the card apply the same logic.
                if (empty($copiesOfCards[$cardId + $i])) {
                    $copiesOfCards[$cardId + $i] = 0;
                }
                $copiesOfCards[$cardId + $i] += $copiesOfCards[$cardId];
            }
        }

        $answer2 = array_sum($copiesOfCards);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
