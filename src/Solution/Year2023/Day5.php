<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day5 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $groups = explode("\n\n", $input);
        $seeds = array_shift($groups);
        [, $seedDetails] = explode(':', $seeds);
        $seeds = explode(' ', trim($seedDetails));

        $groupsDestCollection = [];
        foreach ($groups as $group) {
            $groupsDest = [];
            [, $details] = explode(':', $group);

            foreach (explode("\n", trim($details)) as $line) {
                [$destStart, $sourceStart, $length] = explode(' ', trim($line));

                $groupsDest[] = [$destStart, $sourceStart, $length];

            }
            $groupsDestCollection[] = $groupsDest;
        }

        $smallest = PHP_INT_MAX;
        foreach ($seeds as $seed) {
            $seed = (int) $seed;
            $dest = $seed;
            foreach ($groupsDestCollection as $stage) {
                foreach($stage as $ranges) {
                    [$destStart, $sourceStart, $length] = $ranges;
                    if($seed >= $sourceStart && $seed <= $sourceStart + $length) {
                        $offset = $seed - $sourceStart;
                        $dest = $destStart + $offset;
                        break;
                    }
                }

                $seed = $dest;
            }
            $last = $dest;
            $smallest = min($smallest, $last);

        }
        $answer1 = $smallest;


        // Part 2.
        $seeds = explode(' ', trim($seedDetails));

        $stages = [];
        foreach ($groups as $group) {
            /** @var OffsetRange[] $stageRanges */
            $stageRanges = [];
            [, $details] = explode(':', $group);
            foreach (explode("\n", trim($details)) as $line) {
                [$destStart, $sourceStart, $length] = array_map('intval', explode(' ', trim($line)));
                $stageRanges[] = new OffsetRange((int) $sourceStart, (int) ($sourceStart+$length), (int) ($destStart-$sourceStart));
            }
            $stages[] = $stageRanges;
        }

        $seedRanges = [];
        foreach (array_chunk($seeds, 2) as [$start, $length]) {
            $seedRanges[] = new Range((int) $start, $start+$length);
        }

//        $bigRange = new Range(2, 18);
//
//        $fullyIn1 = new Range(2, 5);
//        $fullyIn2 = new Range(3, 5);
//        $fullyIn3 = new Range(15, 18);
//        $fullyIn4 = new Range(15, 17);
//
//        $before = new Range(0,2);
//        $after = new Range(18,20);
//
//        $partialBefore = new Range(0, 5);
//        $partialAfter = new Range(15, 20);
//
//        $full = new Range(0, 20);
//
//        $bigRange->getOverlap($full);
//        return null;

        foreach($stages as $stage) {
            $stageOverlaps = [];
            foreach($seedRanges as $seedRange) {
                foreach ($stage as $stageRangeNo => $stageRange) {
                    $stageOverlaps = array_merge($stageOverlaps, $stageRange->getOverlap($seedRange));
                    $remainders = $stageRange->getRemainder($seedRange);
                    if (count($remainders) > 1) {
                        $seedRange = $remainders[0];
                        $seedRanges[] = $remainders[1];
                    } elseif (count($remainders) === 0) {
                        $seedRange = null;
                        break;
                    } else {
                        $seedRange = $remainders[0];
                    }
                }

                if ($seedRange !== null) {
                    $stageOverlaps[] = $seedRange;
                }
            }
            $seedRanges = $stageOverlaps;
        }

        $lowest = PHP_INT_MAX;
        foreach($seedRanges as $ranges) {
            $lowest = min($lowest, $ranges->start);
        }
        $answer2 = $lowest;

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}

class Range {
    public function __construct(public readonly int $start, public readonly int $end) {}

    public function getOverlap(Range $range):array {
        // Fully Contained.
        if(
            $range->start >= $this->start && $range->start <= $this->end &&
            $range->end >= $this->start && $range->end <= $this->end
        ) {
//            echo "Fully Contained\n";
//            echo "Overlap > $range->start x $range->end\n";
//            echo "Remainder > \n";
            return [new Range($range->start, $range->end)];
        }
        if(
            $range->start >= $this->end || $range->end <= $this->start
        ) {
//            echo "No Overlap\n";
//            echo "Overlap > \n";
//            echo "Remainder > $range->start x $range->end\n";
            return [];
        }
        if(
            $range->start < $this->start && $range->end > $this->start && $range->end < $this->end
        ) {
//            echo "Overlap Before\n";
//            echo "Overlap > $this->start x $range->end\n";
//            echo "Remainder > $range->start x $this->start\n";
            return [new Range($this->start, $range->end)];
        }
        if(
            $range->start > $this->start && $range->start < $this->end && $range->end > $this->end
        ) {
//            echo "Overlap After\n";
//            echo "Overlap > $range->start x $this->end\n";
//            echo "Remainder > $this->end x $range->end\n";
            return [new Range($range->start, $this->end)];
        }
        if(
            $range->start < $this->start && $range->end > $this->end
        ) {
//            echo "Full Overlap\n";
//            echo "Overlap > $this->start x $this->end\n";
//            echo "Remainder > $range->start x $this->start && $this->end x $range->end\n";
            return [new Range($this->start, $this->end)];
        }
        return [];
    }

    public function getRemainder(self $range):array {
        // Fully Contained.
        if(
            $range->start >= $this->start && $range->start <= $this->end &&
            $range->end >= $this->start && $range->end <= $this->end
        ) {
//            echo "Fully Contained\n";
//            echo "Overlap > $range->start x $range->end\n";
//            echo "Remainder > \n";
            return [];
        }
        if(
            $range->start >= $this->end || $range->end <= $this->start
        ) {
//            echo "No Overlap\n";
//            echo "Overlap > \n";
//            echo "Remainder > $range->start x $range->end\n";
            return [new Range($range->start, $range->end)];
        }
        if(
            $range->start < $this->start && $range->end > $this->start && $range->end < $this->end
        ) {
//            echo "Overlap Before\n";
//            echo "Overlap > $this->start x $range->end\n";
//            echo "Remainder > $range->start x $this->start\n";
            return [new Range($range->start, $this->start)];
        }
        if(
            $range->start > $this->start && $range->start < $this->end && $range->end > $this->end
        ) {
//            echo "Overlap After\n";
//            echo "Overlap > $range->start x $this->end\n";
//            echo "Remainder > $this->end x $range->end\n";
            return [new Range($this->end, $range->end)];
        }
        if(
            $range->start < $this->start && $range->end > $this->end
        ) {
//            echo "Full Overlap\n";
//            echo "Overlap > $this->start x $this->end\n";
//            echo "Remainder > $range->start x $this->start && $this->end x $range->end\n";
            return [new Range($range->start, $this->start), new Range($this->end, $range->end)];
        }
        return [];
    }
}
class OffsetRange extends Range{
    public function __construct(public readonly int $start, public readonly int $end, public readonly int $offset) {}

    public function getOverlap(Range $range):array {
        // Fully Contained.
        if(
            $range->start >= $this->start && $range->start <= $this->end &&
            $range->end >= $this->start && $range->end <= $this->end
        ) {
//            echo "Fully Contained\n";
//            echo "Overlap > $range->start x $range->end\n";
//            echo "Remainder > \n";
            return [new Range($range->start+$this->offset, $range->end+$this->offset)];
        }
        if(
            $range->start >= $this->end || $range->end <= $this->start
        ) {
//            echo "No Overlap\n";
//            echo "Overlap > \n";
//            echo "Remainder > $range->start x $range->end\n";
            return [];
        }
        if(
            $range->start < $this->start && $range->end > $this->start && $range->end < $this->end
        ) {
//            echo "Overlap Before\n";
//            echo "Overlap > $this->start x $range->end\n";
//            echo "Remainder > $range->start x $this->start\n";
            return [new Range($this->start+$this->offset, $range->end+$this->offset)];
        }
        if(
            $range->start > $this->start && $range->start < $this->end && $range->end > $this->end
        ) {
//            echo "Overlap After\n";
//            echo "Overlap > $range->start x $this->end\n";
//            echo "Remainder > $this->end x $range->end\n";
            return [new Range($range->start+$this->offset, $this->end+$this->offset)];
        }
        if(
            $range->start < $this->start && $range->end > $this->end
        ) {
//            echo "Full Overlap\n";
//            echo "Overlap > $this->start x $this->end\n";
//            echo "Remainder > $range->start x $this->start && $this->end x $range->end\n";
            return [new Range($this->start+$this->offset, $this->end+$this->offset)];
        }
        return [];
    }
}
