<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day2 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $input = str_replace([' ', 'Game'], '', $input);
        $answer1 = $answer2 = 0;
        foreach (Parser::iterateByLine($input) as $game) {
            [$gameId, $rounds] = explode(':', $game);
            $isValidGame = true;

            // Reset the count of greatest in a game.
            $greatestRed = $greatestBlue = $greatestGreen = 0;
            foreach (explode(';', $rounds) as $round) {
                // Reset the counts per round.
                $red = $green = $blue = 0;
                foreach (explode(',', $round) as $cubeCount) {
                    // Get the counts in the round.
                    $red += str_contains($cubeCount, 'red') ? ((int)$cubeCount) : 0;
                    $blue += str_contains($cubeCount, 'blue') ? ((int)$cubeCount) : 0;
                    $green += str_contains($cubeCount, 'green') ? ((int)$cubeCount) : 0;

                    // Get the greatest seen.
                    $greatestRed = max($greatestRed, $red);
                    $greatestBlue = max($greatestBlue, $blue);
                    $greatestGreen = max($greatestGreen, $green);
                }
                if ($red > 12 || $blue > 14 || $green > 13) {
                    $isValidGame = false;
                }
            }
            // End of the game.
            $answer1 += $isValidGame ? ((int)$gameId) : 0;
            $answer2 += ($greatestRed * $greatestBlue * $greatestGreen);
        }

        echo "Part 1: {$answer1}\nPart 2: {$answer2}";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
