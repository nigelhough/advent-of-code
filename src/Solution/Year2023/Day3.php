<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2023;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $numbers = [];
        $symbols = [];
        $stars = [];
        $y = 0;
        foreach (Parser::iterateByLine($input) as $row) {
            /** @var Coord[] $runningCoords */
            $runningCoords = [];
            $runningNumber = '';
            foreach (str_split($row) as $x => $value) {
                if (is_numeric($value)) {
                    $runningCoords[] = new Coord($x, $y);
                    $runningNumber .= $value;
                    continue;
                }
                if (!empty($runningCoords)) {
                    $numbers[] = new Number((int)$runningNumber, ...$runningCoords);
                    $runningCoords = [];
                    $runningNumber = '';
                }
                if ($value === '.') {
                    continue;
                }
                $symbols[] = new Coord($x, $y);
                if($value === '*') {
                    $stars[] = new Coord($x, $y);
                }
            }
            if (!empty($runningCoords)) {
                $numbers[] = new Number((int)$runningNumber, ...$runningCoords);
            }
            $y--;
        }

        $answer1 = 0;
        foreach ($numbers as $number) {
            $answer1 += $number->isValid($symbols) ? $number->number : 0;
        }

        $answer2 = 0;
        foreach($stars as $coord) {
            $touching = [];
            foreach ($numbers as $number) {
                if($number->isTouching($coord)) {
                    // If this is the third number the star is touching we can abort early.
                    if(count($touching) == 2) {
                        continue 2;
                    }
                    $touching[] = $number->number;
                }
            }

            // If the star touches 2 numbers include it in the result.
            if(count($touching) === 2) {
                $answer2 += array_product($touching);
            }
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}

class Number
{
    public readonly array $coords;

    public readonly array $xRange;
    public readonly array $yRange;

    public function __construct(public readonly int $number, Coord ...$coords)
    {
        $this->coords = $coords;
        $this->xRange = [
            $this->coords[array_key_first($this->coords)]->getXPosition()-1,
            $this->coords[array_key_last($this->coords)]->getXPosition()+1,
        ];
        $this->yRange = [
            $this->coords[0]->getYPosition()-1,
            $this->coords[0]->getYPosition()+1,
        ];
    }

    public function isValid(array $symbols): bool
    {
        foreach($symbols as $checkSymbol) {
            if($this->isTouching($checkSymbol)) {
                return true;
            }
        }
        return false;
    }

    public function isTouching(Coord $coord) : bool
    {
        return !(
            ($coord->getXPosition() < $this->xRange[0] || $coord->getXPosition() > $this->xRange[1]) ||
            ($coord->getYPosition() < $this->yRange[0] || $coord->getYPosition() > $this->yRange[1])
        );
    }
}