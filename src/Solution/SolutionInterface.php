<?php

declare(strict_types=1);

namespace AdventOfCode\Solution;

use AdventOfCode\Answer\AnswerInterface;

interface SolutionInterface
{
    /**
     * Execute solution code to generate the problem's answer.
     *
     * @param string $input The personal problem input.
     */
    public function execute(string $input): ?AnswerInterface;
}
