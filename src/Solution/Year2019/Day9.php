<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\IntCode;

class Day9 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $initialMemory = array_map('intval', explode(',', $input));
        $inputBuffer = new IntCode\InputBuffer();
        $outputBuffer = new IntCode\OutputBuffer();
        $computer = new IntCode\Computer($inputBuffer, $outputBuffer);

        global $relativeBase;
        $relativeBase = 0;

        $inputBuffer->set(1);
        $code = $computer->compute($initialMemory);
        $output = $outputBuffer->read();
        $answer1 = array_pop($output);

        $relativeBase = 0;
        $outputBuffer->clear();

        $inputBuffer->set(2);
        $code = $computer->compute($initialMemory);
        $output = $outputBuffer->read();
        $answer2 = array_pop($output);

        echo "Part1: {$answer1}\nPart2: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
