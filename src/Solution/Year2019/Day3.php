<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Panel;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }

        $wires = explode(PHP_EOL, $input);
        $wire1 = new Panel\Wire($wires[0]);
        $wire2 = new Panel\Wire($wires[1]);

        $tracer = new Panel\Tracer();
        $shortestDistance = PHP_INT_MAX;
        $shortestSteps = PHP_INT_MAX;
        foreach ($tracer->findIntersections($wire1, $wire2) as $intersection) {
            $shortestDistance = min($shortestDistance, $intersection->getDistanceToCenter());
            $shortestSteps = min($shortestSteps, $intersection->getSteps());
        }
        echo "Shortest Distance: {$shortestDistance}\n";
        $answer1 = $shortestDistance;
        echo "Shortest Steps: {$shortestSteps}\n";
        $answer2 = $shortestSteps;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
