<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\IntCode;
use AdventOfCode\IntCode\InputBuffer;
use AdventOfCode\IntCode\OutputBuffer;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $calculator = new IntCode\Computer(new InputBuffer(), new OutputBuffer());

        $input = array_map('intval', explode(',', $input));

        $noun = 12;
        $verb = 2;
        $answer1 = $code = $calculator->compute($input, 12, 2);
        echo "IntCode: {$code}\n";

        $target = 19690720;
        try {
            $answer2 = $result = $calculator->locate($input, $target);
            echo "Result: {$result}\n";
        } catch (Exception $e) {
            echo "Unable to find Answer! {$e->getMessage()}\n";
        }

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
