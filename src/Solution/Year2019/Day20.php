<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Grid\Portal;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day20 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        //$input = file_get_contents(BASE_DIR . '/cache/2019/20.sample1');
//        $input = file_get_contents(BASE_DIR . '/cache/2019/20.sample2');
//        $input = file_get_contents(BASE_DIR . '/cache/2019/20.sample3');

        //$input = str_replace(' ', '#', $input);

        $map = Map::createFromString($input);

        $portalsCoords = $map->locate(
            function ($location) {
                return ctype_alpha($location);
            }
        );

        $oldPortals = [];
        $portals = [];
        foreach ($portalsCoords as $index => $portal) {
            $coord = Coord::fromString($index);
            $portalEntrance = null;
            $portalPartner = null;
            foreach ($coord->getNeighbours() as $direction => $neighbour) {
                if (ctype_alpha($map->get($neighbour))) {
                    $portalPartner = $map->get($neighbour);
                }
                if ($map->get($neighbour) === ".") {
                    $portalEntrance = $neighbour;
                    $entranceDirection = $direction;
                }
            }
            if (empty($portalEntrance) || empty($entranceDirection) || empty($portalPartner)) {
                // Need both an entrance and a partner to be valid portal.
                continue;
            }
            $portalKey = in_array($entranceDirection, [0, 3]) ? $portal . $portalPartner : $portalPartner . $portal;
            $oldPortals[$portalKey][] = $portalEntrance;
        }
        echo $map->plot(true) . "\n\n";
        // A different recursive Method to debug map method
        $this->recursiveMethod($oldPortals, $map);
        return null;

        $passablePortals = array_filter(
            $oldPortals,
            function ($portal) {
                return count($portal) == 2;
            }
        );
//        var_dump($passablePortals);
        foreach ($passablePortals as $code => $portalDetails) {
            $portals[(string) $portalDetails[0]] = new Portal($code, $portalDetails[0], $portalDetails[1], 1);
            $portals[(string) $portalDetails[1]] = new Portal($code, $portalDetails[1], $portalDetails[0], 1);
        }
        $passablePortals = array_values($passablePortals);

        // Filter Start and End Portals.

        // Find the Shortest Route
        try {
            $route = $map->findRoute($oldPortals['ZZ'][0], $oldPortals['AA'][0], $portals);
        } catch (Exception) {
//            echo $map->plot(true);
            return null;
        }

        echo "Shortest Route : " . count($route) . "\n";
        // Plot map with the shortest route highlighted,
        foreach ($route->getPath()->follow() as $step) {
            $map->mark($step, '*');
        }

        echo "\n" . $map->plot(true);
        return null;
    }

    private function recursiveMethod(array $portals, Map $map)
    {
        // Find Portal Relationships.
        $routes = [];

        $mbr = $map->getMBR();
        $topRight = $mbr[1];

        $outerX = $topRight->getXPosition() - 2;
        $outerY = $topRight->getYPosition() - 2;

        // Loop over all portals.
        foreach ($portals as $startKey => $startPoints) {
            // For each portal get the entrance and exit points.
            foreach ($startPoints as $start) {
                // Loop over all other portals.
                foreach ($portals as $endKey => $endPoints) {
                    // Loop over entrance and exit of other portal.
                    foreach ($endPoints as $end) {
                        // A portal can't connect to itself.
                        if ($startKey === $endKey) {
                            continue;
                        }
                        try {
                            $isOuter = $start->getXPosition() === 2
                                || $start->getXPosition() == $outerX
                                || $start->getYPosition() == 2
                                || $start->getYPosition() == $outerY;

                            // Measure distance between Portals.
                            $route = $map->findRoutePortals($start, $end);
                        } catch (Exception) {
                            continue;
                        }
                        if (!$isOuter || in_array($startKey, ['AA', 'ZZ'])) {
                            $routes['top'][$startKey][$endKey] = count($route);
                        }
                        if (!in_array($startKey, ['AA', 'ZZ'])) {
                            $routes['lower'][$startKey][$endKey] = count($route);
                        }
                    }
                }
            }
        }

        $paths = [];
        $currentPath = ['AA'];
        $shortestKnownCost = PHP_INT_MAX;
        $this->findEnd('AA', $routes, 0, 0, $shortestKnownCost, $currentPath, $paths);
        var_dump($paths);
        echo "Shortest Route (via Recursion) : " . ($paths['cost']) . "\n";
    }

    private function findEnd($start, $routes, $level, $cost, &$shortestKnownCost, &$currentPath, &$paths)
    {
        var_dump($routes);
        $routes = $level === 0 ? $routes['top'] : $routes['lower'];
        foreach ($routes[$start] as $possibleRoute => $routeCost) {
            if (in_array($possibleRoute, $currentPath)) {
                // Dead End
                continue;
            }
            $path = array_merge($currentPath, [$possibleRoute]);
            $totalCost = ($cost + $routeCost) + (count($path) - 2);
            if ($totalCost > $shortestKnownCost) {
                // Too Long.
                continue;
            }
            $path = array_merge($currentPath, [$possibleRoute]);
            if ($possibleRoute === 'ZZ') {
                if ($totalCost < $shortestKnownCost) {
                    $paths = ['cost' => $totalCost, 'path' => $path];
                    $shortestKnownCost = $totalCost;
                }
                continue;
            }
            $this->findEnd(
                $possibleRoute,
                $routes,
                ($level + 1),
                ($cost + $routeCost),
                $shortestKnownCost,
                $path,
                $paths
            );
        }
    }
}
