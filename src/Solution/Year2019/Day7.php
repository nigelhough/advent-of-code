<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\IntCode;

class Day7 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $buffer = new IntCode\InputOutputBuffer();
        $computer = new IntCode\Computer($buffer, $buffer);

        $initialMemory = array_map('intval', explode(',', $input));

        $sequences = (new IntCode\SequenceGenerator())->generate();
        $largest = 0;
        foreach ($sequences as $sequence) {
            $previousPhaseValue = 0;
            foreach ($sequence as $value) {
                $buffer->set($value);
                $buffer->set($previousPhaseValue);
                $computer->compute($initialMemory);
                $output = $buffer->read();
                $previousPhaseValue = $output[count($output) - 1];
            }

            $buffer->clear();
            $largest = max($largest, $previousPhaseValue);
        }

        echo "Largest : {$largest}\n";
        $answer1 = $largest;

        $sequences = (new IntCode\SequenceGenerator())->generate(5, 9);
        $largest = 0;
        foreach ($sequences as $sequence) {
            $bufferSets = [];

            // Create Individual Buffer and Computer for each phase so can be run in parallel.
            foreach ($sequence as $value) {
                $buffer = new IntCode\InputOutputBuffer($value);
                $computer = new IntCode\Computer($buffer, $buffer);
                $generator = $computer->stepFeature($initialMemory);
                $bufferSets[] = [$buffer, $generator];
            }
            $p = 0;
            $previousPhaseValue = 0;
            while (true) {
                /** @var IntCode\InputOutputBuffer $buffer */
                $buffer = $bufferSets[$p][0];
                /** @var \Generator $generator */
                $generator = $bufferSets[$p][1];

                $buffer->set($previousPhaseValue);
                $output = [];
                while ($generator->valid()) {
                    $output = $buffer->read();
                    if (!empty($output)) {
                        break;
                    }
                    $generator->next();
                }
                if (!$generator->valid()) {
                    break;
                }
                $previousPhaseValue = array_pop($output);

                $p = $p < count($bufferSets) - 1 ? $p + 1 : 0;
            }

            $largest = max($largest, $previousPhaseValue);
        }

        echo "Feedback Largest : {$largest}\n";
        $answer2 = $largest;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
