<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Asteroid\Builder;
use AdventOfCode\Asteroid\Point;
use AdventOfCode\Solution\AbstractSolution;

class Day10 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $asteroids = Builder::build($input);

        $mostVisible = 0;
        $center = null;
        $vectors = [];
        foreach ($asteroids as $asteroid) {
            $visible = count($asteroid->getVectorsFromPoint($asteroids, $vectors));
            if ($visible > $mostVisible) {
                $mostVisible = $visible;
                $center = $asteroid;
            }
        }
        echo "Most Visible : {$mostVisible}\n";
        $answer1 = $mostVisible;

// Get the angles of all asteroids from center.
// @todo Fix this Vectors by ref crap.
        /** @var Point[] $vectors */
        $vectors = [];
        $keys = $center->getVectorsFromPoint($asteroids, $vectors);

// Loop until all asteroids destroyed (excluding the center)
        $destroyed = 0;
        $totalAsteroids = count($asteroids);
        while ($destroyed < ($totalAsteroids - 1)) {
            // Iterate through the vectors that have asteroids on them (ordered going round a clock face)
            foreach ($keys as $key) {
                if (empty($vectors[(string)$key])) {
                    // Destroyed all on this vector
                    continue;
                }

                // Find the closest Asteroid on this Vector.
                $shortest = PHP_INT_MAX;
                /** @var Point $shortestVectorAsteroid */
                $shortestVectorAsteroid = null;
                $shortestVectorKey = null;
                foreach ($vectors[(string)$key] as $vectorKey => $vectorAsteroid) {
                    $distance = $vectorAsteroid->distance($center);

                    if ($distance < $shortest) {
                        $shortest = $distance;
                        $shortestVectorAsteroid = $vectorAsteroid;
                        $shortestVectorKey = $vectorKey;
                    }
                }

                // Destroy Asteroid
                $destroyed++;
                unset($vectors[(string)$key][$shortestVectorKey]);

                //$outputPoints = [1, 2, 3, 10, 20, 50, 100, 199, 200, 201, 299];
                $outputPoints = [200];
                if (in_array($destroyed, $outputPoints)) {
                    echo "The {$destroyed} asteroid to be vaporized is at {$shortestVectorAsteroid}.\n";
                }
                if ($destroyed === 200) {
                    $answer2 = $shortestVectorAsteroid->getX() . $shortestVectorAsteroid->getY();
                }
            }
        }

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
