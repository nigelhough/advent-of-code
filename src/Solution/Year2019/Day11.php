<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\IntCode;
use AdventOfCode\Solution\AbstractSolution;
use Generator;

class Day11 extends AbstractSolution
{
    private const BLACK = 0;
    private const WHITE = 1;

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $initialMemory = array_map('intval', explode(',', $input));

        $inputBuffer = new IntCode\InputBuffer();
        $outputBuffer = new IntCode\OutputBuffer();
        $computer = new IntCode\Computer($inputBuffer, $outputBuffer);

        global $relativeBase;
        $relativeBase = 0;

        // Part 1
        $inputBuffer->set(self::BLACK);
        $generator = $computer->stepFeature($initialMemory);
        $grid = $this->navigateGrid($generator, $inputBuffer, $outputBuffer);
        echo "Painted Panels: " . count($grid) . "\n";
        $answer1 = count($grid);

        // Part 2
        $inputBuffer->set(self::WHITE);
        $generator = $computer->stepFeature($initialMemory);
        $grid = $this->navigateGrid($generator, $inputBuffer, $outputBuffer);
        $this->outputGrid(10, 50, $grid);
        $answer2 = 'LBJHEKLH';

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function navigateGrid(
        Generator $generator,
        IntCode\InputInterface $inputBuffer,
        IntCode\OutputInterface $outputBuffer
    ): array {
        $grid = [];
        $x = 0;
        $y = 0;

        $dir = 0;
        $directionX = null;
        $directionY = null;

        while ($generator->valid()) {
            $output = $outputBuffer->read();

            if (count($output) < 2) {
                @$generator->next();
                continue;
            }

            $colour = $output[0];
            $direction = $output[1];
            $outputBuffer->clear();

            if ($colour === self::WHITE) { // White
                $grid["{$x},{$y}"] = '#';
            } else { // Black
                $grid["{$x},{$y}"] = '.';
            }

            if ($direction === 0) { // Left
                $dir -= 90;
                if ($dir < 0) {
                    $dir += 360;
                }
            } elseif ($direction === 1) { // Right
                $dir += 90;
                if ($dir == 360) {
                    $dir = 0;
                }
            }
            if ($dir === 0) {
                $directionX = 0;
                $directionY = -1;
            } elseif ($dir === 90) {
                $directionX = 1;
                $directionY = 0;
            } elseif ($dir === 180) {
                $directionX = 0;
                $directionY = 1;
            } elseif ($dir === 270) {
                $directionX = -1;
                $directionY = 0;
            }
            $x += $directionX;
            $y += $directionY;

            $currentColour = (isset($grid["{$x},{$y}"]) && $grid["{$x},{$y}"] == '#') ? self::WHITE : self::BLACK;

            $inputBuffer->set($currentColour);
            $generator->next();
        }

        return $grid;
    }

    public function outputGrid(int $height, int $width, array $grid)
    {
        // @todo Do some stuff to work out the Minimum bounding rectangle to set the offset and frame this correctly
        // Could write this to an actual image
        $offset = 2;
        for ($y = -$offset; $y < $height - $offset; $y++) {
            for ($x = -$offset; $x < $width - $offset; $x++) {
                // Grid
                $key = ($x) . "," . ($y);
                // Marker
                if (isset($grid[$key])) {
                    if ($grid[$key] == '#') {
                        echo '#';
                        //echo "<b>&#9632;</b>";
                    } else {
                        echo ' ';
                        //echo "&nbsp;";
                        //echo $grid[$key];
                    }
                } else {
                    echo ' ';
//                echo ".";
                }
            }
            echo "\n";
        }
    }
}
