<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\IntCode;
use AdventOfCode\IntCode\InputBuffer;
use AdventOfCode\IntCode\OutputBuffer;

class Day5 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }

        $inputBuffer = new InputBuffer();
        $outputBuffer = new OutputBuffer();
        $computer = new IntCode\Computer($inputBuffer, $outputBuffer);

        $initialMemory = array_map('intval', explode(',', $input));

        $value = 1;
        $inputBuffer->set($value);
        $computer->compute($initialMemory, null, null);
        $output = $outputBuffer->read();
        $answer1 = array_pop($output);

        $value = 5;
        $inputBuffer->set($value);
        $outputBuffer->clear();
        $code = $computer->compute($initialMemory, null, null);
        $output = $outputBuffer->read();
        $answer2 = array_pop($output);

        echo "Part1: {$answer1}\nPart2: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
