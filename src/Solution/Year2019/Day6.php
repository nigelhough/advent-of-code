<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day6 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        global $leaders, $relationships, $orbits;
        $orbits = Parser::iterateByLine($input);
        $leaders = [];
        $relationships = [];

        // Process Input to find all orbits.
        foreach ($orbits as $orbit) {
            $orbitParts = explode(')', $orbit);
            $leader = $orbitParts[0];
            $follower = $orbitParts[1];

            $relationships[$follower] = $leader;
            $leaders[] = $leader;
        }

        // Find Center Point. A follower without a leader.
        $center = null;
        foreach ($leaders as $object) {
            if (!isset($relationships[$object])) {
                $center = $object;
                break;
            }
        }

        // Find Furthest Points. All Followers that don't lead anyone
        $furthestCollection = [];
        foreach (array_keys($relationships) as $follower) {
            if (!in_array($follower, $leaders)) {
                $furthestCollection[] = $follower;
            }
        }

        // For all furthest points.
        $orbits = [];
        $trails = [];
        ini_set('xdebug.max_nesting_level', '1000');
        foreach ($furthestCollection as $furthest) {
            // Find the trail from all the followers to the center.
            $trail = $this->findParents($furthest);
            $count = count($trail);

            // Set the orbit distance to center for all nodes.
            $this->setParentOrbits($furthest, $count);
            $trails[$furthest] = $trail;
            $orbits[$furthest] = $count;

            $myOrbits = array_flip(array_reverse($trail));
        }

        $total = 0;
        foreach ($orbits as $orbit) {
            $total += $orbit;
        }
        echo "Total Orbits : {$total}\n";
        $answer1 = $total;

        $orbitA = 'YOU';
        $orbitB = 'SAN';
        $intersect = array_intersect($trails[$orbitA], $trails[$orbitB]);
        $sharedParent = current($intersect);
//        echo "Orbit A : {$orbitA}\n";
//        echo "Orbit B : {$orbitB}\n";
//        echo "Shared Parent : {$sharedParent}\n";

        $orbitalTransfersA = array_flip($trails[$orbitA])[$sharedParent];
        $orbitalTransfersB = array_flip($trails[$orbitB])[$sharedParent];
        $total = $orbitalTransfersA + $orbitalTransfersB;
        echo "Total Transfers : {$total}";
        $answer2 = $total;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function findParents($follower, $trail = [])
    {
        global $relationships;
        $parent = $relationships[$follower] ?? null;

        if ($parent) {
            $trail[] = $parent;
            return $this->findParents($parent, $trail);
        }

        return $trail;
    }

    private function setParentOrbits($follower, $count)
    {
        global $relationships, $orbits;
        $parent = $relationships[$follower] ?? null;

        // If orbits are already set from this point can skip all nodes from this point.
        if (isset($orbits[$parent])) {
            return;
        }

        $count--;
        if ($parent) {
            $orbits[$parent] = $count;
            $this->setParentOrbits($parent, $count);
        }
    }
}
