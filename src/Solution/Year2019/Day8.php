<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\EncodedImage;

class Day8 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $width = 25;
        $height = 6;
        $colourMap = [
//    1 => [0, 0, 0],
//    0 => [255, 255, 255],
            1 => [70, 20, 20],
            0 => [255, 255, 255],
        ];
        $image = new EncodedImage\Image($input, $height, $width, $colourMap);

        echo "Result : {$image->checkSum()}\n";
        $answer1 = $image->checkSum();

        $image->render(30, BASE_DIR . '/public/2019Day8.png');
        echo "<img width='90%' src='/public/2019Day8.png' />";
        $answer2 = 'AHFCB';

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
