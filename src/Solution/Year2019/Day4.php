<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Password;
use AdventOfCode\Solution\AbstractSolution;

class Day4 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        [$min, $max] = array_map('intval', explode('-', $input));

        $answer1 = $this->countValidInRange(
            new Password\Validator(Password\Validator::PREVENT_DECREASING),
            $min,
            $max
        );
        $answer2 = $this->countValidInRange(
            new Password\Validator(Password\Validator::PREVENT_ALL),
            $min,
            $max
        );
        echo "Valid non decreasing: {$answer1}\nAll Valid: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function countValidInRange(Password\Validator $validator, int $min, int $max): int
    {
        $counter = 0;
        for ($i = $min; $i <= $max; $i++) {
            if ($validator->isValid($i)) {
                $counter++;
            }
        }
        return $counter;
    }
}
