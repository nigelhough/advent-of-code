<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2019;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\FuelCounterUpper;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $masses = Parser::iterateNumbersByLine($input);

        $counter = new FuelCounterUpper\Counter();

        $answer1 = $fuel = $counter->count($masses);
        echo "Module Fuel: {$fuel}\n";

        $answer2 = $totalFuel = $counter->recursiveCount($masses);
        echo "Total Fuel: {$totalFuel}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
