<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day4 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        $input = 'iwrupvqb';
        if (isset($_GET['debug'])) {
            $input = 'abcdef';
        }

        $i = 1;
        $hash = md5($input . $i);

        while (substr($hash, 0, 6) !== '000000') {
            if (($i % 10000) === 0) {
                echo ".";
            }
            if (($i % 600000) === 0) {
                echo "\n";
            }
            $i++;
            $hash = md5($input . $i);
        }

        echo "\n\n";
        echo "{$input}, {$i}, {$hash}";
        return null;
    }
}
