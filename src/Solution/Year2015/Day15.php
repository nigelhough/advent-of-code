<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day15 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3';
        }
        $input = str_replace([': capacity ', ' durability ', ' flavor ', ' texture ', ' calories '], [','], $input);

        $ingredients = [];
        foreach (Parser::iterateByLine($input) as $ingredientDetails) {
            [$ingredient, $capacity, $durability, $flavor, $texture, $calories] = explode(',', $ingredientDetails);

            $ingredients[$ingredient] = [
                'capacity'   => $capacity,
                $ingredients,
                'durability' => $durability,
                'flavor'     => $flavor,
                'texture'    => $texture,
                'calories'   => $calories,
            ];
        }

        $availableSpoons = 100;
        $i = 0;
        $largest = 0;
        $largestList = [];
        foreach ($this->ingredientsCombinations($availableSpoons, array_keys($ingredients)) as $list) {
            $score = $this->calculateScore($this->listToQuantities($list, $ingredients));
            $calories = $this->calculateCalories($this->listToQuantities($list, $ingredients));
            if ($calories !== 500) {
                continue;
            }
            if ($score > $largest) {
                $largestList = $list;
                $largest = $score;
            }
            $i++;
        }

        echo "Largest Score is {$largest} (" . implode(',', $largestList) . ") (" . implode(
            ',',
            array_keys($largestList)
        ) . ").\n";
        return null;
    }

    public function ingredientsCombinations(int $availableSpoons, array $ingredients, array $ingredientList = [])
    {
        if (count($ingredients) === 1) {
            $ingredientList[$ingredients[array_key_first($ingredients)]] = $availableSpoons;
            yield $ingredientList;
            return;
        }
        if ($availableSpoons === 0) {
            foreach ($ingredients as $ingredient) {
                $ingredientList[$ingredient] = 0;
            }
            yield $ingredientList;
            return;
        }
        if (count($ingredients) === 0) {
            return;
        }

        $ingredient = $ingredients[array_key_first($ingredients)];
        for ($i = 0; $i <= $availableSpoons; $i++) {
            $remaining = $availableSpoons - $i;
            $remainingIngredients = array_diff($ingredients, [$ingredient]);
            $newIngredients = $ingredientList;
            $newIngredients[$ingredient] = $i;
            yield from $this->ingredientsCombinations($remaining, $remainingIngredients, $newIngredients);
        }
    }

    public function calculateScore(array $ingredients): int
    {
        return max(0, array_sum(array_column($ingredients, 'capacity'))) *
            max(0, array_sum(array_column($ingredients, 'durability'))) *
            max(0, array_sum(array_column($ingredients, 'flavor'))) *
            max(0, array_sum(array_column($ingredients, 'texture')));
    }

    public function listToQuantities(array $list, array $ingredients): array
    {
        $quantities = [];
        foreach ($list as $ingredient => $quantity) {
            $quantities[$ingredient] = $this->ingredientAtQuantity($ingredients[$ingredient], $quantity);
        }
        return $quantities;
    }

    public function ingredientAtQuantity(array $ingredient, int $quantity): array
    {
        return [
            'capacity'   => $ingredient['capacity'] * $quantity,
            'durability' => $ingredient['durability'] * $quantity,
            'flavor'     => $ingredient['flavor'] * $quantity,
            'texture'    => $ingredient['texture'] * $quantity,
            'calories'   => $ingredient['calories'] * $quantity,
        ];
    }

    public function calculateCalories(array $ingredients): int
    {
        return max(0, array_sum(array_column($ingredients, 'calories')));
    }
}
