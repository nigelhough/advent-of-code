<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day6 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'turn on 0,0 through 999,999
toggle 0,0 through 999,0
turn off 499,499 through 500,500';
        }
        $input = str_replace(['turn on ', 'turn off ', 'toggle ', ' through '], ['n', 'f', 't', ':'], $input);

        $map = new Map();
        $map2 = new Map();
        foreach (Parser::iterateByLine($input) as $instruction) {
            $command = substr($instruction, 0, 1);
            [$start, $end] = explode(':', substr($instruction, 1));
            //echo "{$command} ({$start}) ({$end})\n";

            if ($command === 'n') {
                $map->markRange(Coord::fromString($start), Coord::fromString($end), 'X');
                $map2->incrementRange(Coord::fromString($start), Coord::fromString($end));
            } elseif ($command === 'f') {
                $map->markRange(Coord::fromString($start), Coord::fromString($end), ' ');
                $map2->decrementRange(Coord::fromString($start), Coord::fromString($end));
            } elseif ($command === 't') {
                $map->toggleRange(Coord::fromString($start), Coord::fromString($end), 'X', ' ');
                $map2->incrementRange(Coord::fromString($start), Coord::fromString($end), 2);
            }
            //echo $map->plot();
        }
        $on = $map->locate(function ($location) {
            return $location === 'X';
        });
        $brightness = array_sum(
            $map2->locate(function ($location) {
                return ((int) $location) > 0;
            })
        );

        var_dump(count($on));
        // 406662 too low.

        var_dump($brightness);
        // 14192189 too low
        // 14364040 too low
        return null;
    }
}
