<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\SolutionInterface;

class Day8 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '""
"abc"
"aaa\"aaa"
"\x27"';
        }

        $totalChrs = 0;
        $totalMemoryChrs = 0;
        foreach (Parser::iterateByLine($input) as $line) {
            $totalChrs += strlen($line);
            eval("\$line = {$line};");
            $totalMemoryChrs += strlen($line);
        }
        echo "Total Characters {$totalChrs}. " .
            "In Memory {$totalMemoryChrs}. " .
            "Difference : " . ($totalChrs - $totalMemoryChrs) . "\n\n";

        $totalChrs = 0;
        $encodedChrs = 0;
        foreach (Parser::iterateByLine($input) as $line) {
            $encoded = '"' . addslashes($line) . '"';
            $totalChrs += strlen($line);
            $encodedChrs += strlen($encoded);
        }
        echo "Total Characters {$totalChrs}. " .
            "Encoded {$encodedChrs}. " .
            "Difference : " . ($encodedChrs - $totalChrs) . "\n\n";
        return null;
    }
}
