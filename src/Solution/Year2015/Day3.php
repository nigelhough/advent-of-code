<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day3 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '^>v<';
        }
        if (isset($_GET['debug'])) {
            $input = '^v^v^v^v^v';
        }

        $map = new Map();
        $santaLocation = new Coord(0, 0);
        $robotLocation = new Coord(0, 0);
        $map->mark($santaLocation, '1');

        foreach (str_split($input) as $index => $step) {
            if ($index % 2) {
                $santaLocation = $santaLocation->moveAlongBearing(Bearing::getFromArrowDirection($step));
                $currentCount = $map->get($santaLocation) ?? 0;
                $map->mark($santaLocation, $currentCount + 1);
            } else {
                $robotLocation = $robotLocation->moveAlongBearing(Bearing::getFromArrowDirection($step));
                $currentCount = $map->get($robotLocation) ?? 0;
                $map->mark($robotLocation, $currentCount + 1);
            }
        }

        $visitedHouses = $map->locate(function ($count) {
            return $count > 0;
        });

        echo "Houses Visited " . count($visitedHouses) . "\n";
        return null;
    }
}
