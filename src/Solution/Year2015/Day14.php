<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day14 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.';
        }
        $input = str_replace(
            [' can fly ', ' km/s for ', ' seconds, but then must rest for ', ' seconds.'],
            [',', ',', ',', ''],
            $input
        );

        $splits = [];
        foreach (Parser::iterateByLine($input) as $reindeerDetails) {
            [$reindeer, $speed, $duration, $rest] = explode(',', $reindeerDetails);
            $speed = (int)$speed;
            $duration = (int)$duration;
            $rest = (int)$rest;

            $split = [];
            $furthest = 0;
            $travelled = 0;
            for ($i = 1; $i <= $duration; $i++) {
                $split[$i] = $speed * $i;
                $furthest = $speed * $i;
                $travelled = $i;
            }
            for ($i = 1; $i <= $rest; $i++) {
                $split[$travelled + $i] = $furthest;
            }
            $splits[$reindeer] = $split;
        }

        $seconds = 2503;
        $winners = $this->getWinningReindeer($splits, $seconds);
        echo "Furthest is " .
            array_key_first($winners) .
            " (" .
            implode(",", $winners[array_key_first($winners)]) .
            ").\n\n";

        $points = [];
        for ($s = 1; $s <= $seconds; $s++) {
            $winners = $this->getWinningReindeer($splits, $s);
            foreach ($winners[array_key_first($winners)] as $winner) {
                $points[$winner] = ($points[$winner] ?? 0) + 1;
            }
            echo "Furthest is " .
                array_key_first($winners) .
                " (" . implode(",", $winners[array_key_first($winners)]) . ").\n\n";
        }
        arsort($points);
        echo "Winning Reindeer on Points was " .
            array_key_first($points) .
            " with " .
            $points[array_key_first($points)]
            . " points.";
        return null;
    }

    public function getWinningReindeer(array $splits, int $seconds): array
    {
        $furthest = 0;
        $furthestReindeer = [];
        foreach ($splits as $reindeer => $split) {
            $fullIterations = floor($seconds / count($split));
            $remaining = ($seconds % count($split));
            $travelled = (max($split) * $fullIterations) + ($remaining === 0 ? 0 : $split[$remaining]);
            //echo "{$reindeer} travelled {$travelled} in {$seconds} seconds.\n";

            if ($travelled > $furthest) {
                $furthest = $travelled;
                $furthestReindeer = [$reindeer];
            } elseif ($travelled === $furthest) {
                $furthestReindeer[] = $reindeer;
            }
        }

        return [$furthest => $furthestReindeer];
    }
}
