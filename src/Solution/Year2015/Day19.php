<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day19 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
//        ini_set('max_execution_time', (string) (60 * 60 * 6));
//        ini_set('memory_limit', '4G');

//        if (isset($_GET['debug'])) {
//            $input = 'e => H
        //e => O
        //H => HO
        //H => OH
        //O => HH
//
        //HOHOHO';
//        }

        [$replacements, $subject] = Parser::iterateByGroup($input);
//        $replacementsReversed = array_map(function ($r) {
//            return array_reverse(explode(' => ', $r));
//        }, Parser::iterateByLine($replacements));
        $replacements = array_map(function ($r) {
            return explode(' => ', $r);
        }, Parser::iterateByLine($replacements));

        $target = $subject;
        $part2 = 0;
        while ($target != 'e') {
            $tmp = $target;
            foreach ($replacements as $replacement) {
                [$a, $b] = $replacement;
                if (!str_contains($target, $b)) {
                    continue;
                }

                $pos = strpos($target, $b);
                $target = substr_replace($target, $a, $pos, strlen($b));
                $part2 += 1;
            }
            echo "$target\n";
            if ($tmp == $target) {
                $target = $subject;
                $part2 = 0;
                shuffle($replacements);
            }
        }

        var_dump($part2);


//        $found = [];
//        $this->possibilities($subject, $replacements, $found);
//        //var_dump($found);
//        var_dump(count($found));
//
//        $found = $this->find('e', $subject, $replacements);
//        var_dump($found);

//        echo "\n\n\n";

//        // Should try Reversing this. Start from the subject work back to e.
//        $found = $this->find($subject,'e', $replacementsReversed);
//        var_dump($found);
        return null;
    }

    public function find2(string $subject, string $target, array $replacements, $steps = 0)
    {
        foreach ($replacements as $replacement) {
            [$from, $to] = $replacement;
            $offset = 0;
            while (strpos($subject, $from, $offset) !== false) {
                $pos = strpos($subject, $from, $offset);
                $start = substr($subject, 0, $pos);
                $end = substr($subject, $pos + strlen($from));
                $new = $start . $to . $end;

                if ($new === $target) {
                    return $steps + 1;
                }
                if (strlen($new) > strlen($target)) {
                    return null;
                }

                $totalSteps = $this->find($new, $target, $replacements);
                if ($totalSteps !== null) {
                    return $totalSteps;
                }

                //echo "{$from} >> {$to}, {$pos}, {$new} ({$start}.[{$to}].{$end})\n";
                $offset = $pos + 1;
            }
        }
    }

    public function find(string $subject, string $target, array $replacements)
    {
        $allFound = [$subject => 0];
        $found = [$subject => 0];

        while (!empty($found)) {
            $key = array_key_first($found);
            $value = array_shift($found);

            echo "{$key} ({$value})\n";

            $newFound = [];
            $this->possibilities($key, $replacements, $newFound);

            if (isset($newFound[$target])) {
                return $value + 1;
            }
            foreach ($newFound as $newFinding) {
                if (isset($allFound[$newFinding])) { // || strlen($newFinding) > strlen($target)) {
                    // Don't repeat checking the same value found via another path.
                    continue;
                }
                $allFound[$newFinding] = $value + 1;
                $found[$newFinding] = $value + 1;
            }
        }
    }

    public function possibilities(string $subject, array $replacements, array &$found)
    {
        foreach ($replacements as $replacement) {
            [$from, $to] = $replacement;
            $offset = 0;
            while (strpos($subject, $from, $offset) !== false) {
                $pos = strpos($subject, $from, $offset);
                $start = substr($subject, 0, $pos);
                $end = substr($subject, $pos + strlen($from));
                $new = $start . $to . $end;

                //echo "{$from} >> {$to}, {$pos}, {$new} ({$start}.[{$to}].{$end})\n";
                $found[$new] = $new;
                $offset = $pos + 1;
            }
        }
    }
}
