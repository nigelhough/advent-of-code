<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\RPGSimulator\BattleArena\Standard;
use AdventOfCode\RPGSimulator\Player\Player;
use AdventOfCode\RPGSimulator\Store;
use AdventOfCode\Solution\AbstractSolution;

class Day21 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // Cost  Damage  Armor; no dual-wielding.
        $store = new Store([
                               // You must buy exactly one weapon
                               'Weapons' => [
//                ['none', 0, 0, 0],
        ['Dagger', 8, 4, 0],
        ['Shortsword', 10, 5, 0],
        ['Warhammer', 25, 6, 0],
        ['Longsword', 40, 7, 0],
        ['Greataxe', 74, 8, 0],
                               ],
                               // Armor is optional, but you can't use more than one
                               'Armor'   => [
                                   ['none', 0, 0, 0],
                                   ['Leather', 13, 0, 1],
                                   ['Chainmail', 31, 0, 2],
                                   ['Splintmail', 53, 0, 3],
                                   ['Bandedmail', 75, 0, 4],
                                   ['Platemail', 102, 0, 5],
                               ],
                               // You can buy 0-2 rings (at most one for each hand).
                               'Rings'   => [
                                   ['none L', 0, 0, 0],
                                   ['none R', 0, 0, 0],
                                   ['Damage +1', 25, 1, 0],
                                   ['Damage +2', 50, 2, 0],
                                   ['Damage +3', 100, 3, 0],
                                   ['Defense +1', 20, 0, 1],
                                   ['Defense +2', 40, 0, 2],
                                   ['Defense +3', 80, 0, 3],
                               ],
                           ]);

        $boss = Player::fromString('Boss', $input);
        $battleArena = new Standard();

        $lowestCost = PHP_INT_MAX;
        foreach ($store->iterateGoodsCombinations() as $goods) {
            [$cost, $damage, $armour] = $goods;
            if ($cost >= $lowestCost) {
                // Too Expensive.
                continue;
            }
            $player = new Player('Player 1', 100, $damage, $armour);
            $winner = $battleArena->battle($player, $boss);
            if ($player !== $winner) {
                // Not a Winner.
                continue;
            }
            if ($cost < $lowestCost) {
                $lowestCost = $cost;
            }
        }
        echo "The Lowest Cost for victory is {$lowestCost}\n";

        $highestCost = 0;
        foreach ($store->iterateGoodsCombinations() as $goods) {
            [$cost, $damage, $armour] = $goods;
            if ($cost <= $highestCost) {
                // Too Cheap.
                continue;
            }
            $player = new Player('Player 1', 100, $damage, $armour);
            $winner = $battleArena->battle($player, $boss);
            if ($boss !== $winner) {
                // Not a Loser.
                continue;
            }
            if ($cost > $highestCost) {
                $highestCost = $cost;
            }
        }
        echo "The Highest Cost for Defeat is {$highestCost}\n";
        return null;
    }
}
