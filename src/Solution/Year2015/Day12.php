<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day12 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
//            $input = '{"a":{"b":4},"c":-1}';
//            $input = '{"d":"red","e":[1,2,3,4],"f":5}';
            //$input = '{"d":"green","e":[1,2,3,4],"f":5}';
            $input = '[1,{"c":"red","b":2},3]';
        }

        $number = 0;
        $input = json_decode($input, true);
        array_walk_recursive($input, function ($value) use (&$number) {
            $number += is_numeric($value) ? $value : 0;
        });
        echo "Total of all numbers in JSON {$number} (" . number_format($number) . ").\n";

        $total = 0;
        $this->evaluate(json_decode($input), $total);
        echo "All numbers in JSON excluding red objects {$total} (" . number_format($total) . ").\n";
        return null;
    }

    public function evaluate($object, &$total)
    {
        if (gettype($object) === 'object' && in_array('red', (array) $object)) {
            // Ignore Objects that contain a property with the value red.
            return;
        }

        foreach ((array) $object as $subItem) {
            if (is_numeric($subItem)) {
                $total += $subItem;
                continue;
            }
            if (!in_array(gettype($subItem), ['array', 'object'])) {
                continue;
            }
            $this->evaluate($subItem, $total);
        }
    }
}
