<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day20 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '960';
        }
        $target = (int) $input;

//        for ($n = 1; $n < 102; $n++) {
//            echo "$n :: " . $this->getPresents($n) . " , " . $this->getPresents2($n) . "\n";
//        }
//        return;

//        $binary = '1';
//        $number = bindec($binary);
//        $numbers = [];
//        while (($presents = $this->getPresents($number)) < $target) {
//            $binary .= '1';
//            $number = bindec($binary) + 1;
//            $numbers[] = $number;
//        }
//        $numbers = array_reverse($numbers);
//        $lessThan = $numbers[1];
//        $increment = $numbers[2];
//
//        var_dump($lessThan);
//        var_dump($increment);
//        $this->getPresents($lessThan + $increment);
//        echo "\n\n";

        $start = 776160 - 10000;
        $max = $start + 10000000;
        $increment = 2;
        $largest = 0;
        $largestHouse = 0;
        for ($i = $start; $i < $max; $i += $increment) {
            $presents = $this->getPresents2($i);
            if ($presents > $largest) {
                $diff = ($i - $largestHouse);
                $largest = $presents;
                $largestHouse = $i;
                echo "House " . str_pad(number_format($i), 9) . " gets " . str_pad(number_format($presents), 15) . ", ";
                echo str_pad(number_format($diff), 9) . ", " . decbin($i) . " , " . sqrt($i);
                echo ", " . decbin((int) ($i / 11)) . " , " . sqrt((int) ($i / 11)) . "\n";
            }
            if ($presents > $target) {
                echo "House " . str_pad(number_format($i), 9) . " gets " . str_pad(number_format($presents), 15) . "\n";
                break;
            }
        }

        //your answer is too high
        //You guessed 1965600.
        return null;
    }

    public function getPresents2(int $houseNumber)
    {
        $presents = $houseNumber * 11;
        //$presents = 0;
        //echo "House {$i} (".floor($i/2).")\n";
        for ($n = floor($houseNumber / 2); $n > 0; $n--) {
            if ($houseNumber > ($n * 50)) {
                continue;
            }
            if (($houseNumber % $n) === 0) {
//                echo "{$n} " . ($n * 50) . " - " . $houseNumber % $n . "\n";
                $presents += $n * 11;
            }
        }
//        echo "House {$houseNumber} gets {$presents} (2)\n";
        return $presents;
    }

    public function getPresents(int $houseNumber)
    {
        $presents = $houseNumber * 10;
        //$presents = 0;
        //echo "House {$i} (".floor($i/2).")\n";
        for ($n = floor($houseNumber / 2); $n > 0; $n--) {
            if (($houseNumber % $n) === 0) {
//                echo "{$n} - " . $houseNumber % $n . "\n";
                $presents += $n * 10;
            }
        }
//        echo "House {$houseNumber} gets {$presents}\n";
        return $presents;
    }
}
