<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\RPGSimulator\Player\Player;
use AdventOfCode\RPGSimulator\Player\Wizard;
use AdventOfCode\Solution\AbstractSolution;

class Day22 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // Part 1
        // (You guessed 973.)
        // your answer is too high

        // Part 2
        // You guessed 1422.
        // your answer is too high
        // You guessed 1369.
        // your answer is too high

        $boss = Player::fromString('Boss', $input);
        $wizard = new Wizard(new Player('Player', 50, 0, 0), 500);

//        $boss = new Player('Boss', 14, 8, 0);
//        $wizard = new Wizard(new Player('Wizard', 10, 0, 0), 250);

        $leastManaSpent = PHP_INT_MAX;
        $this->battle($wizard, $boss, 1, 0, [], $leastManaSpent);

        if (php_sapi_name() === 'cli') {
            echo "\n\n";
        }
        echo "Least spent Mana is {$leastManaSpent}";
        return null;
    }

    public function battle(
        Wizard $wizard,
        Player $boss,
        int $turn,
        int $manaSpent,
        array $effects,
        int &$leastManaSpent
    ) {
        if ($turn > 100) {
            exit();
            // Too many turns lets discount this path.
            return;
        }

        // On the next run through the game, you increase the difficulty to hard.
        // At the start of each player turn (before any other effects apply), you lose 1 hit point.
        // If this brings you to or below 0 hit points, you lose.
        $wizard = clone $wizard;
        $boss = clone $boss;
        $wizard->setHitPoints($wizard->getHitPoints() - 1);
        if ($wizard->getHitPoints() <= 0) {
            if (php_sapi_name() === 'cli') {
                echo "BOSS WINS\n";
            }
            // Boss Wins
            // Don't continue this path
            return;
        }

//        $validEffects = $effects;
//        foreach ($validEffects as $turnSince => $effect) {
//            $until = $turnSince + $effect->getTurnDuration();
//            if ($until <= $turn)
//            {
//                unset($validEffects[$turnSince]);
//                // Lapsed effect.
//                continue;
//            }
//        }

        // Effects apply at the start of both the player's turns and the boss' turns.
        // Apply Wizard Effects Before filtering spells as mana may increase and afford more.
        $preEffectArmour = $wizard->getArmour();
        foreach ($effects as $turnSince => $effect) {
            $until = $turnSince + $effect->getTurnDuration();
            if ($until < $turn) {
                unset($effects[$turnSince]);
                // Lapsed effect.
                continue;
            }
            if (php_sapi_name() === 'cli') {
                echo "Effect: until {$until} " . $effect . "\n";
            }
            $wizard = $wizard->applyEffect($effect);
            $boss = $boss->takeEffect($effect);
            if ($until == $turn) {
                unset($effects[$turnSince]);
            }
        }
        if ($boss->getHitPoints() <= 0) {
            if (php_sapi_name() === 'cli') {
                echo "PLAYER WINS\n";
            }
            // Wizard Wins
            if ($manaSpent < $leastManaSpent) {
                $leastManaSpent = $manaSpent;
            }
            // Don't continue this path
            return;
        }

        $wizardSpells = $wizard->getAvailableActions();
        if (count($effects)) {
            foreach ($wizardSpells as $key => $spell) {
                if (!$spell->getEffect()) {
                    continue;
                }
                if (in_array((string)$spell->getEffect(), array_map('strval', $effects))) {
                    unset($wizardSpells[$key]);
                }
            }
        }

//        // Force the first spell to be poison for testing.
//        if($turn === 1) {
//            $wizardSpells = array_filter(
//                $wizardSpells,
//                function(Spell $spell) { return $spell->getName() === 'Recharge';}
//            );
//        }
//        if($turn === 3) {
//            $wizardSpells = array_filter(
//                $wizardSpells,
//                function(Spell $spell) { return $spell->getName() === 'Shield';}
//            );
//        }
//        if($turn === 5) {
//            $wizardSpells = array_filter(
//                $wizardSpells,
//                function(Spell $spell) { return $spell->getName() === 'Drain';}
//            );
//        }
//        if($turn === 7) {
//            $wizardSpells = array_filter(
//                $wizardSpells,
//                function(Spell $spell) { return $spell->getName() === 'Poison';}
//            );
//        }

        if (empty($wizardSpells)) {
            // Wizard Has no possible spells.
            // Can't win down this path.
            return;
        }

        foreach ($wizardSpells as $spell) {
            $newManaSpent = $manaSpent + $spell->getManaCost();
            if ($newManaSpent > $leastManaSpent) {
                // If this goes down a path that costs more than the least spent Mana.
                // This will not be the cheapest.
                continue;
            }

            if (php_sapi_name() === 'cli') {
                echo "\n\n-- Player turn ({$turn}) {$newManaSpent}--\n";
                echo $wizard . "\n";
                echo $boss . "\n";
            }

            $newWizard = clone $wizard;
            $newBoss = clone $boss;
            $newEffects = $effects;

            if (php_sapi_name() === 'cli') {
                echo "Player casts " . $spell . "\n";
            }
            $newWizard = $newWizard->takeAction($spell);

            // Add new Effects.
            if ($effect = $spell->getEffect()) {
                $newEffects[$turn] = $effect;
            }
            $newBoss = $newBoss->applyAction($spell);

            if ($newBoss->getHitPoints() <= 0) {
                if (php_sapi_name() === 'cli') {
                    echo "PLAYER WINS\n";
                }
                // Wizard Wins
                if ($newManaSpent < $leastManaSpent) {
                    $leastManaSpent = $newManaSpent;
                }
                // Don't continue this path
                continue;
            }

            // Reset Armour boost after player turn.
            $newWizard->setArmour($preEffectArmour);

            // Boss Turn.
            $action = $newBoss->getAvailableActions()[0];
            if (php_sapi_name() === 'cli') {
                echo "\n\n-- Boss turn (" . ($turn + 1) . ") {$newManaSpent}--\n";
                echo $newWizard . "\n";
                echo $newBoss . "\n";
                echo "Boss " . $action . "\n";
            }
            // Apply Effects.
            $preEffectArmour = $newWizard->getArmour();
            foreach ($newEffects as $turnSince => $effect) {
                $until = $turnSince + $effect->getTurnDuration();
                if ($until < ($turn + 1)) {
                    unset($newEffects[$turnSince]);
                    // Lapsed effect.
                    continue;
                }
                if (php_sapi_name() === 'cli') {
                    echo "Effect: until {$until} " . $effect . "\n";
                }
                $newWizard = $newWizard->applyEffect($effect);
                $newBoss = $newBoss->takeEffect($effect);
            }
            //echo $newWizard . "\n";
            $newWizard = $newWizard->applyAction($action);
//            echo $newWizard . "\n";
//            echo $newBoss . "\n";

            if ($newBoss->getHitPoints() <= 0) {
                if (php_sapi_name() === 'cli') {
                    echo "PLAYER WINS\n";
                }
                // Wizard Wins
                if ($newManaSpent < $leastManaSpent) {
                    $leastManaSpent = $newManaSpent;
                    //exit;
                }
                // Don't continue this path
                continue;
            }
            if ($newWizard->getHitPoints() <= 0) {
                if (php_sapi_name() === 'cli') {
                    echo "BOSS WINS\n";
                }
                // Boss Wins
                // Don't continue this path
                continue;
            }

            $newWizard->setArmour($preEffectArmour);

            $this->battle($newWizard, $newBoss, $turn + 2, $newManaSpent, $newEffects, $leastManaSpent);
        }
    }
}
