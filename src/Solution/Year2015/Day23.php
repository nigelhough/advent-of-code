<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day23 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // You guessed 2.
        // You guessed 0.

        if (isset($_GET['debug'])) {
            $input = 'hlf a
tpl b
jio b, +2
jie a, +2
jmp +2
inc a
inc a
inc b
inc b';
        }
        $instructions = Parser::iterateByLine($input);

        $register = ['a' => 1, 'b' => 0];
        $index = 0;

        while (isset($instructions[$index])) {
            [$instruction, $bits] = explode(' ', $instructions[$index], 2);
            //echo "{$index} '{$instructions[$index]}' {$register['a']} {$register['b']}\n";

            if ($instruction === 'hlf') {
                $register[$bits] = $register[$bits] / 2;
            } elseif ($instruction === 'tpl') {
                $register[$bits] = $register[$bits] * 3;
            } elseif ($instruction === 'inc') {
                $register[$bits] = $register[$bits] + 1;
            } elseif ($instruction === 'jmp') {
                $index += (int) $bits;
                continue;
            } elseif ($instruction === 'jie') {
                [$subject, $jump] = explode(', ', $bits);
                if ($register[$subject] % 2 === 0) {
                    $index += (int) $jump;
                    continue;
                }
            } elseif ($instruction === 'jio') {
                [$subject, $jump] = explode(', ', $bits);
                if ($register[$subject] === 1) { //$register[$subject] % 2 !== 0) {
                    $index += (int) $jump;
                    continue;
                }
            } else {
                throw new Exception('Unknown Instruction');
            }
            $index++;
        }
        var_dump($register);
        return null;
    }
}
