<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day16 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $known = [
            'children'    => 3,
            'cats'        => 7,
            'samoyeds'    => 2,
            'pomeranians' => 3,
            'akitas'      => 0,
            'vizslas'     => 0,
            'goldfish'    => 5,
            'trees'       => 3,
            'cars'        => 2,
            'perfumes'    => 1,
        ];
        $sues = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$key, $values] = explode(':', $line, 2);
            foreach (explode(',', trim($values)) as $value) {
                [$attribute, $qty] = explode(':', str_replace(' ', '', $value));
                $sues[$key][$attribute] = (int) $qty;
            }
        }

        // Part 1.
        $part1Sues = array_filter($sues, function ($attributes) use ($known) {
            foreach ($attributes as $key => $qty) {
                if (!isset($known[$key])) {
                    continue;
                }
                if ($qty !== $known[$key]) {
                    return false;
                }
            }

            return true;
        });
        echo "Part 1 is " . array_key_first($part1Sues) . "\n";

        // Part 2.
        $part2Sues = array_filter($sues, function ($attributes) use ($known) {
            foreach ($attributes as $key => $qty) {
                if (!isset($known[$key])) {
                    continue;
                }

                $isValid = null;
                if (in_array($key, ['cats', 'trees'])) {
                    $isValid = $qty > $known[$key];
                } elseif (in_array($key, ['pomeranians', 'goldfish'])) {
                    $isValid = $qty < $known[$key];
                } else {
                    $isValid = $qty === $known[$key];
                }

                if (!$isValid) {
                    return false;
                }
            }

            return true;
        });
        echo "Part 2 is " . array_key_first($part2Sues);
        return null;
    }
}
