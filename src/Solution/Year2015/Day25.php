<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day25 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '';
        }
        echo "{$input}\n";

        $position = $this->getPosition(2981, 3075);
        //$position = $this->getPosition(4, 5);
        echo "Position is {$position} (" . number_format($position) . ")\n\n";

        $n = 20151125;
        $i = 1;
        echo "{$i} :: {$n}\n";
        while ($i < $position) {
            $n = (($n * 252533) % 33554393);
            //echo "{$i} :: {$n}\n";
            $i++;
        }
        echo "{$i} :: {$n}\n";
        return null;
    }

    public function getPosition(int $row, int $column): int
    {
        $targetRow = $column + ($row - 1);
        $n = 1;
        for ($i = 1; $i < $targetRow; $i++) {
            $n += $i;
            //echo "{$i} ".($n)."\n";
        }
        return $n + ($column - 1);
    }
}
