<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day24 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // Part2
        // your answer is too high
        // You guessed 139699414.

        ini_set('memory_limit', '4G');
        if (php_sapi_name() !== 'cli') {
            $input = '1
2
3
4
5
7
8
9
10
11';
        }
        $presents = Parser::iterateByLine($input);
        $results = [];
        $groups = ['passenger' => [], 'leftSide' => [], 'rightSide' => []];
        $groups = ['passenger' => [], 'leftSide' => [], 'rightSide' => [], 'trunk' => []];

        $targetWeight = (array_sum(Parser::iterateByLine($input)) / count($groups));
        $smallestPassengerLoad = PHP_INT_MAX;
        $smallestQuantumEntanglement = PHP_INT_MAX;
        $this->solve($groups, $presents, $targetWeight, $smallestPassengerLoad, $smallestQuantumEntanglement, $results);
        echo "\n\n";
        var_dump($smallestQuantumEntanglement);

//        // Get the smallest possible grouping of parcels.
//        $smallestGrouping = PHP_INT_MAX;
//        foreach ($results as $groups) {
//            foreach ($groups as $group) {
//                $smallestGrouping = min($smallestGrouping, count($group));
//            }
//        }
//        // Filter any Results that don't have the smallest grouping.
//        $results = array_filter(
//            $results,
//            function (array $groups) use ($smallestGrouping) {
//                foreach ($groups as $group) {
//                    if (count($group) === $smallestGrouping) {
//                        return true;
//                    }
//                }
//                return false;
//            }
//        );
//
//        $smallestQuantumEntanglement = PHP_INT_MAX;
//        foreach ($results as $groups) {
//            $quantumEntanglement = array_product($groups['passenger']);
//            $smallestQuantumEntanglement = min($quantumEntanglement, $smallestQuantumEntanglement);
//        }
//
//        echo count($results) . "\n\n";
//        echo $smallestGrouping . "\n\n";
//        echo $smallestQuantumEntanglement . "\n\n";
//        var_dump($results);
        return null;
    }

    public function solve(
        $groups,
        $presents,
        $targetWeight,
        &$smallestPassengerLoad,
        &$smallestQuantumEntanglement,
        &$results
    ) {
        if (count($presents) == 0) {
            //$weight = array_sum($groups[array_key_first($groups)]);
            $weight = $targetWeight;
            foreach ($groups as $group) {
                if (array_sum($group) != $weight) {
                    return;
                }
            }
            if (count($groups['passenger']) > $smallestPassengerLoad) {
                // This has more passenger load than a previous result, so won't be out smallest.
                return;
            }
            if (array_product($groups['passenger']) >= $smallestQuantumEntanglement) {
                // This group already meets or exceeds the Smallest Quantum Entanglement, it can't be the lowest
                return;
            }
            echo array_product($groups['passenger']) . "\n";
            $smallestPassengerLoad = count($groups['passenger']);
            $smallestQuantumEntanglement = array_product($groups['passenger']);
//            $results[] = $groups;
            // Done.
            return;
        }
        $present = array_pop($presents);

        foreach ($groups as $key => $group) {
            $newGroups = $groups;

            $newGroups[$key][] = $present;
            if (array_sum($newGroups[$key]) > $targetWeight) {
                // Exceeded the target weight, this path won't work.
                continue;
            }
            if (count($newGroups['passenger']) > $smallestPassengerLoad) {
                // This is permutation has more presents in the passenger section that a previous, can't be optimal.
                continue;
            }
            if (array_product($newGroups['passenger']) >= $smallestQuantumEntanglement) {
                // This group already meets or exceeds the Smallest Quantum Entanglement, it can't be the lowest
                return;
            }

            $this->solve(
                $newGroups,
                $presents,
                $targetWeight,
                $smallestPassengerLoad,
                $smallestQuantumEntanglement,
                $results
            );
        }
    }
}
