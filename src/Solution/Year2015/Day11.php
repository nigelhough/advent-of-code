<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day11 extends AbstractSolution
{
    private const ASCII_A = 97;

    private const ASCII_Z = 122;

    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'ghijklmn';
        }

        $i = 0;
        while (!$this->isValidPassword($input)) {
            //echo "{$i} {$input} (INVALID)\n";
            $input = $this->increment($input);
            $i++;
        }
        echo "1st Valid Password {$input}\n";

        // Part 2
        $input = $this->increment($input);
        $i = 0;
        while (!$this->isValidPassword($input)) {
            //echo "{$i} {$input} (INVALID)\n";
            $input = $this->increment($input);
            $i++;
        }
        echo "2nd Valid Password {$input}\n";
        return null;
    }

    public function isValidPassword(string $password): bool
    {
        $characters = array_map('ord', str_split($password));
        $containsStraight = false;
        foreach ($characters as $index => $character) {
            if (!isset($characters[$index - 1]) || !isset($characters[$index + 1])) {
                continue;
            }
            $containsStraight =
                (($character - 1) === $characters[$index - 1]) && (($character + 1) === $characters[$index + 1]);
            if ($containsStraight) {
                break;
            }
        }
        $pairs = 0;
        foreach ($characters as $index => $character) {
            if (!isset($characters[$index - 1])) {
                continue;
            }
            if ($character === $characters[$index - 1] && $character !== ($characters[$index + 1] ?? '')) {
                $pairs++;
            }
        }

        $containsIllegalCharacter = str_contains($password, 'i') ||
            str_contains($password, 'o') ||
            str_contains($password, 'l');

        return !$containsIllegalCharacter && $containsStraight && ($pairs >= 2);
    }

    public function increment(string $sequence): string
    {
        $characters = array_reverse(str_split($sequence));
        $carry = false;
        foreach ($characters as &$chr) {
            $value = ord($chr);
            $carry = ($value == self::ASCII_Z);
            $value = $carry ? self::ASCII_A : ($value + 1);
            $chr = chr($value);
            if (!$carry) {
                break;
            }
        }
        if ($carry) {
            $characters[] = 'a';
        }
        return strrev(implode($characters));
    }

    public function test()
    {
        $values = ['y', 'z', 'xx', 'xy', 'xz', 'ya', 'yb', 'abc', 'bc', 'abba'];
        foreach ($values as $value) {
            $next = $this->increment($value);
            echo "{$value} >> {$next}\n";
        }
    }
}
