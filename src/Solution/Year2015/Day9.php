<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day9 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141';
        }

        $locations = [];
        $routes = [];
        foreach (Parser::iterateByLine($input) as $route) {
            [$routeDetails, $distance] = explode(' = ', $route);
            [$source, $target] = explode(' to ', $routeDetails);
            $locations[] = $source;
            $locations[] = $target;

            $routes[$source][] = ['target' => $target, 'distance' => (int) $distance];
            $routes[$target][] = ['target' => $source, 'distance' => (int) $distance];
        }
        $locations = array_unique($locations);
//        var_dump($locations);

        $validPaths = [];
        foreach ($locations as $location) {
            $path = [$location];
            $this->findRoute($path, $locations, $routes, 0, $validPaths);
        }
//        var_dump($validPaths);

        $lowest = null;
        $lowestCost = PHP_INT_MAX;
        $highest = null;
        $highestCost = 0;
        foreach ($validPaths as $path) {
            if ($path['cost'] < $lowestCost) {
                $lowest = $path;
                $lowestCost = $path['cost'];
            }
            if ($path['cost'] > $highestCost) {
                $highest = $path;
                $highestCost = $path['cost'];
            }
        }

        echo "The shortest route is " . $lowest['cost'] . ".\n";
        echo "The longest route is " . $highest['cost'] . ".\n";
        return null;
    }

    public function findRoute(array $path, array $locations, array $routes, int $cost, array &$validPaths)
    {
        $toVisit = array_diff($locations, $path);
        if (empty($toVisit)) {
            $validPaths[] = [
                'path' => $path,
                'cost' => $cost,
            ];
            return;
        }
        $lastStop = $path[array_key_last($path)];
        $possibleRoutes = array_filter($routes[$lastStop] ?? [], function ($target) use ($path) {
            return !in_array($target['target'], $path);
        });

        if (empty($possibleRoutes)) {
//            echo "Dead End.\n";
//            var_dump($path);
//            var_dump($lastStop);
//            var_dump($toVisit);
//            echo "\n\n";
            return;
        }

        foreach ($possibleRoutes as $possibleRoute) {
            $newPath = $path;
            $newPath[] = $possibleRoute['target'];

            $this->findRoute($newPath, $locations, $routes, ($cost + $possibleRoute['distance']), $validPaths);
        }
    }
}
