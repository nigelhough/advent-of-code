<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day1 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        $up = substr_count($input, '(');
        $down = substr_count($input, ')');
        $floor = $up - $down;

        $currentFloor = 0;
        $basementOperator = 0;
        foreach (str_split($input) as $key => $operator) {
            if ($operator === '(') {
                $currentFloor++;
            } else {
                $currentFloor--;
            }

            if ($currentFloor === -1) {
                $basementOperator = ($key + 1);
                break;
            }
        }

        echo "
<h3>2015 Day 1</h3> 
<pre>$input</pre>
<p>Final Floor: {$floor}</p>
<p>Basement Operator: {$basementOperator}</p>";

        return null;
    }
}
