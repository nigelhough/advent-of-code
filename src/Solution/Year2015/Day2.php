<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day2 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '2x3x4
1x1x10
1x1x1';
        }
        $total = 0;
        foreach (Parser::iterateByLine($input) as $dimensions) {
            [$l, $w, $h] = array_map('intval', explode('x', $dimensions));
            $top = $l * $w;
            $bottom = $w * $h;
            $side = $h * $l;
            $extra = min($top, $bottom, $side);
            $volume = (2 * $top) + (2 * $bottom) + (2 * $side);
            $required = $extra + $volume;
//            echo "$top, $bottom, $side, $extra, $volume, $required\n";
            $total += $required;
        }
        echo "Total Wrapping Paper Required {$total}\n\n";

        $total = 0;
        foreach (Parser::iterateByLine($input) as $dimensions) {
            $dimensions = explode('x', $dimensions);
            sort($dimensions);
            $shortestDimensions = (array) array_slice($dimensions, 0, 2);
            $ribbonLength = array_sum($shortestDimensions) * 2;
            $bow = array_product($dimensions);
            $total += ($ribbonLength + $bow);
            //echo "Ribbon Length {$ribbonLength}, Ribbon Bow {$bow}\n";
        }
        echo "Total Ribbon Required {$total}\n\n";
        return null;
    }
}
