<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day18 extends AbstractSolution
{
    private const LIGHT_ON = '#';

    private const LIGHT_OFF = '.';

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '.#.#.#
...##.
#....#
..#...
#.#..#
####..';
        }
        $map = Map::createFromString($input);
        [$bottomLeft, $topRight] = $map->getMBR();
        $bottomRight = new Coord($topRight->getXPosition(), $bottomLeft->getYPosition());
        $topLeft = new Coord($bottomLeft->getXPosition(), $topRight->getYPosition());
        $map->mark($bottomLeft, self::LIGHT_ON);
        $map->mark($topRight, self::LIGHT_ON);
        $map->mark($bottomRight, self::LIGHT_ON);
        $map->mark($topLeft, self::LIGHT_ON);

//        echo "Round 0:\n";
//        echo $map->plot(true) . "\n\n";

        for ($round = 1; $round <= 100; $round++) {
            $map2 = clone $map;

            $onLights = $map->locate(function ($location) {
                return $location === self::LIGHT_ON;
            });
            $offLights = $map->locate(function ($location) {
                return $location === self::LIGHT_OFF;
            });
            foreach (array_keys($onLights) as $coord) {
                $on = 0;
                foreach (Coord::fromString($coord)->getSurrounding() as $neighbourCoord) {
                    $on += (int) ($map->get($neighbourCoord) === self::LIGHT_ON);
                }
                //echo "ON {$coord} - {$on}\n";
                // A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
                if (!in_array($on, [2, 3])) {
                    $map2->mark(Coord::fromString($coord), self::LIGHT_OFF);
                    //$map2->toggle(Coord::fromString($coord), self::LIGHT_ON, self::LIGHT_OFF);
                }
            }
            foreach (array_keys($offLights) as $coord) {
                $on = 0;
                foreach (Coord::fromString($coord)->getSurrounding() as $neighbourCoord) {
                    $on += (int) ($map->get($neighbourCoord) === self::LIGHT_ON);
                }
                //echo "OFF {$coord} - {$on}\n";
                // A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
                if ($on === 3) {
                    $map2->mark(Coord::fromString($coord), self::LIGHT_ON);
                    //$map2->toggle(Coord::fromString($coord), self::LIGHT_ON, self::LIGHT_OFF);
                }
            }

            [$bottomLeft, $topRight] = $map->getMBR();
            $bottomRight = new Coord($topRight->getXPosition(), $bottomLeft->getYPosition());
            $topLeft = new Coord($bottomLeft->getXPosition(), $topRight->getYPosition());
            $map2->mark($bottomLeft, self::LIGHT_ON);
            $map2->mark($topRight, self::LIGHT_ON);
            $map2->mark($bottomRight, self::LIGHT_ON);
            $map2->mark($topLeft, self::LIGHT_ON);

//            echo "Round {$round}:\n";
//            echo $map2->plot(true) . "\n\n";

            $map = $map2;
        }

        $onLights = $map->locate(function ($location) {
            return $location === self::LIGHT_ON;
        });
        var_dump(count($onLights));
        return null;
    }
}
