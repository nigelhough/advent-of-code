<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Generator;

class Day13 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.';
        }

        // Normalise Input.
        $input = str_replace(
            [' would gain ', ' would lose ', ' happiness units by sitting next to ', '.'],
            [',', ',-', ',', ''],
            $input
        );

        // Build Happiness Matrix.
        $happinessMatrix = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$person, $happiness, $neighbour] = explode(',', $line);
            $happinessMatrix[$person][$neighbour] = $happiness;
        }
        // Add Nigel.
        $people = array_keys($happinessMatrix);
        foreach ($people as $person) {
            $happinessMatrix['Nigel'][$person] = 0;
            $happinessMatrix[$person]['Nigel'] = 0;
        }

        $people = array_keys($happinessMatrix);
        $person = $people[array_key_first($people)];

        $largest = 0;
        $bestOrder = [];
        $smallest = PHP_INT_MAX;
        $worstOrder = [];
        foreach ($this->listCombinations([$person => $person], $people) as $list) {
            $happiness = $this->calculateCost($list, $happinessMatrix);
            if ($happiness > $largest) {
                $largest = $happiness;
                $bestOrder = $list;
            }
            if ($happiness < $smallest) {
                $smallest = $happiness;
                $worstOrder = $list;
            }
        }
        echo "Best Happiness effect is {$largest} (" . implode(',', $bestOrder) . ")\n";
        echo "Worst Happiness effect is {$smallest} (" . implode(',', $worstOrder) . ")\n";
        return null;
    }

    public function listCombinations(array $list, array $people): Generator
    {
        $remaining = array_diff($people, array_keys($list));
        if (empty($remaining)) {
            yield $list;
        }
        foreach ($remaining as $neighbour) {
            $newList = $list;

            // Point old last item to new last item.
            $newList[array_key_last($newList)] = $neighbour;
            // Add new neighbour pointing at first item.
            $newList[$neighbour] = array_key_first($list);

            yield from $this->listCombinations($newList, $people);
        }
    }

    public function calculateCost(array $order, array $happinessMatrix): int
    {
        $happiness = 0;
        foreach ($order as $person => $neighbour) {
//            echo "{$person} next to {$neighbour} - " . $happinessMatrix[$person][$neighbour] . "\n";
//            echo "{$neighbour} next to {$person} - " . $happinessMatrix[$neighbour][$person] . "\n";
            $happiness += $happinessMatrix[$person][$neighbour] + $happinessMatrix[$neighbour][$person];
        }
//        echo "Happiness {$happiness}\n\n";
        return $happiness;
    }
}
