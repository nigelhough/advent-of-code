<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day7 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT nh -> ke
NOT x -> h
NOT y -> i
e AND x -> nh';
        }

        $wires = $this->traceWires(Parser::iterateByLine($input));
        echo "Part 1, Wire 0 Signal: " . ($wires['a'] ?? 'No A!') . "\n";

        $input = str_replace('19138 -> b', $wires['a'] . ' -> b', $input);
        $wires = $this->traceWires(Parser::iterateByLine($input));
        echo "Part 2, Wire 0 Signal: " . ($wires['a'] ?? 'No A!') . "\n";
        return null;
    }

    public function traceWires(array $instructions)
    {
        $wires = [];
        // Known Wires.
        foreach ($instructions as $key => $instruction) {
            [$left, $wire] = explode(' -> ', $instruction);
            if (intval($instruction) == $left) {
                $wires[$wire] = (int) $left;
                unset($instructions[$key]);
            }
        }

        $i = 0;
        while (count($instructions)) {
            $i++;
            if ($i > 1000) {
                echo "ERROR\n";
                var_dump($instructions);
                var_dump($wires);
                exit;
            }
            foreach ($instructions as $key => $instruction) {
                $instruction = str_replace('NOT', 'null NOT', $instruction);
                [$left, $wire] = explode(' -> ', $instruction);
                [$val1, $operator, $val2] = explode(' ', $left);

                if ($operator === null && $val2 === null) {
                    if ($wires[$val1] !== null) {
                        $wires[$wire] = $wires[$val1];
                        unset($instructions[$key]);
                    }
                    continue;
                }

                if (intval($val1) == $val1) {
                    $val1 = (int) $val1;
                } else {
                    $val1 = $wires[$val1] ?? null;
                }

                if (intval($val2) == $val2) {
                    $val2 = (int) $val2;
                } else {
                    $val2 = $wires[$val2] ?? null;
                }

                if ($operator === 'NOT' && $val2 !== null) {
                    $wires[$wire] = 65535 & ~$val2;
                    unset($instructions[$key]);
                    continue;
                }

                if ($val1 === null || $val2 === null) {
                    continue;
                }

                if ($operator === 'AND') {
                    $wires[$wire] = ($val1 & $val2);
                    unset($instructions[$key]);
                    continue;
                }
                if ($operator === 'OR') {
                    $wires[$wire] = ($val1 | $val2);
                    unset($instructions[$key]);
                    continue;
                }
                if ($operator === 'LSHIFT') {
                    $wires[$wire] = ($val1 << $val2);
                    unset($instructions[$key]);
                    continue;
                }
                if ($operator === 'RSHIFT') {
                    $wires[$wire] = ($val1 >> $val2);
                    unset($instructions[$key]);
                    continue;
                }
            }
        }

        return $wires;
    }
}
