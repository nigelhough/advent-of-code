<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day5 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'ugknbfddgicrmopnuu
aaa
jchzalrnumimnmhp
haegwjzuvuyypxyu
dvszwmarrgswjxmb';
        }
        if (isset($_GET['debug2'])) {
            $input = 'qjhvhtzxzqqjkmpb
xxyxx
uurcxstgmygtbstg
ieodomkazucvgmuy';
        }

        $niceStrings = [];
        $nastyStrings = [];
        foreach (Parser::iterateByLine($input) as $string) {
            $characters = str_split($string);
            $containsVowels = count(
                array_filter($characters, function ($c) {
                    return in_array($c, ['a', 'e', 'i', 'o', 'u']);
                })
            ) >= 3;
            $containsUniqueVowels = count(array_intersect(['a', 'e', 'i', 'o', 'u'], $characters)) >= 3;
            $containsConsecutiveCharacters = (bool) count(
                array_filter($characters, function ($v, $k) use ($characters) {
                    return ($v === ($characters[$k - 1] ?? '')) || ($v === ($characters[$k + 1] ?? ''));
                }, ARRAY_FILTER_USE_BOTH)
            );
            $containsStrings = str_contains($string, 'ab') || str_contains($string, 'cd') || str_contains(
                $string,
                'pq'
            ) || str_contains($string, 'xy');

            $containsPairs = false;
            foreach ($characters as $k => $c) {
                $duo = $c . ($characters[$k + 1] ?? '');
                if (strlen($duo) !== 2) {
                    continue;
                }
                $remainingString = substr($string, 0, $k) . '  ' . substr($string, $k + 2);
                $containsPairs = str_contains($remainingString, $duo);
                if ($containsPairs) {
                    break;
                }
            }

            $containsAfterGap = (bool) count(
                array_filter($characters, function ($v, $k) use ($characters) {
                    return ($v === ($characters[$k - 2] ?? '')) || ($v === ($characters[$k + 2] ?? ''));
                }, ARRAY_FILTER_USE_BOTH)
            );

            //$nice = ($containsVowels && $containsConsecutiveCharacters && !$containsStrings);
            $nice = ($containsPairs && $containsAfterGap);
            if ($nice) {
                $niceStrings[] = $string;
            } else {
                $nastyStrings[] = $string;
            }
        }

        echo "Nice Strings: " . count($niceStrings) . ". Naughty Strings: " . count($nastyStrings);
        return null;
    }
}
