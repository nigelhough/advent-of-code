<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Sets\Factory;
use AdventOfCode\Solution\AbstractSolution;

class Day17 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '20
15
10
5
5';
        }
        $input = Parser::iterateNumbersByLine($input);
        $target = 150;
        $count = 0;
        $fewest = PHP_INT_MAX;
        $fewestSets = [];
        foreach (Factory::powerSet($input) as $set) {
            if (array_sum($set) !== $target) {
                continue;
            }
            $count++;
            if (count($set) < $fewest) {
                $fewest = count($set);
                $fewestSets = [$set];
            } elseif (count($set) === $fewest) {
                $fewestSets[] = $set;
            }
        }
        echo "Container combinations that can meet the target is {$count}.\n";
        echo "The fewest containers that can be used is {$fewest}.\n";
        echo "There are " . count($fewestSets) . " combinations that mee the target with the fewest containers.\n";
        return null;
    }
}
