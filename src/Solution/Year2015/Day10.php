<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2015;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day10 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        ini_set('memory_limit', '4G');
        if (isset($_GET['debug'])) {
            $input = '111221';
        }

//        echo "0 : {$input}\n";
        echo "0 (" . strlen($input) . ")\n";
        for ($i = 1; $i <= 50; $i++) {
            $input = $this->playGame($input);
            //echo "{$i} (" . strlen($input) . ") : {$input}\n";
            echo "{$i} (" . strlen($input) . ")\n";
        }

        echo "\n50th Iteration Length = " . strlen($input) . "\n";
        return null;
    }

    public function playGame($input)
    {
        $parts = [];
        $previous = null;
        $current = [];
        foreach (str_split($input) as $chr) {
            if ($previous != $chr && !empty($current)) {
                $parts[] = $current;
                $current = [];
            }
            $current[] = $chr;
            $previous = $chr;
        }
        if (count($current) > 0) {
            $parts[] = $current;
        }

        $answer = '';
        foreach ($parts as $part) {
            $answer .= count($part) . $part[0];
        }

        return $answer;
    }
}
