<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2016;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = 'ULL
RRDDD
LURDL
UUUUD';
        }

        $width = 3;
        $height = 3;
        $range = range(1, $width * $height);

        $numbers = [];
        $i = 1;
        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                //echo str_pad((string) $i, 4);
                if (in_array($i - 1, $range) && ($i % $width) != 1) {
                    $numbers[$i]['L'] = $i - 1;
                }
                if (in_array($i + 1, $range) && ($i % $width) != 0) {
                    $numbers[$i]['R'] = $i + 1;
                }
                if (in_array($i - $width, $range)) {
                    $numbers[$i]['U'] = $i - $width;
                }
                if (in_array($i + $width, $range)) {
                    $numbers[$i]['D'] = $i + $width;
                }
                $i++;
            }
            //echo "\n";
        }

        $position = 5;
        $code = "";
        foreach (Parser::iterateByLine($input) as $line) {
            foreach (str_split($line) as $instruction) {
                $position = $numbers[$position][$instruction] ?? $position;
            }
            $code .= $position;
        }
        echo $code . "\n";
        $answer1 = $code;

        $numbers = [];
        $numbers[1]['D'] = 3;

        $numbers[2]['R'] = 3;
        $numbers[2]['D'] = 6;

        $numbers[3]['U'] = 1;
        $numbers[3]['R'] = 4;
        $numbers[3]['D'] = 7;
        $numbers[3]['L'] = 2;

        $numbers[4]['L'] = 3;
        $numbers[4]['D'] = 8;

        $numbers[5]['R'] = 6;

        $numbers[6]['U'] = 2;
        $numbers[6]['R'] = 7;
        $numbers[6]['D'] = 'A';
        $numbers[6]['L'] = 5;

        $numbers[7]['U'] = 3;
        $numbers[7]['R'] = 8;
        $numbers[7]['D'] = 'B';
        $numbers[7]['L'] = 6;

        $numbers[8]['U'] = 4;
        $numbers[8]['R'] = 9;
        $numbers[8]['D'] = 'C';
        $numbers[8]['L'] = 7;

        $numbers[9]['L'] = 8;

        $numbers['A']['U'] = 6;
        $numbers['A']['R'] = 'B';

        $numbers['B']['U'] = 7;
        $numbers['B']['R'] = 'C';
        $numbers['B']['D'] = 'D';
        $numbers['B']['L'] = 'A';

        $numbers['C']['U'] = 8;
        $numbers['C']['L'] = 'B';

        $numbers['D']['U'] = 'B';

        $position = 5;
        $code = "";
        foreach (Parser::iterateByLine($input) as $line) {
            foreach (str_split($line) as $instruction) {
                $position = $numbers[$position][$instruction] ?? $position;
            }
            $code .= $position;
        }
        echo $code . "\n";
        $answer2 = $code;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
