<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2016;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Offset;
use AdventOfCode\Grid\Path;
use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'R2, L3';
            $input = 'R8, R4, R4, R8, R8, R4, R4, R8, R8, R4, R4, R8';
            $input = 'R8, R4, R4, R8';
        }

        $start = new Coord(0, 0);
        $location = new Coord(0, 0);
        $bearing = Bearing::get(Bearing::NORTH);
        $revisited = null;
        $path = new Path($start);

        foreach (explode(', ', $input) as $instruction) {
            $direction = substr($instruction, 0, 1);
            $steps = (int) substr($instruction, 1);

            if ($direction === 'R') {
                $bearing = $bearing->rotateClockwise();
            } else {
                $bearing = $bearing->rotateAntiClockwise();
            }
            $location = $location->moveAlongBearing($bearing, $steps);
            //echo "{$instruction} -- From {$path->getLastStep()} to {$location}" . PHP_EOL;
            $travelPath = Path::betweenCoordinates($path->getLastStep(), $location);
            foreach ($travelPath->follow() as $i => $coord) {
                if ($i !== 0 && $path->hasVisited($coord)) {
                    $revisited = $coord;
                    break 2;
                }
                $path = $path->addStep($coord);
            }
        }

        $offset = Offset::createFromCoordinates($start, $location);
        $answer1 = abs($offset->getY()) + abs($offset->getX());
        echo $answer1 . PHP_EOL;

        $offset = Offset::createFromCoordinates($start, $revisited);
        $answer2 = abs($offset->getY()) + abs($offset->getX());
        echo $answer2 . PHP_EOL;

        if (isset($_GET['debug'])) {
            return null;
        }
        return new Answer((string) $answer1, (string) $answer2);
    }
}
