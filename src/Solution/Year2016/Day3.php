<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2016;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        //echo "{$input}\n";

        $answer1 = 301;
//        $answer1 = 10_000_000;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
