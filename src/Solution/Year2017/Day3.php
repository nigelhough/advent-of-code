<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '18';
        }
        $depth = 1;
        $size = 1;

        $last = 0;
        $from = 1;
        while ($depth < 277678) {
            $from = ($last + 1);
            $to = ($size * $size);
            if ($input >= $from && $input <= $to) {
                break;
            }
            $last = ($size * $size);
            $depth++;
            $size += 2;
        }
        $x = ($depth - 1);
        $y = ($depth - 1) - 1;
        $xMod = 0;
        $yMod = -1;
        $val = $from;
        while ($val != $input) {
            //echo "{$x},{$y} - {$val}\n";
            if ($x === ($depth - 1) && $y === -($depth - 1)) {
                $xMod = -1;
                $yMod = 0;
            } elseif ($x === -($depth - 1) && $y === -($depth - 1)) {
                $xMod = 0;
                $yMod = 1;
            } elseif ($x === -($depth - 1) && $y === ($depth - 1)) {
                $xMod = 1;
                $yMod = 0;
            }
            $x += $xMod;
            $y += $yMod;
            $val++;
        }
        echo "{$depth}\n";
        echo "{$val} {$x},{$y}\n";
        echo (new Coord($x, $y))->manhattanDistance(new Coord(0, 0)) . "\n\n";

        $map = new Map();
        $map->mark(new Coord(0, 0), 1);
        $last = 1;
        $depth = 2;
        $size = 3;
        while ($depth < 200) {
            $from = ($last + 1);
            $to = ($size * $size);

            $x = ($depth - 1);
            $y = ($depth - 1) - 1;
            $xMod = 0;
            $yMod = -1;
            $val = $from;
            for ($i = $from; $i <= $to; $i++) {
                $neighbourSum = array_sum($map->getSurrounding(new Coord($x, $y)));
                if ($neighbourSum > $input) {
                    echo "{$x},{$y} - {$val} = {$neighbourSum}\n";
                    break 2;
                }
                $map->mark(new Coord($x, $y), $neighbourSum);

                if ($x === ($depth - 1) && $y === -($depth - 1)) {
                    $xMod = -1;
                    $yMod = 0;
                } elseif ($x === -($depth - 1) && $y === -($depth - 1)) {
                    $xMod = 0;
                    $yMod = 1;
                } elseif ($x === -($depth - 1) && $y === ($depth - 1)) {
                    $xMod = 1;
                    $yMod = 0;
                }
                $x += $xMod;
                $y += $yMod;
                $val++;
            }

            $last = ($size * $size);
            $depth++;
            $size += 2;
        }
        return null;
    }
}
