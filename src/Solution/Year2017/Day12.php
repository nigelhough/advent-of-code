<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day12 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '0 <-> 2
1 <-> 1
2 <-> 0, 3, 4
3 <-> 2, 4
4 <-> 2, 3, 6
5 <-> 6
6 <-> 4, 5';
        }

        // Document Pipe Network.
        $pipes = [];
        foreach (Parser::iterateByLine($input) as $connections) {
            [$source, $targets] = explode(' <-> ', $connections);
            foreach (explode(',', $targets) as $target) {
                $target = (int) $target;
                $source = (int) $source;
                if (!in_array($target, $pipes[$source] ?? [])) {
                    $pipes[$source][] = $target;
                }
                if (!in_array($source, $pipes[$target] ?? [])) {
                    $pipes[$target][] = $source;
                }
            }
        }

        $groups = [];
        $connections = $this->findConnections(0, $pipes);
        $groups[] = $connections;
        echo "Number of Connections in Group containing Zero is " . count($connections) . ".\n\n";

        foreach (array_keys($pipes) as $location) {
            // If a location is in an existing group then skip it.
            foreach ($groups as $group) {
                if (in_array($location, $group)) {
                    continue 2;
                }
            }

            $connections = $this->findConnections($location, $pipes);
            $groups[] = $connections;
        }
        echo "Total number of groups " . count($groups) . ".\n";
        return null;
    }

    public function findConnections(int $source, array $pipes, array $known = [])
    {
        $known[] = $source;

        foreach ($pipes[$source] as $target) {
            if (in_array($target, $known)) {
                continue;
            }
            $known = array_unique(array_merge($known, $this->findConnections($target, $pipes, $known)));
        }

        return $known;
    }
}
