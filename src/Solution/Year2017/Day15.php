<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Generator\Generator;
use AdventOfCode\Generator\Judge;
use AdventOfCode\Solution\AbstractSolution;

class Day15 extends AbstractSolution
{
    public const MILLION = 1000000;

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'Generator A starts with 65
Generator B starts with 8921
';
        }
        echo "{$input}\n";

        [$a, $b] = explode("\n", $input);
        $a = (int)substr($a, strrpos($a, ' ', -1) + 1);
        $b = (int)substr($b, strrpos($b, ' ', -1) + 1);

        $judge = new Judge(
            new Generator($a, 16807),
            new Generator($b, 48271)
        );
//        $judge->debug(5);
        $matches = $judge->judge(40 * self::MILLION);
        echo "Part 1 Matches = {$matches}\n\n";

        // Part 2. (Adds Multipliers)
        $judge = new Judge(
            new Generator($a, 16807, 4),
            new Generator($b, 48271, 8)
        );
//        $judge->debug(5);
        $matches = $judge->judge(5 * self::MILLION);
        echo "Part 2 Matches = {$matches}\n";
        return null;
    }
}
