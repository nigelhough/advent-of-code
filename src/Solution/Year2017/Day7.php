<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Tree\Tree;

class Day7 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // 1117 too high
        if (isset($_GET['debug'])) {
            $input = 'pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)';
        }

        $tree = Tree::fromString($input);

        foreach ($tree->getRootNodes() as $rootNode) {
            echo "'{$rootNode}' is the root node.\n";
        }
        echo "\n";

        // Balance.
//        $tree->setNodeWeight('ptshtrn', 521);

        $completelyBalanced = true;
        $smallestValue = PHP_INT_MAX;
        $smallestNode = null;
        $offset = 0;
        foreach ($tree->iterateNodes() as $node) {
            $balanced = true;

            $unbalanceCount = 0;
            foreach ($tree->getPeers($node) as $peer) {
                if ($peer->getTotalWeight() !== $node->getTotalWeight()) {
                    $balanced = false;
                    $unbalanceCount++;
//                    break;
                }
            }
            if (!isset($peer)) {
                break;
            }

            if (!$balanced && $unbalanceCount > 1) {
                $completelyBalanced = false;

                $smallestValue = min($smallestValue, $node->getWeight());
                if ($node->getWeight() === $smallestValue) {
                    $smallestNode = $node;
                    $offset = $peer->getTotalWeight() - $node->getTotalWeight();
                }
//                echo "{$node} (" . $node->getWeight() .
//                    "), " .
//                    $node->getChildWeight() .
//                    ", " .
//                    $node->getTotalWeight() .
//                    ".\n";
                echo "  {$unbalanceCount} " . $peer->getWeight() . ", " . $peer->getTotalWeight() . "\n\n";
                echo "\n\n";
                break;
            }
        }

        if ($completelyBalanced) {
            echo "Perfectly Balanced like all things should be.\n";
        } else {
            echo "I detect an Unbalance.\n";
            echo "Node '{$smallestNode}' is incorrect. {$smallestValue} incorrect by {$offset}.\n";
            echo "{$smallestNode} weight should become " . ($smallestNode->getWeight() + $offset) . ".\n";
        }
        return null;
    }
}
