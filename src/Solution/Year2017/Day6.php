<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day6 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '0	2	7	0';
        }
        $memoryBanks = Parser::iterateNumbersByTab($input);
        $memory = [];

        $count = 0;
        while (!isset($memory[md5(serialize($memoryBanks))])) {
            $memory[md5(serialize($memoryBanks))] = $count;

            $highestBlocks = max($memoryBanks);
            $highestBank = array_search($highestBlocks, $memoryBanks);
            $bank = $highestBank + 1;
            $memoryBanks[$highestBank] = 0;
            for ($i = 0; $i < $highestBlocks; $i++) {
                if ($bank >= count($memoryBanks)) {
                    $bank = 0;
                }
                $memoryBanks[$bank]++;
                $bank++;
            }
            //var_dump($memoryBanks);
            $count++;
        }

        echo count($memory);
        echo "\n\n";
        echo $memory[md5(serialize($memoryBanks))];
        echo "\n\n";
        echo(count($memory)) - ($memory[md5(serialize($memoryBanks))]);
        return null;
    }
}
