<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '5 1 9 5
7 5 3
2 4 6 8';
        }

        $input = trim($input);

        $rows = [];
        $columns = [];
        foreach (Parser::iterateByLine($input) as $rowNumber => $row) {
            //echo "{$rowNumber} :: {$row}\n";
            foreach (explode("\t", $row) as $columnNumber => $value) {
                $rows[$rowNumber][] = (int) $value;
                $columns[$columnNumber][] = (int) $value;
            }
        }
        //var_dump($rows[0]);
        $rowChecksums = [];
        foreach ($rows as $row) {
            $rowChecksums[] = max($row) - min($row);
        }
        $columnChecksums = [];
        foreach ($columns as $column) {
            $columnChecksums[] = max($column) - min($column);
        }

        echo "Row Checksum " . array_sum($rowChecksums) . ".\n";
        echo "Column Checksum " . array_sum($columnChecksums) . ".\n";

        $total = 0;
        foreach ($rows as $row) {
            $values = $this->findDivisibileNumbers($row);
            $total += max($values) / min($values);
        }
        echo "Part2 CheckSum {$total}.\n";
        // 37222 is too high
        // 32558 is too high
        return null;
    }

    public function findDivisibileNumbers(array $numbers)
    {
        foreach ($numbers as $key => $val) {
            foreach ($numbers as $key2 => $val2) {
                if ($key === $key2) {
                    continue;
                }
                if (($val % $val2) === 0 || ($val2 % $val) === 0) {
                    return [$val, $val2];
                }
            }
        }
        throw new Exception('No valid numbers.');
    }
}
