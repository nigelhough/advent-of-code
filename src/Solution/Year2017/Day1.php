<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '91212129';
            $input = '1234';
            $input = '11221';
        }

        $parts = str_split(trim($input));
        $numbers = [];
        $offset = count($parts) / 2;
        foreach ($parts as $key => $part) {
            $index = $key + $offset >= count($parts) ? ($key + $offset) - count($parts) : ($key + $offset);
            //echo "{$part} {$key} {$index}\n";
            if (!isset($parts[$index]) || $parts[$index] != $part) {
                continue;
            }
            $numbers[] = $part;
        }
        //var_dump($numbers);
        echo array_sum($numbers) . "\n";
        return null;
    }
}
