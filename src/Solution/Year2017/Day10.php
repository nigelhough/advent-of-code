<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\LinkedList\LinkedList;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day10 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $listLength = 256;
        // (You guessed 50850.) your answer is too high
        if (isset($_GET['debug'])) {
            $listLength = 5;
            $input = '3,4,1,5';
        }

        // Part 1
        $currentPosition = 0;
        $skipSize = 0;

        $list = new LinkedList(...range(0, ($listLength - 1)));
        foreach (array_map('intval', explode(',', $input)) as $length) {
            $list->reverseRange($currentPosition + 1, $length);
            $currentPosition += ($length + $skipSize);
            $currentPosition = $currentPosition % count($list);
            $skipSize++;
        }
        $first = $list->getItem(1);
        $second = $list->getItem(2);
        $result = $first * $second;
        echo "{$first} * {$second} = {$result}\n\n";


        // Part 2
        $currentPosition = 0;
        $skipSize = 0;

        $asciiLengths = implode(',', array_map('ord', str_split($input))) . ',17,31,73,47,23';
//        echo "{$asciiLengths}\n";
//        echo "{$input}\n";
        $longerInput = trim(str_repeat($asciiLengths . ',', 64), ',');
//        echo "{$longerInput}\n";
        $list = new LinkedList(...range(0, ($listLength - 1)));
        foreach (array_map('intval', explode(',', $longerInput)) as $length) {
            if ($length > $listLength) {
                continue;
            }
            $list->reverseRange($currentPosition + 1, $length);
            $currentPosition += ($length + $skipSize);
            $currentPosition = $currentPosition % count($list);
            $skipSize++;
        }
//        echo "{$list}\n\n";
        $sparseHash = explode(' ', (string) $list);
        $hashParts = array_chunk($sparseHash, 16);
        $hashParts = array_map(function ($hashPart) {
            return array_reduce($hashPart, function ($carry, $item) {
                return ((int) $carry) ^ ((int) $item);
            });
        }, $hashParts);
        $hashParts = array_map(function ($hashPart) {
            return str_pad(dechex((int) $hashPart), 2, '0', STR_PAD_LEFT);
        }, $hashParts);
        echo "Part 2 :" . implode('', $hashParts) . "\n";
        return null;
    }
}
