<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\LinkedList\LinkedList;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day16 extends AbstractSolution
{
    public const THOUSAND = 1000;

    public const MILLION = 1000000;

    public const BILLION = 1000000000;

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $positions = 16;
        if (isset($_GET['debug'])) {
            $positions = 5;
            $input = 's1,x3/4,pe/b';
        }

        $listArray = [];
        for ($i = 0; $i < $positions; $i++) {
            $listArray[] = chr(97 + $i);
        }
        $commands = explode(',', $input);

        $list = new LinkedList(...$listArray);
        $this->doTheDance($list, $commands);
        echo "Part 1: {$list}\n";

        $list = new LinkedList(...$listArray);
        $startingPositions = (string) $list;

        for ($i = 0; $i < self::BILLION; $i++) {
            $this->doTheDance($list, $commands);

            // We don't want to do this a Billion times.
            // How many full cycles before we are back to the start?
            if (((string) $list) === $startingPositions) {
                // echo "BACK TO THE START {$i}\n";
                break;
            }
        }

        $i++;
        $remaining = self::BILLION % $i;

        for ($i = 0; $i < $remaining; $i++) {
            $this->doTheDance($list, $commands);
        }
        echo "Part 2: {$list}";
        return null;
    }

    public function doTheDance(LinkedList $list, array $commands)
    {
        foreach ($commands as $command) {
            $move = substr($command, 0, 1);
            $details = substr($command, 1);

            switch ($move) {
                case 's':
                    $list->spin((int) $details);
                    break;
                case 'x':
                    [$a, $b] = explode('/', $details);
                    $a = intval($a) + 1;
                    $b = intval($b) + 1;
                    $list->swapPositions($a, $b);
                    break;
                case 'p':
                    [$a, $b] = explode('/', $details);
                    $list->swapItems($a, $b);
                    break;
            }
//                echo "{$command}\n";
//                echo "{$list}\n";
//                echo "\n";
        }
    }
}
