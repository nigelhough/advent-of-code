<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day9 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '{{<!>},{<!>},{<!>},{<a>}}';
            $input = '{{<ab>},{<ab>},{<ab>},{<ab>}}'; // 9
            $input = '{{<a!>},{<a!>},{<a!>},{<ab>}}'; // 3
            $input = '{{{},{},{{}}}}'; // 16
            $input = '<random characters>'; // 16 garbage
            //$input = '<{o"i!a,<{i<a>'; // 10 garbage
        }
        // Clear Comments.
        while (str_contains($input, '!')) {
            $pos = strpos($input, '!');
            $input = substr($input, 0, $pos) . substr($input, $pos + 2);
        }
        // Clear Garbage.
        $garbageLength = 0;
        while (str_contains($input, '<')) {
            $start = strpos($input, '<');
            $end = strpos($input, '>', $start);
            $garbageLength += ($end - $start) - 1;
            $input = substr($input, 0, $start) . substr($input, $end + 1);
        }
        $characters = str_split($input);
        $score = $this->scoreStream($characters);
        echo "Total stream score is {$score}.\n";
        echo "The total length of garbage is {$garbageLength}.\n";
        return null;
    }

    public function scoreStream(array &$characters, int $score = 0): int
    {
        $totalScore = 0;
        while (!empty($characters)) {
            $character = array_shift($characters);

            if ($character === '{') {
                $totalScore += $this->scoreStream($characters, $score + 1);
            } elseif ($character === '}') {
                return $totalScore + $score;
            }
        }
        return $totalScore;
    }
}
