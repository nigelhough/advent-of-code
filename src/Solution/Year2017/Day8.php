<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day8 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10';
        }
        $instructions = Parser::iterateByLine($input);

        $register = [];
        $index = 0;

        $highest = 0;
        while (isset($instructions[$index])) {
            [$registerKey, $bits] = explode(' ', $instructions[$index], 2);
            [$instruction, $condition] = explode(' if ', $bits, 2);
            $condition = "return (\$register['" . substr($condition, 0, strpos($condition, ' ')) . "'] ?? 0)" . substr(
                $condition,
                strpos($condition, ' ')
            ) . ";";
            if (!(bool) eval($condition)) {
                $index++;
                continue;
            }
            //echo "{$index}: {$registerKey} '{$instruction}' {$condition}\n";
            [$instruction, $val] = explode(' ', $instruction);

            if ($instruction === 'inc') {
                $register[$registerKey] = ($register[$registerKey] ?? 0) + $val;
            } elseif ($instruction === 'dec') {
                $register[$registerKey] = ($register[$registerKey] ?? 0) - $val;
            } else {
                throw new Exception('Unknown Instruction');
            }
            $highest = max($highest, max($register));
            $index++;
        }
        echo "Highest value at the end " . max($register) . ".\n";
        echo "Peak value {$highest}.\n";
        return null;
    }
}
