<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day4 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '';
        }

        $validPassphrases = [];
        foreach (Parser::iterateByLine($input) as $passPhrase) {
            $words = explode(' ', $passPhrase);
            foreach ($words as $key => $word) {
                $chrs = str_split($word);
                sort($chrs);
                $word = implode('', $chrs);
                $words[$key] = $word;
            }
            $valid = count($words) === count(array_unique($words));

            if ($valid) {
                $validPassphrases[] = $passPhrase;
            }
        }

        echo count($validPassphrases);
        return null;
    }
}
