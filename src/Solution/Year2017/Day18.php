<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day18 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2';
        }
//        echo "{$input}\n";

        $instructionList = Parser::iterateByLine($input);
        $register = [];
        $frequency = 0;
        for ($i = 0; $i < count($instructionList); $i++) {
            $instruction = $instructionList[$i];

            [$in] = explode(' ', $instruction);

            switch ($in) {
                case 'snd':
                    // X plays a sound with a frequency equal to the value of X.
                    [, $x] = explode(' ', $instruction);
                    $frequency = $register[$x];
                    break;
                case 'set':
                    // X Y sets register X to the value of Y.
                    [, $x, $y] = explode(' ', $instruction);
                    $register[$x] = (is_numeric($y) ? (int)$y : ($register[$y] ?? 0));
                    break;
                case 'add':
                    // X Y increases register X by the value of Y.
                    [, $x, $y] = explode(' ', $instruction);
                    $register[$x] = ($register[$x] ?? 0) + (is_numeric($y) ? (int)$y : ($register[$y] ?? 0));
                    break;
                case 'mul':
                    // X Y sets register
                    // X to the result of multiplying the value contained in register X by the value of Y.
                    [, $x, $y] = explode(' ', $instruction);
                    $register[$x] = ($register[$x] ?? 0) * (is_numeric($y) ? (int)$y : ($register[$y] ?? 0));
                    break;
                case 'mod':
                    // X Y sets register
                    // X to the remainder of dividing the value contained in register X by the value of Y
                    // (that is, it sets X to the result of X modulo Y).
                    [, $x, $y] = explode(' ', $instruction);
                    $register[$x] = ($register[$x] ?? 0) % (is_numeric($y) ? (int)$y : ($register[$y] ?? 0));
                    break;
                case 'rcv':
                    // X recovers the frequency of the last sound played, but only when the value of X is not zero.
                    // (If it is zero, the command does nothing.)
                    [, $x] = explode(' ', $instruction);
                    $x = $register[$x];
                    if ($x !== 0) {
                        echo "Recovered Frequency is {$frequency}";
                        break 2;
                    }
                    break;
                case 'jgz':
                    // X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero.
                    // (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction)
                    [, $x, $y] = explode(' ', $instruction);
                    $x = $register[$x];
                    if ($x !== 0) {
                        $i += (is_numeric($y) ? (int)$y : ($register[$y] ?? 0));
                        $i--;
                        $die = false;
                    }
                    break;
            }
        }

        return new Answer(
            (string)$frequency,
            'dummy'
        );
    }
}
