<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day5 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '0
3
0
1
-3';
        }

        $pointer = 0;
        $steps = 0;
        $instructions = Parser::iterateNumbersByLine($input);
        while (isset($instructions[$pointer])) {
            $jump = $instructions[$pointer];
            if ($jump >= 3) {
                $instructions[$pointer] = $jump - 1;
            } else {
                $instructions[$pointer] = $jump + 1;
            }
            $pointer += $jump;
            $steps++;
        }
        echo $steps;
        return null;
    }
}
