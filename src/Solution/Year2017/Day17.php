<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\LinkedList\LinkedList;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day17 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '3';
        }
        $input = (int) $input;

        $list = new LinkedList('0', '1');
        $list->setFirstItem('1');

        $items = 2017;
        for ($i = 2; $i < ($items + 2); $i++) {
            $list->insertAfterIndex($input, (string) $i);
            $list->setFirstItem((string) $i);
        }
        echo $list->getItemAfter('2017');
        echo "\n\n";
        return null;

        ini_set('memory_limit', '4G');
        $list = new LinkedList('0', '1');
        $list->setFirstItem('1');

        $items = 50000000;
        for ($i = 2; $i < ($items + 2); $i++) {
            $list->insertAfterIndex($input, (string) $i);
            $list->setFirstItem((string) $i);
            echo number_format($i) . "\n";
        }
        $list->setFirstItem('0');
        echo $list->printDelimited();
        return null;
    }
}
