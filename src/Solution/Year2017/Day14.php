<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\LinkedList\LinkedList;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day14 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'flqrgnkx';
        }

        for ($i = 0; $i < 128; $i++) {
            $row = $input . '-' . $i;

            $asciiLengths = implode(',', array_map('ord', str_split($row)));
            //echo "{$row}, {$asciiLengths}\n";
            $hash = $this->knotHash($asciiLengths);
            $bits = $this->hash2Bits($hash);

            $gridRepresentation = str_replace(['1', '0'], ['#', '.'], $bits);
            echo substr($gridRepresentation, 0, 8) . "\n";

            if ($i === 8) {
                break;
            }
        }
        return null;
    }

    public function knotHash(string $subject)
    {
        $listLength = 256;
        $currentPosition = 0;
        $skipSize = 0;

        $asciiLengths = implode(',', array_map('ord', str_split($subject)));
        //$asciiLengths = implode(',', array_map('ord', str_split($subject))) . ',17,31,73,47,23';
//        echo "{$asciiLengths}\n";
//        echo "{$input}\n";
        $longerInput = trim(str_repeat($asciiLengths . ',', 64), ',');
//        echo "{$longerInput}\n";
        $list = new LinkedList(...range(0, ($listLength - 1)));
        foreach (array_map('intval', explode(',', $longerInput)) as $length) {
            if ($length > $listLength) {
                continue;
            }
            $list->reverseRange($currentPosition + 1, $length);
            $currentPosition += ($length + $skipSize);
            $currentPosition = $currentPosition % count($list);
            $skipSize++;
        }
//        echo "{$list}\n\n";
        $sparseHash = explode(' ', (string) $list);
        $hashParts = array_chunk($sparseHash, 16);
        $hashParts = array_map(function ($hashPart) {
            return array_reduce($hashPart, function ($carry, $item) {
                return ((int) $carry) ^ ((int) $item);
            });
        }, $hashParts);
        $hashParts = array_map(function ($hashPart) {
            return str_pad(dechex((int) $hashPart), 2, '0', STR_PAD_LEFT);
        }, $hashParts);

        return implode('', $hashParts);
    }

    public function hash2Bits(string $hash)
    {
        $bits = '';
        foreach (str_split($hash) as $hex) {
            $bits .= str_pad(base_convert($hex, 16, 2), 4, '0', STR_PAD_LEFT);
        }
        return $bits;
    }
}
