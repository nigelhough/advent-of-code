<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day13 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '0: 3
1: 2
4: 4
6: 4';
        }

        $scanners = [];
        foreach (Parser::iterateByLine($input) as $scanner) {
            [$sequenceNo, $length] = explode(': ', $scanner);
            //echo "{$sequenceNo} >> {$length}\n";
            $scanners[$sequenceNo] = $length;
        }

        $cost = $this->countHits($scanners, 1);
        echo "Cost of passing Firewall {$cost}\n\n";

        $i = 0;
        while (!$this->isClearRun($scanners, $i)) {
            $i++;
        }
        echo "Clear run if you start at second {$i}\n";
        return null;

//        $lowestCost = PHP_INT_MAX;
//        $lowestStart = 0;
//        for ($i = 0; $i <= 10000000; $i++) {
//            $cost = $this->countHits($scanners, $i);
//
//            if ($cost < $lowestCost) {
//                $lowestCost = $cost;
//                $lowestStart = $i;
//                if ($cost === 0) {
//                    break;
//                }
//            }
//        }
//        echo "Lost Cost Passing Firewall {$lowestCost}, start at second $lowestStart\n\n";
    }

    public function countHits(array $scanners, int $startPosition = 0): int
    {
        $cost = 0;
        foreach ($scanners as $sequenceNo => $length) {
            $steps = ($length - 1) * 2;
            $myVisit = $startPosition + $sequenceNo;
            $isHit = ($myVisit % $steps) === 0;
//            echo "{$sequenceNo} >> {$length} >> {$steps}, {$myVisit} = {$isHit}\n";
            if ($isHit) {
                $cost += ($myVisit * $length);
            }
        }

        return $cost;
    }

    public function isClearRun(array $scanners, int $startPosition = 0): bool
    {
        foreach ($scanners as $sequenceNo => $length) {
            $steps = ($length - 1) * 2;
            $myVisit = $startPosition + $sequenceNo;
            $isHit = ($myVisit % $steps) === 0;
            if ($isHit) {
                return false;
            }
        }
        return true;
    }
}
