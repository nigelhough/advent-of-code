<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2017;

use AdventOfCode\Grid\Route\Router;
use AdventOfCode\HexGrid\Bearing;
use AdventOfCode\HexGrid\Coord;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day11 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'ne,ne,s,s';
            //$input = 'ne,ne,sw,sw';
            //$input = 'se,sw,se,sw,sw';
        }

        $start = new Coord(0, 0, 0);
        $currentLocation = $start;

        ini_set('max_execution_time', '900');
        // Follow the Path.
        $furthest = 0;
        $highest = 0;
        $points = [];
        $furthestCoord = null;
        foreach (explode(',', trim($input)) as $coordinate) {
            $currentLocation = $currentLocation->moveAlongBearing(Bearing::get(Bearing::rotateHexGrid($coordinate)));
//            $highest = max($highest, Router::route($start, $currentLocation)->count());
//            if (php_sapi_name() == "cli") {
//                echo "{$highest}\n";
//            }

            $distance = $currentLocation->distance($start);
            if ($distance > $furthest) {
                $furthest = $distance;
                $furthestCoord = $currentLocation;
            }
            $points[] = [$distance, $currentLocation];
        }
        //$highest = Router::route($start, $furthestCoord)->count();

        // Use a Buffer. The measurment of Hex coordinates is not quite right.
        // Measuring the steps for each possibleStep is slow.
        usort($points, function (array $a, array $b) {
            return $b[0] <=> $a[0];
        });
        for ($i = 0; $i < 200; $i++) {
            $highest = max($highest, Router::route($start, $points[$i][1])->count());
            //echo "{$i} {$points[$i][1]} ".Router::route($start, $points[$i][1])->count()."\n";
        }

        $route = Router::route($start, $currentLocation);
        echo "Shortest Route has " . $route->count() . " step(s). {$highest}";
        return null;
    }
}
