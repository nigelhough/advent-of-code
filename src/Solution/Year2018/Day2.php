<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2018;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = 'abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab';
        }
        $double = $triple = 0;
        foreach (Parser::iterateByLine($input) as $line) {
            $characters = [];
//            var_dump(str_split($line));
            foreach(str_split($line) as $chr) {
                $characters[$chr] = (isset($characters[$chr]) ? $characters[$chr]+1 : 1);
            }
//            var_dump($characters);
            if(in_array(2, $characters)) {
                $double++;
            }if(in_array(3, $characters)) {
                $triple++;
            }
        }
        $answer1 = $double * $triple;

        $subject = $comparison = '';
        foreach (Parser::iterateByLine($input) as $subject) {
            foreach (Parser::iterateByLine($input) as $comparison) {
                if($subject === $comparison) {
                    continue;
                }
                $similar = similar_text($subject,$comparison);
                if($similar < 25) {
                    continue;
                }
                //echo similar_text($subject,$comparison) . " - {$subject} {$comparison}\n";
                break 2;
            }
        }
        $subject = str_split($subject);
        $comparison = str_split($comparison);
        for($i=0; $i < count($subject); $i++) {
            if($subject[$i] === $comparison[$i]) {
                continue;
            }
            unset($subject[$i]);
            unset($comparison[$i]);
        }

        $answer2 = implode($subject);


        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
