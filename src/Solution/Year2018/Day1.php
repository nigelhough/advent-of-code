<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2018;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Device;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '+1
-2
+3
+1';
        }
//
//        var_dump($input);
//        print_r(new Device\FrequencyChanges($input));

        $calibrator = new Device\Calibrator();
        $frequency = $calibrator->calibrate(new Device\FrequencyChanges($input));

        echo "Frequency: {$frequency}\n\n";
        // Migrated incomplete without part1 being submitted, will save until I want to do 2018
        $answer1 = $frequency;

        $answer2 = $calibrator->duplicate(new Device\FrequencyChanges($input));

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
