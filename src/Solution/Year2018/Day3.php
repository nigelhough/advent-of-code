<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2018;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Grid\Offset;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2';
        }
//        echo "{$input}\n";

        $map = new Map();

        foreach (Parser::iterateByLine($input) as $line) {
            $line = str_replace(' ', '', $line);
            [$key, $details] = explode('@', $line);
            $key = str_replace('#', '', $key);
            [$coords, $range] = explode(':', $details);
            [$rangeX, $rangeY] = explode('x', $range);
            $start = Coord::fromString($coords);

            $map->markRange($start, $start->moveOffset(new Offset(((int)$rangeX) - 1, ((int)$rangeY) - 1)), $key, 'X');
        }

        //        echo $map->plot(true);

        $overlaps = $map->locate(function ($location) {
            return $location === 'X';
        });
        $answer1 = count($overlaps);

        foreach (Parser::iterateByLine($input) as $line) {
            $line = str_replace(' ', '', $line);
            [$key, $details] = explode('@', $line);
            $key = str_replace('#', '', $key);
            [$coords, $range] = explode(':', $details);
            [$rangeX, $rangeY] = explode('x', $range);
            $start = Coord::fromString($coords);

            $values = $map->getRangeValues(
                $start,
                $start->moveOffset(new Offset(((int)$rangeX) - 1, ((int)$rangeY) - 1))
            );
            if (!in_array('X', $values)) {
                $answer2 = $key;
                break;
            }
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
