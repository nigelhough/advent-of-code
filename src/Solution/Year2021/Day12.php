<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day12 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end";
        }

        $relationships = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$source, $target] = explode('-', $line);
            if ($target != 'start') {
                $relationships[$source][] = $target;
            }
            if ($source != 'start' && $target != 'end') {
                $relationships[$target][] = $source;
            }
        }

        $paths = [];
        $completedPathsA = 0;
        $completedPathsB = 0;
        foreach ($relationships['start'] as $firstPoint) {
            $paths[][] = $firstPoint;
        }

        $pathIndex = 0;
        while (isset($paths[$pathIndex]) && $path = $paths[$pathIndex]) {
            // Check all the next steps of this path.
            foreach ($relationships[$path[array_key_last($path)]] as $destination) {
                $isBigCave = strtoupper($destination) === $destination;
                $hasVisited = in_array($destination, $path);
                $hasRevisited = isset($path['revisited']);

                // Stop exploring if:
                // - This is a small cave
                // - Has been visited and
                // - The path has already revisited a small cave.
                if (!$isBigCave && $hasVisited && $hasRevisited) {
//                    echo "revisit: start," . implode(',', array_values($path)) . ',' . $destination . PHP_EOL;
                    continue;
                }

                if ($destination === 'end') {
                    // Completed path.
                    if (!$hasRevisited) {
                        $completedPathsA++;
                    }
                    $completedPathsB++;
                } else {
                    $newPath = $path;
                    if (!$isBigCave && in_array($destination, $newPath)) {
                        $newPath['revisited'] = 'revisited';
                    }
                    $newPath[] = $destination;

                    // Keep exploring.
                    $paths[] = $newPath;
                }
            }
            $pathIndex++;
        }

        $answer1 = $completedPathsA;
        $answer2 = $completedPathsB;

        echo "Paths visiting a small cave once: {$answer1}" . PHP_EOL;
        echo "Paths visiting small caves once and a single small cave twice: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
