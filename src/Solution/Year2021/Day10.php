<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day10 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = '[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]';
        }

        $illegalFinds = [")" => 0, "]" => 0, "}" => 0, ">" => 0];
        $characterScores = ["(" => 1, "{" => 3, "[" => 2, "<" => 4];
        $lineScores = [];
        foreach (Parser::iterateByLine($input) as $line) {
            $chunk = '';
            foreach (str_split($line) as $chr) {
                $chunk .= $chr;
                if (!in_array($chr, [")", "}", "]", ">"])) {
                    // Not a closing bracket, the chunk must go on!
                    continue;
                }
                if (in_array(substr($chunk, -2), ["()", "{}", "[]", "<>"])) {
                    // Oh, what a lovely pair.
                    // Valid closing bracket. Let's get rid of this pair.
                    $chunk = substr($chunk, 0, strlen($chunk) - 2);
                    continue;
                }

                // Dodgy character found. We can break out of the line. This line won't be scored.
                $illegalFinds[$chr]++;
                continue 2;
            }

            // Score valid line.
            $lineScores[] = array_reduce(str_split(strrev($chunk)), function ($total, $chr) use ($characterScores) {
                return ($total * 5) + $characterScores[$chr];
            });
        }

        // Score illegal lines;
        $score = ($illegalFinds[')'] * 3) +
            ($illegalFinds[']'] * 57) +
            ($illegalFinds['}'] * 1197) +
            ($illegalFinds['>'] * 25137);
        $answer1 = $score;
        echo "Total syntax error: $answer1\n";

        // Score legal lines.
        sort($lineScores);
        $middle = floor(count($lineScores) / 2);
        $answer2 = $lineScores[$middle];
        echo "The middle score: $answer2\n";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
