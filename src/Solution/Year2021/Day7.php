<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day7 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '16,1,2,0,4,2,7,1,2,14';
        }
        $fuelRequired = [];
        $constantFuelRequired = [];

        $crabPositions = array_map('intval', explode(",", $input));
        $min = min($crabPositions);
        $max = max($crabPositions);

        $minFuel = PHP_INT_MAX;
        $minConstantFuel = PHP_INT_MAX;
        // Check the number of moves to every horizontal possible. (Between the Min and Max Crab Position)
        for ($i = $min; $i <= $max; $i++) {
            $fuelRequired[$i] = 0;
            $constantFuelRequired[$i] = 0;
            // For every crab position.
            foreach ($crabPositions as $crabPosition) {
                // Abandon when the required fuel goes above an already known minimum.
                if ($fuelRequired[$i] > $minFuel && $constantFuelRequired[$i] > $minConstantFuel) {
                    continue 2;
                }

                // Record the Fuel Differences for each position (for each crab).
                $diff = abs($crabPosition - $i);
                $fuelRequired[$i] += $diff;
                // Record the Differences for constant thrust to each position. For each crab.
                $constantFuelRequired[$i] += ($diff * ($diff + 1)) / 2;
            }
            $minFuel = min($fuelRequired);
            $minConstantFuel = min($constantFuelRequired);
        }

        echo "Minimum Fuel (part1): {$minFuel}\nMinimum Constant Fuel (part1): {$minConstantFuel}";
        return $this->answerFactory->create($minFuel, $minConstantFuel, isset($_GET['debug']));
    }
}
