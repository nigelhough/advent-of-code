<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\AmphipodOrganiser\Game\LessSuperSimpleGame;
use AdventOfCode\AmphipodOrganiser\Game\SimpleGame;
use AdventOfCode\AmphipodOrganiser\Game\SuperSimpleGame;
use AdventOfCode\AmphipodOrganiser\GameState;
use AdventOfCode\AmphipodOrganiser\Move;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day23 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = '#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########';
        }
        // Day 1 Done by hand.
        $answer1 = 17120;

//        echo "{$input}\n\n";

        $extra = "###\n  #D#C#B#A#\n  #D#B#A#C#\n  ";
        $input = str_replace("###\n  ", $extra, $input);
//        echo "{$input}\n\n";

        ini_set('max_execution_time', '0');

//        $game = new SuperSimpleGame();
        $game = new SimpleGame();
//        $game = new Game();

        $gameState = $game->startingState();
        echo $gameState . "\n\n";

        $moves = $gameState->getViableMoves();

        $paths = [];
        $smallest = PHP_INT_MAX;
        $i = 0;
        while (!empty($moves)) {
            $move = array_pop($moves);

            /** @var GameState $iterationGameState */
            $iterationGameState = clone $move->getGameState();
            $iterationGameState->playMove($move);
            if (
                isset($paths[md5(json_encode($iterationGameState->getPositions()))])
                && $paths[md5(json_encode($iterationGameState->getPositions()))] <= $iterationGameState->getScore()
            ) {
                // Already been at this state with a cheaper cost.
                continue;
            }
            $paths[md5(json_encode($iterationGameState->getPositions()))] = $iterationGameState->getScore();

            // Check game state for win.
            $isWinningState = $iterationGameState->isWinningState();
            if ($isWinningState) {
                $smallest = min($smallest, $iterationGameState->getScore());
//                echo "\n\n" . $iterationGameState . "\n" . $iterationGameState->getScore() . " {$smallest}\n";
            }

            if (!$isWinningState) {
                // Add next Moves.
                $viableMoves = $iterationGameState->getViableMoves();
                foreach ($viableMoves as $viableMove) {
                    $moves[] = $viableMove;
                }
            }
            $i++;
            if (php_sapi_name() === 'cli') {
                if ($i % 1000 === 0) {
                    echo ".";
                }
                if ($i % (1000 * 50) === 0) {
                    echo number_format($i) . " \n";
                }
            }
        }
        $answer2 = $smallest;

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return null;
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
