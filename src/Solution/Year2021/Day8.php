<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day8 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = 'be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce';

//            $input = 'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf';
        }

        $digits = [];
        $digits[0] = ['a', 'b', 'c', 'e', 'f', 'g'];
        $digits[1] = ['c', 'f'];
        $digits[2] = ['a', 'c', 'd', 'e', 'g'];
        $digits[3] = ['a', 'c', 'd', 'f', 'g'];
        $digits[4] = ['b', 'c', 'd', 'f'];
        $digits[5] = ['a', 'b', 'd', 'f', 'g'];
        $digits[6] = ['a', 'b', 'd', 'e', 'f', 'g'];
        $digits[7] = ['a', 'c', 'f'];
        $digits[8] = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        $digits[9] = ['a', 'b', 'c', 'd', 'f', 'g'];

        $letters = [];
        $letters['f'] = [0, 1, 3, 4, 5, 6, 7, 8, 9]; // 9
        $letters['a'] = [0, 2, 3, 5, 6, 7, 8, 9]; // 8
        $letters['c'] = [0, 1, 2, 3, 4, 7, 8, 9]; // 8
        $letters['d'] = [2, 3, 4, 5, 6, 8, 9]; // 7
        $letters['g'] = [0, 2, 3, 5, 6, 8, 9]; // 7
        $letters['b'] = [0, 4, 5, 6, 8, 9]; // 6
        $letters['e'] = [0, 2, 6, 8]; // 4

        $lengths = [];
        $lengths[2] = [1];
        $lengths[3] = [7];
        $lengths[4] = [4];
        $lengths[5] = [2, 3, 5];
        $lengths[6] = [0, 6, 9];
        $lengths[7] = [8];
        $singleDigitLengths = array_keys(
            array_filter($lengths, function (array $digits) {
                return count($digits) === 1;
            })
        );

        $count = 0;
        $sum = 0;
        foreach (Parser::iterateByLine($input) as $line) {
            [$signalPatterns, $outputValues] = explode(' | ', $line);
            $occurrences = [];
            foreach (array_keys($letters) as $letter) {
                $occurrences[$letter] = substr_count($signalPatterns, $letter);
            }
            $outputValues = explode(' ', $outputValues);
            $signalPatterns = explode(' ', $signalPatterns);

            // Part 1
            foreach ($outputValues as $value) {
                if (in_array(strlen($value), $singleDigitLengths)) {
                    $count++;
                }
            }

            // Part 2
            // A mapping of known position X to new position Y ([X => Y])
            $mapping = [];
            // A mapping of input signals patterns X to known signal patterns Y
            $signalMapping = [];
            // We can first match all values that have a unique length.
            foreach ($signalPatterns as $key => $pattern) {
                $signalLength = strlen($pattern);
                $matchingLengths = $lengths[$signalLength];
                $numberOfMatchingLengths = count($matchingLengths);
                if ($numberOfMatchingLengths === 1) {
                    $matchingSignal = $matchingLengths[0];
                    $signalMapping[$matchingSignal] = $pattern;
                    unset($signalPatterns[$key]);
                }
            }
            // 8, 7, 4, 1 are all known for having unique lengths
            // 8 and 7 share a common value (A) that both 1 and 4 do not have, so we know A.
            foreach (array_intersect(str_split($signalMapping[8]), str_split($signalMapping[7])) as $possibleA) {
                if (str_contains($signalMapping[1], $possibleA) || str_contains($signalMapping[4], $possibleA)) {
                    continue;
                }
                $mapping['a'] = $possibleA;
            }
            // We can use characters we know have unique occurrences to identify their mapping.
            foreach ($occurrences as $digit => $occurrence) {
                if ($occurrence === 9) {
                    $mapping['f'] = $digit;
                } elseif ($occurrence === 6) {
                    $mapping['b'] = $digit;
                } elseif ($occurrence === 4) {
                    $mapping['e'] = $digit;
                } elseif ($occurrence === 8 && $mapping['a'] !== $digit) {
                    $mapping['c'] = $digit;
                }
            }
            // 4 and 7 share common values (B, D), both 1 and 4 do not have, now B is known we can know D.
            foreach (array_intersect(str_split($signalMapping[4]), str_split($signalMapping[8])) as $possibleD) {
                if (str_contains($signalMapping[1], $possibleD) || str_contains($signalMapping[7], $possibleD)) {
                    continue;
                }
                if (in_array($possibleD, $mapping)) {
                    continue;
                }
                $mapping['d'] = $possibleD;
                break;
            }
            // We should only have one unmapped letter, G
            $unmappedLetter = array_diff(array_keys($letters), $mapping);
            $mapping['g'] = $unmappedLetter[array_key_first($unmappedLetter)];
            $mapping = array_flip($mapping);

            $strNumber = '';
            foreach ($outputValues as $outputValue) {
                // For each output, filter to a digit with matching letters (once mapped)
                $digit = array_filter($digits, function ($possibleDigit) use ($outputValue, $mapping) {
                    // Not a match if the letter count doesn't match.
                    if (count($possibleDigit) !== strlen($outputValue)) {
                        return false;
                    }

                    // Check all letters exist in the possible digit.
                    foreach (str_split($outputValue) as $chr) {
                        if (!in_array($mapping[$chr], $possibleDigit)) {
                            return false;
                        }
                    }
                    // Possible digit must be a match for this output value.
                    return true;
                });
                $strNumber .= array_key_first($digit);
            }
            $sum += (int) $strNumber;
        }

        echo "Number of Unique Letters Found: {$count}\n";
        echo "The Magic Number for Part 2: {$sum}\n";
        $answer1 = $count;
        $answer2 = $sum;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
