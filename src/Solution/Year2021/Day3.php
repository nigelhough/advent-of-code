<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010';
        }

        $c = strlen(Parser::iterateByLine($input)[0]);

        $gammaRate = implode($this->getMostCommonBits(Parser::iterateByLine($input)));
        $epsilonRate = implode($this->getLeastCommonBits(Parser::iterateByLine($input)));
        echo "Gamma Rate: {$gammaRate} (" . bindec($gammaRate) . ")\n";
        echo "Epsilon Rate: {$epsilonRate} (" . bindec($epsilonRate) . ")\n";
        echo "Power Consumption (Part1): " . bindec($gammaRate) * bindec($epsilonRate) . "\n\n";
        $answer1 = bindec($gammaRate) * bindec($epsilonRate);

        $oxygenList = Parser::iterateByLine($input);
        $co2List = Parser::iterateByLine($input);
        for ($i = 0; $i < $c; $i++) {
            $mostCommon = $this->getMostCommonBits($oxygenList)[$i];
            if (count($oxygenList) > 1) {
                $oxygenList = array_filter($oxygenList, function (string $number) use ($i, $mostCommon) {
                    return $mostCommon === substr($number, $i, 1);
                });
            }
            $mostCommon = $this->getMostCommonBits($co2List)[$i];
            if (count($co2List) > 1) {
                $co2List = array_filter($co2List, function (string $number) use ($i, $mostCommon) {
                    return $mostCommon !== substr($number, $i, 1);
                });
            }
        }

        $oRate = array_pop($oxygenList);
        $cRate = array_pop($co2List);
        echo "Oxygen Rate: {$oRate} (" . bindec($oRate) . ")\n";
        echo "CO2 Rate: {$cRate} (" . bindec($cRate) . ")\n";
        echo "Life Support Rating (Part2): " . bindec($oRate) * bindec($cRate) . "\n\n";
        $answer2 = bindec($oRate) * bindec($cRate);

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    /**
     * Get an array of the most common bits at each position in a list of binary numbers.
     *
     * @param array $binaryNumbers A list of binary numbers.
     *
     * @return array
     */
    public function getMostCommonBits(array $binaryNumbers): array
    {
        return array_map(function ($bit) {
            return ($bit[0] > $bit[1]) ? '0' : '1';
        }, $this->getBits($binaryNumbers));
    }

    /**
     * Get a list containing counts of bit values at each position in the array.
     *
     * @param array $binaryNumbers A list of binary numbers.
     *
     * @return array
     */
    public function getBits(array $binaryNumbers): array
    {
        $bits = [];
        foreach ($binaryNumbers as $binaryNumber) {
            foreach (str_split($binaryNumber) as $index => $bit) {
                $bits[$index][$bit] = isset($bits[$index][$bit]) ? $bits[$index][$bit] + 1 : 1;
            }
        }
        // Ensure bits have a value for 1 and 0. Prevents null checks elsewhere.
        foreach ($bits as $index => $bit) {
            $bits[$index][0] = $bit[0] ?? 0;
            $bits[$index][1] = $bit[1] ?? 0;
        }
        return $bits;
    }

    /**
     * Get an array of the least common bits at each position in a list of binary numbers.
     *
     * @param array $binaryNumbers A list of binary numbers.
     *
     * @return array
     */
    public function getLeastCommonBits(array $binaryNumbers): array
    {
        return array_map(function ($bit) {
            return ($bit[0] < $bit[1]) ? '0' : '1';
        }, $this->getBits($binaryNumbers));
    }
}
