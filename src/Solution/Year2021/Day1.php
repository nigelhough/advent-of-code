<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Arrays\Chunks;
use AdventOfCode\Arrays\Comparisons;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Generator;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '199
200
208
210
200
207
240
269
260
263';
        }

        $answer1 = $this->collectionIncreases(Chunks::progressiveChunk(Parser::iterateNumbersByLine($input)));
        $answer2 = $this->collectionIncreases(Chunks::progressiveChunk(Parser::iterateNumbersByLine($input), 3));

        echo "Part 1: {$answer1}\nPart 2: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));

        return new Answer(
            (string) $this->collectionIncreases(Chunks::progressiveChunk(Parser::iterateNumbersByLine($input))),
            (string) $this->collectionIncreases(Chunks::progressiveChunk(Parser::iterateNumbersByLine($input), 3))
        );
    }

    /**
     * Count the number of increases within a depth collection.
     *
     * @param Generator $depthCollection An iterable collection of depth measurements.
     *
     * @return int
     */
    public function collectionIncreases(Generator $depthCollection): int
    {
        $increased = 0;
        foreach (Comparisons::previousValue($depthCollection) as $comparisonMeasurements) {
            [$current, $previous] = $comparisonMeasurements;
            if (array_sum($current) > array_sum($previous)) {
                $increased++;
            }
        }
        return $increased;
    }
}
