<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'forward 5
down 5
forward 8
up 3
down 8
forward 2';
        }

        $pos = 0;
        $pos2 = 0;
        $depth = 0;
        $depth2 = 0;
        $aim = 0;
        foreach (Parser::iterateByLine($input) as $instruction) {
            [$direction, $value] = explode(' ', $instruction);
            $value = (int) $value;
            if ($direction === 'forward') {
                $depth2 += ($value * $aim);
                $pos += $value;
                $pos2 += $value;
            } elseif ($direction === 'down') {
                $depth += $value;
                $aim += $value;
            } elseif ($direction === 'up') {
                $depth -= $value;
                $aim -= $value;
            }
        }
        $answer1 = $depth * $pos;
        $answer2 = $depth2 * $pos2;

        echo "Part 1: {$answer1}\nPart 2: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
