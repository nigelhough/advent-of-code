<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day13 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5';
        }
        [$points, $folds] = explode("\n\n", $input);

        $map = new Map();

        foreach (explode("\n", $points) as $point) {
            $map->mark(Coord::fromString($point), '#');
        }

        $folds = str_replace('fold along ', '', $folds);
        foreach (explode("\n", $folds) as $instruction) {
            [$direction, $value] = explode('=', $instruction);
            foreach ($map->knownCoordinates() as $coordinate) {
                if (($direction === 'x' ? $coordinate->getXPosition() : $coordinate->getYPosition()) > $value) {
                    $map->clearCoordinate($coordinate, true);

                    $diffX = $direction === 'x' ? $coordinate->getXPosition() - $value : 0;
                    $diffY = $direction === 'y' ? $coordinate->getYPosition() - $value : 0;
                    $map->mark(
                        new Coord(
                            $coordinate->getXPosition() - ($diffX * 2),
                            $coordinate->getYPosition() - ($diffY * 2)
                        ),
                        '#'
                    );
                }
            }

            if ($answer1 === null) {
                $answer1 = $map->count();
            }
        }
        echo "Known points after first fold: {$answer1}\n\n";

        $map->scale();
        echo $map->plot(true, ' ') . "\n";
        $answer2 = 'EAHKRECP';

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
