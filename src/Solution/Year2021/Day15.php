<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;

class Day15 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581';
        }

        // Part 1.
        $map = Map::createFromString($input);
        $riskMap = $this->riskMap($map);
        $answer1 = $riskMap->get($map->getMBR()[1]);
        echo "Lowest Risk: " . $answer1 . PHP_EOL;

        // Part 2.
        $map = $this->projectMap($map);
        $riskMap = $this->riskMap($map);
        $answer2 = $riskMap->get($map->getMBR()[1]);
        echo "Lowest Risk (5x): " . $answer2 . PHP_EOL;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function projectMap(Map $map, int $projections = 5): Map
    {
        $projectedMap = new Map();

        $end = $map->getMBR()[1];
        $newX = $newY = 0;
        for ($yMod = 0; $yMod < $projections; $yMod++) {
            for ($x = 0; $x <= $end->getXPosition(); $x++) {
                for ($xMod = 0; $xMod < $projections; $xMod++) {
                    for ($y = 0; $y <= $end->getYPosition(); $y++) {
                        $value = ((int) $map->get(new Coord($x, $y))) + $xMod + $yMod;
                        if ($value > 9) {
                            $value = $value - 9;
                        }
                        $projectedMap->mark(new Coord($newY, $newX), $value);
                        $newX++;
                    }
                }
                $newX = 0;
                $newY++;
            }
        }
        return $projectedMap;
    }

    /**
     * Create a risk map from a map of coordinate risk.
     * The risk map returns the cost to get to a coordinate from 0,0
     *
     * @param Map $map The map of coordinate risks.
     *
     * @return Map
     */
    private function riskMap(Map $map): Map
    {
        $riskMap = new Map();
        $end = $map->getMBR()[1];

        // Iterate the points on the map in diagonal stripes.
        foreach ($this->diagonalPoints($end->getXPosition() + 1) as $point) {
            // Start always has a weight of 0.
            if ($point->isSame(Coord::fromString('0,0'))) {
                $riskMap->mark($point, 0);
                continue;
            }

            $riskToPoint = PHP_INT_MAX;
            foreach ($point->getNeighbours() as $neighbour) {
                // Check the neighbour is on the map.
                if ($map->get($neighbour) === null) {
                    continue;
                }
                // Check we have a risk up until this point.
                if ($riskMap->get($neighbour) === null) {
                    continue;
                }

                $riskToPoint = min($riskToPoint, $riskMap->get($neighbour));
            }
            // Risk is the  lowest risk to a surrounding coordinate plus the risk of this coordinate.
            $risk = $riskToPoint + $map->get($point);
            // Add to the risk map.
            $riskMap->mark($point, $risk);

            // Check neighbouring coordinates to see if you need to backtrack and recalculate.
            $backTrack = [];
            foreach ($point->getNeighbours() as $neighbour) {
                $backTrack[] = [$point, $neighbour];
            }
            // Iteratively backtrack.
            while (!empty($backTrack)) {
                [$point, $neighbour] = array_shift($backTrack);
                // Check the neighbour is on the map.
                if ($map->get($neighbour) === null) {
                    continue;
                }
                // Check we have a risk up until this point.
                if ($riskMap->get($neighbour) === null) {
                    continue;
                }

                $newRisk = $map->get($neighbour) + $riskMap->get($point);
                if ($riskMap->get($neighbour) > $newRisk) {
                    $riskMap->mark($neighbour, $newRisk);
                    foreach ($neighbour->getNeighbours() as $newNeighbour) {
                        $backTrack[] = [$neighbour, $newNeighbour];
                    }
                }
            }
        }

        return $riskMap;
    }

    /**
     * Create a list of points in a diagonal order from top left to bottom right.
     * This really confused me.
     *
     * @return Coord[]
     */
    private function diagonalPoints(int $width): array
    {
        $points = [];

        $cells = 1;

        // First Half increasing
        for ($diagonal = 0; $diagonal < $width; $diagonal++) {
            $y = 0;
            $x = $diagonal;
            for ($c = 0; $c < $cells; $c++) {
                $points[] = new Coord($x, $y);
                $y++;
                $x--;
            }
            $cells++;
        }
        for ($diagonal = 1; $diagonal < $width; $diagonal++) {
            $y = $diagonal;
            $x = $width - 1;
            for ($c = 0; $c < $cells; $c++) {
                $points[] = new Coord($x, $y);
                $y++;
                $x--;
            }
            $cells--;
        }

        return $points;
    }
}
