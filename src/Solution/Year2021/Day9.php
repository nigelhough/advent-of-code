<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;

class Day9 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '2199943210
3987894921
9856789892
8767896789
9899965678';
        }

        $map = Map::createFromString($input);

        // Get Low Points.
        $lowest = $map->locate(function ($low, $lowPoint) use ($map) {
            foreach ($map->getNeighbours(Coord::fromString($lowPoint)) as $neighbour) {
                // A neighbour is lower, this is not a low point.
                if ($neighbour < $low) {
                    return false;
                }
            }
            return true;
        });
        $answer1 = array_sum($lowest) + count($lowest);
        echo "Risk Level Sum: {$answer1}\n";

        $basinSizes = [];
        foreach ($lowest as $lowPoint => $low) {
            $basin = $this->findBasin(Coord::fromString($lowPoint), $map);
            $basinSizes[] = count($basin);
            //echo "LOW {$lowPoint} - {$low} count: ".count($basin)."\n";
        }
        rsort($basinSizes);
        $answer2 = $basinSizes[0] * $basinSizes[1] * $basinSizes[2];
        echo "Three largest Basin Sizes: {$answer2}";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    /**
     * Find the basin around low points on the map.
     *
     * @param Coord $point A low point
     * @param Map   $map   The map.
     *
     * @return array
     */
    public function findBasin(Coord $point, Map $map): array
    {
        $toVisit = [$point];
        $visited = [];

        while (!empty($toVisit)) {
            $visitPoint = array_pop($toVisit);
            $visited[(string) $visitPoint] = $visitPoint;

            foreach ($map->getNeighbours($visitPoint) as $newKey => $newValue) {
                // Skip if already visited or is a high point.
                if (isset($visited[$newKey]) || $newValue === '9') {
                    continue;
                }
                $toVisit[] = Coord::fromString($newKey);
            }
        }

        return $visited;
    }
}
