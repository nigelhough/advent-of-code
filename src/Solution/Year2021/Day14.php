<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day14 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = 'NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C';
        }
        [$templates, $rules] = explode("\n\n", $input);
        $firstChr = substr($templates, 0, 1);
        $polymers = [];
        foreach (explode("\n", $rules) as $rule) {
            [$pair, $insertion] = explode(' -> ', $rule);
            $polymers[$pair] = $insertion;
        }


        $currentPairs = [];
        $pairs = $this->getPairs($templates);
        foreach ($pairs as $pa) {
            $currentPairs[$pa] = ($currentPairs[$pa] ?? 0) + 1;
        }

        $pairsAfter10 = [];
        for ($i = 1; $i <= 40; $i++) {
            $newPairs = [];
            foreach ($currentPairs as $pair => $occurrences) {
                $insertion = $polymers[$pair];
                $newPairOne = substr($pair, 0, 1) . $insertion;
                $newPairTwo = $insertion . substr($pair, 1);
//                echo "{$pair} => {$occurrences}  ($newPairOne).[$insertion].($newPairTwo) >> ($none),($ntwo)\n";
                $newPairs[$newPairOne] = ($newPairs[$newPairOne] ?? 0) + $occurrences;
                $newPairs[$newPairTwo] = ($newPairs[$newPairTwo] ?? 0) + $occurrences;
            }
            $currentPairs = $newPairs;
            if ($i === 10) {
                $pairsAfter10 = $newPairs;
            }
        }

        $characters = $this->getCharacterOccurrences($pairsAfter10, $firstChr);
        $smallest = array_shift($characters);
        $largest = array_pop($characters);
        $answer1 = $largest - $smallest;

        $characters = $this->getCharacterOccurrences($currentPairs, $firstChr);
        $smallest = array_shift($characters);
        $largest = array_pop($characters);
        $answer2 = $largest - $smallest;

        echo "Part 1: $answer1\nPart 2: $answer2";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function getCharacterOccurrences(array $pairs, string $firstChr): array
    {
        $characters = [];
        // Count the right character of all pairs.
        foreach ($pairs as $pair => $occurrences) {
            $rChr = substr($pair, 1);
            $characters[$rChr] = (isset($characters[$rChr]) ? ($characters[$rChr] + $occurrences) : $occurrences);
        }
        // Extra one for the first character as it is not the right of any pair.
        $characters[$firstChr]++;
        asort($characters);
        return $characters;
    }

    public function getPairs($string): array
    {
        $a = [];
        $last = '';
        foreach (str_split($string) as $chr) {
            if ($last === '') {
                $last = $chr;
                continue;
            }
            $a[] = $last . $chr;

            $last = $chr;
        }

        return $a;
    }
}
