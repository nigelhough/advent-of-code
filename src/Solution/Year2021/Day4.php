<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Bingo\Board;
use AdventOfCode\Bingo\Game;
use AdventOfCode\Solution\AbstractSolution;

class Day4 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7';
        }
        [$calls, $boards] = explode("\n\n", $input, 2);

        $bingo = new Game($boards);

        foreach (array_map('intval', explode(",", $calls)) as $call) {
            $bingo->call($call);
            if ($bingo->gameCompleted()) {
                break;
            }
        }

        $completedBoards = $bingo->getCompletedBoards();
        usort($completedBoards, function (Board $a, Board $b) {
            return $a->getCalls() <=> $b->getCalls();
        });

        $answer1 = $completedBoards[array_key_first($completedBoards)]->score();
        $answer2 = $completedBoards[array_key_last($completedBoards)]->score();

        echo "Part 1: {$answer1}\n";
        echo "Part 2: {$answer2}\n";
//        echo implode(
//            "\n",
//            array_map(function (Board $board) {
//                return "Score: {$board->score()}, " .
//                    "Calls: {$board->getCalls()}, " .
//                    "LastCall: {$board->getLastCall()}, " .
//                    "Unmarked: " . implode(
//                        ',',
//                        array_keys($board->getUnMarkedNumbers())
//                    );
//            }, $completedBoards)
//        );

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
