<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\SnailNumber\SnailNumber;
use AdventOfCode\Solution\AbstractSolution;

class Day18 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]';
        }

        $lines = array_map(fn($line) => new SnailNumber($line), Parser::iterateByLine($input));
        $snailNumber = SnailNumber::processList(...$lines);

        $answer2 = 0;
        foreach ($lines as $subject) {
            foreach ($lines as $target) {
                $snailNumber = SnailNumber::processList($subject, $target);
                $answer2 = max($answer2, $snailNumber->getMagnitude());
            }
        }

        $answer1 = $snailNumber->getMagnitude();

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
