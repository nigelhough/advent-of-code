<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day11 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526';
        }
        $map = Map::createFromString($input);
        //echo $map->plot() . PHP_EOL;

        $flashes = 0;
        $steps = 0;
        while ($map->count() !== count($map->locate(fn ($v) => $v === 0))) {
            $steps++;
            // First, the energy level of each octopus increases by 1
            $map->incrementRange(...$map->getMBR());

            // Then, any octopus with an energy level greater than 9 flashes.
            $flashing = $map->locate(fn ($v) => $v > 9);
            $visited = [];
            while (count($flashing) > 0) {
                $flashes++;
                $flashCoord = array_key_first($flashing);
                array_shift($flashing);
                $visited[$flashCoord] = $flashCoord;
                // This increases the energy level of all adjacent octopuses by 1,
                // including octopuses that are diagonally adjacent.
                foreach ((Coord::fromString($flashCoord))->getSurrounding() as $surroundCoord) {
                    if ($map->get($surroundCoord) === null) {
                        continue;
                    }
                    if (isset($visited[(string)$surroundCoord])) {
                        continue;
                    }
                    $map->incrementRange($surroundCoord, $surroundCoord);
                    // First Flash.
                    if ($map->get($surroundCoord) === 10) {
                        $flashing[(string)$surroundCoord] = 10;
                    }
                }
            }

            // Finally, any octopus that flashed during this step has its energy level set to 0,
            // as it used all of its energy to flash.
            foreach ($map->locate(fn ($v) => $v > 9) as $key => $value) {
                $map->mark(Coord::fromString($key), 0);
            }

            if ($steps === 100) {
                $answer1 = $flashes;
                echo "Step {$steps}. Flashes {$flashes}." . PHP_EOL;
            }
        }
        echo "Step {$steps}. Flashes {$flashes}." . PHP_EOL;
        $answer2 = $steps;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
