<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Grid\Path;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day5 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2';
        }

        $map1 = new Map();
        $map2 = new Map();
        foreach (Coord::setListFromString($input, setDelimiter: ' -> ') as [$start, $end]) {
            $path = Path::betweenCoordinates($start, $end);
            $map2->incrementAlongPath($path);

            if ((Bearing::toCoordinate($start, $end))->isDiagonal()) {
                continue;
            }
            $map1->incrementAlongPath($path);
        }

        $expensiveLocations = $map1->locate(function ($c) {
            return ($c > 1);
        });
        echo "Expensive Points (1): " . count($expensiveLocations) . "\n";
//        echo $map1->plot(true);

        $expensiveLocations2 = $map2->locate(function ($c) {
            return ($c > 1);
        });
        echo "Expensive Points (2): " . count($expensiveLocations2) . "\n";
//        echo $map2->plot(true);
        return $this->answerFactory->create(count($expensiveLocations), count($expensiveLocations2));
    }
}
