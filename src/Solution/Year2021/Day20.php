<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;

class Day20 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###';
        }

        $input = str_replace(['.', '#'], ['0', '1'], $input);
        [$enhancement, $baseImage] = explode("\n\n", $input);
        $enhancement = str_split($enhancement);
        $baseMap = Map::createFromString($baseImage);

        $hashes = [];
        $evenInfiniteCharacter = '0';
        $oddInfiniteCharacter = $enhancement[511] === '0' ? '1' : '0';
        for ($i = 0; $i < 50; $i++) {
            $outputImage = new Map();
            [$start, $end] = $baseMap->getMBR();
            for ($x = $start->getXPosition() - 1; $x <= $end->getXPosition() + 1; $x++) {
                for ($y = $start->getYPosition() - 1; $y <= $end->getYPosition() + 1; $y++) {
                    $position = new Coord($x, $y);
                    $string = '';
//                    foreach ($position->getEncompassing() as $surround) {
                    foreach ($position->getSurrounding() as $surround) {
                        $string .= $baseMap->get($surround) ?? (($i % 2 === 0) ? $evenInfiniteCharacter : $oddInfiniteCharacter);
                    }
                    $outputImage->mark($position, $enhancement[bindec($string)]);
                }
            }
            $baseMap = $outputImage;

            if($i == 1) {
                $hashes = $baseMap->locate(fn($coord) => $coord === '1');
            }
        }

        $answer1 = count($hashes);
        $hashes = $baseMap->locate(fn($coord) => $coord === '1');
        $answer2 = count($hashes);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
