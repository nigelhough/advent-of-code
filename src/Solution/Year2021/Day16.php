<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day16 extends AbstractSolution
{
    // If the length type ID is 0, then the next 15 bits are a number that represents the total length
    private const LENGTH_TYPE_TOTAL = '0';

    // If the length type ID is 1, then the next 11 bits are a number that represents the number of sub-packets
    private const LENGTH_TYPE_SUB = '1';

    //Packets with type ID 4 represent a literal value
    private const PACKET_TYPE_LITERAL = 4;

    //Packets with type ID 0 are sum packets
    private const PACKET_TYPE_SUM = 0;

    //Packets with type ID 1 are product packets
    private const PACKET_TYPE_PRODUCT = 1;

    //Packets type ID 2 are minimum packets
    private const PACKET_TYPE_MINIMUM = 2;

    //Packets type ID 3 are maximum packets
    private const PACKET_TYPE_MAXIMUM = 3;

    //Packets type ID 5 are greater than packets
    private const PACKET_TYPE_GREATHER = 5;

    //Packets type ID 6 are less than packets
    private const PACKET_TYPE_LESS = 6;

    //Packets type ID 7 are equal to packets
    private const PACKET_TYPE_EQUAL = 7;

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '9C0141080250320F1802104A08';
        }

        // Convert input to binary string representing a packet.
        $packet = '';
        foreach (str_split($input) as $str) {
            $packet .= str_pad(decbin((int) base_convert($str, 16, 10)), 4, '0', STR_PAD_LEFT);
        }

        $versions = [];
        $answer2 = $this->parsePacket($packet, $versions);

        $answer1 = array_sum($versions);
        echo "\nPart1: {$answer1}\nPart2: {$answer2}";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    /**
     * Process a packet, returning the result of an operation.
     *
     * @param string $packet   The packet to process, including sub-packages
     * @param array  $versions A bucket to collect discovered version numbers in.
     *
     * @return int
     */
    private function parsePacket(string &$packet, array &$versions): int
    {
        // Read packet header.
        $versions[] = bindec(substr($packet, 0, 3));
        $type = bindec(substr($packet, 3, 3));
        $packet = substr($packet, 6);

        // Packets with type ID 4 represent a literal value
        if ($type === self::PACKET_TYPE_LITERAL) {
            // Literal value packets encode a single binary number. To do this,
            $literal = '';
            do {
                $bit = substr($packet, 0, 5);
                $packet = substr($packet, 5);
                $literal .= substr($bit, 1);
                //Each group is prefixed by a 1 bit except the last group, which is prefixed by a 0 bit.
            } while (str_starts_with($bit, '1'));

            return bindec($literal);
        }

        // Every other type of packet (any packet with a type ID other than 4) represent an operator
        $lengthType = substr($packet, 0, 1);
        $packet = substr($packet, 1);

        // If the length type ID is 0, then the next 15 bits are a number that represents the total length
        // If the length type ID is 1, then the next 11 bits are a number that represents the number of sub-packets
        $length = $lengthType === self::LENGTH_TYPE_TOTAL ? 15 : 11;
        $subPacketLength = bindec(substr($packet, 0, $length));
        $packet = substr($packet, $length);

        // Read sub packets.
        $subPacketValues = [];
        if ($lengthType === self::LENGTH_TYPE_TOTAL) {
            $subPackets = substr($packet, 0, $subPacketLength);
            $packet = substr($packet, $subPacketLength);
            // Parse all sub packets in the fixed string equal to the length.
            while (trim($subPackets, '0') !== '') {
                $literalValue = $this->parsePacket($subPackets, $versions);
                $subPacketValues[] = $literalValue;
            }
        } elseif ($lengthType === self::LENGTH_TYPE_SUB) {
            // Pass a number of packets equal to the length.
            for ($i = 0; $i < $subPacketLength; $i++) {
                $literalValue = $this->parsePacket($packet, $versions);
                $subPacketValues[] = $literalValue;
            }
        }

        // Perform Operation.
        return match ($type) {
            self::PACKET_TYPE_SUM => array_sum($subPacketValues),
            self::PACKET_TYPE_PRODUCT => array_product($subPacketValues),
            self::PACKET_TYPE_MINIMUM => min($subPacketValues),
            self::PACKET_TYPE_MAXIMUM => max($subPacketValues),
            self::PACKET_TYPE_GREATHER => $subPacketValues[0] > $subPacketValues[1] ? 1 : 0,
            self::PACKET_TYPE_LESS => $subPacketValues[0] < $subPacketValues[1] ? 1 : 0,
            self::PACKET_TYPE_EQUAL => $subPacketValues[0] === $subPacketValues[1] ? 1 : 0,
        };
    }
}
