<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Answer\Answer;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day6 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '3,4,3,1,2';
        }
        $fish = array_map('intval', explode(',', $input));
        $fishDays = array_fill(0, 9, 0);
        foreach ($fish as $days) {
            $fishDays[$days]++;
        }

        $answer1 = '';
        $noDays = 256;
        for ($day = 1; $day <= $noDays; $day++) {
            $day0 = $fishDays[0];
            $day1 = $fishDays[1];
            $day2 = $fishDays[2];
            $day3 = $fishDays[3];
            $day4 = $fishDays[4];
            $day5 = $fishDays[5];
            $day6 = $fishDays[6];
            $day7 = $fishDays[7];
            $day8 = $fishDays[8];

            $fishDays[0] = $day1;
            $fishDays[1] = $day2;
            $fishDays[2] = $day3;
            $fishDays[3] = $day4;
            $fishDays[4] = $day5;
            $fishDays[5] = $day6;
            $fishDays[6] = $day7 + $day0;
            $fishDays[7] = $day8;
            $fishDays[8] = $day0;

            if ($day === 80) {
                $answer1 = (string) array_sum($fishDays);
                echo "After {$day} days the number of lanternfish is: " . array_sum($fishDays) . "\n\n";
            }
        }

        $answer2 = (string) array_sum($fishDays);
        echo "After {$noDays} days the number of lanternfish is: " . array_sum($fishDays) . "\n";
        return new Answer($answer1, $answer2);
    }
}
