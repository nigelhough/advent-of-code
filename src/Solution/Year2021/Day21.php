<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Dies\FixedDie;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day21 extends AbstractSolution
{
    private const QUANTUM_ROLLS = [
        3 => 1,
        4 => 3,
        5 => 6,
        6 => 7,
        7 => 6,
        8 => 3,
        9 => 1,
    ];

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = 'Player 1 starting position: 4
Player 2 starting position: 8';
        }
        echo "{$input}\n\n";
        $players = [];
        foreach (Parser::iterateByLine($input) as $playerLine) {
            $playerLine = str_replace(' starting position', '', $playerLine);
            [$player, $position] = explode(':', $playerLine);
            $players[] = [
                'player' => $player,
                'position' => (int) $position,
                'score' => 0,
            ];
        }
        $player1Score = $players[0]['score'];
        $player1Position = $players[0]['position'];
        $player2Score = $players[1]['score'];
        $player2Position = $players[1]['position'];

        $die = new FixedDie();
        $dieRoll = $die->roll();
        $boardSpaces = 10;
        $scoreTarget = 1000;
        $rollCount = 0;
        while (true) {
            foreach ($players as &$player) {
                $rolls = [];
                for ($i = 0; $i < 3; $i++) {
                    $rolls[] = $dieRoll->current();
                    $dieRoll->next();
                    $rollCount++;
                }
                $player['position'] = ($player['position'] + array_sum($rolls)) % $boardSpaces ?: $boardSpaces;
                $player['score'] += $player['position'];
                if ($player['score'] >= $scoreTarget) {
                    $lowest = min(array_column($players, 'score'));
                    $answer1 = ($lowest * $rollCount);
//                    echo "\n$lowest * $rollCount = $answer1 \n";
                    break 2;
                }
            }
        }

        $player1OverallScore = 0;
        $player2OverallScore = 0;
        $activeGameStates = [];
        $turn = 1;
        $activeGameStates["{$player1Score}_{$player1Position}_{$player2Score}_{$player2Position}_{$turn}"] = 1;
        $scoreTarget = 21;

        while ($activeGameStates) {
            $gameStateKey = array_key_first($activeGameStates);
            $count = $activeGameStates[$gameStateKey];
            unset($activeGameStates[$gameStateKey]);
            foreach (self::QUANTUM_ROLLS as $roll => $factor) {
                [$player1Score, $player1Position, $player2Score, $player2Position, $turn] = explode('_', $gameStateKey);

                if ($turn == 1) {
                    $player1Score += $player1Position = (($player1Position + $roll) % $boardSpaces ?: $boardSpaces);
                    $turn = 2;
                    if ($player1Score >= $scoreTarget) {
                        $player1OverallScore += ($count * $factor);
                        continue;
                    }
                } else {
                    $player2Score += $player2Position = (($player2Position + $roll) % $boardSpaces ?: $boardSpaces);
                    $turn = 1;
                    if ($player2Score >= $scoreTarget) {
                        $player2OverallScore += ($count * $factor);
                        continue;
                    }
                }
                $key = "{$player1Score}_{$player1Position}_{$player2Score}_{$player2Position}_{$turn}";
                if (!isset($activeGameStates[$key])) {
                    $activeGameStates[$key] = 0;
                }
                $activeGameStates[$key] += ($count * $factor);
            }
        }
        $answer2 = max([$player1OverallScore, $player2OverallScore]);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
