<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2021;

use AdventOfCode\Cube\Cuboid;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day22 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $reactor = [];
        foreach (Parser::iterateByLine($input) as $step) {
            [$state, $coords] = explode(' ', $step);
            [$xRange, $yRange, $zRange] = explode(',', $coords);
            [$xMin, $xMax] = explode('..', substr($xRange, 2));
            [$yMin, $yMax] = explode('..', substr($yRange, 2));
            [$zMin, $zMax] = explode('..', substr($zRange, 2));
            for ($x = max(-50, $xMin); $x <= min(50, $xMax); $x++) {
                for ($y = max(-50, $yMin); $y <= min(50, $yMax); $y++) {
                    for ($z = max(-50, $zMin); $z <= min(50, $zMax); $z++) {
                        $key = "{$x},{$y},{$z}";
                        if ($state === 'on') {
                            $reactor[$key] = 1;
                        } else {
                            unset($reactor[$key]);
                        }
                    }
                }
            }
        }
        $answer1 = count($reactor);

        /** @var Cuboid[] $onCubes */
        $onCubes = [];
        foreach (Parser::iterateByLine($input) as $step) {
            [$state, $ranges] = explode(' ', $step);
            $cube = Cuboid::fromString($ranges);

            // If none of the existing On Cubes intersect then this can be applied as a whole cube.
            if (!$cube->intersectsWithAny(...$onCubes)) {
                // If on, add this as an on cube.
                if ($state == 'on') {
                    $onCubes[(string) $cube] = $cube;
                }
                // (off just don't add it)
                continue;
            }
            foreach ($onCubes as $key => $onCube) {
                // If this on cube doesn't intersect, it can remain a whole cube on.
                if (!$intersection = $onCube->getIntersection($cube)) {
                    continue;
                }
                // Remove the whole on cube that intersects.
                unset($onCubes[$key]);
                // Add the parts of the onCube that don't intersect to the list of new cubes.
                foreach ($onCube->getInverseIntersections($intersection) as $newCube) {
                    $onCubes[(string) $newCube] = $newCube;
                }
            }
            if ($state == 'on') {
                // If this is an on state then this new cube will be on (any intersecting will remove the overlaps)
                $onCubes[(string) $cube] = $cube;
            }
        }
        $answer2 = array_reduce($onCubes, fn(int $carry, Cuboid $cuboid) => $carry + $cuboid->getVolume(), 0);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
