<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $elves = Parser::iterateByGroup($input);
        $elves = array_map(function ($e) {
            return Parser::iterateNumbersByLine($e);
        }, $elves);
        $elvesTotals = array_map('array_sum', $elves);
        $answer1 = max($elvesTotals);

        rsort($elvesTotals);
        $topThree = array_slice($elvesTotals, 0, 3);
        $answer2 = array_sum($topThree);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
