<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day13 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        // Sum keys of groups where comparison result is true.
        $answer1 = 0;
        foreach (Parser::iterateByGroup($input) as $key => $group) {
            [$a, $b] = array_map(fn($a) => json_decode($a, true), Parser::iterateByLine($group));
            if ($this->compare($a, $b) === -1) {
                $answer1 += ($key + 1);
            }
        }

        // Sort lines by comparison result and multiply two known keys.
        $input = str_replace("\n\n", "\n", $input) . "\n[[2]]\n[[6]]";
        $lines = Parser::iterateByLine($input);
        usort(
            $lines,
            fn($lineA, $lineB) => ($this->compare(json_decode($lineA, true), json_decode($lineB, true)))
        );
        $answer2 = (array_search('[[2]]', $lines) + 1) * (array_search('[[6]]', $lines) + 1);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function compare($a, $b): int
    {
        for ($i = 0; isset($a[$i]) || isset($b[$i]); $i++) {
            // Either side empty
            if (!isset($a[$i]) || !isset($b[$i])) {
                // Has left run out?
                return !isset($a[$i]) ? -1 : 1;
            }

            // Compare numbers
            if (is_numeric($a[$i]) && is_numeric($b[$i])) {
                if ($a[$i] == $b[$i]) {
                    // Equal
                    continue;
                }
                // Is left smaller than right?
                return $a[$i] < $b[$i] ? -1 : 1;
            }

            // Compare lists (cast to catch mixed type)
            $result = $this->compare((array) $a[$i], (array) $b[$i]);
            if ($result === 0) {
                continue;
            }
            return $result;
        }
        return 0;
    }
}
