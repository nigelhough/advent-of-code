<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Device\DataStream;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day6 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw';
        }
        $dataStream = new DataStream($input);
        $answer1 = $dataStream->packetMarkerEnd;
        $answer2 = $dataStream->messageMarkerEnd;

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
