<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day9 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = 'R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20';
        }

        $input = str_replace(
            ['U', 'R', 'D', 'L'],
            [Bearing::NORTH, Bearing::EAST, Bearing::SOUTH, Bearing::WEST],
            $input
        );

        $headPosition = new Coord(0, 0);
        $tails = [
            '1' => new Coord(0, 0),
            '2' => new Coord(0, 0),
            '3' => new Coord(0, 0),
            '4' => new Coord(0, 0),
            '5' => new Coord(0, 0),
            '6' => new Coord(0, 0),
            '7' => new Coord(0, 0),
            '8' => new Coord(0, 0),
            '9' => new Coord(0, 0),
        ];
        $positions1 = [];
        $positions9 = [];
        $positions1[(string) $tails['1']] = $tails['1'];
        $positions9[(string) $tails['9']] = $tails['9'];
        foreach (Parser::iterateByLine($input) as $line) {
            [$bearing, $steps] = explode(' ', $line);
            for ($s = 0; $s < (int) $steps; $s++) {
                $headPosition = $headPosition->moveAlongBearing(Bearing::get($bearing));
                foreach ($tails as $tailId => &$tailPosition) {
                    $target = $tails[$tailId - 1] ?? $headPosition;

                    if ($target->isSame($tailPosition)) {
                        // The tail is in the same location as head, which is cool.
                        continue;
                    }
                    $isInSurrounding = false;
                    foreach ($target->getSurrounding() as $surroundingCoord) {
                        if ($surroundingCoord->isSame($tailPosition)) {
                            // The tail is within one step of the head, which is cool.
                            $isInSurrounding = true;
                            break;
                        }
                    }
                    if ($isInSurrounding) {
                        continue;
                    }

                    // Check if any of the target's neighbouring coordinates are in line of sight. N,S,E,W
                    $validCoordinate = array_flip(array_map('strval', $target->getNeighbours()));
                    if (!$newTailPosition = $tailPosition->getAnyFirstValidLineOfSight($validCoordinate, 10)) {
                        // Check if any of the target's surrounding coordinates are in line of sight.
                        $validCoordinate = array_flip(array_map('strval', $target->getSurrounding()));
                        $newTailPosition = $tailPosition->getAnyFirstValidLineOfSight($validCoordinate, 10);
                    }
                    $tailPosition = $newTailPosition;
                }
                $positions1[(string) $tails['1']] = $tails['1'];
                $positions9[(string) $tails['9']] = $tails['9'];
            }
//            $map = new Map();
//            $map->mark(new Coord(0, 0), 's');
//            foreach (array_reverse($tails, true) as $tailId => $tailPosition) {
//                $map->mark($tailPosition, $tailId);
//            }
//            $map->mark($headPosition, 'H');
//            echo $map->plot(true) . "\n\n";
        }

        $answer1 = count($positions1);
        $answer2 = count($positions9);

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
