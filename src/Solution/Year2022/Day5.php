<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day5 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = '    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2';
        }
        $input = str_replace(' 1   2   3 ', "\n 1   2   3 ", $input);
        [$crates, , $steps] = explode("\n\n", $input);
        echo $crates . "\n\n";
        $crates = array_map(fn($line) => str_split($line), explode("\n", $crates));
        $stacks = [];
        for ($i = 0; $i < count($crates[0]); $i++) {
            $stack = array_filter(array_column($crates, $i), fn($c) => !in_array($c, ['[', ']', ' ']));
            if (empty($stack)) {
                continue;
            }
            $stacks[] = $stack;
        }

        $answer1Stacks = $stacks;
        $answer2Stacks = $stacks;
        $steps = str_replace(['move ', ' from ', ' to '], ',', $steps);
        foreach (Parser::iterateByLine($steps) as $step) {
            [$move, $from, $to] = array_map('intval', explode(',', trim($step, ',')));
            $from -= 1;
            $to -= 1;

            // Answer 1.
            $crates = array_reverse(array_slice($answer1Stacks[$from], 0, $move));
            $answer1Stacks[$from] = array_slice($answer1Stacks[$from], $move);
            $answer1Stacks[$to] = array_merge($crates, $answer1Stacks[$to]);

            // Answer 2.
            $crates = array_slice($answer2Stacks[$from], 0, $move);
            $answer2Stacks[$from] = array_slice($answer2Stacks[$from], $move);
            $answer2Stacks[$to] = array_merge($crates, $answer2Stacks[$to]);
        }
        $answer1 = array_reduce($answer1Stacks, fn(string $carry, array $stack) => $carry . $stack[0], '');
        $answer2 = array_reduce($answer2Stacks, fn(string $carry, array $stack) => $carry . $stack[0], '');

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
