<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day2 extends AbstractSolution
{
    private const ROCK = 'A';
    private const PAPER = 'B';
    private const SCISSORS = 'C';

    private const SCORES = [
        self::ROCK => 1,
        self::PAPER => 2,
        self::SCISSORS => 3,
    ];

    private const WINNING_MOVE = [
        self::SCISSORS => self::ROCK,
        self::PAPER => self::SCISSORS,
        self::ROCK => self::PAPER,
    ];
    private const LOSING_MOVE = [
        self::ROCK => self::SCISSORS,
        self::SCISSORS => self::PAPER,
        self::PAPER => self::ROCK,
    ];

    private const LOSE = 'X';
    private const DRAW = 'Y';
    private const WIN = 'Z';

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = 0;
        if (isset($_GET['sample'])) {
            $input = 'A Y
B X
C Z';
        }

        // De-duplicate games.
        $rounds = [];
        foreach (Parser::iterateByLine($input) as $round) {
            if (!isset($rounds[$round])) {
                $rounds[$round] = 1;
                continue;
            }
            $rounds[$round]++;
        }

        foreach ($rounds as $round => $factor) {
            [$opponentMove, $playerMove] = explode(' ', $round);
            $answer1 += ($this->play($opponentMove, $playerMove) * $factor);
            $answer2 += ($this->playOutcome($opponentMove, $playerMove) * $factor);
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function play(string $opponentMove, string $playerMove): int
    {
        $playerMove = str_replace(['X', 'Y', 'Z'], [self::ROCK, self::PAPER, self::SCISSORS], $playerMove);

        return self::SCORES[$playerMove] +
            ($this->isWinner($opponentMove, $playerMove) ? 6 : 0) +
            ($this->isDraw($opponentMove, $playerMove) ? 3 : 0);
    }

    private function playOutcome(string $opponentMove, string $outcome): int
    {
        $playerMove = $this->outcomeMove($opponentMove, $outcome);
        return $this->play($opponentMove, $playerMove);
    }

    private function outcomeMove(string $opponentMove, string $outcome): string
    {
        if ($outcome === self::DRAW) {
            return $opponentMove;
        }
        return ($outcome === self::WIN) ?
            self::WINNING_MOVE[$opponentMove] :
            self::LOSING_MOVE[$opponentMove];
    }

    private function isDraw(string $opponentMove, string $playerMove): bool
    {
        return ($opponentMove === $playerMove);
    }

    private function isWinner(string $opponentMove, string $playerMove): bool
    {
        return self::WINNING_MOVE[$opponentMove] === $playerMove;
    }
}
