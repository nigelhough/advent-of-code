<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Alphabet\Alphabet;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day12 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = 'Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi';
        }

        $map = Map::createFromString($input);
        $start = Coord::fromString(array_key_first($map->locate(fn($location) => $location === 'E')));
        $destination = Coord::fromString(array_key_first($map->locate(fn($location) => $location === 'S')));
        $map->mark($start, 'z');
        $map->mark($destination, 'a');

        // Find the shortest route from destination to start.
        $route = $map->findRoute($start, function (Map $map, Coord $location) use ($destination) {
            return $location->isSame($destination);
        }, function (Map $map, Coord $location, Coord $neighbour): bool {
            if ($map->get($neighbour) === null) {
                return false;
            }
            return Alphabet::chrToNum($map->get($neighbour)) >= (Alphabet::chrToNum($map->get($location)) - 1);
        });
        foreach ($route->getPath()->follow() as $step) {
            $map->mark($step, '.');
        }
        echo $map->plot(true) . "\n\n";
        $answer1 = $route->count();


        $map = Map::createFromString($input);
        $map->mark($start, 'z');
        $map->mark($destination, 'a');
        // Find the shortest route from destination to any a.
        $route = $map->findRoute($start, function (Map $map, Coord $location) {
            return $map->get($location) === 'a';
        }, function (Map $map, Coord $location, Coord $neighbour): bool {
            if ($map->get($neighbour) === null) {
                return false;
            }
            return Alphabet::chrToNum($map->get($neighbour)) >= (Alphabet::chrToNum($map->get($location)) - 1);
        });
        foreach ($route->getPath()->follow() as $step) {
            $map->mark($step, '.');
        }
        echo $map->plot(true);
        $answer2 = $route->count();

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
