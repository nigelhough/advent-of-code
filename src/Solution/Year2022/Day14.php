<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day14 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9';
        }
//        echo "{$input}\n";

        // Draw Map.
        $map = new Map();
        foreach (Parser::iterateByLine($input) as $line) {
            $points = explode(' -> ', $line);
            $lastPoint = $points[0];
            for ($i = 1; $i < count($points); $i++) {
                $map->markRange(Coord::fromString($lastPoint), Coord::fromString($points[$i]));
                $lastPoint = $points[$i];
            }
        }
//        echo $map->plot(true) . "\n\n";
        $lowestY = $map->getMBR()[1]->getYPosition();

        $sandEntry = new Coord(500, 0);
        // Extend Map
        $seabed = $map->getMBR()[1]->getYPosition() + 2;
        for ($i = (500+160)-($lowestY*2); $i < (500-160)+($lowestY*2); $i++) {
            $map->mark(new Coord($i, $seabed));
        }

        $answer1 = 0;
        $answer2 = 0;
        while (($landingPoint = $map->fall($sandEntry)) !== null) {
            $map->mark($landingPoint, 'O');
            if (!$answer1 && $landingPoint->getYPosition() > $lowestY) {
                $answer1 = $answer2;
            }
            $answer2++;
            if ($landingPoint->isSame($sandEntry)) {
                break;
            }
        }
        echo $map->plot(true, ' ') . "\n\n";

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
