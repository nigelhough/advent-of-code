<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day10 extends AbstractSolution
{
    private const NOOP = 'noop';

    public function execute(string $input): ?AnswerInterface
    {
        $x = 1;
        $cycles = 0;
        $signalStrength = 0;
        $output = [];
        $cycles = $this->incrementCycles($cycles, $x, $signalStrength, $output);
        foreach (Parser::iterateByLine($input) as $line) {
            [$instruction, $value] = explode(' ', $line . ' ');
            if ($instruction === self::NOOP) {
                $cycles = $this->incrementCycles($cycles, $x, $signalStrength, $output);
                continue;
            }
            $cycles = $this->incrementCycles($cycles, $x, $signalStrength, $output);
            $x += (int) $value;
            $cycles = $this->incrementCycles($cycles, $x, $signalStrength, $output);
        }
        $answer1 = $signalStrength;
        $answer2 = 'RFKZCPEF';

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n\n";
        echo implode("\n", array_map(fn(array $a) => implode('', $a), $output));

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function incrementCycles(int $cycles, int $x, int &$signalStrength, array &$output, int $scale = 2): int
    {
        static $rowId = 0;
        for ($i = 0; $i < $scale; $i++) {
            $output[$rowId + $i][] = in_array($cycles % 40, [$x - 1, $x, $x + 1])
                ? str_repeat('█', $scale)
                : str_repeat(' ', $scale);
        }
        $cycles++;
        if (($cycles + 20) % 40 === 0) {
            $signalStrength += $x * $cycles;
        }
        if ($cycles % 40 === 0) {
            $rowId += $scale;
        }
        return $cycles;
    }
}
