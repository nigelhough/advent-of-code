<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\MonkeyInTheMiddle\Monkey;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day11 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $input = str_replace(
            [
                'Monkey',
                'Starting items:',
                'Operation: new = old',
                'Test: divisible by',
                'If true: throw to monkey',
                'If false: throw to monkey',
                ':',
                ' ',
            ],
            '',
            $input
        );
        /** @var Monkey[] $monkeys */
        $monkeys = [];
        /** @var Monkey[] $state */
        $state = [];
        /** @var int[] $divisors */
        $divisors = [];
        // Parse creating monkeys and state.
        foreach (Parser::iterateByGroup($input) as $group) {
            [$monkeyId, $itemList, $operation, $test, ,] = explode("\n", $group);
            $monkey = new Monkey((int) $monkeyId, $operation, (int) $test);
            $monkeys[(int) $monkeyId] = $monkey;
            $state[$monkeyId] = array_map('intval', explode(',', $itemList));
            $divisors[] = (int) $test;
        }
        // Parse and set relationships.
        foreach (Parser::iterateByGroup($input) as $group) {
            [$monkeyId, , , , $trueId, $falseId] = explode("\n", $group);
            $monkeys[(int) $monkeyId]->setTrueMonkey($monkeys[(int) $trueId]);
            $monkeys[(int) $monkeyId]->setFalseMonkey($monkeys[(int) $falseId]);
        }
        $lcm = (int) array_reduce($divisors, fn($carry, $item) => gmp_lcm($carry, $item), 1);

        $inspections = $this->playMonkeyInTheMiddle(
            20,
            $monkeys,
            $state,
            fn(int $worryLevel): int => (int) floor($worryLevel / 3)
        );
        rsort($inspections);
        $answer1 = array_product(array_slice($inspections, 0, 2));

        $inspections = $this->playMonkeyInTheMiddle(
            10000,
            $monkeys,
            $state,
            fn(int $worryLevel): int => $worryLevel % $lcm
        );
        rsort($inspections);
        $answer2 = array_product(array_slice($inspections, 0, 2));

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    /**
     * @param int $rounds
     * @param Monkey[] $monkeys
     * @param int[][] $state
     * @return array
     */
    public function playMonkeyInTheMiddle(int $rounds, array $monkeys, array $state, callable $relief): array
    {
        /** @var int[] $inspections */
        $inspections = [];
        foreach ($monkeys as $monkey) {
            $inspections[$monkey->id] = 0;
        }

        for ($round = 1; $round <= $rounds; $round++) {
            foreach ($monkeys as $monkeyId => $monkey) {
                foreach ($state[$monkeyId] as $key => $worryLevel) {
                    unset($state[$monkeyId][$key]);
                    // Inspect.
                    // Operation shows how your worry level changes as that monkey inspects an item.
                    $worryLevel = $monkey->operate($worryLevel);
                    $inspections[$monkeyId]++;
                    // After each monkey inspects an item but before it tests your worry level, you are relieved.
                    $worryLevel = $relief($worryLevel);

                    // Test.
                    $targetMonkey = $monkey->test($worryLevel);

                    // Throw.
                    $state[$targetMonkey->id][] = $worryLevel;
                }
            }
        }

        return $inspections;
    }
}
