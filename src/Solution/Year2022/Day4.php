<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day4 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = 0;
        $answer2 = 0;
        foreach (Parser::iterateByLine($input) as $pairs) {
            $pairs = array_map(fn($pair) => explode('-', $pair), explode(',', $pairs));
            if (self::isEitherContained(...$pairs)) {
                $answer1++;
                $answer2++;
                continue;
            }
            if (self::isOverlap(...$pairs)) {
                $answer2++;
            }
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public static function isEitherContained(array $subject, array $comparison): bool
    {
        return self::isContained($subject, $comparison) || self::isContained($comparison, $subject);
    }

    public static function isContained(array $subject, array $comparison): bool
    {
        return $subject[0] >= $comparison[0] && $subject[1] <= $comparison[1];
    }

    public static function isOverlap(array $subject, array $comparison): bool
    {
        return $subject[1] >= $comparison[0] && $subject[0] <= $comparison[1];
    }
}
