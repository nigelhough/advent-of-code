<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day8 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['sample'])) {
            $input = '30373
25512
65332
33549
35390';
        }

        $rows = array_map('str_split', explode("\n", $input));
        $columns = [];
        for ($i = 0; $i < count($rows[0]); $i++) {
            $columns[] = array_column($rows, $i);
        }

        $visible = 0;
        $tallest = 0;
        foreach ($this->iterateGrid($rows, $columns) as [$x, $y, $myHeight]) {
            $directions = [
                array_reverse(array_slice($rows[$x], 0, $y)),
                array_slice($rows[$x], $y + 1),
                array_reverse(array_slice($columns[$y], 0, $x)),
                array_slice($columns[$y], $x + 1),
            ];

            $views = [];
            foreach ($directions as $direction) {
                $view = 0;
                foreach ($direction as $tree) {
                    $view++;
                    if ($tree >= $myHeight) {
                        break;
                    }
                }
                $views[] = $view;
            }
            $tallest = max($tallest, array_product($views));

            $isOnEdge = $x == 0 || $x == (count($rows) - 1) || $y == 0 || $y == (count($columns) - 1);
            if ($isOnEdge) {
                $visible++;
                continue;
            }

            foreach ($directions as $direction) {
                if (max($direction) < $myHeight) {
                    $visible++;
                    continue 2;
                }
            }
        }

        $answer1 = $visible;
        $answer2 = $tallest;

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    public function iterateGrid(array $rows, array $columns): \Generator
    {
        for ($x = 0; $x < count($rows[0]); $x++) {
            for ($y = 0; $y < count($columns[0]); $y++) {
                yield [$x, $y, $rows[$x][$y]];
            }
        }
    }
}
