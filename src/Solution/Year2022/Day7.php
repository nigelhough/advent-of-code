<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day7 extends AbstractSolution
{
    private const TOTAL_DISK_BYTES = 70000000;
    private const UPGRADE_SIZE_BYTES = 30000000;

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $workingDirectory = [];
        $exclusiveDirectorySize = [];
        $inclusiveDirectorySize = [];
        $lines = explode('$ ', substr($input, 2));
        foreach ($lines as $line) {
            [$statement, $data] = explode("\n", $line . "\n", 2);
            [$command, $option] = explode(' ', $statement . ' ', 2);
            $option = trim($option);
            $command = trim($command);
            if ($command === 'cd') {
                if ($option === '..') {
                    array_pop($workingDirectory);
                    continue;
                }
                if ($option === '/') {
                    $workingDirectory = ['/'];
                    continue;
                }
                $workingDirectory[] = $option;
                continue;
            }
            if ($command !== 'ls') {
                throw new \Exception('Bad Command');
            }
            foreach (array_filter(explode("\n", $data)) as $file) {
                [$size, $name] = explode(' ', $file, 2);
                if ($size === 'dir') {
                    continue;
                }
                $size = (int) trim($size);

                $path = '';
                foreach ($workingDirectory as $key => $dir) {
                    $path .= $dir . '/';
                    if (!isset($inclusiveDirectorySize[$path])) {
                        $inclusiveDirectorySize[$path] = 0;
                    }
                    $inclusiveDirectorySize[$path] += $size;
                    if ($key === (count($workingDirectory) - 1)) {
                        if (!isset($exclusiveDirectorySize[$path])) {
                            $exclusiveDirectorySize[$path] = 0;
                        }
                        $exclusiveDirectorySize[$path] += $size;
                    }
                }
            }
        }
//        var_dump($inclusiveDirectorySize);
//        var_dump($exclusiveDirectorySize);
        $answer1 = array_sum(array_filter($inclusiveDirectorySize, fn($size) => $size <= 100000));

        $used = $inclusiveDirectorySize['//'];
        $gap = self::UPGRADE_SIZE_BYTES - (self::TOTAL_DISK_BYTES - $used);
        $answer2 = min(array_filter($inclusiveDirectorySize, fn($size) => $size >= $gap));

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
