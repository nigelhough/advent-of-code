<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Alphabet\Alphabet;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day3 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $rucksacks = Parser::iterateByLine($input);
        $groups = array_chunk($rucksacks, 3);
        $answer1 = 0;
        $answer2 = 0;
        foreach ($groups as $groupRucksacks) {
            // Part 1.
            $rucksacks = array_map(function ($contents) {
                return str_split($contents, strlen($contents) / 2);
            }, $groupRucksacks);
            foreach ($rucksacks as [$compartment1, $compartment2]) {
                foreach (array_unique(array_intersect(str_split($compartment1), str_split($compartment2))) as $item) {
                    $answer1 += Alphabet::chrToNum($item);
                }
            }
            // Part 2.
            $rucksackItems = array_map('str_split', $groupRucksacks);
            foreach (array_unique(array_intersect(...$rucksackItems)) as $item) {
                $answer2 += Alphabet::chrToNum($item);
            }
        }

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
