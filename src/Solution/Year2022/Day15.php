<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2022;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Grid\Shape\Diamond;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day15 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        $yTarget = 2000000;
        $rangeTarget = 4000000;
        if (isset($_GET['sample'])) {
            $yTarget = 10;
            $rangeTarget = 20;
            $input = 'Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3';
        }
        $input = str_replace(['Sensor at x=', ' y=', ' closest beacon is at x='], '', $input);

        /** @var Coord[] $beacons */
        $beacons = [];
        $beaconsOnYInRange = 0;
        /** @var Diamond[] $areas */
        $areas = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$sensor, $beacon] = explode(':', $line);
            $beacon = Coord::fromString($beacon);
            $coord = Coord::fromString($sensor);
            $areas[] = Diamond::createFromCenter(
                $coord,
                (int) $coord->manhattanDistance($beacon)
            );

            $existing = false;
            foreach ($beacons as $existingBeacon) {
                if ($existingBeacon->isSame($beacon)) {
                    // Seen it.
                    $beacon = $existingBeacon;
                    $existing = true;
                    break;
                }
            }
            if (!$existing) {
                $beacons[] = $beacon;
                if (
                    $beacon->getYPosition() === $yTarget &&
                    $beacon->getXPosition() >= 0 &&
                    $beacon->getXPosition() <= $rangeTarget
                ) {
                    $beaconsOnYInRange++;
                }
            }
        }
//        $this->plot($areas, $beacons);

        // Sum the ranges on targetY, minus the becaons on that Y.
        $ranges = $this->getRanges($areas, $yTarget);
        $ranges = $this->mergeRanges($ranges);
        $answer1 = array_reduce($ranges, fn($carry, $range) => $carry + ($range[1] - $range[0]) + 1, 0);
        $answer1 -= $beaconsOnYInRange;

        // Check all rows until a coordinate is not in the range.
        $x = $y = 0;
        for ($row = 0; $row <= $rangeTarget; $row++) {
            $ranges = $this->getRanges($areas, $row);
            $ranges = $this->mergeRanges($ranges);
            foreach ($ranges as $range) {
                [$start, $end] = $range;
                if ($start > 0) {
                    $y = $row;
                    $x = $start - 1;
                    break 2;
                } elseif ($end < $rangeTarget) {
                    $y = $row;
                    $x = $end - 1;
                    break 2;
                }
            }
        }
        $frequency = ($x * 4000000) + $y;
        $answer2 = $frequency;

        echo "\nPart1: {$answer1}\nPart2: {$answer2}\n";
        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    /**
     * @param Diamond[] $areas
     * @param int $y
     * @return array
     */
    private function getRanges(array $areas, int $y): array
    {
        $ranges = [];
        foreach ($areas as $area) {
            $range = $area->getXRangeOnY($y);
            if ($range === null) {
                continue;
            }
            $ranges[] = $range;
        }
        return $ranges;
    }

    /**
     * @todo This slows the whole thing down. Make it quicker or find another way to do it.
     * Can you sort the areas or ranges?
     */
    private function mergeRanges(array $ranges): array
    {
        $i = 0;
        while (isset($ranges[$i])) {
            $range = $ranges[$i];
            foreach ($ranges as $key => $existingRange) {
                if ($i === $key) {
                    continue;
                }
                if (Day4::isEitherContained($range, $existingRange) || Day4::isOverlap($range, $existingRange)) {
                    $ranges[$i] = [min($range[0], $existingRange[0]), max($range[1], $existingRange[1])];
                    unset($ranges[$key]);
                    $ranges = array_values($ranges);
                    continue 2;
                }
            }
            $i++;
        }

        return $ranges;
    }

    /**
     * @param Diamond[] $areas
     * @param array $beacons
     */
    private function plot(array $areas, array $beacons): void
    {
        // Plot.
        $map = new Map();
        foreach ($beacons as $beacon) {
            $map->mark($beacon, 'B');
        }
        foreach ($areas as $area) {
            foreach ($area->getPoints() as $point) {
                $map->mark($point);
            }
//            foreach ($area->getPointsOnY($yTarget) as $point) {
//                $map->mark($point);
//            }
        }
        echo $map->plot(true);
    }
}
