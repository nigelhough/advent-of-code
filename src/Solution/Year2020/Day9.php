<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day9 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $numbers = Parser::iterateByLine($input);
        $numbers = array_map('intval', $numbers);
        $preambleLength = 25;
        $pointer = $preambleLength;
        $preamble = array_slice($numbers, $pointer - $preambleLength, $pointer);

        $target = null;
        while ($subject = $numbers[$pointer]) {
            $valid = false;
            foreach ($preamble as $key => $amble) {
                $myPreamble = array_values($preamble);
                array_splice($myPreamble, $key, 1);
                if ($this->getTarget($amble, $myPreamble, $subject)) {
                    $valid = true;
                    break;
                }
            }

            if (!$valid) {
                $target = $subject;
                break;
            }

            $pointer++;
            $preamble = array_slice($numbers, $pointer - $preambleLength, $pointer);
        }
        var_dump($target);

        foreach ($numbers as $pointer => $number) {
            $length = 1;
            while (array_sum(array_slice($numbers, $pointer, $length)) < $target) {
                //var_dump(array_slice($numbers, $pointer, $length));
                $length++;
            }
            if ($target === array_sum(array_slice($numbers, $pointer, $length))) {
                $range = array_slice($numbers, $pointer, $length);
                $min = min($range);
                $max = max($range);
                var_dump($min + $max);
                break;
            }
        }
        return null;
    }

    private function getTarget($value, $list, $target = 2020)
    {
        if (in_array($target - $value, $list)) {
            return $target - $value;
        }

        return null;
    }
}
