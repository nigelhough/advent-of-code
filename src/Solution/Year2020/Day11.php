<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Map;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day11 implements SolutionInterface
{
    public const SEAT_OCCUPIED = '#';

    public const SEAT_EMPTY = 'L';

    public function execute(string $input): ?AnswerInterface
    {
        ini_set('max_execution_time', '60');

//        $input = 'L.LL.LL.LL
        //LLLLLLL.LL
        //L.L.L..L..
        //LLLL.LL.LL
        //L.LL.LL.LL
        //L.LLLLL.LL
        //..L.L.....
        //LLLLLLLLLL
        //L.LLLLLL.L
        //L.LLLLL.LL';

//        $map = Map::createFromString('.##.##.
        //#.#.#.#
        //##...##
        //...L...
        //##...##
        //#.#.#.#
        //.##.##.');

        $map = Map::createFromString($input);
        $activeMap = clone $map;
        $maxVisibility = substr_count($input, "\n");

        $seatChanges = 1;
        $i = 1;
        // Continue until stable.
        while ($seatChanges > 0) {
            $seatChanges = 0;
            $seats = $map->locate(
                function ($location) {
                    return in_array($location, [self::SEAT_EMPTY, self::SEAT_OCCUPIED]);
                }
            );
            foreach ($seats as $coords => $seat) {
                $coord = Coord::fromString($coords);
                $neighbours = $coord->getFirstValidLineOfSight($seats, $maxVisibility);

                if ($seat === self::SEAT_EMPTY) {
                    $surrounded = true;
                    foreach ($neighbours as $neighbour) {
                        $neighbourValue = $map->get($neighbour);
                        if (is_null($neighbourValue) || $neighbourValue === '.') {
                            continue;
                        }
                        if ($neighbourValue === self::SEAT_OCCUPIED) {
                            $surrounded = false;
                            // Occupied Adjacent Seat
                            break;
                        }
                    }

                    if ($surrounded) {
                        $activeMap->mark($coord, self::SEAT_OCCUPIED);
                        $seatChanges++;
                    }
                }
                if ($seat === self::SEAT_OCCUPIED) {
                    $countOccupied = 0;
                    foreach ($neighbours as $neighbour) {
                        $neighbourValue = $map->get($neighbour);
                        $countOccupied += (int) ($neighbourValue === self::SEAT_OCCUPIED);
                    }
                    if ($countOccupied >= 5) {
                        $activeMap->mark($coord, self::SEAT_EMPTY);
                        $seatChanges++;
                    }
                }
            }
            $map = clone $activeMap;
            $i++;
        }

        $occupied = $map->locate(
            function ($location) {
                return in_array($location, [self::SEAT_OCCUPIED]);
            }
        );

        echo "Iteration " . ($i + 1) . "\n";
        echo "Occupied " . count($occupied) . "\n";
//        echo $map->plot(true);
//        echo "\n\n";
        return null;
    }
}
