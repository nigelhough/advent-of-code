<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day15 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            //$input = '0,3,6';
            //$input = '3,1,2';
            //$input = '3,2,1';
            //$input = '2,3,1';
            //$input = '1,2,3';
            $input = '2,1,3';
        }

        $numbers = explode(',', trim($input));
        $turnNumber = count($numbers) + 1;
        $i = 1;
        foreach ($numbers as $number) {
            echo "Turn {$i}: {$number}\n";
            $i++;
        }
        $lastNumber = array_pop($numbers);
        $sequence = array_flip($numbers);
        $number = 0;
        for ($i = $turnNumber; $i < 30000001; $i++) {
            if (!isset($sequence[$lastNumber])) {
                $number = 0;
            } else {
                $number = ($i - 1) - ($sequence[$lastNumber] + 1);
            }
            $sequence[$lastNumber] = $i - 2;

            $lastNumber = $number;
            //echo "Turn {$i}: {$number}\n";
        }
        echo "Turn " . ($i - 1) . ": {$number}\n";
        return null;
    }
}
