<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day19 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        ini_set('max_execution_time', '900');

        if (isset($_GET['debug'])) {
            $input = '1: 2 3 | 3 2
3: 4 5 | 5 4
4: "a"
0: 4 1 5
5: "b"
2: 4 4 | 5 5

ababbb
bababa
abbbab
aaabbb
aaaabbb';
        }
        if (isset($_GET['debug2'])) {
            $input = '42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba';
        }

//        // Part2.
        $input = str_replace(['8: 42', '11: 42 31'], ['8: 42 | 42 8', '11: 42 31 | 42 11 31'], $input);

        [$rules, $messages] = Parser::iterateByGroup($input);
//        echo $rules . "\n\n";
        $rules = Parser::iterateByLine($rules);
        $messages = Parser::iterateByLine($messages);
        $rulePermutations = [];

        // Known Rules.
        foreach ($rules as $key => $instruction) {
            [$ruleNo, $rule] = explode(': ', $instruction);
            $isString = (substr($rule, 0, 1) === '"' && substr($rule, -1) === '"');
            if ($isString) {
                $rulePermutations[$ruleNo][] = trim($rule, '"');
                unset($rules[$key]);
            }
        }

        $i = 0;
        while (count($rules)) {
//            echo count($rules) . "\n";
            $i++;
            if ($i > 10) {
                // Some Missing rules, but that is valid with infinite loops.
//                echo "ERROR\n";
//                var_dump($rules);
                break;
            }
            foreach ($rules as $key => $instruction) {
                [$ruleNo, $rule] = explode(': ', $instruction);
                if (!str_contains($rule, '|')) {
                    $ruleParts = explode(' ', $rule);
                    $thisRulePermutations = [];
                    foreach ($ruleParts as $part) {
                        if (!isset($rulePermutations[$part])) {
                            // Can't match the rule as not all parts known. Skip to next.
                            continue 2;
                        }

                        $thisRulePermutations = $this->addPermutations($thisRulePermutations, $rulePermutations[$part]);
//                        echo "p({$rule}) ({$part}) " . count($thisRulePermutations) . "\n";
                    }
                    $rulePermutations[$ruleNo] = $thisRulePermutations;
                    unset($rules[$key]);
                }

                if (str_contains($rule, '|')) {
                    $subRules = explode(' | ', $rule);
                    $permutations = [];
                    foreach ($subRules as $subRule) {
                        $ruleParts = explode(' ', $subRule);
                        $thisRulePermutations = [];
                        foreach ($ruleParts as $part) {
                            if ($part === $ruleNo) {
                                continue 3;
//                                $p = $permutations;
////                                echo "$rule\n";
////                                var_dump($ruleParts);
////                                var_dump($part);
////                                var_dump($permutations);
////                                echo "INFINITE\n";
////                                return;
                            } elseif (isset($rulePermutations[$part])) {
                                $p = $rulePermutations[$part];
                            } else {
                                continue 3;
                            }

//                            echo "{$part}\n";
//                            var_dump($thisRulePermutations);
                            $thisRulePermutations = $this->addPermutations($thisRulePermutations, $p);
//                            var_dump($thisRulePermutations);
//                            var_dump($rulePermutations[$part]);
//                            echo "\n\n";
//                            echo "p({$subRule}) ({$part}) " . count($thisRulePermutations) . "\n";
                        }
//                        var_dump($thisRulePermutations);
                        $permutations = array_merge($permutations, $thisRulePermutations);
                    }
                    $rulePermutations[$ruleNo] = $permutations;
                    unset($rules[$key]);
                }
            }
        }

//        echo "HERE";
//        return;
//
        if (count($rules) > 0) {
//            echo "\n\n";
//            var_dump($rulePermutations[42]);
//            var_dump($rulePermutations[31]);
//            //var_dump($rulePermutations);
//            return;
        }

        $lengths = [];
        foreach ($rulePermutations[42] ?? [] as $perm) {
            if (!in_array(strlen($perm), $lengths)) {
                $lengths[] = strlen($perm);
            }
        }
        $lengths2 = [];
        foreach ($rulePermutations[31] ?? [] as $perm) {
            if (!in_array(strlen($perm), $lengths2)) {
                $lengths2[] = strlen($perm);
            }
        }
        if (
            count($lengths) !== 1 || count(
                $lengths2
            ) !== 1 || (isset($lengths[0], $lengths2[0]) && $lengths[0] !== $lengths2[0])
        ) {
            echo "SEGMENT ERROR\n";
            return null;
        }
        $segmentSize = $lengths[0];

        $validStarts = $rulePermutations[42];
        $validEnds = $rulePermutations[31];

        $matches = [];
        foreach ($messages as $message) {
            // All of the segments are even size, that makes this easier.
            $chunks = str_split($message, $segmentSize);

            // Work through all of the valid starts.
            $starts = 0;
            foreach ($chunks as $key => $chunk) {
                if (in_array($chunk, $validStarts)) {
                    $starts++;
                    unset($chunks[$key]);
                    continue;
                }
                break;
            }

            // The rest must be valid ends.
            $ends = 0;
            foreach ($chunks as $key => $chunk) {
                if (in_array($chunk, $validEnds)) {
                    $ends++;
                    unset($chunks[$key]);
                    continue;
                }
                break;
            }

            // I must have no more chunks. Some valid starts and ends.
            // I also need more starts than ends because starts can be infinite but ends must match a start.
            if (empty($chunks) && ($starts > 0 && $ends > 0) && ($starts > $ends)) {
                $matches[] = $message;
            }
        }

        echo count($matches);
        return null;
    }

    public function addPermutations($thisRulePermutations, $parts)
    {
        if (empty($thisRulePermutations)) {
            return $parts;
        }

//        echo "<pre>";
        $myPermutations = [];
        foreach ($parts as $additional) {
            if (
                is_string($additional) || (is_array($additional) && count($additional) === 1 && is_string(
                    $additional[0]
                ))
            ) {
//                var_dump($thisRulePermutations);
                $additional = is_array($additional) ? $additional[0] : $additional;
                // If the additional requirement is a string, just add it to the existing possibilities.
                $myPermutations = array_merge(
                    $myPermutations,
                    array_map(
                        function ($perm) use ($additional) {
                            return $perm . $additional;
                        },
                        $thisRulePermutations
                    )
                );
                continue;
            }

            echo 'SHOULD NOT GET HERE';
            var_dump($additional);
            exit;
        }
        return $myPermutations;
    }
}
