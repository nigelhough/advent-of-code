<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\HandHeldGameConsole\GameConsole;
use AdventOfCode\HandHeldGameConsole\InfiniteLoopException;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day8 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $commands = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$instruction, $argument] = explode(' ', $line);
            $argument = (int) $argument;
            $commands[] = [$instruction, $argument];
        }

        $gameConsole = new GameConsole();

        try {
            $accumulator = $gameConsole->boot($commands);
        } catch (InfiniteLoopException $exception) {
            $accumulator = $exception->getAccumulator();
            echo "Exited with Infinite Loop : {$accumulator}\n";
        }

        $changabale = [];
        foreach (Parser::iterateByLine($input) as $pointer => $line) {
            [$instruction, $argument] = explode(' ', $line);

            if ($instruction === 'acc') {
                continue;
            }
            $changabale[] = $pointer;
        }

        foreach ($changabale as $change) {
            $changedCommands = array_values($commands);
            // Flip a command.
            $changedCommands[$change][0] = $changedCommands[$change][0] === 'jmp' ? 'nop' : 'jmp';

            try {
                $accumulator = $gameConsole->boot($changedCommands);
                echo "Exited with Accoumulator : {$accumulator}\n";
                break;
            } catch (InfiniteLoopException $exception) {
                continue;
            }
        }
        return null;
    }
}
