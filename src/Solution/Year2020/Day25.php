<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;
use Exception;

class Day25 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '5764801
17807724';
        }
        [$publicKey1, $publicKey2] = explode("\n", $input);
        $publicKey1 = (int) $publicKey1;
        $publicKey2 = (int) $publicKey2;

        $loopSize1 = $this->calculateLoopSize($publicKey1);
        $loopSize2 = $this->calculateLoopSize($publicKey2);

        $privateKey1 = $this->transform($publicKey1, $loopSize2);
        $privateKey2 = $this->transform($publicKey2, $loopSize1);

        if ($privateKey1 !== $privateKey2) {
            throw new Exception('Invalid Private Keys');
        }

        echo "Private Key: {$privateKey1}. (" . number_format($privateKey1) . ") \n";
        return null;
    }

    public function calculateLoopSize(int $publicKey): int
    {
        for ($subject = 1; $subject < 2000000; $subject++) {
            $value = 1;
            for ($loopSize = 1; $loopSize < 2000000; $loopSize++) {
                // Set the value to itself multiplied by the subject number.
                $value = $value * $subject;
                //Set the value to the remainder after dividing the value by 20201227.
                $value = ($value % 20201227);
                if ($value == $publicKey) {
                    return $loopSize;
                }
            }
        }

        throw new Exception('Unable to calculate loop size');
    }

    public function transform(int $subject, int $loopSize): int
    {
        $value = 1;
        for ($l = 0; $l < $loopSize; $l++) {
            // Set the value to itself multiplied by the subject number.
            $value = $value * $subject;
            //Set the value to the remainder after dividing the value by 20201227.
            $value = ($value % 20201227);
        }
        return $value;
    }
}
