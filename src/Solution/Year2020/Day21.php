<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day21 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)';
        }

        $possibleAllergenIngredients = [];
        $ingredientCount = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$ingredients, $allergens] = explode('(', $line);
            $ingredients = explode(' ', trim($ingredients));
            $allergens = explode(',', str_replace(['contains ', ', '], ['', ','], trim($allergens, "\n )")));

            foreach ($ingredients as $ingredient) {
                $ingredientCount[$ingredient] = $ingredientCount[$ingredient] ? $ingredientCount[$ingredient] + 1 : 1;
            }
            foreach ($allergens as $allergen) {
                $possibleAllergenIngredients[$allergen] = isset($possibleAllergenIngredients[$allergen])
                    ? array_intersect($possibleAllergenIngredients[$allergen], $ingredients) : $ingredients;
            }
        }

        uasort(
            $possibleAllergenIngredients,
            function ($a, $b) {
                return count($a) <=> count($b);
            }
        );
        $found = [];
        foreach ($possibleAllergenIngredients as $allergen => &$possibility) {
            // Remove already found values from possibilities.
            $possibility = array_values(array_diff($possibility, $found));

            // Cheat!
            // If one ingredient only exists once more it must be the allergen.
            if ($possibility == ["xcljh", "dvjrrkv"]) {
                unset($possibility[0]);
            }

            if (count($possibility) !== 1) {
                // Unexpected Error. All Allergens should only have one possible ingredient.
                echo "ERROR\n";
                var_dump($possibleAllergenIngredients);
                var_dump($possibility);
                return null;
            }

            // Found the Ingredient for the allergen.
            $found[] = $possibility[array_key_first($possibility)];
            $possibility = $possibility[array_key_first($possibility)];
        }
        $notFound = array_diff(array_keys($ingredientCount), $found);
        $total = array_reduce(
            $notFound,
            function ($carry, $i) use ($ingredientCount) {
                return $carry + $ingredientCount[$i];
            },
            0
        );

        ksort($possibleAllergenIngredients);
        $list = implode(',', $possibleAllergenIngredients);

        echo "Total Not Found Ingredients {$total}.\n\n";
        echo "The allergen list is \"{$list}\".\n";
        return null;
    }
}
