<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Grid\Map;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day20 extends AbstractSolution
{
    private $orientations = [
        ['flipped' => 0, 'rotated' => 0],
        ['flipped' => 1, 'rotated' => 0],
        ['flipped' => 2, 'rotated' => 0],
        ['flipped' => 0, 'rotated' => 1],
        ['flipped' => 1, 'rotated' => 1],
        ['flipped' => 0, 'rotated' => 2],
        ['flipped' => 0, 'rotated' => 3],
        ['flipped' => 1, 'rotated' => 3],
    ];

    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = file_get_contents(BASE_DIR . '/cache/2020/20.sample1');
        }
        if (isset($_GET['debug2'])) {
            $input = file_get_contents(BASE_DIR . '/cache/2020/20.sample2');
        }

        $pieces = [];
        $tileData = [];
        foreach (Parser::iterateByGroup($input) as $pieceData) {
            [$metaData, $data] = explode(":", $pieceData);
            $data = trim($data);
            $id = (int) trim($metaData, "\n Tile");

            $pieces[$id] = Map::createFromString($data);
            $tileData[$id] = $data;
        }

        $map = file_get_contents(BASE_DIR . '/cache/2020/20-' . md5($input) . '.map');

        // Skip Map for Part2.
        if (empty($map) && !(($_GET['debug'] ?? '') === 'map')) {
            $solutions = [];
            $sqrt = sqrt(count($pieces));
            $iterator = $pieces;

            if (md5($input) === '70a6b492d517583b4ad742df1a6588dd') {
                // Part2 Shortcut, Known Answer
                $iterator = [3881 => $pieces[3881]];
            }

            foreach ($iterator as $pieceId => $piece) {
                foreach ($this->orientations as $orientations) {
                    $layout = [
                        [
                            'id'      => $pieceId,
                            'flipped' => $orientations['flipped'],
                            'rotated' => $orientations['rotated'],
                        ],
                    ];
                    $this->findLayout($layout, $pieces, $sqrt, $solutions);
                }
            }

            if (empty($solutions)) {
                echo "No Solutions!";
                return null;
            }

            $solution = $solutions[0];
            $solutionIds = array_column($solution, 'id');
            $corners = [
                $solutionIds[0],
                $solutionIds[$sqrt - 1],
                $solutionIds[count($solution) - 1],
                $solutionIds[count($solution) - $sqrt],
            ];

            echo "Solutions : " . number_format(count($solutions)) . "\n";
            echo "Number of Pieces " . count($pieces) . "\n";
            echo "Square Root " . sqrt(count($pieces)) . "\n";
            echo "Corners " . implode(',', $corners) . "\n";
            echo "Corner Sums " . array_product($corners) . " (" . number_format(array_product($corners)) . ")\n";

            $lines = [];
            foreach (array_chunk($solution, (int) $sqrt) as $rowNumber => $row) {
                foreach ($row as $colNumber => $data) {
                    $piece = $pieces[$data['id']];
                    if ($data['flipped'] === 1) {
                        $piece = $piece->flipY();
                    } elseif ($data['flipped'] === 2) {
                        $piece = $piece->flipX();
                    }
                    if ($data['rotated'] === 1) {
                        $piece = $piece->rotate90();
                    } elseif ($data['rotated'] === 2) {
                        $piece = $piece->rotate180();
                    } elseif ($data['rotated'] === 3) {
                        $piece = $piece->rotate90()->rotate90()->rotate90();
                    }
                    $tileImage = trim($piece->plot(true));

                    foreach (explode("\n", $tileImage) as $lineNumber => $line) {
                        if ($lineNumber === 0 || $lineNumber === (strlen($line) - 1)) {
                            continue;
                        }
                        $lineNo = $lineNumber + (strlen($line) * ($rowNumber + 1));
                        $lines[$lineNo] = ($lines[$lineNo] ?? '') . substr($line, 1, strlen($line) - 2);
                    }
                }
            }
            $map = implode("\n", $lines);
            file_put_contents(BASE_DIR . '/cache/2020/20-' . md5($input) . '.map', $map);
        }

        if ($_GET['debug'] === 'map') {
            $map = '.####...#####..#...###..
#####..#..#.#.####..#.#.
.#.#...#.###...#.##.##..
#.#.##.###.#.##.##.#####
..##.###.####..#.####.##
...#.#..##.##...#..#..##
#.##.#..#.#..#..##.#.#..
.###.##.....#...###.#...
#.####.#.#....##.#..#.#.
##...#..#....#..#...####
..#.##...###..#.#####..#
....#.##.#.#####....#...
..##.##.###.....#.##..#.
#...#...###..####....##.
.#.##...#.##.#.#.###...#
#.###.#..####...##..#...
#.###...#.##...#.######.
.###.###.#######..#####.
..##.#..#..#.#######.###
#.#..##.########..#..##.
#.#####..#.#...##..#....
#....##..#.#########..##
#...#.....#..##...###.##
#..###....##.#...##.##.#';
        }

        $seaMonster = Map::createFromString(
            '                  # 
#    ##    ##    ###
 #  #  #  #  #  #   ',
            ignoreCharacters: [' ']
        );

        $map = Map::createFromString($map);

        $found = [];
        foreach ($this->orientations as $orientation) {
            if ($orientation['flipped'] === 1) {
                $map = $map->flipY();
            } elseif ($orientation['flipped'] === 2) {
                $map = $map->flipX();
            }
            if ($orientation['rotated'] === 1) {
                $map = $map->rotate90();
            } elseif ($orientation['rotated'] === 2) {
                $map = $map->rotate180();
            } elseif ($orientation['rotated'] === 3) {
                $map = $map->rotate90()->rotate90()->rotate90();
            }

            $found = $map->locateMap($seaMonster);
            if (count($found) > 0) {
                break;
            }
        }

        $seaMonster = Map::createFromString(
            '                  0 
0    00    00    000
 0  0  0  0  0  0   ',
            ignoreCharacters: [' ']
        );
        foreach ($found as $monster) {
            $map->markMap($monster, $seaMonster);
        }

        $hashes = $map->locate(function ($location) {
            return $location === '#';
        });
//        $monsters = $map->locate(function ($location) {
//            return $location === '0';
//        });

        echo "Water Roughness " . count($hashes) . "\n\n";
        echo $map->plot(true) . "\n\n";
        return null;
    }

    public function findLayout($layout, $pieces, $sqrt, &$solutions)
    {
        $remainingTiles = array_diff(array_keys($pieces), array_column($layout, 'id'));

        if (count($remainingTiles) === 0) {
            $solutions[] = $layout;
            return;
        }

        $position = count($layout);
        $left = $this->getLeft($position, $sqrt);
        $above = $this->getAbove($position, $sqrt);

        foreach ($remainingTiles as $tile) {
            foreach ($this->orientations as $orientations) {
                $possible = [
                    'id'      => $tile,
                    'flipped' => $orientations['flipped'],
                    'rotated' => $orientations['rotated'],
                ];
                if (!is_null($left) && !$this->isValidLeft($layout[$left], $possible, $pieces)) {
                    continue;
                }
                if (!is_null($above) && !$this->isValidAbove($layout[$above], $possible, $pieces)) {
                    continue;
                }
                $possibleLayout = $layout;
                $possibleLayout[] = $possible;
                $this->findLayout($possibleLayout, $pieces, $sqrt, $solutions);
            }
        }
    }

    private function getLeft($position, $sqrt): ?int
    {
        return ($position % $sqrt) === 0 ? null : $position - 1;
    }

    private function getAbove($position, $sqrt): ?int
    {
        return (($position - $sqrt) >= 0) ? (int) ($position - $sqrt) : null;
    }

    private function isValidLeft(array $leftDetails, array $subjectDetail, array $pieces): bool
    {
        /** @var Map $subject */
        $subject = $pieces[$subjectDetail['id']];
        if ($subjectDetail['flipped'] === 1) {
            $subject = $subject->flipY();
        } elseif ($subjectDetail['flipped'] === 2) {
            $subject = $subject->flipX();
        }
        if ($subjectDetail['rotated'] === 1) {
            $subject = $subject->rotate90();
        } elseif ($subjectDetail['rotated'] === 2) {
            $subject = $subject->rotate180();
        } elseif ($subjectDetail['rotated'] === 3) {
            $subject = $subject->rotate90()->rotate90()->rotate90();
        }

        /** @var Map $left */
        $left = $pieces[$leftDetails['id']];
        if ($leftDetails['flipped'] === 1) {
            $left = $left->flipY();
        } elseif ($leftDetails['flipped'] === 1) {
            $left = $left->flipX();
        }
        if ($leftDetails['rotated'] === 1) {
            $left = $left->rotate90();
        } elseif ($leftDetails['rotated'] === 2) {
            $left = $left->rotate180();
        } elseif ($leftDetails['rotated'] === 3) {
            $left = $left->rotate90()->rotate90()->rotate90();
        }

        return $subject->leftEdgeMatches($left);
    }

    private function isValidAbove(array $aboveDetails, array $subjectDetail, array $pieces): bool
    {
        /** @var Map $subject */
        $subject = $pieces[$subjectDetail['id']];
        if ($subjectDetail['flipped'] === 1) {
            $subject = $subject->flipY();
        } elseif ($subjectDetail['flipped'] === 2) {
            $subject = $subject->flipX();
        }
        if ($subjectDetail['rotated'] === 1) {
            $subject = $subject->rotate90();
        } elseif ($subjectDetail['rotated'] === 2) {
            $subject = $subject->rotate180();
        } elseif ($subjectDetail['rotated'] === 3) {
            $subject = $subject->rotate90()->rotate90()->rotate90();
        }

        /** @var Map $above */
        $above = $pieces[$aboveDetails['id']];
        if ($aboveDetails['flipped'] === 1) {
            $above = $above->flipY();
        } elseif ($aboveDetails['flipped'] === 1) {
            $above = $above->flipX();
        }
        if ($aboveDetails['rotated'] === 1) {
            $above = $above->rotate90();
        } elseif ($aboveDetails['rotated'] === 2) {
            $above = $above->rotate180();
        } elseif ($aboveDetails['rotated'] === 3) {
            $above = $above->rotate90()->rotate90()->rotate90();
        }

        return $subject->topEdgeMatches($above);
    }
}
