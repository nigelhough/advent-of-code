<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Cube\Coord;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day17 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        ini_set('max_execution_time', '60');
        if (isset($_GET['debug'])) {
            $input = '.#.
..#
###';
        }
        $dimensions = 4;
        $iterations = 6;
        $x = 0;
        $y = 0;
        $z = 0;
        $w = 0;
        $coordinates = [];
        foreach (explode("\n", $input) as $line) {
            foreach (str_split($line) as $value) {
                $coordinates["{$x},{$y},{$z},{$w}"] = $value;
                $x++;
            }
            $x = 0;
            $y--;
        }

        $on = array_reduce(
            $coordinates,
            function ($carry, $state) {
                return $carry + (int) ($state === '#');
            },
            0
        );
        echo "Cycle #0. Active = {$on}\n";

        for ($i = 1; $i <= $iterations; $i++) {
            $newCoordinates = array_filter($coordinates, function ($state) {
                return $state === '#';
            });
            $processed = [];
            foreach ($coordinates as $coordinate => $state) {
                $coord = Coord::fromString($coordinate);
                $this->toggleState($coord, $coordinates, $newCoordinates, $dimensions);
                foreach ($coord->getSurrounding($dimensions) as $surroundingCoord) {
                    if (isset($processed[(string) $surroundingCoord])) {
                        continue;
                    }
                    $this->toggleState($surroundingCoord, $coordinates, $newCoordinates, $dimensions);
                    $processed[(string) $surroundingCoord] = true;
                }
                $processed[(string) $coord] = true;
            }
            $coordinates = $newCoordinates;
            $on = array_reduce(
                $coordinates,
                function ($carry, $state) {
                    return $carry + (int) ($state === '#');
                },
                0
            );

            echo "Cycle #{$i}. Active = {$on}\n";
        }
//        var_dump($coordinates);

//        echo "\n\n";
//        echo "-2\n" . $this->plot($coordinates, new Coord(4, 1, 0), new Coord(-2,-4, 0), -2) . "\n";
//        echo "-1\n" . $this->plot($coordinates, new Coord(4, 1, 0), new Coord(-2,-4, 0), -1) . "\n";
//        echo "0\n" . $this->plot($coordinates, new Coord(4, 1, 0), new Coord(-2,-4, 0), 0) . "\n";
//        echo "1\n" . $this->plot($coordinates, new Coord(4, 1, 0), new Coord(-2,-4, 0), 1) . "\n";
//        echo "2\n" . $this->plot($coordinates, new Coord(4, 1, 0), new Coord(-2,-4, 0), 2) . "\n";
        return null;
    }

    private function toggleState(Coord $coord, array $coordinates, array &$newCoordinates, int $dimensions = 3): void
    {
        $state = $coordinates[(string) $coord] ?? '.';
        $neighboursOn = array_reduce(
            $coord->getSurrounding($dimensions),
            function ($carry, $coord) use ($coordinates) {
                return $carry + ((isset($coordinates[(string) $coord]) && $coordinates[(string) $coord] === '#') ? 1
                        : 0);
            },
            0
        );
//        echo (string) $coord . "({$state}) > " . (string) $neighboursOn . "\n";
//        $newCoordinates[(string) $coord] = $newCoordinates[(string) $coord] ?? '.';
        if ($state === '#') {
            if (!($neighboursOn === 2 || $neighboursOn === 3)) {
                $newCoordinates[(string) $coord] = '.';
            }
        }

        if ($state === '.') {
            if ($neighboursOn === 3) {
                $newCoordinates[(string) $coord] = '#';
            }
        }
    }

    public function plot($coordinates, $topRight, $bottomLeft, $z = 0, $invert = false): string
    {
        $plot = "";
        for ($y = $topRight->getYPosition(); $y >= $bottomLeft->getYPosition(); $y--) {
            $line = "";
            for ($x = $bottomLeft->getXPosition(); $x <= $topRight->getXPosition(); $x++) {
                $line .= $coordinates["{$x},{$y},{$z}"] ?? '.';
            }
            $plot .= $invert ? strrev("{$line}\n") : "{$line}\n";
        }
        return $invert ? strrev($plot) : $plot;
    }
}
