<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day4 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $inputString): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        $input = [];
        foreach (explode("\n\n", $inputString) as $block) {
            $input[] = explode("\n", $block);
        }

        $passports = 0;
        $validPassports = 0;
        foreach ($input as $passport) {
            $passportString = '';
            foreach ($passport as $line) {
                $passportString .= ' ' . trim($line);
            }
            $passportString = trim($passportString);
            $values = explode(' ', $passportString);
            $pairs = [];
            foreach ($values as $value) {
                [$key, $value] = explode(':', $value);
                $pairs[$key] = $value;
            }
            $byrExists = array_key_exists('byr', $pairs);
            $iyrExists = array_key_exists('iyr', $pairs);
            $eyrExists = array_key_exists('eyr', $pairs);
            $hgtExists = array_key_exists('hgt', $pairs);
            $hclExists = array_key_exists('hcl', $pairs);
            $eclExists = array_key_exists('ecl', $pairs);
            $pidExists = array_key_exists('pid', $pairs);

            $exists = $byrExists
                && $iyrExists
                && $eyrExists
                && $hgtExists
                && $hclExists
                && $eclExists
                && $pidExists;

            if ($exists) {
                $passports++;
            }

            // byr (Birth Year) - four digits; at least 1920 and at most 2002.
            $byrValid = $byrExists && (int) $pairs['byr'] >= 1920 && (int) $pairs['byr'] <= 2002;
            if ($byrValid) {
                //echo $pairs['byr'] . "\n";
            }

            //iyr (Issue Year) - four digits; at least 2010 and at most 2020.
            $iyrValid = $iyrExists && (int) $pairs['iyr'] >= 2010 && (int) $pairs['iyr'] <= 2020;
            if ($iyrValid) {
                //echo $pairs['iyr'] . "\n";
            }

            //eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
            $eyrValid = $eyrExists && (int) $pairs['eyr'] >= 2020 && (int) $pairs['eyr'] <= 2030;
            if ($eyrValid) {
                //echo $pairs['eyr'] . "\n";
            }

            //hgt (Height) - a number followed by either cm or in:
            //If cm, the number must be at least 150 and at most 193.
            //If in, the number must be at least 59 and at most 76.
            $hgtValid = $hgtExists
                && (
                    (substr($pairs['hgt'], -2) == 'cm' && ((int) $pairs['hgt'] >= 150 && (int) $pairs['hgt'] <= 193))
                    || (substr($pairs['hgt'], -2) == 'in' && ((int) $pairs['hgt'] >= 59 && (int) $pairs['hgt'] <= 76))
                );
            if ($hgtValid) {
                //echo $pairs['hgt'] . "\n";
            }

            //hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            $hclValid = $hclExists && strlen($pairs['hcl']) === 7 && substr($pairs['hcl'], 0, 1) === '#'
                && preg_match('/^#[a-z0-9]+$/', $pairs['hcl']);


            if (!$hclValid) {
                //echo $pairs['hcl'] . "\n";
            }

            //ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            $eclValid = $eclExists && in_array($pairs['ecl'], ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']);
            if ($eclValid) {
                //echo $pairs['ecl'] . "\n";
            }

            //pid (Passport ID) - a nine-digit number, including leading zeroes.
            $pidValid = $pidExists && strlen($pairs['pid']) === 9;
            if ($pidValid) {
                //echo $pairs['pid'] . "\n";
            }

            //cid (Country ID) - ignored, missing or not.
            $cidValid = true;

            $isValid = $byrValid
                && $iyrValid
                && $eyrValid
                && $hgtValid
                && $hclValid
                && $eclValid
                && $pidValid
                && $cidValid;

            if ($isValid) {
                $validPassports++;
                //var_dump($pairs);
            }
        }
        echo "\nPassports = {$passports}";
        $answer1 = $passports;
        echo "\nValid Passports = {$validPassports}";
        $answer2 = $validPassports;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
