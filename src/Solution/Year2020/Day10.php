<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day10 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        //$input = file_get_contents(BASE_DIR . '/cache/2020/10.sample');
        //$input = file_get_contents(BASE_DIR . '/cache/2020/10.sample2');
        $joltageAdapters = Parser::iterateByLine($input);
        $joltageAdapters = array_map('intval', $joltageAdapters);
        sort($joltageAdapters);

        $deviceJoltage = max($joltageAdapters) + 3;

        $current = 0;
        $adaptersByJump = [];
        foreach ($joltageAdapters as $adapter) {
            if (!isset($adaptersByJump[$adapter - $current])) {
                $adaptersByJump[$adapter - $current] = 0;
            }
            $adaptersByJump[$adapter - $current]++;
            $current = $adapter;
        }
        if (!isset($adaptersByJump[$deviceJoltage - $current])) {
            $adaptersByJump[$deviceJoltage - $current] = 0;
        }
        $adaptersByJump[$deviceJoltage - $current]++;
        $solution = $adaptersByJump[1] * $adaptersByJump[3];
        echo "Part 1 Solution : {$solution}\n\n";

        array_unshift($joltageAdapters, 0);
        array_push($joltageAdapters, $deviceJoltage);

        $adapterSequences = [];
        $adapterSequence = [];
        foreach ($joltageAdapters as $key => $adapter) {
            $adapterSequence[] = $adapter;
            if (isset($joltageAdapters[$key + 1]) && $joltageAdapters[$key + 1] !== $adapter + 1) {
                // Out of Sequence. Spit.
                $adapterSequences[] = $adapterSequence;
                $adapterSequence = [];
            }
        }
        $adapterSequences[] = $adapterSequence;

        $combinations = 1;
        foreach ($adapterSequences as $adapterSequence) {
            if (count($adapterSequence) === 3) {
                $combinations = $combinations * 2;
            }
            if (count($adapterSequence) === 4) {
                $combinations = $combinations * 4;
            }
            if (count($adapterSequence) === 5) {
                $combinations = $combinations * 7;
            }
        }
        echo "Distinct Adapter arrangements : {$combinations} (" . number_format($combinations) . ")\n\n";
        return null;
    }
}
