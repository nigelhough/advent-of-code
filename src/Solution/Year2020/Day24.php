<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Grid\Map;
use AdventOfCode\HexGrid\Bearing;
use AdventOfCode\HexGrid\Coord;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day24 extends AbstractSolution
{
    private const TILE_BLACK = '#';

    private const TILE_WHITE = '0';

    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew';
        }

        $input = str_replace(['ne', 'nw', 'sw', 'se', 'e', 'w'], ['NE,', 'NW,', 'SW,', 'SE,', 'E,', 'W,'], $input);

        $map = new Map();
        foreach (Parser::iterateByLine($input) as $instructions) {
            $coord = Coord::fromString('0, 0, 0');
            foreach (explode(",", trim($instructions, ',')) as $instruction) {
                $coord = $coord->moveAlongBearing(Bearing::get($instruction));
            }
            $map->toggle($coord, self::TILE_BLACK, self::TILE_WHITE);
        }
        $blackTiles = $map->locate([$this, 'isBlack']);
        echo "Day 0: " . count($blackTiles) . "\n";

        for ($day = 1; $day <= 100; $day++) {
            $map2 = clone $map;

            // Toggle Black Tiles.
            foreach ($map->locate([$this, 'isBlack']) as $coord => $tile) {
                $coord = Coord::fromString($coord);
                $blackCount = 0;
                foreach ($coord->getNeighbours() as $neighbour) {
                    if ($this->isBlack($map->get($neighbour))) {
                        $blackCount++;
                    }
                    // Include any White tiles neighbouring a black tile (on original map).
                    if (!$this->isBlack($map->get($neighbour))) {
                        $map->mark($neighbour, self::TILE_WHITE);
                    }
                }
                // Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
                if ($blackCount === 0 || $blackCount > 2) {
                    $map2->mark($coord, self::TILE_WHITE);
                }
            }
            // Toggle White tiles.
            foreach ($map->locate([$this, 'isWhite']) as $coord => $tile) {
                $coord = Coord::fromString($coord);
                $blackCount = 0;
                foreach ($coord->getNeighbours() as $neighbour) {
                    if ($this->isBlack($map->get($neighbour))) {
                        $blackCount++;
                    }
                }
                // Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
                if ($blackCount === 2) {
                    $map2->mark($coord, self::TILE_BLACK);
                }
            }
            $map = $map2;
        }
        $blackTiles = $map->locate([$this, 'isBlack']);
        echo "Day " . ($day - 1) . ": " . count($blackTiles) . "\n";
        return null;
    }

    public function isBlack(?string $tileValue)
    {
        return $tileValue === self::TILE_BLACK;
    }

    public function isWhite(?string $tileValue)
    {
        return $tileValue === self::TILE_WHITE;
    }
}
