<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;

class Day13 extends AbstractSolution
{
    public const ACTION_FORWARD = 'F';

    public const ROTATE_CLOCKWISE = 'R';

    public const ROTATE_ANTI_CLOCKWISE = 'L';

    public function execute(string $input): ?AnswerInterface
    {
        $offset = 817187320000000;
        if (isset($_GET['debug'])) {
            $offset = 1068773;
            $input = file_get_contents(BASE_DIR . '/cache/2020/13.sample1');
        }
        [$currentTime, $busList] = Parser::iterateByLine($input);
        $currentTime = (int)$currentTime;

        if (isset($_GET['debug2'])) {
            $offset = 100;
            $busList = '17,x,13,19';
        }
        if (isset($_GET['debug3'])) {
            $offset = 10000;
            $busList = '1789,37,47,1889';
        }

        $busTimes = explode(',', $busList);
        $busTimes = array_map('intval', $busTimes);
        $busTimes = array_filter($busTimes);

        $sortedTime = $busTimes;
        rsort($sortedTime);
        $longest = $sortedTime[0];
        $nextLongest = $sortedTime[1];

        $earliest = PHP_INT_MAX;
        $earliestDepart = null;
        $earliestId = null;

        $firstDepart = null;
        $firstId = null;

        for ($t = $currentTime; $t < $currentTime + $longest; $t++) {
            foreach ($busTimes as $busTime) {
                if (($t % $busTime) === 0) {
                    $arrivalTime = $t + $busTime;

                    if ($firstDepart === null) {
                        $firstDepart = $t;
                        $firstId = $busTime;
                    }
                    if ($arrivalTime < $earliest) {
                        $earliest = $arrivalTime;
                        $earliestId = $busTime;
                        $earliestDepart = $t;
                    }
                }
            }
        }
        $wait = $firstDepart - $currentTime;
        echo "Answer 1 : " . ($wait * $firstId) . "\n";
        echo "\n\n";

        echo "Part 2 Completed. Solution was Butchered in attempting to Optimise the Increment Number\n" .
            "I was using the largest value as the increment. " .
            "It can only be a valid solution when the largest is correct.\n" .
            "I am convinced that the increment can be increased if it matches the longest and 2nd Longest\n" .
            "I kept getting an increment not divisible by my solution\n";
        return null;
        ini_set('max_execution_time', '10');

        $first = $busTimes[0];
        $longestPosition = array_flip($busTimes)[$longest];
        $nextLongestPosition = array_flip($busTimes)[$nextLongest];
        var_dump($longestPosition);
        var_dump($nextLongestPosition);
        $relationship = ($longestPosition - $nextLongestPosition);

        $t = 1;
        $increment = $longest;
        $increments = [];
        while (count($increments) < 4) {
            if (
                (($t + $longestPosition) % $longest) === 0 &&
                ((($t + $longestPosition) + $relationship) % $nextLongest) === 0
            ) {
                $increments[] = $t;
            }
            $t++;
        }
        $earliest = $increments[0];
        $earliestStart = $earliest - $longestPosition;
        $increment = $increments[1] - $increments[0];
        echo "Earliest = {$earliest}\n";
        echo "Earliest Start = {$earliestStart}\n";
        echo "Increment = {$increment}\n";
        var_dump($increments);
        return null;
//
//
//        $earliestLongest = null;
//        for($t = $offset; $t < $offset+$increment; $t++) {
//            if(($t % $longest) === 0) {
//                $earliestLongest = $t;
//                break;
//            }
//        }
//        $earliestStart = $earliestLongest-$longestPosition; //($earliestLongest - $longestPosition) - $increment;
//        echo "Increment Longest = {$earliestLongest}\n";
//        echo "Increment Start = {$earliestStart}\n";
//
//        $first = $busTimes[0];
//        echo "First = {$first}\n";
//        var_dump($first % $earliestStart);
//        return;

        $t = $earliestStart;
//        var_dump($earliestStart);
//        var_dump($increment);
        while ($t <= (1202161486 + $increment)) {
            echo "{$t}\n";
            $t += $increment;
            if (($t % 10000000) === 0) {
                echo $t . "\n";
            }

            if (($t % $first) !== 0) {
                continue;
            }
            foreach ($busTimes as $offset => $busTime) {
//                echo ($t+$offset) . " " . $busTime . " (" . (($t+$offset) % $busTime) . ")\n";
                if ((($t + $offset) % $busTime) !== 0) {
                    continue 2;
                }
            }
            echo "$t\n";

            break;
        }

        echo "\n\n";
        echo "Answer 2 : {$t} (" . number_format($t) . ")\n";
        return null;
    }
}
