<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;
use AdventOfCode\Grid\Offset;
use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day12 extends AbstractSolution
{
    public const ACTION_FORWARD = 'F';

    public const ROTATE_CLOCKWISE = 'R';

    public const ROTATE_ANTI_CLOCKWISE = 'L';

    public function execute(string $input): ?AnswerInterface
    {
        $start = new Coord(0, 0);
        // THIS SHOULD NOT BE AN OFFSET BUT A HEADING.
        $waypointOffset = new Offset(10, -1);

        if (isset($_GET['debug'])) {
            $input = file_get_contents(BASE_DIR . '/cache/2020/12.sample1');
        }
        if (isset($_GET['debug2'])) {
            $input = file_get_contents(BASE_DIR . '/cache/2020/12.sample2');
            $start = new Coord(10, 10);
            $waypointOffset = new Offset(0, -3);
        }

        $shipLocation = $start;
        $waypointLocation = $shipLocation->moveOffset($waypointOffset);
        foreach (Parser::iterateByLine($input) as $command) {
            $action = substr($command, 0, 1);
            $value = (int) substr($command, 1);

            if ($action === self::ACTION_FORWARD) {
                // Move ship in waypoint direction, x times.
//                $shipLocation->moveAlongBearing($waypointOffset->getBearing(), $value);
                $shipLocation = $shipLocation->moveOffset($waypointOffset, $value);
                // Move Waypoint inline with the ship.
                $waypointLocation = $shipLocation->moveOffset($waypointOffset);
            } elseif (Bearing::isValidBearingIdentifier($action)) {
                // Move waypoint along supplied bearing.
                $waypointLocation = $waypointLocation->moveAlongBearing(Bearing::get($action), $value);
                // Recalculate waypoint offset from new location.
                $waypointOffset = Offset::createFromCoordinates($waypointLocation, $shipLocation);
            } elseif (in_array($action, [self::ROTATE_ANTI_CLOCKWISE, self::ROTATE_CLOCKWISE])) {
                // Rotate waypoint around ship.
                $waypointLocation = $waypointLocation->rotateAroundCentralPoint(
                    $shipLocation,
                    $value,
                    $action === self::ROTATE_CLOCKWISE
                );
                // Recalculate waypoint offset from new location.
                $waypointOffset = Offset::createFromCoordinates($waypointLocation, $shipLocation);
            }
            //echo "s{$shipLocation}, w{$waypointLocation}, o$waypointOffset ({$command})\n\n";
        }

        echo "Final Location: {$shipLocation}\n";
        $distance = $start->manhattanDistance($shipLocation);
        echo "Distance from Start to Destination: {$distance} (" . number_format($distance) . ")";
        return null;
    }
}
