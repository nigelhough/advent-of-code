<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day23 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = '389125467';
        }

        $cupLabels = str_split($input);
        $cupLabels = array_map('intval', $cupLabels);

        $lowest = min($cupLabels);
        $highest = max($cupLabels);

        echo "{$input}\n";
        $cups = [];
        foreach ($cupLabels as $index => $cupLabel) {
            $cups[$cupLabel] = $cupLabels[$index + 1] ?? array_key_first($cups);
        }

        // Add Cups from Highest to 1 Million.
        $next = $highest + 1;
        $cups[array_key_last($cups)] = $next;
        while ($next < 1000000) {
            $cups[$next] = $next + 1;
            $next++;
        }
        $cups[$next] = array_key_first($cups);

        $highest = max($cups);
        $current = array_key_first($cups);

        $move = 1;
        $target = 10000000;
        while ($move <= $target) {
            // Get the next 3 cups.
            $next1 = $cups[$current];
            $next2 = $cups[$next1];
            $next3 = $cups[$next2];
            $after = $cups[$next3];

            //echo "\nMove {$move} :: ({$current}) [{$next1} {$next2} {$next3}] {$after}\n";
            // Remove 3 cups by pointing current at next.
            $cups[$current] = $after;

            // Find Destination.
            $destination = $current - 1;
            while (!isset($cups[$destination]) || in_array($destination, [$next1, $next2, $next3])) {
                if ($destination < $lowest) {
                    $destination = $highest;
                    continue;
                }
                $destination--;
            }
            //echo "Dest: {$destination}\n";
            // Move 3 cups after Destination.
            $cups[$next3] = $cups[$destination];
            $cups[$destination] = $next1;

            $current = $cups[$current];
            $move++;
            if (php_sapi_name() == "cli" && ($move % intval($target * 0.05)) === 0) {
                // Show increment every 5 %
                echo ".";
            }
        }

        // Part 1 Answer.
//        $index = 1;
//        while ($cups[$index] !== 1) {
//            echo $cups[$index];
//            $index = $cups[$index];
//        }

        // Part 2 Answer.
        $after1 = $cups[1];
        $after2 = $cups[$after1];
        echo "1 > {$after1} > {$after2} = " . ($after1 * $after2) . "\n";
        return null;
    }
}
