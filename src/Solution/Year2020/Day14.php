<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day14 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0';
        }
        if (isset($_GET['debug2'])) {
            $input = 'mask = 0X11XX1X010X01101000X01X011101100000
mem[5445] = 1446
mask = 10101111X101011X1XX10111X00X001X0X0X
mem[5445] = 12173642';
        }
        if (isset($_GET['debug3'])) {
            $input = 'mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1';
        }

        $positiveMask = 0;
        $negativeMask = 0;
        $memory = [];
        foreach (Parser::iterateByLine($input) as $line) {
            if (substr($line, 0, 4) === 'mask') {
                $mask = substr($line, 7);
                $positiveMask = bindec(str_replace('X', '0', $mask));
                $negativeMask = bindec(str_replace(['1', '0', 'X'], ['X', '1', '0'], $mask));
                continue;
            }
            [$memPosition, $value] = explode('=', str_replace(['mem[', ']', ' '], '', $line));
            $memory[$memPosition] = (((int) $value | $positiveMask) & ~$negativeMask);
        }
        $total = array_sum($memory);
        echo "Sum of Memory 1: {$total}\n";

        $memory = [];
        $mask = '';
        foreach (Parser::iterateByLine($input) as $line) {
            if (substr($line, 0, 4) === 'mask') {
                $mask = substr($line, 7);
                continue;
            }
            $line = str_replace(['mem[', ']', ' '], '', $line);
            [$memPosition, $value] = explode('=', $line);
            $value = (int) $value;
            $memPosition = (int) $memPosition;

            $memoryString = str_pad(decbin($memPosition), 36, '0', STR_PAD_LEFT);
            $memoryArray = str_split($memoryString);
            foreach (str_split(str_pad($mask, 36, '0', STR_PAD_LEFT)) as $index => $chr) {
                if ($chr === '0') {
                    continue;
                }
                $memoryArray[$index] = $chr;
            }
            $memoryKeys = [];
            $this->permutations($memoryArray, $memoryKeys);
            foreach ($memoryKeys as $memoryIndex) {
                $memory[bindec($memoryIndex)] = $value;
            }
        }
        $total = array_sum($memory);
        echo "Sum of Memory 2: {$total}\n";
        return null;
    }

    public function permutations(array $memoryArray, array &$memoryKeys)
    {
        $indexes = array_keys(
            array_filter($memoryArray, function ($chr) {
                return $chr === 'X';
            })
        );
        if (count($indexes) < 1) {
            $memoryKeys[] = implode('', $memoryArray);
        } else {
            $index = $indexes[0];
            $memoryArray[$index] = 1;
            $this->permutations($memoryArray, $memoryKeys);
            $memoryArray[$index] = 0;
            $this->permutations($memoryArray, $memoryKeys);
        }
    }
}
