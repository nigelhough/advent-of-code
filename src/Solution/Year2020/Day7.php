<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day7 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        $input = str_replace([' bags', ' bag'], ['', ''], $input);
        $children = [];
        $parents = [];
        foreach (Parser::iterateByLine($input) as $line) {
            [$parentBag, $contents] = explode('contain', $line);
            foreach (explode(",", trim($contents)) as $relationship) {
                [$quantity, $childBag] = explode(' ', trim($relationship, ' .'), 2);

                $children[trim($parentBag)][] = [trim($childBag), (int) $quantity];
                $parents[trim($childBag)][] = trim($parentBag); //[trim($parentBag), (int) $quantity];
            }
        }

        $foundParents = [];
        $this->findParents('shiny gold', $parents, $foundParents);
        echo count(array_unique($foundParents)) . " parent bags can contain shiny gold.\n\n";

        $foundChildren = [];
        $count = $this->findChildren('shiny gold', 1, $children, $foundChildren);
        echo "shiny gold bag contains {$count} bags.";
        return null;
    }

    public function findParents($child, $parents, &$found)
    {
        if (!array_key_exists($child, $parents)) {
            return 0;
        }
        $count = 0;
        foreach ($parents[$child] as $parent) {
            $found = array_merge($found, $parents[$child]);
            $count += count($parents[$child]);
            $count += $this->findParents($parent, $parents, $found);
        }

        return $count;
    }

    public function findChildren($parent, $multiplier, $children, &$found)
    {
        if (!array_key_exists($parent, $children)) {
            return 0;
        }
        $count = 0;
        foreach ($children[$parent] as $relationship) {
            $found = array_merge($found, $children[$parent]);
            $count += $relationship[1] * $multiplier;
            $count += $this->findChildren($relationship[0], $relationship[1] * $multiplier, $children, $found);
        }

        return $count;
    }
}
