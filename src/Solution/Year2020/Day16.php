<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day16 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12';
        }
        if (isset($_GET['debug2'])) {
            $input = 'class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9
99,6,1000';
        }

        [$ruleString, $myTicket, $otherTicketsString] = Parser::iterateByGroup($input);
        $rules = [];

        // Extract Ticket Rules.
        $ruleString = str_replace(' ', '', $ruleString);
        foreach (Parser::iterateByLine($ruleString) as $rule) {
            [$ruleKey, $constraints] = explode(":", $rule);
            $rules[$ruleKey] = explode("or", $constraints);
        }

        // Extract my ticket.
        $myTicket = substr($myTicket, strpos($myTicket, "\n") + 1);

        // Extract other tickets.
        $tickets = [];
        $otherTicketsString = substr($otherTicketsString, strpos($otherTicketsString, "\n") + 1);
        foreach (Parser::iterateByLine($otherTicketsString) as $otherTicket) {
            $tickets[] = explode(",", $otherTicket);
        }

        // Fill an array of ticket positions with all possible ticket fields.
        $possiblePositions = array_fill(0, substr_count($myTicket, ",") + 1, array_keys($rules));

        $invalidValues = [];
        foreach ($tickets as $ticket) {
            $isValidTicket = true;
            $possibleValues = [];
            foreach ($ticket as $valuePosition => $ticketValue) {
                $isValidTicketValue = false;
                foreach ($rules as $ruleKey => $rule) {
                    foreach ($rule as $constraint) {
                        [$min, $max] = explode("-", $constraint);

                        if ($ticketValue >= $min && $ticketValue <= $max) {
                            // Valid Value if one of the constraints passes.
                            $isValidTicketValue = true;
                            $possibleValues[$valuePosition][] = $ruleKey;
                            // No need to pass further constraints.
                            break 1;
                        }
                    }
                }
                if (!$isValidTicketValue) {
                    $isValidTicket = false;
                    $invalidValues[] = (int) $ticketValue;
                    // We don't care about the other ticket values as we ignore them for invalid tickets.
                    break;
                }
            }
            if ($isValidTicket) {
                foreach ($possibleValues as $index => $values) {
                    $possiblePositions[$index] = array_intersect($possiblePositions[$index], $values);
                    continue;
                }
            }
        }

        // Sort possible positions to have the least possibilities first.
        // PRESERVE THE INDEXS which are the ticket fields.
        uasort(
            $possiblePositions,
            function ($a, $b) {
                return count($a) <=> count($b);
            }
        );

        $positions = [];
        $found = [];
        foreach ($possiblePositions as $index => $field) {
            // Filter out found fields, should reduce as we go.
            $field = array_filter(
                $field,
                function ($key) use ($found) {
                    return !in_array($key, $found);
                }
            );
            if (count($field) === 1) {
                $correct = $field[array_key_first($field)];
                $positions[$index] = $correct;
                $found[] = $correct;
            } else {
                echo "\nOH NO!!!\nWe had too many options and couldn't identify a field's value.\n";
                break;
            }
        }

        $parts = [];
        // Filter to departure fields.
        $positions = array_filter(
            $positions,
            function ($key) {
                return substr($key, 0, 9) === 'departure';
            }
        );

        // Collect my ticket values for desired positions.
        $myTicketParts = explode(",", $myTicket);
        foreach ($positions as $index => $part) {
            $parts[] = (int) $myTicketParts[$index];
        }

        // Sum incorrect ticket values.
        $sumIncorrect = array_sum($invalidValues);
        echo "Part 1, sum of all invalid ticket values {$sumIncorrect} (" . number_format($sumIncorrect) . ").\n\n";

        // Product of my tickets departure parts.
        $productMyTicket = array_product($parts);
        echo "Part 2, product of all values from my ticket {$productMyTicket} (" . number_format(
            $productMyTicket
        ) . ").";
        return null;
    }
}
