<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day5 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        $input = str_replace(['B', 'F', 'R', 'L'], [1, 0, 1, 0], $input);

        $seats = [];
        foreach (explode("\n", $input) as $ticket) {
            $seatNumber = bindec(substr($ticket, 0, 7));
            $seatColumn = bindec(substr($ticket, 7, 3));

            $seatId = ($seatNumber * 8) + $seatColumn;
            $seats[] = $seatId;
        }

        $highest = max($seats);
        $lowest = min($seats);

        $mySeat = -1;
        $ignoreSeats = [-1, 1];
        for ($i = $lowest; $i < $highest; $i++) {
            $seatOccupied = in_array($i, $seats);
            if ($seatOccupied || in_array($i, $ignoreSeats)) {
                continue;
            }
            $hasNeighbours = in_array($i - 1, $seats) && in_array($i + 1, $seats);
            if (!$hasNeighbours) {
                continue;
            }

            $mySeat = $i;
            break;
        }

        echo "Highest Seat: {$highest}\nMy Seat: {$mySeat}";
        return null;
    }
}
