<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day18 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        // 175783959471 too low

        if (isset($_GET['debug'])) {
            $input = '1 + (2 * 3) + (4 * (5 + 6))';
        }
        if (isset($_GET['debug2'])) {
            $input = '1 + 2 * 3 + 4 * 5 + 6';
        }
        if (isset($_GET['debug3'])) {
            $input = '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2';
        }
        if (isset($_GET['debug4'])) {
            $input = '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2';
        }

        $answers = [];
        foreach (Parser::iterateByLine($input) as $equation) {
            $answers[] = $this->calculate($equation, [$this, 'calculateBasic']);
        }
        $result = array_sum($answers);
        echo "Part 1 Answer {$result} (" . number_format($result) . ")\n";

        $answers = [];
        foreach (Parser::iterateByLine($input) as $equation) {
            $answers[] = $this->calculate($equation, [$this, 'calculateAdvanced']);
        }
        $result = array_sum($answers);
        echo "Part 2 Answer {$result} (" . number_format($result) . ")\n";
        return null;
    }

    public function calculate(string $equation, callable $calcFunction): int
    {
        // While the equation has Parenthesis.
        while (preg_match_all('/(?<=\()[^()]+(?=\))/', $equation, $subEquations, PREG_OFFSET_CAPTURE)) {
            // Iterate all of the most nested equations.
            foreach (array_reverse($subEquations[0]) as $subEquation) {
                // Extract and perform calculation in parenthesis.
                $offset = $subEquation[1];
                $before = substr($equation, 0, $offset - 1);
                $after = substr($equation, $offset + strlen($subEquation[0]) + 1);
                $answer = $calcFunction(substr($equation, $offset, strlen($subEquation[0])));
                // Rebuild equation with sub-equation calculated.
                $equation = $before . $answer . $after;
            }
        }
        // Calculate final equation.
        return $calcFunction($equation);
    }

    public function calculateAdvanced(string $equation)
    {
        $equation = str_replace([' + '], ['+'], $equation);
        // Iterate equations parts between multiplication.
        return array_reduce(explode(' * ', $equation), function ($carry, $item) {
            // Resolve the parts of the equation between multiplication with basic calculation.
            return $carry * $this->calculateBasic($item);
        }, 1);
    }

    public function calculateBasic(string $equation)
    {
        $equation = str_replace(['+ ', '* '], ['+', '*'], $equation);
        // Iteration all parts of equation.
        return array_reduce(explode(' ', $equation), function ($carry, $item) {
            // Evaluate equation part.
            eval("\$carry = {$carry}{$item};");
            return $carry;
        }, '0+');
    }
}
