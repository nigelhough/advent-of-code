<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day3 extends AbstractSolution
{
    private const TREE = '#';

    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        $input = Parser::iterateByLine($input);
        $trees = $this->treeCheck($input, 3, 1);
        echo "\nTrees Hit: {$trees}\n\n";
        $answer1 = $trees;

        $slopes = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]];

        $treeCount = [];
        foreach ($slopes as $slope) {
            [$right, $down] = $slope;

            $trees = $this->treeCheck($input, $right, $down);
            $treeCount[] = $trees;
            echo "{$right}, {$down} >> {$trees}\n";
        }
        $product = array_reduce($treeCount, function ($carry, $item) {
            return ($carry * $item);
        }, 1);
        echo "Product of Tress : {$product}";
        $answer2 = $product;

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function treeCheck($input, $right, $down): int
    {
        $currentX = 0;
        $trees = 0;
        foreach ($input as $index => $line) {
            if (($index % $down) !== 0) {
                continue;
            }
            if ($currentX >= strlen($line)) {
                $currentX = $currentX - strlen($line);
            }
            $char = substr($line, $currentX, 1);
            if ($char === self::TREE) {
                $trees++;
            }
            //echo substr($line, 0, $currentX) . "0" . substr($line, $currentX + 1) . "\n";
            $currentX += $right;
        }
        return $trees;
    }
}
