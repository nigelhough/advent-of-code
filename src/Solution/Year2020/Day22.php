<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\AbstractSolution;

class Day22 extends AbstractSolution
{
    public function execute(string $input): ?AnswerInterface
    {
        if (isset($_GET['debug'])) {
            $input = 'Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
';
        }

        $players = [];
        $hands = [];
        $input = str_replace('Player ', '', $input);
        foreach (Parser::iterateByGroup($input) as $playerDetails) {
            [$player, $cards] = explode(":", $playerDetails);
            $player = trim($player);
            $cards = trim($cards);

            $players[] = $player;
            $hands[$player] = explode("\n", $cards);
            $hands[$player] = array_map('intval', $hands[$player]);
        }

        $winner = $this->playGame($players, $hands);

        $score = 0;
        foreach (array_reverse($hands[$winner]) as $index => $card) {
            $score += (($index + 1) * $card);
        }
        echo "The winner is Player {$winner}.\n";
        echo "The final score is {$score} (" . number_format($score) . ")\n\n";
        return null;
    }

    public function playGame(array $players, array &$hands): int
    {
        $round = 1;
        $winner = null;
        $history = [];
        while ($round < 500000) {
            if (empty($hands[1]) || empty($hands[2])) {
                break;
            }
            $handsKey = md5(serialize($hands));
            if (isset($history[$handsKey])) {
                $winner = 1;
                break;
            }
            $history[$handsKey] = $handsKey;

            $cards = [];
            $recursiveCombat = true;
            // Draw Cards.
            foreach ($players as $player) {
                $cards[$player] = array_shift($hands[$player]);
                if (count($hands[$player]) < $cards[$player]) {
                    $recursiveCombat = false;
                }
            }
            if ($recursiveCombat) {
                // Recursive combat.
                $subHands = [];
                foreach ($hands as $player => $hand) {
                    $subHands[$player] = array_slice($hand, 0, $cards[$player]);
                }
                $winner = $this->playGame($players, $subHands);
            } else {
                // High card wins.
                arsort($cards);
                $winner = array_key_first($cards);
            }

            // Sort cards by winner.
            uksort(
                $cards,
                function ($a, $b) use ($winner) {
                    return $b == $winner;
                }
            );
            $hands[$winner] = array_merge($hands[$winner], $cards);

            $round++;
        }

        return $winner;
    }
}
