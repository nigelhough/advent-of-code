<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Answer\AnswerInterface;
use AdventOfCode\Solution\SolutionInterface;

class Day6 implements SolutionInterface
{
    public function execute(string $input): ?AnswerInterface
    {
        $anyoneAnswered = 0;
        $everyoneAnswered = 0;
        foreach (Parser::iterateByGroup($input) as $group) {
            $individuals = substr_count($group, "\n") + 1;
            $uniqueAnswers = array_unique(str_split(str_replace("\n", "", $group)));
            $anyoneAnswered += count($uniqueAnswers);

            foreach ($uniqueAnswers as $answered) {
                $everyoneAnswered += (int) (substr_count($group, $answered) == $individuals);
            }
        }
        var_dump($anyoneAnswered);
        var_dump($everyoneAnswered);
        return null;
    }
}
