<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day2 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        $input = Parser::iterateByLine($input);

        $correctPasswords = 0;
        foreach ($input as $value) {
            [$parts, $char, $password] = explode(" ", $value);
            [$min, $max] = explode("-", $parts);
            $char = trim($char, ":");
            $count = substr_count($password, $char);
            $charInRange = ($count >= $min && $count <= $max);
            $isValid = $charInRange;
            if ($isValid) {
                $correctPasswords++;
            }
        }
        $answer1 = $correctPasswords;
        echo "Correct Passwords (Policy 1) = {$correctPasswords}\n";

        $correctPasswords = 0;
        foreach ($input as $value) {
            [$parts, $char, $password] = explode(" ", $value);
            [$p1, $p2] = explode("-", $parts);
            $char = trim($char, ":");

            $passChar1 = substr($password, (int) $p1 - 1, 1);
            $passChar2 = substr($password, (int) $p2 - 1, 1);

            $char1Correct = $passChar1 === $char;
            $char2Correct = $passChar2 === $char;

            $isValid = ($char1Correct && !$char2Correct) || (!$char1Correct && $char2Correct);
            if ($isValid) {
                $correctPasswords++;
            }
        }
        $answer2 = $correctPasswords;
        echo "Correct Passwords (Policy 2) = {$correctPasswords}\n";

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }
}
