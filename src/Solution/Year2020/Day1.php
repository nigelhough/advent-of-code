<?php

declare(strict_types=1);

namespace AdventOfCode\Solution\Year2020;

use AdventOfCode\Input\Parser;
use AdventOfCode\Solution\AbstractSolution;
use AdventOfCode\Answer\AnswerInterface;

class Day1 extends AbstractSolution
{
    /**
     * @inheritDoc
     */
    public function execute(string $input): ?AnswerInterface
    {
        $answer1 = $answer2 = null;
        if (isset($_GET['sample'])) {
            $input = '';
        }
        $input = Parser::iterateNumbersByLine($input);

        foreach ($input as $value) {
            if ($diff = $this->getTarget($value, $input)) {
//                echo "{$diff} + {$value} = 2020\n";
                echo "{$diff} * {$value} = " . ($diff * $value);
                $answer1 = $diff * $value;
                break;
            }
        }
        echo "\n";

        sort($input);
        foreach ($input as $value1) {
            foreach ($input as $value2) {
                if ($diff = $this->getTarget($value1 + $value2, $input)) {
//                    echo "{$diff} + {$value1} + {$value2} = " . ($diff + $value1 + $value2) . "\n";
                    echo "{$diff} * {$value1} * {$value2}  = " . ($diff * $value1 * $value2);
                    $answer2 = $diff * $value1 * $value2;
                    break 2;
                }
            }
        }

        return $this->answerFactory->create($answer1, $answer2, isset($_GET['sample']));
    }

    private function getTarget($value, $list, $target = 2020)
    {
        if (in_array($target - $value, $list)) {
            return $target - $value;
        }

        return null;
    }
}
