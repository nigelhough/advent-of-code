<?php

declare(strict_types=1);

namespace AdventOfCode\Solution;

use AdventOfCode\Answer\Factory;

abstract class AbstractSolution implements SolutionInterface
{
    public function __construct(protected Factory $answerFactory)
    {
    }
}
