<?php

declare(strict_types=1);

namespace AdventOfCode\EncodedImage;

use Exception;

class Image
{
    private $raw;

    private $height;

    private $width;

    private $colourMap;

    public function __construct(string $raw, int $height, int $width, array $colourMap)
    {
        $this->raw = $raw;
        $this->height = $height;
        $this->width = $width;
        $this->colourMap = $colourMap;
    }

    public function checkSum(): int
    {
        $zeroLayer = $this->getLayerWithFewestZeros();
        return substr_count($zeroLayer, '1') * substr_count($zeroLayer, '2');
    }

    private function getLayerWithFewestZeros(): string
    {
        $zeroLayer = '00000000000000000000000000000000000000000000000000000000000000000000000000000000';
        foreach ($this->getLayers() as $layer) {
            $zeros = substr_count($layer, '0');

            if ($zeros < substr_count($zeroLayer, '0')) {
                $zeroLayer = $layer;
            }
        }

        return $zeroLayer;
    }

    private function getLayers(): array
    {
        return (array) str_split($this->raw, $this->width * $this->height);
    }

    public function render(int $scale = 1, $filename = 'result.png')
    {
        $gd = imagecreatetruecolor($this->width * $scale, $this->height * $scale);
        if ($gd === false) {
            throw new Exception('Unable to create image');
        }
        $colours = $this->getColours($gd);

        $resultImage = $this->getRawImage();
        $resultImage = $this->scaleImage($resultImage, $scale);

        foreach ($resultImage as $y => $line) {
            foreach ($line as $x => $value) {
                imagesetpixel($gd, $x, $y, $colours[$value]);
            }
        }

        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0777, true);
        }
        imagepng($gd, $filename);
    }

    private function getColours($gd): array
    {
        $colours = [];
        foreach ($this->colourMap as $key => $colour) {
            $colours[$key] = imagecolorallocate($gd, $colour[0], $colour[1], $colour[2]);
        }

        return $colours;
    }

    private function getRawImage(): array
    {
        $resultImage = [];
        $colourKeys = array_keys($this->colourMap);
        for ($i = 0; $i < $this->width * $this->height; $i++) {
            foreach ($this->getLayers() as $layer) {
                if (isset($layer[$i]) && in_array($layer[$i], $colourKeys)) {
                    $resultImage[$i] = $layer[$i];
                    break;
                }
            }
        }

        return array_chunk($resultImage, $this->width);
    }

    private function scaleImage(array $image, int $scale): array
    {
        if ($scale === 1) {
            return $image;
        }

        $resultImage = [];
        foreach ($image as $layer) {
            $newLayer = [];
            foreach ($layer as $value) {
                for ($i = 0; $i < $scale; $i++) {
                    $newLayer[] = $value;
                }
            }
            for ($i = 0; $i < $scale; $i++) {
                $resultImage[] = $newLayer;
            }
        }

        return $resultImage;
    }
}
