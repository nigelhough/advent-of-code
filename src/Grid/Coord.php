<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

class Coord implements CoordinateInterface
{
    public function __construct(private int $xPosition, private int $yPosition)
    {
    }

    /**
     * @return self[][]
     */
    public static function setListFromString(
        string $list,
        string $listDelimiter = "\n",
        string $setDelimiter = ' ',
        string $delimiter = ','
    ): array {
        return array_map(function (string $coords) use ($setDelimiter, $delimiter) {
            return self::setFromString($coords, $setDelimiter, $delimiter);
        }, explode($listDelimiter, $list));
    }

    /**
     * @return self[]
     */
    public static function setFromString(string $coords, string $setDelimiter = ' ', string $delimiter = ','): array
    {
        return array_map(function (string $coord) use ($delimiter) {
            return self::fromString($coord, $delimiter);
        }, explode($setDelimiter, $coords));
    }

    public static function fromString(string $coord, string $delimiter = ','): self
    {
        [$x, $y] = explode($delimiter, $coord);
        return new self((int)$x, (int)$y);
    }

    public function rotateAroundCentralPoint(self $center, int $degrees, bool $rotationClockwise = true): self
    {
        // @todo Write a test and Optimise this logic.
        $location = new Coord($this->getXPosition(), $this->getYPosition());

        // @todo Support a base greather than 4.
        $rotations = ($degrees / 90) % 4;
        $rotations = $rotationClockwise ? $rotations : (-$rotations + 4);

        for ($i = 0; $i < $rotations; $i++) {
            $xDifference = $location->getXPosition() - $center->getXPosition();
            $yDifference = $location->getYPosition() - $center->getYPosition();
            $location = $center->moveOffset(new Offset(-$yDifference, +$xDifference));
        }
        return $location;
    }

    public function getXPosition(): int
    {
        return $this->xPosition;
    }

    public function getYPosition(): int
    {
        return $this->yPosition;
    }

    public function moveOffset(Offset $offset, int $iterations = 1): self
    {
        $x = $this->getXPosition();
        $y = $this->getYPosition();
        for ($i = 0; $i < $iterations; $i++) {
            $x = $x + $offset->getX();
            $y = $y + $offset->getY();
        }
        return new self($x, $y);
    }

    public function moveAlongBearing(Bearing $bearing, int $distance = 1): self
    {
        return new self(
            $this->xPosition + ($bearing->getXMod() * $distance),
            $this->yPosition + ($bearing->getYMod() * $distance)
        );
    }

    public function getOffset(self $coord): Offset
    {
        return Offset::createFromCoordinates($this, $coord);
    }

    public function __toString()
    {
        return "{$this->xPosition},{$this->yPosition}";
    }

    public function getCost(): int
    {
        return 1;
    }

    /**
     * @return self[]
     */
    public function getNeighbours(): array
    {
        $coordinates = [];
        foreach (Bearing::neighbours() as $bearing) {
            [$xMod, $yMod] = $bearing->getModifiers();
            $coordinates[] = new self($this->getXPosition() + $xMod, $this->getYPosition() + $yMod);
        }
        return $coordinates;
    }

    /**
     * @param int $sightRange The range of sight that distance should extend to.
     *
     * @return self[]
     */
    public function getLineOfSight(int $sightRange): array
    {
        if ($sightRange <= 0) {
            return [];
        }
        if ($sightRange === 1) {
            return $this->getSurrounding();
        }
        $coordinates = [];
        for ($distance = 1; $distance <= $sightRange; $distance++) {
            foreach (Bearing::surrounding() as $bearing) {
                [$xMod, $yMod] = $bearing->getModifiers();
                $coordinates[] = new self(
                    $this->getXPosition() + ($xMod * $distance),
                    $this->getYPosition() + ($yMod * $distance)
                );
            }
        }
        return $coordinates;
    }

    /**
     * @return self[]
     */
    public function getSurrounding(): array
    {
        $coordinates = [];
        foreach (Bearing::surrounding() as $bearing) {
            [$xMod, $yMod] = $bearing->getModifiers();
            $coordinates[] = new self($this->getXPosition() + $xMod, $this->getYPosition() + $yMod);
        }
        return $coordinates;
    }

    /**
     * @return self[]
     */
    public function getFirstValidLineOfSight(array $validLocations, int $maxDistance = 100): array
    {
        $bearings = Bearing::surrounding();

        $coordinates = [];
        for ($distance = 1; $distance <= $maxDistance; $distance++) {
            foreach ($bearings as $bearingKey => $bearing) {
                [$xMod, $yMod] = $bearing->getModifiers();
                $x = $this->getXPosition() + ($xMod * $distance);
                $y = $this->getYPosition() + ($yMod * $distance);
                if (isset($validLocations["{$x},{$y}"])) {
                    $coordinates[] = new self($x, $y);
                    unset($bearings[$bearingKey]);
                }
            }
            if (empty($bearings)) {
                break;
            }
        }
        return $coordinates;
    }

    /**
     * @return self
     */
    public function getAnyFirstValidLineOfSight(array $validLocations, int $maxDistance = 100): ?self
    {
        $bearings = Bearing::surrounding();

        for ($distance = 1; $distance <= $maxDistance; $distance++) {
            foreach ($bearings as $bearing) {
                [$xMod, $yMod] = $bearing->getModifiers();
                $x = $this->getXPosition() + ($xMod * $distance);
                $y = $this->getYPosition() + ($yMod * $distance);
                if (isset($validLocations["{$x},{$y}"])) {
                    return new self($x, $y);
                }
            }
        }
        return null;
    }

    /**
     * Is this point exactly between two others.
     *
     * @param self $source
     * @param self $target
     *
     * @return bool
     */
    public function isBetween(self $source, self $target): bool
    {
        $dxc = $this->xPosition - $source->xPosition;
        $dyc = $this->yPosition - $source->yPosition;

        $dxl = $target->xPosition - $source->xPosition;
        $dyl = $target->yPosition - $source->yPosition;

        $cross = $dxc * $dyl - $dyc * $dxl;

        if ($cross != 0) {
            return false;
        }

        if (abs($dxl) >= abs($dyl)) {
            return $dxl > 0
                ?
                $source->xPosition <= $this->xPosition && $this->xPosition <= $target->xPosition
                :
                $target->xPosition <= $this->xPosition && $this->xPosition <= $source->xPosition;
        } else {
            return $dyl > 0
                ?
                $source->yPosition <= $this->yPosition && $this->yPosition <= $target->yPosition
                :
                $target->yPosition <= $this->yPosition && $this->yPosition <= $source->yPosition;
        }
    }

    /**
     * Distance from this coordinate to a target.
     *
     * @param self $target
     *
     * @return float
     */
    public function distance($target): float
    {
        return $this->manhattanDistance($target);
    }

    /**
     * Manhattan Distance from this coordinate to a target.
     *
     * @param self $target
     *
     * @return float
     */
    public function manhattanDistance(self $target): float
    {
        return abs($target->yPosition - $this->yPosition) + abs($target->xPosition - $this->xPosition);
    }

    /**
     * Is this coordinate the same as another.
     *
     * @param CoordinateInterface $subject
     *
     * @return bool
     */
    public function isSame($subject)
    {
        return ($this->xPosition === $subject->getXPosition()) && ($this->yPosition === $subject->getYPosition());
    }
}
