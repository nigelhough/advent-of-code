<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

class Portal implements CoordinateInterface
{
    public function __construct(
        private string $code,
        private Coord $entrance,
        private Coord $exit,
        private int $cost = 1
    ) {
    }

    public function getXPosition(): int
    {
        return $this->entrance->getXPosition();
    }

    public function getYPosition(): int
    {
        return $this->entrance->getYPosition();
    }

    public function getNeighbours(): array
    {
        $neighbours = $this->entrance->getNeighbours();
        $neighbours[] = $this->exit;
        return $neighbours;
    }

    public function __toString()
    {
        return (string)$this->entrance;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return Coord
     */
    public function getEntrance(): Coord
    {
        return $this->entrance;
    }

    /**
     * @return Coord
     */
    public function getExit(): Coord
    {
        return $this->exit;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @param self $target
     *
     * @return float
     */
    public function distance($target): float
    {
        if ($this->isSame($target)) {
            return 0;
        }
        return $this->entrance->distance($target);
    }

    public function isSame($subject)
    {
        return $this->entrance->isSame($subject);
    }

    public function getSurrounding(): array
    {
        $surroundings = $this->entrance->getSurrounding();
        $surroundings[] = $this->exit;
        return $surroundings;
    }
}
