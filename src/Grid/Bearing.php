<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use LogicException;

class Bearing
{
    public const NORTH = 'N';

    public const NORTH_EAST = 'NE';

    public const EAST = 'E';

    public const SOUTH_EAST = 'SE';

    public const SOUTH = 'S';

    public const SOUTH_WEST = 'SW';

    public const WEST = 'W';

    public const NORTH_WEST = 'NW';

    public function __construct(private int $xMod, private int $yMod)
    {
    }

    public static function toCoordinate(CoordinateInterface $start, CoordinateInterface $end): self
    {
        // Vertical Line
        if ($end->getXPosition() === $start->getXPosition()) {
            if ($end->getYPosition() > $start->getYPosition()) {
                return self::get(self::SOUTH);
            }
            return self::get(self::NORTH);
        }
        // Horizontal Line
        if ($end->getYPosition() === $start->getYPosition()) {
            if ($end->getXPosition() > $start->getXPosition()) {
                return self::get(self::EAST);
            }
            return self::get(self::WEST);
        }

        // Diagonal Line.
        if ($end->getXPosition() > $start->getXPosition()) {
            if ($end->getYPosition() > $start->getYPosition()) {
                return self::get(self::SOUTH_EAST);
            }
            if ($end->getYPosition() < $start->getYPosition()) {
                return self::get(self::NORTH_EAST);
            }
        }

        $offset = Offset::createFromCoordinates($start, $end);
        if (abs($offset->getX()) !== abs($offset->getY())) {
            throw new LogicException('Coordinates are not in a diagonal line');
        }

        // Diagonal Line.
        if ($end->getXPosition() < $start->getXPosition()) {
            if ($end->getYPosition() > $start->getYPosition()) {
                return self::get(self::SOUTH_WEST);
            }
            if ($end->getYPosition() < $start->getYPosition()) {
                return self::get(self::NORTH_WEST);
            }
        }

        throw new LogicException('Unable to identify bearing');
    }

    public static function get(string $bearingIdentifier): self
    {
        if (!self::isValidBearingIdentifier($bearingIdentifier)) {
            throw new LogicException("Invalid Bearing Identifier ({$bearingIdentifier})");
        }
        return match ($bearingIdentifier) {
            // 4 Directions.
            self::NORTH => new self(0, -1),
            self::SOUTH => new self(0, 1),
            self::EAST => new self(1, 0),
            self::WEST => new self(-1, 0),
            // 8 Directions.
            self::NORTH_EAST => new self(1, -1),
            self::SOUTH_EAST => new self(1, 1),
            self::SOUTH_WEST => new self(-1, 1),
            self::NORTH_WEST => new self(-1, -1),
        };
    }

    public static function isValidBearingIdentifier(string $identifier): bool
    {
        return in_array(
            $identifier,
            [
                self::NORTH,
                self::NORTH_EAST,
                self::EAST,
                self::SOUTH_EAST,
                self::SOUTH,
                self::SOUTH_WEST,
                self::WEST,
                self::NORTH_WEST,
            ]
        );
    }

    public static function fromOffset(Offset $offset)
    {
        if ($offset->getX() === 0 && $offset->getY() === 0) {
            throw new LogicException('Can not create bearing from no offset');
        }
        if (
            !($offset->getX() === 0 || $offset->getY() === 0) &&
            !in_array($offset->getX() / $offset->getY(), [-1, 0, 1])
        ) {
            // Only supports Bearing inline with a 3 by 3 grid.
            throw new LogicException('Unsupported Bearing.');
        }
        return new Bearing(
            $offset->getX() === 0 ? 0 : ($offset->getX() > 0 ? 1 : -1),
            $offset->getY() === 0 ? 0 : ($offset->getY() > 0 ? 1 : -1)
        );
    }

    public static function getFromArrowDirection(string $arrowDirection)
    {
        if (!in_array($arrowDirection, ['^', 'v', '>', '<'])) {
            throw new LogicException("Invalid Arrow Direction ({$arrowDirection})");
        }
        return match ($arrowDirection) {
            // 4 Directions.
            '^' => self::get(self::NORTH),
            'v' => self::get(self::SOUTH),
            '>' => self::get(self::EAST),
            '<' => self::get(self::WEST),
        };
    }

    /**
     * @return self[]
     */
    public static function neighbours(): array
    {
        return [
            self::NORTH => new self(0, -1),
            self::EAST => new self(1, 0),
            self::SOUTH => new self(0, 1),
            self::WEST => new self(-1, 0),
        ];
    }

    /**
     * @return self[]
     */
    public static function surrounding(): array
    {
        return [
            self::NORTH => new self(0, -1),
            self::NORTH_EAST => new self(1, -1),
            self::EAST => new self(1, 0),
            self::SOUTH_EAST => new self(1, 1),
            self::SOUTH => new self(0, 1),
            self::SOUTH_WEST => new self(-1, 1),
            self::WEST => new self(-1, 0),
            self::NORTH_WEST => new self(-1, -1),
        ];
    }

    public function rotateClockwise(): self
    {
        return match ((string)$this) {
            // 4 Directions.
            self::NORTH => self::get(self::EAST),
            self::EAST => self::get(self::SOUTH),
            self::SOUTH => self::get(self::WEST),
            self::WEST => self::get(self::NORTH),
//            // 8 Directions.
//            self::NORTH_EAST => self::get(self::NORTH_EAST),
//            self::SOUTH_EAST => self::get(self::SOUTH_EAST),
//            self::SOUTH_WEST => self::get(self::SOUTH_WEST),
//            self::NORTH_WEST => self::get(self::NORTH_WEST),
        };
    }

    public function rotateAntiClockwise(): self
    {
        return match ((string)$this) {
            // 4 Directions.
            self::NORTH => self::get(self::WEST),
            self::WEST => self::get(self::SOUTH),
            self::SOUTH => self::get(self::EAST),
            self::EAST => self::get(self::NORTH),
//            // 8 Directions.
//            self::NORTH_EAST => self::get(self::NORTH_EAST),
//            self::SOUTH_EAST => self::get(self::SOUTH_EAST),
//            self::SOUTH_WEST => self::get(self::SOUTH_WEST),
//            self::NORTH_WEST => self::get(self::NORTH_WEST),
        };
    }

    public function getModifiers(): array
    {
        return [$this->xMod, $this->yMod];
    }

    /**
     * @return int
     */
    public function getXMod(): int
    {
        return $this->xMod;
    }

    /**
     * @return int
     */
    public function getYMod(): int
    {
        return $this->yMod;
    }

    public function isSame(self $subject)
    {
        return ($this->xMod === $subject->xMod) && ($this->yMod === $subject->yMod);
    }

    public function isDiagonal(): bool
    {
        return ($this->xMod === 1 && $this->yMod === -1) || // NORTH_EAST
            ($this->xMod === 1 && $this->yMod === 1) || // SOUTH_EAST
            ($this->xMod === -1 && $this->yMod === 1) || // SOUTH_WEST
            ($this->xMod === -1 && $this->yMod === -1); // NORTH_WEST
    }

    public function __toString()
    {
        return match ([$this->xMod, $this->yMod]) {
            [0, -1] => self::NORTH,
            [0, 1] => self::SOUTH,
            [1, 0] => self::EAST,
            [-1, 0] => self::WEST,
            [1, -1] => self::NORTH_EAST,
            [1, 1] => self::SOUTH_EAST,
            [-1, 1] => self::SOUTH_WEST,
            [-1, -1] => self::NORTH_WEST,
        };
    }
}
