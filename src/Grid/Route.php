<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use Countable;

class Route implements Countable
{
    /** @var CoordinateInterface */
    private $target;

    /** @var Path */
    private $path;

    /**
     * Route constructor.
     *
     * @param CoordinateInterface $target
     * @param Path                $path
     */
    public function __construct(CoordinateInterface $target, Path $path)
    {
        $this->target = $target;
        $this->path = $path;
    }

    /**
     * @return CoordinateInterface
     */
    public function getTarget(): CoordinateInterface
    {
        return $this->target;
    }

    /**
     * @return Path
     */
    public function getPath(): Path
    {
        return $this->path;
    }

    public function count(): int
    {
        return $this->path->count();
    }

    public function cost(): int
    {
        return $this->path->cost();
    }
}
