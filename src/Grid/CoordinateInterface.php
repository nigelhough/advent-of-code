<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use Stringable;

interface CoordinateInterface extends Stringable
{
    public function getXPosition(): int;

    public function getYPosition(): int;

    public function getCost(): int;

    public function getNeighbours(): array;

    public function isSame($subject);

    public function distance($target): float;

    public function getSurrounding(): array;
}
