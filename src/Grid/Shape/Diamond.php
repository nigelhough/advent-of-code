<?php

declare(strict_types=1);

namespace AdventOfCode\Grid\Shape;

use AdventOfCode\Grid\Bearing;
use AdventOfCode\Grid\Coord;

class Diamond
{
    public function __construct(
        public readonly Coord $north,
        public readonly Coord $east,
        public readonly Coord $south,
        public readonly Coord $west
    ) {
    }

    public static function createFromCenter(Coord $center, int $radius): self
    {
        return new self(
            $center->moveAlongBearing(Bearing::get(Bearing::NORTH), $radius),
            $center->moveAlongBearing(Bearing::get(Bearing::EAST), $radius),
            $center->moveAlongBearing(Bearing::get(Bearing::SOUTH), $radius),
            $center->moveAlongBearing(Bearing::get(Bearing::WEST), $radius)
        );
    }

    /**
     * @return \Generator|Coord[]
     */
    public function getPoints(): \Generator
    {
        $centerX = $this->north->getXPosition();
        $width = 0;
        for ($y = $this->north->getYPosition(); $y <= $this->south->getYPosition(); $y++) {
            for ($x = ($centerX - $width); $x <= ($centerX + $width); $x++) {
                yield new Coord($x, $y);
            }
            if ($y >= $this->east->getYPosition()) {
                $width--;
            } else {
                $width++;
            }
        }
    }

    public function getXRangeOnY(int $targetY): ?array
    {
        if ($targetY < $this->north->getYPosition() || $targetY > $this->south->getYPosition()) {
            // Y Axis not in range of area.
            return null;
        }

        $centerY = $this->east->getYPosition();
        $centerWidth = $this->east->getXPosition() - $this->west->getXPosition();
        $centerOffset = abs($centerY - $targetY);
        $targetWidth = $centerWidth - ($centerOffset * 2);

        $centerX = $this->north->getXPosition();
        $start = $centerX - ($targetWidth / 2);
        $end = $centerX + ($targetWidth / 2);

        return [$start, $end];
    }
}
