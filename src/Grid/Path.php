<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use Countable;
use Exception;
use Generator;

class Path implements Countable
{
    private $steps = [];

    public function __construct(CoordinateInterface ...$steps)
    {
        $this->steps = $steps;
    }

    public static function betweenCoordinates(
        CoordinateInterface $start,
        CoordinateInterface $end,
        int $stepLimit = 5000
    ) {
        $bearing = Bearing::toCoordinate($start, $end);

        $i = 0;
        $steps = [];
        $newPosition = clone $start;
        do {
            $steps[] = $newPosition;
            if ($newPosition->isSame($end)) {
                return new self(...$steps);
            }
            $i++;
        } while (($newPosition = $newPosition->moveAlongBearing($bearing)) && $i < $stepLimit);

        throw new Exception("Couldn't find the end of the line. {$start} to {$end}");
    }

    public function follow(): Generator
    {
        foreach ($this->steps as $step) {
            yield $step;
        }
    }

    public function addStep(CoordinateInterface $step): self
    {
        return new self(...array_merge($this->steps, [$step]));
    }

    public function count(): int
    {
        return count($this->steps);
    }

    public function getFirstStep(): CoordinateInterface
    {
        return $this->steps[array_key_first($this->steps)];
    }

    public function getLastStep(): CoordinateInterface
    {
        return $this->steps[array_key_last($this->steps)];
    }

    public function cost(): int
    {
        $cost = 0;
        foreach ($this->steps as $step) {
            $cost += $step->getCost();
        }
        return $cost;
    }

    public function hasVisited(CoordinateInterface $location): bool
    {
        foreach ($this->steps as $step) {
            if ($step->isSame($location)) {
                return true;
            }
        }
        return false;
    }
}
