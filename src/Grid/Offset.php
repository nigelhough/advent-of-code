<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

/**
 * The Offset between two grid coordinates.
 *
 * @package AdventOfCode\Grid
 */
class Offset
{
    public function __construct(private int $xOffset, private int $yOffset)
    {
    }

    public static function createFromCoordinates(CoordinateInterface $source, CoordinateInterface $target): self
    {
        return new self(
            $source->getXPosition() - $target->getXPosition(),
            $source->getYPosition() - $target->getYPosition(),
        );
    }

    public function __toString()
    {
        return "{$this->xOffset},{$this->yOffset}";
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->xOffset;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->yOffset;
    }

    public function getBearing(): Bearing
    {
        return Bearing::fromOffset($this);
    }
}
