<?php

declare(strict_types=1);

namespace AdventOfCode\Grid\Route;

use AdventOfCode\Grid\CoordinateInterface;
use AdventOfCode\Grid\Path;
use AdventOfCode\Grid\Route;
use Exception;

class Router
{
    /**
     * Find The Shortest Route between two points.
     * Considers all coordinates along a path equal with no increased cost
     *
     * @param CoordinateInterface $source      The Start of The Route
     * @param CoordinateInterface $destination The Destination of The Route.
     *
     * @return Route
     */
    public static function route(CoordinateInterface $source, CoordinateInterface $destination): Route
    {
        if ($source->isSame($destination)) {
            return new Route($source, new Path());
        }

        $path = new Path($source);

        // Don't get lost, limit loop.
        while ($path->count() < 100000) {
            $nextStep = null;
            foreach ($path->getLastStep()->getNeighbours() as $possibleStep) {
                if ($destination->isSame($possibleStep)) {
                    return new Route($destination, $path);
                }
                if (is_null($nextStep) || $destination->distance($possibleStep) < $destination->distance($nextStep)) {
                    $nextStep = $possibleStep;
                }
            }
            if ($path->hasVisited($nextStep)) {
                throw new Exception('This looks familiar, it\'s gone horribly wrong, we\'ve already been here!');
            }
            $path = $path->addStep($nextStep);
        }

        throw new Exception('Couldn\'t find a valid route.');
    }
}
