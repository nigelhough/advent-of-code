<?php

declare(strict_types=1);

namespace AdventOfCode\Grid;

use AdventOfCode\Alphabet\Alphabet;
use Countable;
use Exception;
use Generator;

class Map implements Countable
{
    public $coordinates = [];

    private $wall;

    private $clearSpace;

    /** @var CoordinateInterface */
    private $bottomLeft = null;

    /** @var CoordinateInterface */
    private $topRight = null;


    public function __construct($wall = '#', $clearSpace = '.')
    {
        $this->wall = $wall;
        $this->clearSpace = $clearSpace;
    }

    public static function createFromString(
        string $mapData,
        CoordinateInterface $startCoord = null,
        array $ignoreCharacters = []
    ): self {
        $map = new self();
        $x = !empty($startCoord) ? $startCoord->getXPosition() : 0;
        $y = !empty($startCoord) ? $startCoord->getYPosition() : 0;

        foreach (explode("\n", $mapData) as $line) {
            foreach (str_split($line) as $value) {
                if (!in_array($value, $ignoreCharacters)) {
                    $map->mark(new Coord($x, $y), $value);
                }
                $x++;
            }
            $x = 0;
            $y++;
        }

        return $map;
    }

    public function mark(CoordinateInterface $coord, $value = '#')
    {
        $this->coordinates[(string) $coord] = $value;
        $this->updateMBR($coord);
    }

    public function updateMBR(CoordinateInterface $coord)
    {
        if (is_null($this->bottomLeft)) {
            $bottomX = $coord->getXPosition();
            $bottomY = $coord->getYPosition();
        } else {
            $bottomX = $coord->getXPosition() < $this->bottomLeft->getXPosition() ? $coord->getXPosition()
                : $this->bottomLeft->getXPosition();
            $bottomY = $coord->getYPosition() < $this->bottomLeft->getYPosition() ? $coord->getYPosition()
                : $this->bottomLeft->getYPosition();
        }
        $this->bottomLeft = new Coord($bottomX, $bottomY);

        if (is_null($this->topRight)) {
            $this->topRight = $coord;
            $topX = $coord->getXPosition();
            $topY = $coord->getYPosition();
        } else {
            $topX = $coord->getXPosition() > $this->topRight->getXPosition() ? $coord->getXPosition()
                : $this->topRight->getXPosition();
            $topY = $coord->getYPosition() > $this->topRight->getYPosition() ? $coord->getYPosition()
                : $this->topRight->getYPosition();
        }
        $this->topRight = new Coord($topX, $topY);
    }

    public function count(): int
    {
        return count($this->coordinates);
    }

    public function toggle(CoordinateInterface $coord, $value1, $value2)
    {
        if (isset($this->coordinates[(string) $coord])) {
            $this->coordinates[(string) $coord] = $this->coordinates[(string) $coord] === $value1 ? $value2
                : $value1;
        } else {
            $this->coordinates[(string) $coord] = $value1;
        }
        $this->updateMBR($coord);
    }

    public function markMap(Coord $startLocation, self $map)
    {
        $x = $startLocation->getXPosition();
        $y = $startLocation->getYPosition();
        foreach ($map->coordinates as $targetAddress => $targetValue) {
            [$tX, $tY] = explode(',', $targetAddress);
            $sampleX = $x + (int) $tX;
            $sampleY = $y + (int) $tY;
            $this->coordinates["{$sampleX},{$sampleY}"] = $targetValue;
        }
    }

    public function fillFromPoint(CoordinateInterface $start, $value, int $iterations = 3, ?callable $filter = null)
    {
        $points[(string) $start] = $start;

        for ($i = 0; $i < $iterations; $i++) {
            foreach ($points as $knownPoint) {
                foreach ($knownPoint->getNeighbours() as $neighbour) {
                    if (isset($filter) && $i > 2 && !$filter($neighbour)) {
                        continue;
                    }
                    $points[(string) $neighbour] = $neighbour;
                }
            }
        }
        //var_dump($knownPoint);

        foreach ($points as $point) {
            $this->coordinates[(string) $point] = $value;
            $this->updateMBR($point);
        }
    }

    public function markRange(CoordinateInterface $start, CoordinateInterface $end, $value = '#', $flagOverlaps = null)
    {
        $xs = [$start->getXPosition(), $end->getXPosition()];
        $ys = [$start->getYPosition(), $end->getYPosition()];

        for ($x = min($xs); $x <= max($xs); $x++) {
            for ($y = min($ys); $y <= max($ys); $y++) {
                if ($flagOverlaps !== null && isset($this->coordinates["{$x},{$y}"])) {
                    $this->coordinates["{$x},{$y}"] = $flagOverlaps;
                    continue;
                }
                $this->coordinates["{$x},{$y}"] = $value;
            }
        }
        $this->updateMBR($start);
        $this->updateMBR($end);
    }

    public function getRangeValues(CoordinateInterface $start, CoordinateInterface $end)
    {
        $values = [];
        $xs = [$start->getXPosition(), $end->getXPosition()];
        $ys = [$start->getYPosition(), $end->getYPosition()];

        for ($x = min($xs); $x <= max($xs); $x++) {
            for ($y = min($ys); $y <= max($ys); $y++) {
                if (!isset($this->coordinates["{$x},{$y}"])) {
                    continue;
                }
                $values[] = $this->coordinates["{$x},{$y}"];
            }
        }
        return $values;
    }

    public function toggleRange(CoordinateInterface $start, CoordinateInterface $end, $value1, $value2)
    {
        $xs = [$start->getXPosition(), $end->getXPosition()];
        $ys = [$start->getYPosition(), $end->getYPosition()];

        for ($x = min($xs); $x <= max($xs); $x++) {
            for ($y = min($ys); $y <= max($ys); $y++) {
                if (isset($this->coordinates["{$x},{$y}"])) {
                    $this->coordinates["{$x},{$y}"] = $this->coordinates["{$x},{$y}"] === $value1 ? $value2 : $value1;
                } else {
                    $this->coordinates["{$x},{$y}"] = $value1;
                }
            }
        }
        $this->updateMBR($start);
        $this->updateMBR($end);
    }

    public function incrementRange(CoordinateInterface $start, CoordinateInterface $end, $increment = 1)
    {
        $xs = [$start->getXPosition(), $end->getXPosition()];
        $ys = [$start->getYPosition(), $end->getYPosition()];

        for ($x = min($xs); $x <= max($xs); $x++) {
            for ($y = min($ys); $y <= max($ys); $y++) {
                if (isset($this->coordinates["{$x},{$y}"])) {
                    $this->coordinates["{$x},{$y}"] = $this->coordinates["{$x},{$y}"] + $increment;
                } else {
                    $this->coordinates["{$x},{$y}"] = $increment;
                }
            }
        }
        $this->updateMBR($start);
        $this->updateMBR($end);
    }

    public function incrementAlongPath(Path $path, $increment = 1)
    {
        foreach ($path->follow() as $coord) {
            if (isset($this->coordinates[(string) $coord])) {
                $this->coordinates[(string) $coord] = $this->coordinates[(string) $coord] + $increment;
            } else {
                $this->coordinates[(string) $coord] = $increment;
            }
        }

        $this->updateMBR($path->getFirstStep());
        $this->updateMBR($path->getLastStep());
    }

    public function decrementRange(CoordinateInterface $start, CoordinateInterface $end, $decrement = 1)
    {
        $xs = [$start->getXPosition(), $end->getXPosition()];
        $ys = [$start->getYPosition(), $end->getYPosition()];

        for ($x = min($xs); $x <= max($xs); $x++) {
            for ($y = min($ys); $y <= max($ys); $y++) {
                if (isset($this->coordinates["{$x},{$y}"])) {
                    $newValue = $this->coordinates["{$x},{$y}"] - $decrement;
                    $this->coordinates["{$x},{$y}"] = $newValue > 0 ? $newValue : 0;
                } else {
                    $this->coordinates["{$x},{$y}"] = 0;
                }
            }
        }
        $this->updateMBR($start);
        $this->updateMBR($end);
    }

    public function clearCoordinate(CoordinateInterface $coord, bool $remove = false)
    {
        $this->coordinates[(string) $coord] = $this->clearSpace;
        if ($remove) {
            unset($this->coordinates[(string) $coord]);
        }
        if (
            $coord->getXPosition() <= $this->bottomLeft->getXPosition() ||
            $coord->getXPosition() >= $this->topRight->getXPosition() ||
            $coord->getYPosition() <= $this->bottomLeft->getYPosition() ||
            $coord->getYPosition() >= $this->topRight->getYPosition()
        ) {
            $this->recalculateMBR();
        }
    }

    private function recalculateMBR()
    {
        $smallestX = $smallestY = $greatestX = $greatestY = null;
        foreach ($this->knownCoordinates() as $coord) {
            if ($smallestX === null) {
                $smallestX = $coord->getXPosition();
                $smallestY = $coord->getYPosition();
                $greatestX = $coord->getXPosition();
                $greatestY = $coord->getYPosition();
            }

            if ($coord->getXPosition() < $smallestX) {
                $smallestX = $coord->getXPosition();
            }
            if ($coord->getXPosition() > $greatestX) {
                $greatestX = $coord->getXPosition();
            }

            if ($coord->getYPosition() < $smallestY) {
                $smallestY = $coord->getYPosition();
            }
            if ($coord->getYPosition() > $greatestY) {
                $greatestY = $coord->getYPosition();
            }
        }

        $this->bottomLeft = new Coord($smallestX, $smallestY);
        $this->topRight = new Coord($greatestX, $greatestY);
    }

    public function knownCoordinates(): Generator
    {
        foreach (array_keys($this->coordinates) as $coord) {
            yield Coord::fromString($coord);
        }
    }

    public function plot($invert = false, $clearSpace = null): string
    {
        $plot = "";
        for ($y = $this->topRight->getYPosition(); $y >= $this->bottomLeft->getYPosition(); $y--) {
            $line = "";
            for ($x = $this->bottomLeft->getXPosition(); $x <= $this->topRight->getXPosition(); $x++) {
                $line .= $this->coordinates["{$x},{$y}"] ?? ($clearSpace ?? $this->clearSpace);
            }
            $plot .= $invert ? strrev("{$line}\n") : "{$line}\n";
        }
        return $invert ? strrev($plot) : $plot;
    }

    public function locate(callable $locationChecker)
    {
        return array_filter($this->coordinates, $locationChecker, ARRAY_FILTER_USE_BOTH);
    }

    public function locateMap(self $map): array
    {
        $found = [];
        foreach ($this->coordinates as $address => $value) {
            [$x, $y] = array_map('intval', explode(',', $address));
            foreach ($map->coordinates as $targetAddress => $targetValue) {
                [$tX, $tY] = array_map('intval', explode(',', $targetAddress));
                $sampleX = $x + $tX;
                $sampleY = $y + $tY;
                if (
                    !(
                        isset($this->coordinates["{$sampleX},{$sampleY}"]) &&
                        $this->coordinates["{$sampleX},{$sampleY}"] === $targetValue
                    )
                ) {
                    // If a sample value doesn't match the target it can't be what we are locating.
                    // Skip this starting coord.
                    continue 2;
                }
            }
            // Found All Target Points.
            $found[] = Coord::fromString($address);
        }

        return $found;
    }

    /**
     * @todo Merge this with the duplicate function below.
     */
    public function findRoute(
        CoordinateInterface $source,
        callable $isTarget,
        callable $neighbourEvaluation
    ): Route {
        if ($isTarget($this, $source)) {
            return new Route($source, new Path());
        }

        $found = [];
        $shortest = null;
        $initialPath = new Path($source);

        $possibleRoutes = [];
        // Get Locations Neighbours.
        $newNeighbours = array_filter(
            $source->getNeighbours(),
            function ($neighbour) use ($initialPath, $neighbourEvaluation, $source) {
                return !$initialPath->hasVisited($neighbour) && $neighbourEvaluation($this, $source, $neighbour);
            }
        );

        foreach ($newNeighbours as $neighbour) {
            $possibleRoutes[] = new Route($neighbour, $initialPath);
        }

        $coordinateLowestCost = [];

        while (!empty($possibleRoutes)) {
            /** @var Route $route */
            $route = array_shift($possibleRoutes);
            $location = $route->getTarget();
            $path = $route->getPath();

            // Found the target.
            if ($isTarget($this, $location)) {
                // Found Target
                $found[] = $route;
                if (is_null($shortest) || ($route->cost() < $shortest->cost())) {
                    $shortest = $route;
                }
                continue;
            }

            // If this route is the same or greater cost than the shortest then route from this can't be shortest.
            if (isset($shortest) && $route->cost() >= $shortest->cost()) {
                continue;
            }

            // Have I reached this coordinate at a cheaper cost on another route?
            if (
                isset($coordinateLowestCost[(string) $location])
                && $coordinateLowestCost[(string) $location] <= $route->cost()
            ) {
                continue;
            }
            $coordinateLowestCost[(string) $location] = $route->cost();

            $newNeighbours = array_filter(
                $location->getNeighbours(),
                function ($neighbour) use ($path, $neighbourEvaluation, $location) {
                    return !$path->hasVisited($neighbour) && $neighbourEvaluation($this, $location, $neighbour);
                }
            );

            foreach ($newNeighbours as $neighbour) {
                // Have I reached this coordinate at a cheaper cost on another route?
                if (
                    isset($coordinateLowestCost[(string) $neighbour])
                    && $coordinateLowestCost[(string) $neighbour] <= $route->cost()
                ) {
                    continue;
                }
                $possibleRoutes[] = new Route($neighbour, $path->addStep($location));
            }
        }

        if (empty($found)) {
            throw new Exception('Unreachable Location');
        }

        return $shortest;
    }

    public function findRoutePortals(
        CoordinateInterface $source,
        CoordinateInterface $target,
        array $portals = []
    ): Route {
        if ($source->isSame($target)) {
            return new Route($source, new Path());
        }

        $found = [];
        $shortest = null;
        if (isset($portals[(string) $source])) {
            $source = $portals[(string) $source];
        }
        $initialPath = new Path($source);

        $possibleRoutes = [];
        // Get Locations Neighbours.
        $newNeighbours = array_filter(
            $source->getNeighbours(),
            function ($neighbour) use ($initialPath) {
                return !$initialPath->hasVisited($neighbour) && $this->get($neighbour) === ".";
            }
        );
        foreach ($newNeighbours as $neighbour) {
            $possibleRoutes[] = new Route($neighbour, $initialPath);
        }

        while (!empty($possibleRoutes)) {
            /** @var Route $route */
            //$route = array_pop($possibleRoutes);
            $route = array_shift($possibleRoutes);
            $location = $route->getTarget();
            $path = $route->getPath();
            if (isset($portals[(string) $location])) {
                $location = $portals[(string) $location];
            }

            if ($location->isSame($target)) {
                // Found Target
                $found[] = $route;
                if (is_null($shortest) || ($route->cost() < $shortest->cost())) {
                    $shortest = $route;
                }
                continue;
            }

            // Clear Space
            $newNeighbours = array_filter(
                $location->getNeighbours(),
                function ($neighbour) use ($path) {
                    return !$path->hasVisited($neighbour) && $this->get($neighbour) === ".";
                }
            );

            // If this route is the same or greater cost than shortest then route from this can't be shortest.
            if (isset($shortest) && $route->cost() >= $shortest->cost()) {
                continue;
            }

            foreach ($newNeighbours as $neighbour) {
                $possibleRoutes[] = new Route($neighbour, $path->addStep($location));
            }
        }

        if (empty($found)) {
            throw new Exception('Unreachable Location');
        }

        return $shortest;
    }

    public function get(CoordinateInterface $coord)
    {
        return $this->coordinates[(string) $coord] ?? null;
    }

    public function getNeighbours(CoordinateInterface $coord): array
    {
        $neighbours = [];
        foreach ($coord->getNeighbours() as $neighbour) {
            if (isset($this->coordinates[(string) $neighbour])) {
                $neighbours[(string) $neighbour] = $this->coordinates[(string) $neighbour];
            }
        }
        return $neighbours;
    }

    public function getSurrounding(CoordinateInterface $coord): array
    {
        $neighbours = [];
        foreach ($coord->getSurrounding() as $neighbour) {
            if (isset($this->coordinates[(string) $neighbour])) {
                $neighbours[(string) $neighbour] = $this->coordinates[(string) $neighbour];
            }
        }
        return $neighbours;
    }

    public function explore(Coord $position): array
    {
        $visited = [];
        $visited[(string) $position] = $position;
        $found = [];
        $initialPath = new Path($position);

        $possibleLocations = [];
        foreach ($position->getNeighbours() as $neighbour) {
            $possibleLocations[] = new Route($neighbour, $initialPath);
        }

        while (!empty($possibleLocations)) {
            /** @var Route $route */
            $route = array_pop($possibleLocations);
            $location = $route->getTarget();
            $path = $route->getPath();
            $visited[] = $location;

            if ($this->coordinates[(string) $location] === $this->wall) {
                // Wall, Can't Visit
                continue;
            }
            if ($this->coordinates[(string) $location] !== $this->clearSpace) {
                // Found Treasure
                $found[] = $route;
                continue;
            }

            // Clear Space
            $newNeighbours = array_filter(
                $location->getNeighbours(),
                function ($neighbour) use ($visited) {
                    return !in_array($neighbour, $visited);
                }
            );
            foreach ($newNeighbours as $neighbour) {
                $possibleLocations[] = new Route($neighbour, $path->addStep($location));
            }
        }

        return $found;
    }

    public function leftEdgeMatches(self $onLeft): bool
    {
        $rightEdge = $this->getMBR()[1]->getXPosition();
        for ($y = $this->getMBR()[0]->getYPosition(); $y <= $this->getMBR()[1]->getYPosition(); $y++) {
            if ($onLeft->coordinates["{$rightEdge},{$y}"] !== $this->coordinates["0,{$y}"]) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return CoordinateInterface[]
     */
    public function getMBR(): array
    {
        return [$this->bottomLeft, $this->topRight];
    }

    public function topEdgeMatches(self $above): bool
    {
        $bottomEdge = $this->getMBR()[1]->getYPosition();
        for ($x = $this->getMBR()[0]->getXPosition(); $x <= $this->getMBR()[1]->getXPosition(); $x++) {
            if ($above->coordinates["{$x},{$bottomEdge}"] !== $this->coordinates["{$x},0"]) {
                return false;
            }
        }

        return true;
    }

    public function rotate90(): self
    {
        $rotated = new self();
        $newY = 0;
        for ($x = $this->getMBR()[0]->getXPosition(); $x <= $this->getMBR()[1]->getXPosition(); $x++) {
            $newX = 0;
            for ($y = $this->getMBR()[1]->getYPosition(); $y >= $this->getMBR()[0]->getYPosition(); $y--) {
                $rotated->coordinates["{$newX},{$newY}"] = $this->coordinates["{$x},{$y}"];
                $newX++;
            }
            $newY++;
        }
        foreach ($this->getMBR() as $coord) {
            $rotated->updateMBR($coord);
        }
        return $rotated;
    }

    public function rotate180(): self
    {
        return $this->flipX()->flipY();
    }

    public function flipY(): self
    {
        $flipped = new self();
        $xEdge = $this->getMBR()[1]->getYPosition();
        for ($y = $this->getMBR()[0]->getYPosition(); $y <= $this->getMBR()[1]->getYPosition(); $y++) {
            for ($x = $this->getMBR()[0]->getXPosition(); $x <= $this->getMBR()[1]->getXPosition(); $x++) {
                $newY = $xEdge - $y;
                $flipped->coordinates["{$x},{$newY}"] = $this->coordinates["{$x},{$y}"];
            }
        }
        foreach ($this->getMBR() as $coord) {
            $flipped->updateMBR($coord);
        }

        return $flipped;
    }

    public function flipX(): self
    {
        $flipped = new self();
        $yEdge = $this->getMBR()[1]->getXPosition();
        for ($y = $this->getMBR()[0]->getYPosition(); $y <= $this->getMBR()[1]->getYPosition(); $y++) {
            for ($x = $this->getMBR()[0]->getXPosition(); $x <= $this->getMBR()[1]->getXPosition(); $x++) {
                $newX = $yEdge - $x;
                $flipped->coordinates["{$newX},{$y}"] = $this->coordinates["{$x},{$y}"];
            }
        }
        foreach ($this->getMBR() as $coord) {
            $flipped->updateMBR($coord);
        }

        return $flipped;
    }

    public function scale(int $factor = 2)
    {
        $newCoordinates = [];

        foreach ($this->coordinates as $coord => $value) {
            $coord = Coord::fromString($coord);

            for ($xFactor = 0; $xFactor < $factor; $xFactor++) {
                for ($yFactor = 0; $yFactor < $factor; $yFactor++) {
                    $x = ($coord->getXPosition() * $factor) + $xFactor;
                    $y = ($coord->getYPosition() * $factor) + $yFactor;
                    $newCoordinates["{$x},{$y}"] = $value;

                    $x = ($coord->getXPosition() * $factor);
                    $y = ($coord->getYPosition() * $factor) + $yFactor;
                    $newCoordinates["{$x},{$y}"] = $value;

                    $x = ($coord->getXPosition() * $factor) + $xFactor;
                    $y = ($coord->getYPosition() * $factor) + $yFactor;
                    $newCoordinates["{$x},{$y}"] = $value;
                }
            }
        }

        $this->coordinates = $newCoordinates;
        $this->recalculateMBR();
    }

    public function fall(Coord $entry): ?Coord
    {
        $point = $entry;
        $possibleMoves = [
            $point->moveAlongBearing(Bearing::get(Bearing::SOUTH)),
            $point->moveAlongBearing(Bearing::get(Bearing::SOUTH_WEST)),
            $point->moveAlongBearing(Bearing::get(Bearing::SOUTH_EAST)),
        ];
        $lowestY = $this->getMBR()[1]->getYPosition();
        while ($point->getYPosition() < $lowestY+2) {
            // Try falling to possible positions.
            foreach ($possibleMoves as $possibleMove) {
                if ($this->get($possibleMove) === null) {
                    // Free space.
                    $point = $possibleMove;
                    $possibleMoves = [
                        $point->moveAlongBearing(Bearing::get(Bearing::SOUTH)),
                        $point->moveAlongBearing(Bearing::get(Bearing::SOUTH_WEST)),
                        $point->moveAlongBearing(Bearing::get(Bearing::SOUTH_EAST)),
                    ];
                    // Try the next spot.
                    continue 2;
                }
            }
            // Nowhere to fall.
            return $point;
        }

        // Nothing after limit.
        return null;
    }
}
