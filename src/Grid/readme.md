# Grid

Functionality for Grid coordinate systems.

Has grown to support multiple coordinate systems 2d, 3d, 4d. Generic Coordinate interface was added, although some
places may still expect as 2d coordinate.

## ToDo

Seperate some of the logic. The Map class has grown and handles a lot of logic that could be seperated.

Ideas for names for class responsibility.

* cartographer (a person who draws or produces maps)
* charterer (a person or organization that charters a ship or aircraft)
* explorer (a person who explores a new or unfamiliar area)
