<?php

declare(strict_types=1);

namespace AdventOfCode;

use AdventOfCode\Answer;
use AdventOfCode\Answer\Factory;
use AdventOfCode\Solution\EmptySolution;
use AdventOfCode\Solution\Legacy;
use DateTime;
use DateTimeZone;
use Exception;
use Psr\Http\Message\ResponseInterface;
use SebastianBergmann\Timer\Timer;
use Twig\Environment;

class SolutionController
{
    public function __construct(private int $leaderboardId, private Factory $answerFactory)
    {
    }

    public function solution(
        int $year,
        int $day,
        string $input,
        Environment $template,
        LeaderBoard\Individual\IndividualLeaderboardRepository $userLeague,
        Answer\ProcessorInterface $resultProcessor,
        ResponseInterface $response
    ): ResponseInterface {
        $file = BASE_DIR . "/public/{$year}/Day{$day}/index.php";
        $className = "\AdventOfCode\Solution\Year{$year}\Day{$day}";
        if (file_exists($file)) {
            $solution = new Legacy($file);
        } elseif (class_exists($className)) {
            $solution = new $className($this->answerFactory);
        } else {
            $solution = new EmptySolution($year, $day);
        }

        $timer = new Timer();
        $timer->start();

        ob_start();
        try {
            $answer = $solution->execute($input);
            $output = ob_get_contents();
        } catch (Exception $e) {
            throw $e;
        }
        ob_end_clean();
        $time = $timer->stop();

        $processorResults = new Answer\Processor\ResultStack();
        if ($answer !== null) {
            $processorResults = $resultProcessor->process($answer, $year, $day);
        }

        // Advent of Code Always Starts midnight in EST.
        $now = new DateTime('now', new DateTimeZone('America/New_York'));
        $nextYearAllowed = ($year < (int) $now->format('Y'));
        $nextDayAllowed = $nextYearAllowed || ((int) $now->format('d')) > $day;
        $nextYearAllowedDay = !(($year + 1 === (int) $now->format('Y')) && $day > ((int) $now->format('d')));

        $response->getBody()->write(
            $template->render(
                'solution.html',
                [
                    'day'              => $day,
                    'year'             => $year,
                    'noSolution'       => get_class($solution) === EmptySolution::class,
                    'previousYear'     => $year > 2015 ? "/" . ($year - 1) . "/{$day}" : null,
                    'nextYear'         => ($nextYearAllowed && $nextYearAllowedDay) ? "/" . ($year + 1) . "/{$day}"
                        : null,
                    'previousDay'      => $day > 1 ? "/{$year}/" . ($day - 1) : null,
                    'nextday'          => $nextDayAllowed ? "/{$year}/" . ($day + 1) : null,
                    'output'           => $output . "\n\n>",
                    'processorResults' => $processorResults,
                    'time'             => $time->asString(),
                    'leaderboardId'    => $this->leaderboardId,
                    'userLeague'       => $userLeague,
                ]
            )
        );

        return $response;
    }

    public function generate(int $year, int $day, ResponseInterface $response)
    {
        // Solution Class.
        $file = BASE_DIR . "/src/Solution/Year{$year}/Day{$day}.php";
        $template = BASE_DIR . "/resources/Solution.template";
        $content = str_replace(['{DAY}', '{YEAR}'], [$day, $year], file_get_contents($template));
        $this->writeFile($file, $content);

        // Test.
        $file = BASE_DIR . "/tests/Solution/Year{$year}/Day{$day}Test.php";
        $template = BASE_DIR . "/resources/Test.template";
        $content = str_replace(['{DAY}', '{YEAR}'], [$day, $year], file_get_contents($template));
        $this->writeFile($file, $content);

        return $response->withStatus(302)->withHeader('Location', "/{$year}/{$day}");
    }

    private function writeFile(string $filename, $content)
    {
        if (file_exists($filename)) {
            // Already exists.
            return;
        }
        if (!is_dir(dirname($filename))) {
            // If directory doesn't exist.
            mkdir(dirname($filename), 0700, true);
        }

        file_put_contents($filename, $content);
    }
}
