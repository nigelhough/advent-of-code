<?php

declare(strict_types=1);

namespace AdventOfCode\Arrays;

use Generator;

class Chunks
{
    /**
     * Progressively chunk an array by a chunk size.
     *
     * @param array $array     The array to chunk.
     * @param int   $chunkSize The size to return chunks for.
     *
     * @return Generator
     */
    public static function progressiveChunk(array $array, int $chunkSize = 1): Generator
    {
        $chunk = [];
        foreach ($array as $value) {
            if (count($chunk) < $chunkSize) {
                $chunk[] = $value;
                continue;
            }
            yield $chunk;

            // Reset new collection.
            array_shift($chunk);
            $chunk[] = $value;
        }
        yield $chunk;
    }
}
