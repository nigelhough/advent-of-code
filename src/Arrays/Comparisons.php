<?php

declare(strict_types=1);

namespace AdventOfCode\Arrays;

use Generator;

class Comparisons
{
    /**
     * Yields a value and it's previous value in the array.
     * Based on the array ordering.
     *
     * @param iterable $array An array to compare values within.
     *
     * @return Generator
     */
    public static function previousValue(iterable $array): Generator
    {
        $previous = null;
        foreach ($array as $value) {
            if ($previous === null) {
                $previous = $value;
                continue;
            }
            yield [$value, $previous];

            $previous = $value;
        }
    }
}
