<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

class ScoreFactory
{
    public const SCORE_POSITION = 2;

    /**
     * @return ScoreInterface[]
     */
    public function buildScores(array $scoreDetails): array
    {
        $scores = [];
        foreach ($scoreDetails as $tile) {
            $scores[] = $this->buildScore($tile);
        }
        return $scores;
    }

    public function buildScore(array $scoreDetails): ScoreInterface
    {
        return new Score($scoreDetails[self::SCORE_POSITION]);
    }
}
