<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

class Score implements ScoreInterface
{
    private $score;

    public function __construct(int $score)
    {
        $this->score = $score;
    }

    public function getScore(): int
    {
        return $this->score;
    }
}
