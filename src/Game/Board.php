<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

class Board
{
    /** @var TileInterface[][] */
    private $position;

    /**
     * @param TileInterface[] $tiles
     */
    public function __construct(array $tiles)
    {
        $this->updatePositions($tiles);
    }

    /**
     * @param TileInterface[] $tiles
     */
    public function updatePositions(array $tiles)
    {
        foreach ($tiles as $tile) {
            $this->position[$tile->getXPosition()][$tile->getYPosition()] = $tile;
        }
    }

    public function render()
    {
        $height = count($this->position[0]);
        $width = count($this->position);

        if (php_sapi_name() == "cli") {
            system('clear');
        }

        for ($h = 0; $h < $height; $h++) {
            for ($w = 0; $w < $width; $w++) {
                echo $this->getCoord($w, $h);
            }
            echo "\n";
        }
    }

    private function getCoord($x, $y)
    {
        if (isset($this->position[$x][$y])) {
            return ($this->position[$x][$y])->render();
        }
        return ".";
    }
}
