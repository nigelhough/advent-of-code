<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

interface ScoreInterface
{
    public function getScore(): int;
}
