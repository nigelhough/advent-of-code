<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

class InstructionSorter
{
    public const INSTRUCTION_LENGTH = 3;

    private $tileFactory;

    private $scoreFactory;

    public function __construct(TileFactory $tileFactory, ScoreFactory $scoreFactory)
    {
        $this->tileFactory = $tileFactory;
        $this->scoreFactory = $scoreFactory;
    }

    /**
     * @return TileInterface[]
     */
    public function getTiles(array $instructions): array
    {
        $tileDetails = array_filter(array_chunk($instructions, self::INSTRUCTION_LENGTH), [$this, 'isTileInstruction']);
        return $this->tileFactory->buildTiles($tileDetails);
    }

    public function getScores(array $instructions): array
    {
        $scoreDetails = array_filter(
            array_chunk(
                $instructions,
                self::INSTRUCTION_LENGTH
            ),
            [$this, 'isScoreInstruction']
        );
        return $this->scoreFactory->buildScores($scoreDetails);
    }

    private function isTileInstruction(array $instuction)
    {
        return !$this->isScoreInstruction($instuction);
    }

    private function isScoreInstruction(array $instuction)
    {
        return $instuction[0] === -1 && $instuction[1] === 0;
    }
}
