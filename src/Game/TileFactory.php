<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

use AdventOfCode\Game\Tile\Ball;
use AdventOfCode\Game\Tile\Block;
use AdventOfCode\Game\Tile\EmptyTile;
use AdventOfCode\Game\Tile\Paddle;
use AdventOfCode\Game\Tile\Wall;
use Exception;

class TileFactory
{
    public const X_POSITION = 0;

    public const Y_POSITION = 1;

    public const ID = 2;

    /**
     * @return TileInterface[]
     */
    public function buildTiles(array $tileDetails): array
    {
        $tiles = [];
        foreach ($tileDetails as $tile) {
            $tiles[] = $this->buildTile($tile);
        }
        return $tiles;
    }

    public function buildTile(array $tile): TileInterface
    {
        $xPosition = $tile[self::X_POSITION];
        $yPosition = $tile[self::Y_POSITION];
        $tileId = $tile[self::ID];

        switch ($tileId) {
            case EmptyTile::TILE_ID:
                return new EmptyTile($xPosition, $yPosition);
            case Wall::TILE_ID:
                return new Wall($xPosition, $yPosition);
            case Block::TILE_ID:
                return new Block($xPosition, $yPosition);
            case Paddle::TILE_ID:
                return new Paddle($xPosition, $yPosition);
            case Ball::TILE_ID:
                return new Ball($xPosition, $yPosition);
            default:
                throw new Exception("Invalid Tile Identifier {$tileId}");
        }
    }
}
