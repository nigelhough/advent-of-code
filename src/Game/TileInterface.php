<?php

declare(strict_types=1);

namespace AdventOfCode\Game;

interface TileInterface
{
    public function getId(): int;

    public function getXPosition(): int;

    public function getYPosition(): int;

    public function render(): string;
}
