<?php

declare(strict_types=1);

namespace AdventOfCode\Game\Tile;

class Ball extends AbstractTile
{
    public const TILE_ID = 4;

    public function getId(): int
    {
        return self::TILE_ID;
    }

    public function render(): string
    {
        return 'O';
    }
}
