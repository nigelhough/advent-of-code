<?php

declare(strict_types=1);

namespace AdventOfCode\Game\Tile;

use AdventOfCode\Game\TileInterface;

abstract class AbstractTile implements TileInterface
{
    public function __construct(private int $xPosition, private int $yPosition)
    {
    }

    public function getXPosition(): int
    {
        return $this->xPosition;
    }

    public function getYPosition(): int
    {
        return $this->yPosition;
    }
}
