<?php

declare(strict_types=1);

namespace AdventOfCode\Game\Tile;

class EmptyTile extends AbstractTile
{
    public const TILE_ID = 0;

    public function getId(): int
    {
        return self::TILE_ID;
    }

    public function render(): string
    {
        if (php_sapi_name() == "cli") {
            return " ";
        }
        return '&nbsp;';
    }
}
