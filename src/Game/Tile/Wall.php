<?php

declare(strict_types=1);

namespace AdventOfCode\Game\Tile;

class Wall extends AbstractTile
{
    public const TILE_ID = 1;

    public function getId(): int
    {
        return self::TILE_ID;
    }

    public function render(): string
    {
        return '#';
    }
}
