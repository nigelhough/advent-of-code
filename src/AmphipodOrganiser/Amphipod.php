<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class Amphipod
{
    public function __construct(public readonly int $id, public readonly string $type, public readonly int $cost)
    {
    }

    public function getName(): string
    {
        return "{$this->id}{$this->type}";
    }

    public function getLocation(GameState $gameState): Node
    {
        return $gameState->getPositions()[$this->id];
    }

    public function isWinningPosition(GameState $gameState): bool
    {
        $currentLocation = $this->getLocation($gameState);
        $isWinningPosition = false;
        if ($currentLocation->tunnel == $this->type) {
            $isWinningPosition = true;
            // I am on a matching type.
            foreach ($currentLocation->getRequiresMatching() as $required) {
                if ($required->getOccupant($gameState)->type !== $this->type) {
                    $isWinningPosition = false;
                    break;
                }
            }
        }
        return $isWinningPosition;
    }

    public function getPossibleMoves(GameState $gameState): array
    {
        $moves = [];
        $checked = [];

        if ($this->isWinningPosition($gameState)) {
            return [];
        }

        $possibleMoves = array_map(
            fn(Connection $c) => new MoveDetails($this->id, $c->node->id, $c->steps),
            $this->getLocation($gameState)->getConnections()
        );
        while ($possibleMoves) {
            $possibleMove = array_pop($possibleMoves);
            $possibleNodeId = $possibleMove->nodeId;
            $possibleSteps = $possibleMove->steps;
            $possibleNode = $gameState->nodeList[$possibleNodeId];

            if (in_array($possibleNodeId, $checked)) {
                continue;
            }
            $checked[] = $possibleNodeId;
            if ($possibleNode->getOccupant($gameState)) {
                continue;
            }

            $canStay = true;
            if ($this->getLocation($gameState)->isCorridor && $possibleNode->isCorridor) {
                // Can't move between spaces within the corridor.
                $canStay = false;
            }
            if (
                $this->getLocation($gameState)->tunnel !== null &&
                $possibleNode->tunnel !== null &&
                $this->getLocation($gameState)->tunnel == $possibleNode->tunnel
            ) {
                // Can't move from tunnel to same tunnel.
                $canStay = false;
            }
            if (
                $possibleNode->tunnel !== null &&
                $possibleNode->tunnel != $this->type
            ) {
                // Can't stay in a tunnel that isn't my type.
                $canStay = false;
            }
            foreach ($possibleNode->getRequiresMatching() as $nodeRequiringMatch) {
                if (!$nodeRequiringMatch->getOccupant($gameState) || $nodeRequiringMatch->getOccupant(
                        $gameState
                    )->type != $this->type) {
                    // Not occupied or Mismatch.
                    $canStay = false;
                    break;
                }
            }
            if ($canStay) {
                $moves[] = new MoveDetails($this->id, $possibleNodeId, $possibleSteps);
            }
            foreach ($possibleNode->getConnections() as $connection) {
                if ($connection->node === $this->getLocation($gameState)) {
                    continue;
                }
                if (
                    in_array($connection->node->id, $checked) ||
                    in_array($connection->node->id, array_map(fn(MoveDetails $m) => $m->nodeId, $possibleMoves)) ||
                    in_array($connection->node->id, array_map(fn(MoveDetails $m) => $m->nodeId, $moves))
                ) {
                    continue;
                }
                $possibleMoves[] = new MoveDetails(
                    $this->id,
                    $connection->node->id,
                    $connection->steps + $possibleSteps
                );
            }
        }
        return $moves;
    }
}
