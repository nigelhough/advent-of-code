<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class NodeRelationship
{
    public function __construct(
        public readonly Node $node1,
        public readonly Node $node2,
        public readonly int $steps,
        public readonly array $moveThrough,
        public readonly array $requiresMatching
    ) {
    }
}
