<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class Connection
{
    public function __construct(public readonly Node $node, public readonly int $steps)
    {
    }
}
