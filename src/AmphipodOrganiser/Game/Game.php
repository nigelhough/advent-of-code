<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser\Game;

use AdventOfCode\AmphipodOrganiser\Amphipod;
use AdventOfCode\AmphipodOrganiser\Connection;
use AdventOfCode\AmphipodOrganiser\GameInterface;
use AdventOfCode\AmphipodOrganiser\GameState;
use AdventOfCode\AmphipodOrganiser\Node;

class Game implements GameInterface
{
    public function getAmphipods(): array
    {
        return [
            1 => new Amphipod(1, 'A', 1),
            2 => new Amphipod(2, 'A', 1),
            3 => new Amphipod(3, 'A', 1),
            4 => new Amphipod(4, 'A', 1),
            5 => new Amphipod(5, 'B', 10),
            6 => new Amphipod(6, 'B', 10),
            7 => new Amphipod(7, 'B', 10),
            8 => new Amphipod(8, 'B', 10),
            9 => new Amphipod(9, 'C', 100),
            10 => new Amphipod(10, 'C', 100),
            11 => new Amphipod(11, 'C', 100),
            12 => new Amphipod(12, 'C', 100),
            13 => new Amphipod(13, 'D', 1000),
            14 => new Amphipod(14, 'D', 1000),
            15 => new Amphipod(15, 'D', 1000),
            16 => new Amphipod(16, 'D', 1000),
        ];
    }

    public function getNodes(): array
    {
        // Corridor
        $nodes = [
            1 => $node1 = new Node(1, [], true),
            2 => $node2 = new Node(2, [], true),
            3 => $node3 = new Node(3, [], true),
            4 => $node4 = new Node(4, [], true),
            5 => $node5 = new Node(5, [], true),
            6 => $node6 = new Node(6, [], true),
            7 => $node7 = new Node(7, [], true),
            // A Burrow
            11 => $node11 = new Node(11, [], tunnel: 'A'),
            10 => $node10 = new Node(10, [$node11], tunnel: 'A'),
            9 => $node9 = new Node(9, [$node11, $node10], tunnel: 'A'),
            8 => $node8 = new Node(8, [$node11, $node10, $node9], tunnel: 'A'),
            // B Burrow
            15 => $node15 = new Node(15, [], tunnel: 'B'),
            14 => $node14 = new Node(14, [$node15], tunnel: 'B'),
            13 => $node13 = new Node(13, [$node15, $node14], tunnel: 'B'),
            12 => $node12 = new Node(12, [$node15, $node14, $node13], tunnel: 'B'),
            // C Burrow
            19 => $node19 = new Node(19, [], tunnel: 'C'),
            18 => $node18 = new Node(18, [$node19], tunnel: 'C'),
            17 => $node17 = new Node(17, [$node19, $node18], tunnel: 'C'),
            16 => $node16 = new Node(16, [$node19, $node18, $node17], tunnel: 'C'),
            // D Burrow
            23 => $node23 = new Node(23, [], tunnel: 'D'),
            22 => $node22 = new Node(22, [$node23], tunnel: 'D'),
            21 => $node21 = new Node(21, [$node23, $node22], tunnel: 'D'),
            20 => $node20 = new Node(20, [$node23, $node22, $node21], tunnel: 'D'),
        ];

        // Connections
        // Corridor
        $node1->setConnections([new Connection($node2, 1)]);
        $node2->setConnections([new Connection($node1, 1), new Connection($node3, 2), new Connection($node8, 2)]);
        $node3->setConnections(
            [
                new Connection($node2, 2),
                new Connection($node8, 2),
                new Connection($node12, 2),
                new Connection($node4, 2),
            ]
        );
        $node4->setConnections(
            [
                new Connection($node3, 2),
                new Connection($node12, 2),
                new Connection($node16, 2),
                new Connection($node5, 2),
            ]
        );
        $node5->setConnections(
            [
                new Connection($node4, 2),
                new Connection($node16, 2),
                new Connection($node20, 2),
                new Connection($node6, 2),
            ]
        );
        $node6->setConnections([new Connection($node5, 2), new Connection($node20, 2), new Connection($node7, 1)]);
        $node7->setConnections([new Connection($node6, 1)]);
        // A Burrow
        $node8->setConnections([new Connection($node2, 2), new Connection($node3, 2), new Connection($node9, 1)]);
        $node9->setConnections([new Connection($node8, 1), new Connection($node10, 1)]);
        $node10->setConnections([new Connection($node9, 1), new Connection($node11, 1)]);
        $node11->setConnections([new Connection($node10, 1)]);
        // B Burrow
        $node12->setConnections([new Connection($node3, 2), new Connection($node4, 2), new Connection($node13, 1)]);
        $node13->setConnections([new Connection($node12, 1), new Connection($node14, 1)]);
        $node14->setConnections([new Connection($node13, 1), new Connection($node15, 1)]);
        $node15->setConnections([new Connection($node14, 1)]);
        // C Burrow
        $node16->setConnections([new Connection($node4, 2), new Connection($node5, 2), new Connection($node17, 1)]);
        $node17->setConnections([new Connection($node16, 1), new Connection($node18, 1)]);
        $node18->setConnections([new Connection($node17, 1), new Connection($node19, 1)]);
        $node19->setConnections([new Connection($node18, 1)]);
        // D Burrow
        $node20->setConnections([new Connection($node5, 2), new Connection($node6, 2), new Connection($node21, 1)]);
        $node21->setConnections([new Connection($node20, 1), new Connection($node22, 1)]);
        $node22->setConnections([new Connection($node21, 1), new Connection($node23, 1)]);
        $node23->setConnections([new Connection($node22, 1)]);

        return $nodes;
    }

    public function startingState(): GameState
    {
        $amphipods = $this->getAmphipods();
        $nodes = $this->getNodes();

        return new GameState(
            [
                // A Burrow
                $amphipods[1]->id => $nodes[16],
                $amphipods[2]->id => $nodes[21],
                $amphipods[3]->id => $nodes[18],
                $amphipods[4]->id => $nodes[19],
                // B Burrow
                $amphipods[5]->id => $nodes[12],
                $amphipods[6]->id => $nodes[17],
                $amphipods[7]->id => $nodes[14],
                $amphipods[8]->id => $nodes[11],
                // C Burrow
                $amphipods[9]->id => $nodes[20],
                $amphipods[10]->id => $nodes[13],
                $amphipods[11]->id => $nodes[22],
                $amphipods[12]->id => $nodes[23],
                // D Burrow
                $amphipods[13]->id => $nodes[8],
                $amphipods[14]->id => $nodes[9],
                $amphipods[15]->id => $nodes[10],
                $amphipods[16]->id => $nodes[15],
            ],
            $nodes,
            $amphipods
        );
    }
}
