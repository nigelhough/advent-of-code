<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser\Game;

use AdventOfCode\AmphipodOrganiser\Amphipod;
use AdventOfCode\AmphipodOrganiser\Connection;
use AdventOfCode\AmphipodOrganiser\GameInterface;
use AdventOfCode\AmphipodOrganiser\GameState;
use AdventOfCode\AmphipodOrganiser\Node;

class SimpleGame implements GameInterface
{
    public function getAmphipods(): array
    {
        return [
            1 => new Amphipod(1, 'A', 1),
            2 => new Amphipod(2, 'A', 1),
            3 => new Amphipod(3, 'B', 10),
            4 => new Amphipod(4, 'B', 10),
            5 => new Amphipod(5, 'C', 100),
            6 => new Amphipod(6, 'C', 100),
            7 => new Amphipod(7, 'D', 1000),
            8 => new Amphipod(8, 'D', 1000),
        ];
    }

    public function getNodes(): array
    {
        // Corridor
        $nodes = [
            1 => $node1 = new Node(1, [], true),
            2 => $node2 = new Node(2, [], true),
            3 => $node3 = new Node(3, [], true),
            4 => $node4 = new Node(4, [], true),
            5 => $node5 = new Node(5, [], true),
            6 => $node6 = new Node(6, [], true),
            7 => $node7 = new Node(7, [], true),
            // A Burrow
            9 => $node9 = new Node(9, [], tunnel: 'A'),
            8 => $node8 = new Node(8, [$node9], tunnel: 'A'),
            // B Burrow
            11 => $node11 = new Node(11, [], tunnel: 'B'),
            10 => $node10 = new Node(10, [$node11], tunnel: 'B'),
            // C Burrow
            13 => $node13 = new Node(13, [], tunnel: 'C'),
            12 => $node12 = new Node(12, [$node13], tunnel: 'C'),
            // D Burrow
            15 => $node15 = new Node(15, [], tunnel: 'D'),
            14 => $node14 = new Node(14, [$node15], tunnel: 'D'),
        ];

        // Connections
        // Corridor
        $node1->setConnections([new Connection($node2, 1)]);
        $node2->setConnections([new Connection($node1, 1), new Connection($node3, 2), new Connection($node8, 2)]);
        $node3->setConnections(
            [
                new Connection($node2, 2),
                new Connection($node8, 2),
                new Connection($node10, 2),
                new Connection($node4, 2),
            ]
        );
        $node4->setConnections(
            [
                new Connection($node3, 2),
                new Connection($node10, 2),
                new Connection($node12, 2),
                new Connection($node5, 2),
            ]
        );
        $node5->setConnections(
            [
                new Connection($node4, 2),
                new Connection($node12, 2),
                new Connection($node14, 2),
                new Connection($node6, 2),
            ]
        );
        $node6->setConnections([new Connection($node5, 2), new Connection($node14, 2), new Connection($node7, 1)]);
        $node7->setConnections([new Connection($node6, 1)]);
        // A Burrow
        $node8->setConnections([new Connection($node2, 2), new Connection($node3, 2), new Connection($node9, 1)]);
        $node9->setConnections([new Connection($node8, 1)]);
        // B Burrow
        $node10->setConnections([new Connection($node3, 2), new Connection($node4, 2), new Connection($node11, 1)]);
        $node11->setConnections([new Connection($node10, 1)]);
        // C Burrow
        $node12->setConnections([new Connection($node4, 2), new Connection($node5, 2), new Connection($node13, 1)]);
        $node13->setConnections([new Connection($node12, 1)]);
        // D Burrow
        $node14->setConnections([new Connection($node5, 2), new Connection($node6, 2), new Connection($node15, 1)]);
        $node15->setConnections([new Connection($node14, 1)]);

        return $nodes;
    }

    public function startingState(): GameState
    {
        $amphipods = $this->getAmphipods();
        $nodes = $this->getNodes();
        return new GameState(
            [
                // A Burrow
                $amphipods[1]->id => $nodes[12],
                $amphipods[2]->id => $nodes[13],
                // B Burrow
                $amphipods[3]->id => $nodes[10],
                $amphipods[4]->id => $nodes[9],
                // C Burrow
                $amphipods[5]->id => $nodes[14],
                $amphipods[6]->id => $nodes[15],
                // D Burrow
                $amphipods[7]->id => $nodes[8],
                $amphipods[8]->id => $nodes[11],
            ],
            $nodes,
            $amphipods
        );
    }
}
