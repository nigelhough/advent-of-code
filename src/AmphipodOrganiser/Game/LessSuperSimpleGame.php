<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser\Game;

use AdventOfCode\AmphipodOrganiser\Amphipod;
use AdventOfCode\AmphipodOrganiser\Connection;
use AdventOfCode\AmphipodOrganiser\GameInterface;
use AdventOfCode\AmphipodOrganiser\GameState;
use AdventOfCode\AmphipodOrganiser\Node;

class LessSuperSimpleGame implements GameInterface
{
    public function getAmphipods(): array
    {
        return [
            1 => new Amphipod(1, 'A', 1),
            2 => new Amphipod(2, 'B', 10),
            3 => new Amphipod(3, 'C', 100),
            4 => new Amphipod(4, 'D', 1000),
            5 => new Amphipod(5, 'D', 1000),
        ];
    }

    public function getNodes(): array
    {
        // Corridor
        $nodes = [
            1 => $node1 = new Node(1, [], true),
            2 => $node2 = new Node(2, [], true),
            3 => $node3 = new Node(3, [], true),
            4 => $node4 = new Node(4, [], true),
            5 => $node5 = new Node(5, [], true),
            6 => $node6 = new Node(6, [], true),
            7 => $node7 = new Node(7, [], true),
            // A Burrow
            8 => $node8 = new Node(8, [], tunnel: 'A'),
            // B Burrow
            9 => $node9 = new Node(9, [], tunnel: 'B'),
            // C Burrow
            10 => $node10 = new Node(10, [], tunnel: 'C'),
            // D Burrow
            11 => $node11 = new Node(11, [], tunnel: 'D'),
            12 => $node12 = new Node(12, [], tunnel: 'D'),
        ];

        // Connections
        // Corridor
        $node1->setConnections([new Connection($node2, 1)]);
        $node2->setConnections([new Connection($node1, 1), new Connection($node3, 2), new Connection($node8, 2)]);
        $node3->setConnections(
            [
                new Connection($node2, 2),
                new Connection($node8, 2),
                new Connection($node9, 2),
                new Connection($node4, 2),
            ]
        );
        $node4->setConnections(
            [
                new Connection($node3, 2),
                new Connection($node9, 2),
                new Connection($node10, 2),
                new Connection($node5, 2),
            ]
        );
        $node5->setConnections(
            [
                new Connection($node4, 2),
                new Connection($node10, 2),
                new Connection($node11, 2),
                new Connection($node6, 2),
            ]
        );
        $node6->setConnections([new Connection($node5, 2), new Connection($node11, 2), new Connection($node7, 1)]);
        $node7->setConnections([new Connection($node6, 1)]);
        // A Burrow
        $node8->setConnections([new Connection($node2, 2), new Connection($node3, 2)]);
        // B Burrow
        $node9->setConnections([new Connection($node3, 2), new Connection($node4, 2)]);
        // C Burrow
        $node10->setConnections([new Connection($node4, 2), new Connection($node5, 2)]);
        // D Burrow
        $node11->setConnections([new Connection($node5, 2), new Connection($node6, 2), new Connection($node12, 1)]);
        $node12->setConnections([new Connection($node11, 1)]);

        return $nodes;
    }

    public function startingState(): GameState
    {
        $amphipods = $this->getAmphipods();
        $nodes = $this->getNodes();
        return new GameState(
            [
                // A Burrow
                $amphipods[3]->id => $nodes[12],
                // B Burrow
                $amphipods[2]->id => $nodes[9],
                // C Burrow
                $amphipods[5]->id => $nodes[11],
                // D Burrow
                $amphipods[1]->id => $nodes[10],
                $amphipods[4]->id => $nodes[8],
            ],
            $nodes,
            $amphipods
        );
    }
}
