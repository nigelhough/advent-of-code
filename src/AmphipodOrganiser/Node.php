<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class Node
{
    private array $connections;

    public function __construct(
        public readonly int $id,
        private array $requiresMatching,
        public readonly bool $isCorridor = false,
        public readonly ?string $tunnel = null
    ) {
    }

    /**
     * @return Connection[]
     */
    public function getConnections(): array
    {
        return $this->connections;
    }

    /**
     * @return Node[]
     */
    public function getRequiresMatching(): array
    {
        return $this->requiresMatching;
    }

    public function setConnections(array $connections): void
    {
        if (!empty($this->connections)) {
            throw new \Exception();
        }
        $this->connections = $connections;
    }

    public function getOccupant(GameState $gameState): ?Amphipod
    {
        foreach ($gameState->getPositions() as $key => $position) {
            if ($position->id === $this->id) {
                return $gameState->amphipodList[$key];
            }
        }
        return null;
    }
}
