<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class GameState implements \Stringable
{
    /**
     * @param int $score
     * @param Node[] $positions
     * @param Node[] $nodeList
     * @param Amphipod[] $amphipodList
     */
    public function __construct(
        private array $positions,
        public array &$nodeList,
        public array &$amphipodList,
        private int $score = 0
    ) {
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function playMove(Move $move): void
    {
        $this->positions[$move->moveDetails->amphipodId] = $this->nodeList[$move->moveDetails->nodeId];
        $this->score += ($move->moveDetails->steps * $this->amphipodList[$move->moveDetails->amphipodId]->cost);
    }

    public function isWinningState(): bool
    {
        if (count($this->amphipodList) == 4) {
            return str_contains(
                (string) $this,
                '###A#B#C#D###'
            );
        }

        if (count($this->amphipodList) == 5) {
            return str_contains(
                (string) $this,
                '###A#B#C#D###
#########D###'
            );
        }

        if (count($this->amphipodList) == 8) {
            return str_contains(
                (string) $this,
                '###A#B#C#D###
  #A#B#C#D#'
            );
        }


        return str_contains(
            (string) $this,
            '###A#B#C#D###
  #A#B#C#D#
  #A#B#C#D#
  #A#B#C#D#'
        );
    }

    /**
     * @return Node[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @return Move[]
     */
    public function getViableMoves(): array
    {
        $moves = [];
        foreach ($this->amphipodList as $amphipod) {
            $possibleMoves = array_filter($amphipod->getPossibleMoves($this));
            if (empty($possibleMoves)) {
                continue;
            }
            foreach ($possibleMoves as $possibleMove) {
                $moves[] = new Move($possibleMove, $this);
            }
        }
        return $moves;
    }

    public function __toString(): string
    {
        if (count($this->amphipodList) == 4) {
            $template = "#############
#|1||2|.|3|.|4|.|5|.|6||7|#
###|8|#|9|#|10|#|11|###
  #########";
        } elseif (count($this->amphipodList) == 5) {
            $template = "#############
#|1||2|.|3|.|4|.|5|.|6||7|#
###|8|#|9|#|10|#|11|###
#########|12|###
  #########";
        } elseif (count($this->amphipodList) == 8) {
            $template = "#############
#|1||2|.|3|.|4|.|5|.|6||7|#
###|8|#|10|#|12|#|14|###
  #|9|#|11|#|13|#|15|#
  #########";
        } else {
            $template = "#############
#|1||2|.|3|.|4|.|5|.|6||7|#
###|8|#|12|#|16|#|20|###
  #|9|#|13|#|17|#|21|#
  #|10|#|14|#|18|#|22|#
  #|11|#|15|#|19|#|23|#
  #########";
        }

        foreach ($this->amphipodList as $amphipod) {
            $template = str_replace('|' . $amphipod->getLocation($this)->id . '|', $amphipod->type, $template);
        }

        $template = str_replace([
            '|1|',
            '|2|',
            '|3|',
            '|4|',
            '|5|',
            '|6|',
            '|7|',
            '|8|',
            '|9|',
            '|10|',
            '|11|',
            '|12|',
            '|13|',
            '|14|',
            '|15|',
            '|16|',
            '|17|',
            '|18|',
            '|19|',
            '|20|',
            '|21|',
            '|22|',
            '|23|',
        ], '.', $template);

        return $template;
    }
}
