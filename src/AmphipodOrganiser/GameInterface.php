<?php

namespace AdventOfCode\AmphipodOrganiser;

interface GameInterface
{
    /**
     * @return Amphipod[]
     */
    public function getAmphipods(): array;

    public function getNodes(): array;

    public function startingState(): GameState;
}
