<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class Move implements \Stringable
{
    public function __construct(public readonly MoveDetails $moveDetails, private GameState $gameState)
    {
    }

    public function getGameState(): GameState
    {
        return $this->gameState;
    }

    public function __toString()
    {
        return (string) $this->moveDetails;
    }
}
