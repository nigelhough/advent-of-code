<?php

declare(strict_types=1);

namespace AdventOfCode\AmphipodOrganiser;

class MoveDetails implements \Stringable
{
    public function __construct(
        public readonly int $amphipodId,
        public readonly int $nodeId,
        public readonly int $steps
    ) {
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
