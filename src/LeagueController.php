<?php

declare(strict_types=1);

namespace AdventOfCode;

use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class LeagueController
{
    public function __construct(private int $leaderboardId)
    {
    }

    public function league(
        ResponseInterface $response,
        Environment $template,
        LeaderBoard\RepositoryInterface $leaderboardRepository
    ): ResponseInterface {
        $leaderboard = $leaderboardRepository->get((int)date('Y'), $this->leaderboardId);

        $response->getBody()->write(
            $template->render(
                'league.html',
                [
                    'leaderboard' => $leaderboard,
                ]
            )
        );
        return $response;
    }
}
