<?php

declare(strict_types=1);

namespace AdventOfCode\Bingo;

class Number
{
    private bool $marked = false;

    public function __construct(public readonly int $number)
    {
    }

    public function mark(): void
    {
        $this->marked = true;
    }

    public function isMarked(): bool
    {
        return $this->marked;
    }
}
