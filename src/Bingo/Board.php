<?php

declare(strict_types=1);

namespace AdventOfCode\Bingo;

class Board
{
    private int $calls = 0;

    private ?int $lastCall = null;

    private bool $isComplete = false;

    /**
     * @param Number[]   $numbers
     * @param Number[][] $rows
     * @param Number[][] $columns
     * @param Number[][] $corners
     */
    public function __construct(
        private array $numbers,
        private array $rows,
        private array $columns,
        private array $corners
    ) {
    }

    public static function parse(string $board): self
    {
        $numbers = [];
        $rows = [];
        $columns = [];
        $corners = [];
        $rowData = explode("\n", $board);
        foreach ($rowData as $rowId => $row) {
            $rowNumbers = array_map('intval', explode(" ", trim(str_replace("  ", " ", $row))));
            foreach ($rowNumbers as $columnId => $number) {
                $numberObject = new Number($number);
                $numbers[$number] = $numberObject;
                $rows[$rowId][$number] = $numberObject;
                $columns[$columnId][$number] = $numberObject;

                if (
                    ($rowId === 0 && ($columnId === 0 || $columnId === (count($rowNumbers) - 1))) ||
                    ($rowId === (count($rowData) - 1) && ($columnId === 0 || $columnId === (count($rowNumbers) - 1)))
                ) {
                    $corners[$number] = $numberObject;
                }
            }
        }
        return new self($numbers, $rows, $columns, $corners);
    }

    public function call(int $number): void
    {
        $this->calls++;
        $this->lastCall = $number;
        if (!array_key_exists($number, $this->numbers)) {
            // Don't have the number on the board.
            return;
        }
        $this->numbers[$number]->mark();
        $this->isComplete = $this->hasCompletedLine();
    }

    public function hasCompletedLine(): bool
    {
        foreach ([$this->rows, $this->columns] as $line) {
            foreach ($line as $row) {
                $remaining = array_filter($row, function (Number $number) {
                    return !$number->isMarked();
                });
                if (count($remaining) === 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public function isComplete(): bool
    {
        return $this->isComplete;
    }

    public function getCalls(): int
    {
        return $this->calls;
    }

    public function getLastCall(): ?int
    {
        return $this->lastCall;
    }

    public function score(): int
    {
        $sumOfUnmarked = array_sum(array_keys($this->getUnMarkedNumbers()));
        return $sumOfUnmarked * $this->lastCall;
    }

    /**
     * @return Number[]
     */
    public function getUnMarkedNumbers(): array
    {
        return array_filter($this->numbers, function (Number $number) {
            return !$number->isMarked();
        });
    }
}
