<?php

declare(strict_types=1);

namespace AdventOfCode\Bingo;

class Game
{
    /** @var Board[] */
    protected array $boards = [];

    public function __construct(string $boardsString)
    {
        foreach (explode("\n\n", $boardsString) as $board) {
            $this->boards[] = Board::parse($board);
        }
    }

    public function call(int $number): void
    {
        foreach ($this->boards as $board) {
            // Don't keep calling and marking a completed board.
            if (!$board->isComplete()) {
                $board->call($number);
            }
        }
    }

    public function hasCompletedBoard(): bool
    {
        foreach ($this->boards as $board) {
            if ($board->isComplete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Board[]
     */
    public function getCompletedBoards(): array
    {
        return array_filter($this->boards, function (Board $board) {
            return $board->isComplete();
        });
    }

    public function gameCompleted(): bool
    {
        return count($this->getIncompleteBoards()) === 0;
    }

    /**
     * @return Board[]
     */
    public function getIncompleteBoards(): array
    {
        return array_filter($this->boards, function (Board $board) {
            return !$board->isComplete();
        });
    }
}
