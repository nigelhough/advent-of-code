<?php

declare(strict_types=1);

namespace AdventOfCode\Bingo;

class NumberCollection
{
    public function __construct(protected array $numbers)
    {
    }
}
