<?php

declare(strict_types=1);

namespace AdventOfCode\Tree;

use AdventOfCode\Input\Parser;
use Countable;
use Generator;
use Stringable;

class Tree implements Stringable, Countable
{
    /**
     * Tree constructor.
     *
     * @param NodeInterface[] $nodes
     */
    public function __construct(private array $nodes)
    {
    }

    public static function fromString(string $tree): self
    {
        $nodes = [];
        foreach (Parser::iterateByLine($tree) as $nodeData) {
            $node = Node::fromString($nodeData);
            $nodes[$node->getName()] = $node;
        }
        foreach (Parser::iterateByLine($tree) as $nodeData) {
            [$nodeInfo, $children] = explode(' -> ', $nodeData . ' -> ');
            [$nodeName] = explode(' ', $nodeInfo);
            $nodeName = trim($nodeName);
            foreach (array_map('trim', array_filter(explode(',', trim($children)))) as $child) {
                $nodes[$nodeName]->addChild($nodes[$child]);
                $nodes[$child]->setParent($nodes[$nodeName]);
            }
        }
        return new self($nodes);
    }

    /**
     * @return Generator|NodeInterface[]
     */
    public function getRootNodes(): Generator
    {
        foreach ($this->nodes as $node) {
            if ($node->hasParent()) {
                continue;
            }

            yield $node;
        }
    }

    public function __toString(): string
    {
        return 'Bonsai';
    }

    public function count(): int
    {
        return count($this->nodes);
    }

    /**
     * @return NodeInterface[]|Generator
     */
    public function iterateNodes(): Generator
    {
        foreach ($this->nodes as $node) {
            yield $node;
        }
    }

    /**
     * @param NodeInterface $node
     *
     * @return NodeInterface[]|Generator
     */
    public function getPeers(NodeInterface $node): Generator
    {
        foreach ($this->nodes as $peerNode) {
            if ($node === $peerNode || $node->getParent() !== $peerNode->getParent()) {
                continue;
            }

            yield $peerNode;
        }
    }

    public function setNodeWeight(string $nodeId, int $weight): void
    {
        $this->nodes[$nodeId]->setWeight($weight);
    }
}
