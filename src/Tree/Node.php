<?php

declare(strict_types=1);

namespace AdventOfCode\Tree;

class Node implements NodeInterface
{
    /**
     * Node constructor.
     *
     * @param string $name
     * @param int $weight
     * @param NodeInterface|null $parent
     * @param NodeInterface[] $children
     */
    public function __construct(
        private string $name,
        private int $weight,
        private ?NodeInterface $parent = null,
        private array $children = []
    ) {
    }

    public static function fromString(string $node): self
    {
        [$nodeInfo] = explode(' -> ', $node);
        [$nodeName, $weight] = explode(' ', $nodeInfo);
        $nodeName = trim($nodeName);
        $weight = (int)trim($weight, '() ');
        return new self($nodeName, $weight);
    }

    public function setParent(NodeInterface $node)
    {
        $this->parent = $node;
    }

    public function addChild(NodeInterface $node)
    {
        $this->children[] = $node;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    public function getChildWeight(): int
    {
        $weight = 0;
        foreach ($this->children as $child) {
            $weight += $child->getWeight();
        }

        return $weight;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getTotalWeight(): int
    {
        $weight = $this->getWeight();
        foreach ($this->children as $child) {
            $weight += $child->getTotalWeight();
        }

        return $weight;
    }

    public function hasParent(): bool
    {
        return $this->parent !== null;
    }

    public function getParent(): ?NodeInterface
    {
        return $this->parent;
    }

    public function hasChildren(): bool
    {
        return $this->count() > 0;
    }

    public function count(): int
    {
        return count($this->children);
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
