<?php

declare(strict_types=1);

namespace AdventOfCode\Tree;

use Countable;
use Stringable;

interface NodeInterface extends Countable, Stringable
{
    public function getName(): string;

    public function getWeight(): int;

    public function hasParent(): bool;

    public function getParent(): ?NodeInterface;

    public function hasChildren(): bool;

    public function getChildren(): array;

    public function getChildWeight(): int;

    public function getTotalWeight(): int;

    public function setWeight(int $weight): void;
}
