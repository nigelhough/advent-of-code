<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

class Path
{
    private $direction;

    private $length;

    public function __construct(string $step)
    {
        $this->direction = substr($step, 0, 1);
        $this->length = (int) substr($step, 1);
    }

    /**
     * @return int
     */
    public function getXIncrement(): int
    {
        if ($this->direction == 'R') {
            return 1;
        } elseif ($this->direction == 'L') {
            return -1;
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getYIncrement(): int
    {
        if ($this->direction == 'U') {
            return 1;
        } elseif ($this->direction == 'D') {
            return -1;
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }
}
