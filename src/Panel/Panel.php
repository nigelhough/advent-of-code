<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

class Panel
{
    private $panel = [];

    private $intersections = [];

    public function addWire(Wire $wire)
    {
        $xLocation = 0;
        $yLocation = 0;
        $steps = 0;

        foreach ($wire->getPath() as $path) {
            for ($i = 0; $i < $path->getLength(); $i++) {
                $point = new Point($wire, $steps);

                // Intersection
                if (isset($this->panel[$xLocation][$yLocation])) {
                    /** @var Point $existingPoint */
                    $existingPoint = $this->panel[$xLocation][$yLocation][0];
                    if (
                        !($xLocation === 0 && $yLocation === 0) // Center isn't an Intersection.
                        && !($existingPoint->getWire() === $wire) // Don't care if it intersects with itself.
                        && !(isset($this->panel[$xLocation][$yLocation][1])) // Don't care if already intersected.
                    ) {
                        $this->intersections[] = new Intersection($xLocation, $yLocation, $existingPoint, $point);
                    }
                }

                $this->panel[$xLocation][$yLocation][] = $point;

                $xLocation += $path->getXIncrement();
                $yLocation += $path->getYIncrement();
                $steps++;
            }
        }
    }

    /**
     * @return Intersection[]
     */
    public function getIntersections(): array
    {
        return $this->intersections;
    }
}
