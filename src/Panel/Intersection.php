<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

class Intersection
{
    private $x;

    private $y;

    private $point1;

    private $point2;

    public function __construct(int $x, int $y, Point $point1, Point $point2)
    {
        $this->x = $x;
        $this->y = $y;
        $this->point1 = $point1;
        $this->point2 = $point2;
    }

    public function getDistanceToCenter(): int
    {
        return (int) (abs($this->x) + abs($this->y));
    }

    public function getSteps(): int
    {
        return $this->point1->getSteps() + $this->point2->getSteps();
    }
}
