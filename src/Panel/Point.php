<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

class Point
{
    private $wire;

    private $steps;

    public function __construct(Wire $wire, int $steps)
    {
        $this->wire = $wire;
        $this->steps = $steps;
    }

    /**
     * @return Wire
     */
    public function getWire(): Wire
    {
        return $this->wire;
    }

    /**
     * @return int
     */
    public function getSteps(): int
    {
        return $this->steps;
    }
}
