<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

class Tracer
{
    /**
     * @param Wire $wire1
     * @param Wire $wire2
     *
     * @return Intersection[]
     */
    public function findIntersections(Wire $wire1, Wire $wire2): array
    {
        $panel = new Panel();
        $panel->addWire($wire1);
        $panel->addWire($wire2);
        return $panel->getIntersections();
    }
}
