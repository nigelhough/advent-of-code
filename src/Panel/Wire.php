<?php

declare(strict_types=1);

namespace AdventOfCode\Panel;

use Generator;

class Wire
{
    /** @var Path[] */
    private $pathway = [];

    public function __construct(string $wirePath)
    {
        foreach (explode(',', $wirePath) as $path) {
            $this->pathway[] = new Path($path);
        }
    }

    /**
     * @return Generator|Path[]
     */
    public function getPath(): Generator
    {
        foreach ($this->pathway as $path) {
            yield $path;
        }
    }
}
