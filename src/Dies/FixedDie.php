<?php

declare(strict_types=1);

namespace AdventOfCode\Dies;

class FixedDie extends SeededDie
{
    public function __construct()
    {
        parent::__construct(range(1, 100));
    }
}
