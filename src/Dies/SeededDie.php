<?php

declare(strict_types=1);

namespace AdventOfCode\Dies;

class SeededDie implements DieInterface
{
    public function __construct(protected array $seed)
    {
    }

    public function roll(): \Generator
    {
        while (true) {
            foreach ($this->seed as $roll) {
                yield $roll;
            }
        }
    }
}
