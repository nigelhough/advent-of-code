<?php

namespace AdventOfCode\Dies;

interface DieInterface
{
//    public function getRange(): int;
//
//    public function getMax(): int;
//
//    public function getMin(): int;
//
    /**
     * @return \Generator|int[]
     */
    public function roll(): \Generator;
}
