<?php

namespace AdventOfCode\Generator;

/**
 * Judge the Matches between 2 Generators.
 *
 * I assume there is an optimisation if we get a repeat number from a generator as it will be a loop. So we know how
 * many cycles can occur in the iterations.
 */
class Judge
{
    public const MATCHING_BITS = 65535;

    public function __construct(private Generator $generatorA, private Generator $generatorB)
    {
    }

    public function judge(int $iterations): int
    {
        $matches = 0;
        for ($i = 0; $i < $iterations; $i++) {
            if (
                ($this->generatorA->generate() & self::MATCHING_BITS) ===
                ($this->generatorB->generate() & self::MATCHING_BITS)
            ) {
                $matches++;
            }
        }
        return $matches;
    }

    /**
     * Print the generated values as decimals.
     */
    public function debug(int $iterations): void
    {
        // Mark the Generator point so that it doesn't break the actual runs with debug.
        $this->generatorA->mark();
        $this->generatorB->mark();

        for ($i = 0; $i < $iterations; $i++) {
            $a = $this->generatorA->generate();
            $b = $this->generatorB->generate();

            echo str_pad((string)$a, 12) . "    " . str_pad((string)$b, 12) . "\n";
        }

        // Reset the Generator.
        $this->generatorA->reset();
        $this->generatorB->reset();
    }
}
