<?php

namespace AdventOfCode\Generator;

/**
 * Generator to generate numbers.
 * Why isn't this a Generator? This should really be a \Generator
 */
class Generator
{
    public const DIVIDER = 2147483647;

    private int $mark;

    public function __construct(private int $value, private int $factor, private int $multiplier = 1)
    {
        $this->mark = $this->value;
    }

    /**
     * This should be a \Generator
     *
     * @return int
     */
    public function generate(): int
    {
        $this->value = ($this->value * $this->factor) % self::DIVIDER;
        if ($this->multiplier === 1) {
            return $this->value;
        }

        while (($this->value % $this->multiplier) !== 0) {
            $this->value = ($this->value * $this->factor) % self::DIVIDER;
        }

        return $this->value;
    }

    public function mark()
    {
        $this->mark = $this->value;
    }

    public function reset()
    {
        $this->value = $this->mark;
    }
}
