<?php

namespace AdventOfCode\SnailNumber;

class ActualNumber
{
    public function __construct(private int $number)
    {
    }

    public function getValue(): int
    {
        return $this->number;
    }

    public function increment(int $increment): void
    {
        $this->number += $increment;
    }

    public function __toString(): string
    {
        return "{$this->number}";
    }

    public function split(): Pair
    {
        return new Pair(new self((int)floor($this->number / 2)), new self((int)ceil($this->number / 2)));
    }
}
