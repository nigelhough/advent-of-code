<?php

namespace AdventOfCode\SnailNumber;

class Pair implements \Stringable
{
    public function __construct(private Pair|ActualNumber $x, private Pair|ActualNumber $y)
    {
    }

    public static function createIteratively(array $numbers): Pair
    {
        if (count($numbers) !== 2) {
            throw new \Exception('Noooooooo');
        }

        return new Pair(
            is_array($numbers[0]) ? self::createIteratively($numbers[0]) : new ActualNumber($numbers[0]),
            is_array($numbers[1]) ? self::createIteratively($numbers[1]) : new ActualNumber($numbers[1])
        );
    }

    public function findExplodingPair($depth = 0)
    {
        if ($depth >= 4 && $this->x instanceof ActualNumber && $this->y instanceof ActualNumber) {
            return $this;
        }
        $explosion = null;
        if ($this->x instanceof Pair) {
            $explosion = $this->x->findExplodingPair($depth + 1);
        }
        if ($explosion === null && $this->y instanceof Pair) {
            return $this->y->findExplodingPair($depth + 1);
        }
        return $explosion;
    }

    public function explode(Pair $pair)
    {
        if ($this->x === $pair) {
            $this->x = new ActualNumber(0);
            return;
        }
        if ($this->y === $pair) {
            $this->y = new ActualNumber(0);
            return;
        }

        if ($this->x instanceof Pair) {
            $this->x->explode($pair);
        }
        if ($this->y instanceof Pair) {
            $this->y->explode($pair);
        }
    }

    public function split(ActualNumber $number)
    {
        if ($this->x === $number) {
            $this->x = $number->split();
            return;
        }
        if ($this->y === $number) {
            $this->y = $number->split();
            return;
        }

        if ($this->x instanceof Pair) {
            $this->x->split($number);
        }
        if ($this->y instanceof Pair) {
            $this->y->split($number);
        }
    }

    /**
     * @return ActualNumber[]
     */
    public function getActualNumbers(): array
    {
        $numbers = [];
        if ($this->x instanceof self) {
            $numbers = array_merge($numbers, $this->x->getActualNumbers());
        } elseif ($this->x instanceof ActualNumber) {
            $numbers[] = $this->x;
        }
        if ($this->y instanceof self) {
            $numbers = array_merge($numbers, $this->y->getActualNumbers());
        } elseif ($this->y instanceof ActualNumber) {
            $numbers[] = $this->y;
        }

        return $numbers;
    }

    public function getMagnitude(): int
    {
        $sum = 0;
        if ($this->x instanceof self) {
            $sum += $this->x->getMagnitude() * 3;
        } elseif ($this->x instanceof ActualNumber) {
            $sum += ($this->x->getValue() * 3);
        }
        if ($this->y instanceof self) {
            $sum += $this->y->getMagnitude() * 2;
        } elseif ($this->x instanceof ActualNumber) {
            $sum += ($this->y->getValue() * 2);
        }

        return $sum;
    }

    public function __toString(): string
    {
        return "[{$this->x},{$this->y}]";
    }
}
