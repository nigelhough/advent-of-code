<?php

namespace AdventOfCode\SnailNumber;

class SnailNumber implements \Stringable
{
    private Pair $topPair;

    /**
     * @var ActualNumber[]
     */
    private array $numbers;

    public function __construct(string $snailNumber)
    {
        $this->topPair = Pair::createIteratively(json_decode($snailNumber));
        $this->numbers = $this->topPair->getActualNumbers();
    }

    public static function processList(self ...$snailNumbers): self
    {
        /** @var self $previous */
        $previous = null;
        $snailNumber = null;
        foreach ($snailNumbers as $snailNumber) {
            if ($previous === null) {
                $previous = $snailNumber;
                $snailNumber->reduce();
                continue;
            }

            $snailNumber = $snailNumber->reduce();
            $snailNumber = $previous->add($snailNumber);
            $snailNumber->reduce();
            $previous = $snailNumber;
        }

        return $snailNumber;
    }

    public function add(self $addition): self
    {
        return new self(json_encode([json_decode((string)$this), json_decode((string)$addition)]));
    }

    public function getMagnitude(): int
    {
        return $this->topPair->getMagnitude();
    }

    public function reduce(): self
    {
        $split = true;
        while ($split) {
            do {
                $exploded = $this->explode();
            } while ($exploded);
            $split = $this->split();
        }

        return $this;
    }

    public function explode(): bool
    {
        $explodingPair = $this->topPair->findExplodingPair();

        // We have an explosion.
        if ($explodingPair !== null) {
            // Explode!!!
            $this->topPair->explode($explodingPair);

            // Check the exploding pair is valid.
            $actualNumbers = $explodingPair->getActualNumbers();
            if (count($actualNumbers) !== 2) {
                throw new \Exception('What have we just exploded!');
            }

            foreach ($this->numbers as $i => $number) {
                if ($number === $actualNumbers[0] && isset($this->numbers[($i - 1)])) {
                    // We have found the X of the exploding number.
                    // and it has a number before it.
                    $this->numbers[($i - 1)]->increment($number->getValue());
                }

                if ($number === $actualNumbers[1] && isset($this->numbers[($i + 1)])) {
                    // We have found the Y of the exploding number.
                    // and it has a number after it.
                    $this->numbers[($i + 1)]->increment($number->getValue());
                }
            }

            // Refresh actuals.
            $this->numbers = $this->topPair->getActualNumbers();
            return true;
        }

        return false;
    }

    public function split(): bool
    {
        // Find the first splitting number.
        $splittingNumber = null;
        foreach ($this->numbers as $number) {
            if ($number->getValue() >= 10) {
                $splittingNumber = $number;
                break;
            }
        }

        // Nothing to split.
        if ($splittingNumber === null) {
            return false;
        }

        // Split!!!
        $this->topPair->split($splittingNumber);

        // Refresh actuals.
        $this->numbers = $this->topPair->getActualNumbers();

        return true;
    }

    public function __toString(): string
    {
        return (string)$this->topPair;
    }
}
