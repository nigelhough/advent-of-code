<?php

declare(strict_types=1);

namespace AdventOfCode\Cube;

class Bearing
{
    public function __construct(private int $xMod, private int $yMod, private int $zMod, private int $wMod = 0)
    {
    }

    /**
     * @return self[]
     */
    public static function surrounding(int $dimensions = 3): array
    {
        $twoDimensions = [
            new self(0, -1, 0, 0),
            new self(1, -1, 0, 0),
            new self(1, 0, 0, 0),
            new self(1, 1, 0, 0),
            new self(0, 1, 0, 0),
            new self(-1, 1, 0, 0),
            new self(-1, 0, 0, 0),
            new self(-1, -1, 0, 0),
        ];
        $threeDimensions = [
            new self(0, 0, 1, 0),
            new self(0, -1, 1, 0),
            new self(1, -1, 1, 0),
            new self(1, 0, 1, 0),
            new self(1, 1, 1, 0),
            new self(0, 1, 1, 0),
            new self(-1, 1, 1, 0),
            new self(-1, 0, 1, 0),
            new self(-1, -1, 1, 0),
            new self(0, 0, -1, 0),
            new self(0, -1, -1, 0),
            new self(1, -1, -1, 0),
            new self(1, 0, -1, 0),
            new self(1, 1, -1, 0),
            new self(0, 1, -1, 0),
            new self(-1, 1, -1, 0),
            new self(-1, 0, -1, 0),
            new self(-1, -1, -1, 0),
        ];

        $fourDimensions = [
            new self(0, 0, 0, 1),
            new self(0, -1, 0, 1),
            new self(1, -1, 0, 1),
            new self(1, 0, 0, 1),
            new self(1, 1, 0, 1),
            new self(0, 1, 0, 1),
            new self(-1, 1, 0, 1),
            new self(-1, 0, 0, 1),
            new self(-1, -1, 0, 1),
            new self(0, 0, 1, 1),
            new self(0, -1, 1, 1),
            new self(1, -1, 1, 1),
            new self(1, 0, 1, 1),
            new self(1, 1, 1, 1),
            new self(0, 1, 1, 1),
            new self(-1, 1, 1, 1),
            new self(-1, 0, 1, 1),
            new self(-1, -1, 1, 1),
            new self(0, 0, -1, 1),
            new self(0, -1, -1, 1),
            new self(1, -1, -1, 1),
            new self(1, 0, -1, 1),
            new self(1, 1, -1, 1),
            new self(0, 1, -1, 1),
            new self(-1, 1, -1, 1),
            new self(-1, 0, -1, 1),
            new self(-1, -1, -1, 1),
            new self(0, 0, 0, -1),
            new self(0, -1, 0, -1),
            new self(1, -1, 0, -1),
            new self(1, 0, 0, -1),
            new self(1, 1, 0, -1),
            new self(0, 1, 0, -1),
            new self(-1, 1, 0, -1),
            new self(-1, 0, 0, -1),
            new self(-1, -1, 0, -1),
            new self(0, 0, 1, -1),
            new self(0, -1, 1, -1),
            new self(1, -1, 1, -1),
            new self(1, 0, 1, -1),
            new self(1, 1, 1, -1),
            new self(0, 1, 1, -1),
            new self(-1, 1, 1, -1),
            new self(-1, 0, 1, -1),
            new self(-1, -1, 1, -1),
            new self(0, 0, -1, -1),
            new self(0, -1, -1, -1),
            new self(1, -1, -1, -1),
            new self(1, 0, -1, -1),
            new self(1, 1, -1, -1),
            new self(0, 1, -1, -1),
            new self(-1, 1, -1, -1),
            new self(-1, 0, -1, -1),
            new self(-1, -1, -1, -1),
        ];

        if ($dimensions === 4) {
            return array_merge($twoDimensions, $threeDimensions, $fourDimensions);
        } elseif ($dimensions === 3) {
            return array_merge($twoDimensions, $threeDimensions);
        }

        return $twoDimensions;
    }

    public function getModifiers(): array
    {
        return [$this->xMod, $this->yMod, $this->zMod, $this->wMod];
    }

    /**
     * @return int
     */
    public function getXMod(): int
    {
        return $this->xMod;
    }

    /**
     * @return int
     */
    public function getYMod(): int
    {
        return $this->yMod;
    }

    /**
     * @return int
     */
    public function getZMod(): int
    {
        return $this->zMod;
    }

    public function isSame(self $subject)
    {
        return ($this->xMod === $subject->xMod) && ($this->yMod === $subject->yMod) && ($this->zMod === $subject->zMod);
    }
}
