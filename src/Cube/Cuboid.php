<?php

declare(strict_types=1);

namespace AdventOfCode\Cube;

class Cuboid implements \Stringable
{
    public function __construct(public readonly Coord $bottomCorner, public readonly Coord $topCorner)
    {
    }

    public static function fromString(string $ranges): self
    {
        [$xRange, $yRange, $zRange] = explode(',', $ranges);
        [$xMin, $xMax] = array_map('intval', explode('..', substr($xRange, 2)));
        [$yMin, $yMax] = array_map('intval', explode('..', substr($yRange, 2)));
        [$zMin, $zMax] = array_map('intval', explode('..', substr($zRange, 2)));

        return new Cuboid(new Coord($xMin, $yMin, $zMin), new Coord($xMax + 1, $yMax + 1, $zMax + 1));
    }

    public function getVolume(): int
    {
        $xLength = $this->topCorner->getXPosition() - $this->bottomCorner->getXPosition();
        $yLength = $this->topCorner->getYPosition() - $this->bottomCorner->getYPosition();
        $zLength = $this->topCorner->getZPosition() - $this->bottomCorner->getZPosition();

        return $xLength * $yLength * $zLength;
    }

    public function getIntersection(Cuboid $cuboid): ?Cuboid
    {
        $isXIntersection = ($cuboid->bottomCorner->getXPosition() < $this->topCorner->getXPosition())
            && ($cuboid->topCorner->getXPosition() > $this->bottomCorner->getXPosition());
        if (!$isXIntersection) {
            return null;
        }
        $isYIntersection = ($cuboid->bottomCorner->getYPosition() < $this->topCorner->getYPosition())
            && ($cuboid->topCorner->getYPosition() > $this->bottomCorner->getYPosition());
        if (!$isYIntersection) {
            return null;
        }
        $isZIntersection = ($cuboid->bottomCorner->getZPosition() < $this->topCorner->getZPosition())
            && ($cuboid->topCorner->getZPosition() > $this->bottomCorner->getZPosition());
        if (!$isZIntersection) {
            return null;
        }

        return new self(
            new Coord(
                max($this->bottomCorner->getXPosition(), $cuboid->bottomCorner->getXPosition()),
                max($this->bottomCorner->getYPosition(), $cuboid->bottomCorner->getYPosition()),
                max($this->bottomCorner->getZPosition(), $cuboid->bottomCorner->getZPosition()),
            ),
            new Coord(
                min($this->topCorner->getXPosition(), $cuboid->topCorner->getXPosition()),
                min($this->topCorner->getYPosition(), $cuboid->topCorner->getYPosition()),
                min($this->topCorner->getZPosition(), $cuboid->topCorner->getZPosition()),
            )
        );
    }

    public function getInverseIntersections(Cuboid $cuboid): array
    {
        if ($this->getIntersection($cuboid) === null) {
            return [];
        }
        $inverseIntersections = [];
        $a = $this->bottomCorner;
        $b = $this->topCorner;
        $c = $cuboid->bottomCorner;
        $d = $cuboid->topCorner;

        $xPairs = [
            [$a->getXPosition(), $c->getXPosition()],
            [$c->getXPosition(), $d->getXPosition()],
            [$d->getXPosition(), $b->getXPosition()],
        ];
        $yPairs = [
            [$a->getYPosition(), $c->getYPosition()],
            [$c->getYPosition(), $d->getYPosition()],
            [$d->getYPosition(), $b->getYPosition()],
        ];
        $zPairs = [
            [$a->getZPosition(), $c->getZPosition()],
            [$c->getZPosition(), $d->getZPosition()],
            [$d->getZPosition(), $b->getZPosition()],
        ];

        foreach ($xPairs as [$bX, $tX]) {
            if ($tX <= $bX) {
                continue;
            }
            foreach ($yPairs as [$bY, $tY]) {
                if ($tY <= $bY) {
                    continue;
                }
                foreach ($zPairs as [$bZ, $tZ]) {
                    if ($tZ <= $bZ) {
                        continue;
                    }
                    $c = new self(new Coord($bX, $bY, $bZ), new Coord($tX, $tY, $tZ));
                    if ($c->isSame($cuboid)) {
                        continue;
                    }
                    $inverseIntersections[] = $c;
                }
            }
        }

        return $inverseIntersections;
    }

    public function intersectsWithAny(Cuboid ...$cuboids): bool
    {
        foreach ($cuboids as $cuboid) {
            if ($this->getIntersection($cuboid)) {
                return true;
            }
        }
        return false;
    }

    public function __toString()
    {
        return ((string) $this->bottomCorner) . 'x' . ((string) $this->topCorner);
    }

    public function isSame(Cuboid $subject): bool
    {
        return $this->bottomCorner->isSame($subject->bottomCorner) && $this->topCorner->isSame($subject->topCorner);
    }
}
