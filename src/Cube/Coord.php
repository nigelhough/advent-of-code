<?php

declare(strict_types=1);

namespace AdventOfCode\Cube;

use AdventOfCode\Grid\CoordinateInterface;
use Exception;

class Coord implements CoordinateInterface
{
    public function __construct(
        private int $xPosition,
        private int $yPosition,
        private int $zPosition,
        private int $wPosition = 0
    ) {
    }

    public static function fromString(string $coord): self
    {
        [$x, $y, $z, $w] = explode(',', $coord);
        return new self((int)$x, (int)$y, (int)$z, (int)$w);
    }

    public function getCost(): int
    {
        return 1;
    }

    public function getNeighbours(): array
    {
        throw new Exception('Not Implemented');
    }

    /**
     * @param self $subject
     *
     * @return bool
     */
    public function isSame($subject)
    {
        return
            ($this->xPosition === $subject->getXPosition())
            && ($this->yPosition === $subject->getYPosition())
            && ($this->zPosition === $subject->getZPosition())
            && ($this->wPosition === $subject->getWPosition());
    }

    /**
     * @return int
     */
    public function getXPosition(): int
    {
        return $this->xPosition;
    }

    /**
     * @return int
     */
    public function getYPosition(): int
    {
        return $this->yPosition;
    }

    /**
     * @return int
     */
    public function getZPosition(): int
    {
        return $this->zPosition;
    }

    /**
     * @return int
     */
    public function getWPosition(): int
    {
        return $this->wPosition;
    }

    public function __toString()
    {
        return "{$this->xPosition},{$this->yPosition},{$this->zPosition},{$this->wPosition}";
    }

    /**
     * @return self[]
     */
    public function getSurrounding(int $dimensions = 3): array
    {
        $coordinates = [];
        foreach (Bearing::surrounding($dimensions) as $bearing) {
            [$xMod, $yMod, $zMod, $wMod] = $bearing->getModifiers();
            $coordinates[] = new self(
                $this->getXPosition() + $xMod,
                $this->getYPosition() + $yMod,
                $this->getZPosition() + $zMod,
                $this->getWPosition() + $wMod
            );
        }
        return $coordinates;
    }

    /**
     * @param self $target
     *
     * @return float
     */
    public function distance($target): float
    {
        return 0;
    }
}
