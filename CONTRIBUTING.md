# Contributing
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Use [Github Flow](https://guides.github.com/introduction/flow/index.html), So All Code Changes Happen Through Pull Requests
Pull requests are the best way to propose changes to the codebase (we use [Github Flow](https://guides.github.com/introduction/flow/index.html)). We actively welcome your pull requests:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code passes the build.
6. Issue that pull request!


## Use a Consistent Coding Style
We follow [PSR-2: Coding Style Guide](http://www.php-fig.org/psr/psr-2/)

* You can run `vendor/bin/phpcs -p src` to validate coding style

We follow [PHP Package Development Standard: Standard filesystem skeleton](https://github.com/php-pds/skeleton/tree/1.0.0)

* You can run `vendor/bin/pds-skeleton validate` to validate filesystem structure

## References
This document was adapted from https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62 which was in turn adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)
